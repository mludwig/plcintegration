/**
 * basic testing for the s7 communication with siemens PLCs
 */

#include "sapi_s7.h"
#include <boost/algorithm/string.hpp>


#include <iostream>
#include <unistd.h>
#include <ctime>
#include <map>
#include <sys/time.h>

//Used for splitting uint32 into bytes
union int32union {
	uint32_t number; // occupies 4 bytes
	uint8_t byteContents[4]; // occupies 4 bytes
} Int32union;

//Used for splitting uint16 into bytes
union uint16union {
	uint16_t number; // occupies 2 bytes
	uint8_t byteContents[2]; // occupies 2 bytes
} Uint16union;

void transformAddress(std::string source, std::string &destination)
{
	std::vector <std::string> fields;
	boost::split( fields, source, boost::is_any_of( "." ) );
	std::string address = fields[1];
	boost::erase_all(address, "DB");

	if (address.find("F") != std::string::npos)
	{
		boost::erase_all(address, "F");
		boost::erase_all(address, "D");
		std::string rets = fields[0] + ",REAL" + address;
		destination = rets.c_str();
	}
	else if (address.find("D") != std::string::npos)
	{
		boost::erase_all(address, "D");
		std::string rets = fields[0] + ",D" + address;
		destination = rets.c_str();
	}
	else if (address.find("W") != std::string::npos)
	{
		boost::erase_all(address, "W");
		std::string rets = fields[0] + ",W" + address;
		destination = rets.c_str();
	}
	else
	{
		std::cout << "Error: Invalid address format." << std::endl;
		destination = "Error";
	}
	std::cout <<  "transformAddress source= " << source << " destination= " << destination << std::endl;
}


/* receive any message from communication system */
void receiveWriteConfirmation(ord32 vfdHandle, int32 last_event_expected, int timeoutMs)
{
	int32 msg;
	int ret;
    clock_t begin_time = clock();
	ord16  orderid_received;
	ord16 connection_received;
	struct timeval tv0, tv1;
	struct timezone tz;
	gettimeofday( &tv0, &tz );
   do  {
        msg = s7_receive( vfdHandle, &connection_received, &orderid_received);
        switch( msg ) {
        case S7_NO_MSG: {
            break;
        }
        case S7_WRITE_CNF: {
            ret = s7_get_write_cnf();
            if ( ret != S7_OK ) {
            	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " ret= " << ret << " failed ! "<< std::endl;
			} else {
				gettimeofday( &tv1, &tz );
				float delta = (( float ) tv1.tv_sec - ( float ) tv0.tv_sec ) * 1000
						+ (( float) tv1.tv_usec - ( float ) tv0.tv_usec ) / 1000;
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " msg= " << msg << " got write cnf confirmed OK, took " << delta << " ms" << std:: endl;
			}
            break;
        }
        default: {
            s7_discard_msg();
           	std::cout << __FILE__ << " " << __LINE__ << " " << __FUNCTION__ << " discard "<< std::endl;
           break;
        }
        }
    }
    while( msg != last_event_expected && ((static_cast<float>( clock () - begin_time ) /  CLOCKS_PER_SEC)<(static_cast<float>(timeoutMs)/1000)) );
    if(last_event_expected != msg) {
    	std::cout << "(Timeout: no message received)" << std::endl;
    } else {
    	std::cout << "OK: confirmation for expected event= " << last_event_expected << " received" << std::endl;
    }
}


/**
 * set up and demonstrate basic comm with a PLC at index 0
 * CP : communication processor
 * VFD : virtual field device: a simulated controller
 *
 * discovery works in a sequence:
 * 1. discover installed CPs, select one
 * 2. discover the number of VFDs for each CP, and their handles. Use the configured names
 * 3. log into each VFD
 * 4. discover the number of connections on each VFD and get their references
 * 5. log out of each VFD
 */
int main( int argc, char **argv){


	std::cout << "version " << __DATE__ << " " << __TIME__ << std::endl;
	{
		// discovered names of all CPs, but we use only the first: CP0
		std::vector<std::string> cp_name_vect;

		// configured CP0
		std::vector<std::string> connection_name_vect;
		connection_name_vect.push_back("S7_TSPP");
		connection_name_vect.push_back("S7_TSPP2");

		// discovered names of VFDs on CP0
		std::vector<std::string> vfdName_vect;
		std::vector<ord32> vfdHandle_vect;

		// VFD name
		std::map<std::string,ord16> connection_map;

		int32 ret;
		char vfd_name[S7_MAX_NAMLEN+1];
		char cp_name[S7_MAX_DEVICELEN+1];
		ord16      numberOfCPs;

		/* only use first device CP0, but we could talk to several as well */
		int deviceIndex = 0;
		ret = s7_get_device( deviceIndex, &numberOfCPs, cp_name );
		std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
				<< " deviceIndex= " << deviceIndex << " : "
				<< " s7_get_device ret= " << ret
				<< " numberOfCPs= " << numberOfCPs
				<< " dev_name= " << cp_name
				<< std::endl;
		cp_name_vect.push_back( cp_name );

		/**
		 * loop over all CPs (communication processors), usually, but we just
		 * use CP0 (the first device).
		 */
		ord16 numberOfVFDs;
		ord16 cpIndex = 0;
		for ( int i = 0; i < numberOfCPs; i++ ){
			cpIndex = i;
			char dummy[S7_MAX_NAMLEN+1];

			// number of VDSs on CP0
			ret = s7_get_vfd( cp_name, cpIndex, &numberOfVFDs, dummy);
			std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
					<< " cpIndex= " << cpIndex << " : "
					<< " s7_get_vfd ret= " << ret
					<< " numberOfVFDs= " << numberOfVFDs
					<< std::endl;

			/**
			 * loop over all VFDs of the CP0 and get their names and handles
			 * Each VFD must have a configured name.
			 */
			for ( int ivfd = 0; ivfd < numberOfVFDs; ivfd++ ){
				ord16 dummy;

				// VFD names, same call
				ret = s7_get_vfd( cp_name, ivfd, &dummy, vfd_name);
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " cpIndex= " << cpIndex << " : "
						<< " s7_get_vfd ret= " << ret
						<< " vfd_name= " << vfd_name
						<< std::endl;
				vfdName_vect.push_back( vfd_name );

				// VFD handles
				ord32 vfd_handle;
				ret = s7_init( cp_name, vfd_name, &vfd_handle );
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " cpIndex= " << cpIndex << " : "
						<< " vfd_name= " << vfd_name
						<< " s7_init ret= " << ret
						<< std::endl;
				vfdHandle_vect.push_back( vfd_handle );

				/**
				 * get the connection reference (~connection handle) for each named connection
				 * Each connection must have a configured name, and you must use the
				 * correct handle of the VFD for it.
				 */
				ord16 connHandle;
				std::string conn_name = connection_name_vect[ ivfd ];
				ret = s7_get_cref( vfd_handle, const_cast<char*>(conn_name.c_str()), &connHandle);
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " cpIndex= " << cpIndex << " : "
						<< " vfd_name= " << vfd_name
						<< " s7_get_cref ret= " << ret
						<< " connHandle= " << connHandle
						<< std::endl;

				// initiate the connection as well
				ret = s7_initiate_req( vfd_handle, connHandle );
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " cpIndex= " << cpIndex << " : "
						<< " conn_name= " << conn_name
						<< " s7_initiate_req ret= " << ret << std::endl;

				connection_map.insert( std::pair<std::string,ord16>( vfd_name, connHandle) );

				ord16 dummy1 = 0;
				ord16 numberOfConnectionsThisVFD;
				ord16 connection_reference;
				char connection_name[S7_MAX_NAMLEN+1];
				ret = s7_get_conn(vfd_handle, dummy1, &numberOfConnectionsThisVFD,
						&connection_reference, connection_name );

				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " cpIndex= " << cpIndex << " : "
						<< " vfd_name= " << vfd_name
						<< " s7_get_conn ret= " << ret
						<< " numberOfConnectionsThisVFD= " << numberOfConnectionsThisVFD
						<< std::endl;

				for ( ord16 iconn = 0; iconn < numberOfConnectionsThisVFD; iconn++ ){
					// same call
					ret = s7_get_conn(vfd_handle, iconn, &numberOfConnectionsThisVFD,
							&connection_reference, connection_name );
					std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
							<< " cpIndex= " << cpIndex << " : "
							<< " vfd_name= " << vfd_name
							<< " s7_get_conn ret= " << ret
							<< " iconn= " << iconn
							<< " connection_name= " << connection_name
							<< " connection_reference= " << (int) connection_reference
							<< std::endl;

					/* shutdown */
					ret = s7_shut( vfd_handle );
					std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
							<< " vfd_name= " << vfd_name << " s7_shut ret= " << ret
							<< "  vfd_handle= " << vfd_handle << std::endl;

				} // connections per VFD
			} // VFD
		} // CP

		/**
		 * version Feb 28 2020 15:02:54
		 * if the have one CP0, two VFD for it, and each VFD has one connection, then we can communicate
		 * with both VFDs in parallel for multithreading. One has to logon to a CP and a VFD in order to use
		 * it's connection(s).
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 103 main deviceIndex= 0 :  s7_get_device ret= 0 numberOfCPs= 1 dev_name= h1_0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 123 main cpIndex= 0 :  s7_get_vfd ret= 0 numberOfVFDs= 2
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 138 main cpIndex= 0 :  s7_get_vfd ret= 0 vfd_name= VFD
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 148 main cpIndex= 0 :  vfd_name= VFD s7_init ret= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 163 main cpIndex= 0 :  vfd_name= VFD s7_get_cref ret= 0 connHandle= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 179 main cpIndex= 0 :  vfd_name= VFD s7_get_conn ret= 0 numberOfConnectionsThisVFD= 1
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 190 main cpIndex= 0 :  vfd_name= VFD s7_get_conn ret= 0 iconn= 0 connection_name= S7_TSPP connection_reference= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 201 main vfd_name= VFD s7_shut ret= 0  vfd_handle= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 138 main cpIndex= 0 :  s7_get_vfd ret= 0 vfd_name= VFD2
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 148 main cpIndex= 0 :  vfd_name= VFD2 s7_init ret= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 163 main cpIndex= 0 :  vfd_name= VFD2 s7_get_cref ret= 0 connHandle= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 179 main cpIndex= 0 :  vfd_name= VFD2 s7_get_conn ret= 0 numberOfConnectionsThisVFD= 1
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 190 main cpIndex= 0 :  vfd_name= VFD2 s7_get_conn ret= 0 iconn= 0 connection_name= S7_TSPP2 connection_reference= 0
/home/mludwig/projects/PLCintegration-master/plcintegration/S7-test/main.cpp 201 main vfd_name= VFD2 s7_shut ret= 0  vfd_handle= 0
		 *
		 */
	}


	/**
	 * demonstrate some simple communication
	 */
	struct timeval tv0, tv1;
	struct timezone tz;

	/* logon to VFD */
	ord32 vfdHandle;
	char cp_name[S7_MAX_DEVICELEN+1] = "h1_0";
	char vfd_name[S7_MAX_DEVICELEN+1] = "VFD";
	int32 ret;

	ret = s7_init( cp_name, vfd_name, &vfdHandle );
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " s7_init ret= " << ret
			<< std::endl;

	/* connection */
	char connection_name[S7_MAX_DEVICELEN+1] = "S7_TSPP";
	ord16 connection;
	ret = s7_get_cref( vfdHandle, connection_name, &connection);
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " connection_name= " << connection_name
			<< " s7_get_cref ret= " << ret
			<< " connection= " << connection << std::endl;

	// request to initiate connection
	ret = s7_initiate_req( vfdHandle, connection );
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " connection_name= " << connection_name
			<< " s7_initiate_req ret= " << ret << std::endl;


	// wait for init confirmation, poll on reception until S7_INITIATE_CNF is confirmed
	ord16  orderid_received;
	ord16 connection_received;
	int32 msg;
	int32 last_event_expected = S7_INITIATE_CNF;
	const int msRecieveTimeoutInit = 5000;

	gettimeofday( &tv0, &tz );
	do {
		msg = s7_receive( vfdHandle, &connection_received, &orderid_received );
		//std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
		//		<< " s7_receive msg= " << msg
		//		<< " connection_received= " << (int) connection_received
		//		<< " orderid_received= " << (unsigned int) orderid_received << std::endl;
		switch ( msg ) {
		case S7_NO_MSG: {
			//std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			//		<< " no message " << std:: endl;
			break;
		}
		case S7_INITIATE_CNF: {
			std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
					<< " msg= " << msg << " got initiate cnf" << std:: endl;
			int32 ret0 = s7_get_initiate_cnf();
			if ( ret0 == S7_OK ) {
				gettimeofday( &tv1, &tz );
				float delta = (( float ) tv1.tv_sec - ( float ) tv0.tv_sec ) * 1000
						+ (( float) tv1.tv_usec - ( float ) tv0.tv_usec ) / 1000;
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " msg= " << msg << " got initiate cnf confirmed OK, took " << delta << " ms" << std:: endl;
			} else {
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
						<< " msg= " << msg << " got initiate cnf failed, exiting.. " << std:: endl;
				exit(-1);
			}
			break;
		}
		default:{
			s7_discard_msg();
			std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
					<< " msg= " << msg << " unexpected msg, discarding" << std:: endl;
			break;
		}
		}
		usleep( 100000 ); // 100ms
	} while ( msg != last_event_expected );



	/* handshake */
	const int msRecieveTimeout = 1000;
	std::cout << "---write IP ---" << std::endl;
	const uint8_t ip1 = 128;
	const uint8_t ip2 = 141;
	const uint8_t ip3 = 159;
	const uint8_t ip4 = 20;
	ord16  orderid = 1;
	struct S7_WRITE_PARA_LONG write_para;
    write_para.access = S7_ACCESS_SYMB_ADDRESS;
    strcpy( write_para.var_name, "DB100,D0,1" );
    write_para.var_length = 4;
    write_para.value[0] = ip1;
    write_para.value[1] = ip2;
    write_para.value[2] = ip3;
    write_para.value[3] = ip4;

    // fails
    ret = s7_write_long_req( vfdHandle, connection, orderid, &write_para, NULL );
	std::cout << "s7_write_long_req ret= " << ret << std::endl;
    if( ret != S7_OK )   {
    	std::cout << "ERROR: s7_write_long_req ret= " << ret << std::endl;
    }
    // Wait for write confirmation
    receiveWriteConfirmation( vfdHandle, S7_WRITE_CNF, msRecieveTimeout );

	return 0;
}




