/**
 * basic testing for the s7 communication with siemens PLCs
 */

#include "sapi_s7.h"
#include <boost/algorithm/string.hpp>


#include <iostream>
#include <unistd.h>
#include <ctime>


//Used for splitting uint32 into bytes
union int32union {
    uint32_t number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} Int32union;

//Used for splitting uint16 into bytes
union uint16union {
    uint16_t number; // occupies 2 bytes
    uint8_t byteContents[2]; // occupies 2 bytes
} Uint16union;

void transformAddress(std::string source, std::string &destination)
{
	std::vector <std::string> fields;
	boost::split( fields, source, boost::is_any_of( "." ) );
	std::string address = fields[1];
	boost::erase_all(address, "DB");

	if (address.find("F") != std::string::npos)
	{
		boost::erase_all(address, "F");
		boost::erase_all(address, "D");
		std::string rets = fields[0] + ",REAL" + address;
		destination = rets.c_str();
	}
	else if (address.find("D") != std::string::npos)
	{
		boost::erase_all(address, "D");
		std::string rets = fields[0] + ",D" + address;
		destination = rets.c_str();
	}
	else if (address.find("W") != std::string::npos)
	{
		boost::erase_all(address, "W");
		std::string rets = fields[0] + ",W" + address;
		destination = rets.c_str();
	}
	else
	{
		std::cout << "Error: Invalid address format." << std::endl;
		destination = "Error";
	}
	std::cout <<  "transformAddress source= " << source << " destination= " << destination << std::endl;
}

int main( int argc, char **argv){


	std::cout << "version " << __DATE__ << " " << __TIME__ << std::endl;

	// administer
	int32 ret;
	char vfd_name[S7_MAX_NAMLEN+1];
	char dev_name[S7_MAX_DEVICELEN+1];
	ord16      number;

	/* only use first device */
	int deviceNumber = 0;
	ret = s7_get_device( deviceNumber, &number, dev_name );
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " s7_get_device ret= " << ret
			<< " number= " << number
			<< " dev_name= " << dev_name
			<< std::endl;

	/* only use first vfd of first device */
	ret = s7_get_vfd( dev_name, deviceNumber, &number, vfd_name);
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " s7_get_vfd ret= " << ret
			<< " number= " << number
			<< " vfd_name= " << vfd_name
			<< std::endl;

	/* initialize s7 */
	std::string h1file = "h1_0"; // .dat
	ord32 cp_descr_ptr;
//	ret = s7_init( dev_name, const_cast<char*>(h1file.c_str()), &cp_descr_ptr );
	ret = s7_init( dev_name, vfd_name, &cp_descr_ptr );
	std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__
			<< " s7_init ret= " << ret
			<< std::endl;





#if 0
	{
		std::string plcname0 = "S7_TSPP";
		std::string vfdname0 = "VFD";


		// Internal parameters used by softnet library to identify the connection
		ord32 m_cpDescr0 = -1;
		ord16 m_cref0 = -1;
		int ret = s7_init(const_cast<char*>(h1file.c_str()) ,const_cast<char*>(vfdname0.c_str()) ,&m_cpDescr0);
		std::cout << vfdname0 << " s7_init ret= " << ret << " m_cpDescr0= " << m_cpDescr0 << std::endl;

		ret = s7_get_cref(m_cpDescr0, const_cast<char*>(plcname0.c_str()), &m_cref0);
		std::cout << plcname0 << " s7_get_cref ret= " << ret << "  m_cref0= " << m_cref0 << std::endl;

		ord16 orderid;
		int32 msg;
		int noMessageCounter = 0;
		clock_t begin_time = clock();
		int32 last_event_expected = -1;
		std::cout << plcname0 << "---receiving messages---" << std::endl;
		do
		{
			msg = s7_receive( m_cpDescr0, &m_cref0, &orderid );
			std::cout << plcname0 << " s7_receive msg= " << msg << std::endl;
			switch( msg ) {
			case S7_NO_MSG: {
				noMessageCounter++;
				break;
			}
			case S7_INITIATE_CNF: {
				int conf=s7_get_initiate_cnf();
				std::cout << vfdname0 << " s7_get_initiate_cnf conf= " << conf << std::endl;
				break;
			}
			default:{
				s7_discard_msg();
				break;
			}
			} // switch
			usleep( 100000 );
		} // do
		while(msg != last_event_expected
				&& ((float( clock () - begin_time ) / CLOCKS_PER_SEC)< 5)
				&& ( noMessageCounter < 10 ));
	}
#endif
	{
		std::string plcname1 = "S7_TSPP2";
		std::string vfdname1 = "VFD2";
		ord32 m_cpDescr1 = -1;
		ord16 m_cref1 = -1;
		ord16 orderid = 1;


		int ret = s7_init(const_cast<char*>(h1file.c_str()) ,const_cast<char*>(vfdname1.c_str()) ,&m_cpDescr1);
		std::cout << vfdname1 << " s7_init ret= " << ret << " m_cpDescr1= " << m_cpDescr1 << std::endl;
		ret = s7_get_cref(m_cpDescr1, const_cast<char*>(plcname1.c_str()), &m_cref1);
		std::cout << plcname1 << " s7_get_cref ret= " << ret << "  m_cref1= " << m_cref1 << std::endl;

#if 0
		{
		/**
		 * understand address transformation
		 */
			std::string newAddress = "xxx";
			transformAddress("DB148.DBW16", newAddress);

		}
#endif
		{
			std::cout << plcname1 << "---write 4 bytes IP to handshake PLC---" << std::endl;
			std::string s7Adress = "DB100,D0,1";

			// client IP to write into, so that we get a response: 128.141.159.20
			const uint8_t ip1 = 128;
			const uint8_t ip2 = 141;
			const uint8_t ip3 = 159;
			const uint8_t ip4 = 20;

			struct S7_WRITE_PARA_LONG write_para;
			write_para.access=S7_ACCESS_SYMB_ADDRESS;
			strcpy(write_para.var_name, s7Adress.c_str());
			write_para.var_length=4;
			write_para.value[0] = ip1;
			write_para.value[1] = ip2;
			write_para.value[2] = ip3;
			write_para.value[3] = ip4;

			int ret=s7_write_long_req(m_cpDescr1, m_cref1, orderid, &write_para, NULL);

			if( ret != S7_OK )
			{
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " ERROR: s7 handshake/write request ret= " << ret << std::endl;
				int lastErr = s7_last_iec_err_no();
				std::cout << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " s7_last_iec_err_no= " << lastErr << std::endl;
				exit (0);
			}
		}





#if 0
			struct S7_WRITE_PARA para;
			para.access=S7_ACCESS_SYMB_ADDRESS;
			std::string addr_siemens = "DB100,D0,1";
			strcpy(para.var_name, addr_siemens.c_str() );
			para.var_length=2;
			uint16_t value = 8;
			Uint16union.number = value;
			para.value[1] = Uint16union.byteContents[0];
			para.value[0] = Uint16union.byteContents[1];
			std::cout << "Trying to write " << static_cast<int>( value ) << std::endl;
			int ret = s7_write_req(m_cpDescr1, m_cref1, 0, &para, NULL);
#endif


		{
			/**
			 * write an integer
			 *
			 * #Config Line : CPC_AnalogInput;deviceNumber;Alias[,DeviceLinkList];Description - ElectricalDiagram;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;RangeMax;RangeMin;HHLimit;HLimit;LLimit;LLLimit;AlarmActive;DriverDeadbandValue;DriverDeadbandType;ArchiveMode;TimeFilter;SMSCat;AlarmMessage;AlarmAck;addr_StsReg01;addr_EvStsReg01;addr_PosSt;addr_HFSt;addr_ManReg01;addr_MPosR;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;
			 * CPC_AnalogInput;1;TEST3_AI_0001;AI_0001 - ;;;;tssp_300;AI;AnalogInput_Small;;#;100.0;0.0;;;;;false;0.01;2;O;0;;;false;DB148.DBW0;DB150.DBD2;DB155.DBD0F;DB155.DBD4F;DB157.DBW0;DB157.DBD2F;;;;1;;;;;1;TEST3_AI_0001;
			 */

			std::cout << plcname1 << "---write uint16---" << std::endl;

			std::string newAddress16;
			std::string s7Adress16 = "DB148.DBW0";
			transformAddress(s7Adress16, newAddress16);
			struct S7_WRITE_PARA write_para16;
			write_para16.access=S7_ACCESS_SYMB_ADDRESS;
			strcpy(write_para16.var_name, newAddress16.c_str());
			write_para16.var_length=2;
			uint16_t value16 = 8;
			Uint16union.number = value16;
			write_para16.value[1] = Uint16union.byteContents[0];
			write_para16.value[0] = Uint16union.byteContents[1];
			std::cout << "Trying to write " << static_cast<int>( value16 )
					<< " in address " << newAddress16 << std::endl;
			int ret = s7_write_req(m_cpDescr1, m_cref1, 0, &write_para16, NULL);
			if( ret != S7_OK )
			{
				std::cout << "ERROR: s7 write request ret= " << ret << std::endl;
				//shutdown();
			}
			//Wait for confirmation
			//receiveWrite(S7_WRITE_CNF, msRecieveTimeout);



			std::cout << plcname1 << "---write uint32---" << std::endl;
			uint32_t value32 = 8;
			std::string s7Adress = "DB148.DBW0";
			std::string newAddress;
			transformAddress(s7Adress, newAddress);
			struct S7_WRITE_PARA_LONG write_para;
			write_para.access=S7_ACCESS_SYMB_ADDRESS;
			strcpy(write_para.var_name,newAddress.c_str());
			write_para.var_length=4;
			Int32union.number = value32;
			write_para.value[0] = Int32union.byteContents[3];
			write_para.value[1] = Int32union.byteContents[2];
			write_para.value[2] = Int32union.byteContents[1];
			write_para.value[3] = Int32union.byteContents[0];
			ret = s7_write_long_req(m_cpDescr1, m_cref1, 0, &write_para, NULL);
			if( ret != S7_OK ){
				std::cout << "ERROR: s7 write long request returned: [" << ret << "]" << std::endl;
			}

		}
		{
			/**
			 * #Config Line : PLCCONFIG;S7,thePlcType;thePlcName;theApplicationName;theLocalId;theLocalRack;theLocalSlot;theLocalConnResource;thePartnerRack;thePartnerSlot;thePartnerConnResource;theConnectionTimeout;thePlcIPAddress;thePlcDsIpAddress;thePlcCounterAddress;thePlcCommandAddress;thePlcInfoAddress;theBaselineVersionAddress;theApplicationVersionAddress;theResourcePackageVersionMajorAddress;theResourcePackageVersionMinorAddress;theResourcePackageVersionSmallAddress;theResourcePackageVersionString;
			 * PLCCONFIG;S7,S7-300 PN/DP;PLCCOIS33;tssp_300;1;0;2;10;0;0;10;5000;137.138.25.115;DB100.DBD0;DB100.DBW4;DB100.DBW6;DB100.DBW16;DB118.DBD0F;DB100.DBD48F;DB100.DBW52;DB100.DBW54;DB100.DBW56;1.12.0-beta-16;
			 */
			std::cout << plcname1 << "---handshake: write IP---" << std::endl;
			std::string handshakeAddress = "DB100,D0,1"; // datablock 100, range=doubleword(=4bytes)start at word 0, 1 units(=4bytes x 1)
			//std::string s7Adress = "DB100.DBD0";

			// IP of the plc: 137.138.25.115
			// IP of the client: 128.141.159.20 pcen33951 as registered
			const uint8_t ip1 = 137;
			const uint8_t ip2 = 138;
			const uint8_t ip3 = 25;
			const uint8_t ip4 = 115;

			struct S7_WRITE_PARA_LONG write_para;
			write_para.access=S7_ACCESS_SYMB_ADDRESS;
			// strcpy(write_para.var_name, s7Adress.c_str() );
			std::cout << plcname1 << " address= "  << handshakeAddress << std::endl;

			strcpy(write_para.var_name, handshakeAddress.c_str() );
			write_para.var_length = 4;
			write_para.value[0] = ip1;
			write_para.value[1] = ip2;
			write_para.value[2] = ip3;
			write_para.value[3] = ip4;

			ret = s7_write_long_req( m_cpDescr1, m_cref1, orderid, &write_para, NULL );
			if(ret != S7_OK ) {
				std::cout << "ERROR: s7 write long request returned: [" << ret << "]" << std::endl;
			}
			//Wait for confirmation
			//    receiveWrite(S7_WRITE_CNF, msRecieveTimeout);
		}
	}
	return 0;
}




