<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0"
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:output method="text"></xsl:output>
	<xsl:include href="Design/CommonFunctions.xslt" />
	<xsl:template name="commands">
	add_custom_command(OUTPUT ${PROJECT_SOURCE_DIR}/AddressSpace/include/<xsl:value-of select="fnc:ASClassName(@name)"/>.h ${PROJECT_SOURCE_DIR}/AddressSpace/src/<xsl:value-of select="fnc:ASClassName(@name)"/>.cpp 
	COMMAND python ${PROJECT_SOURCE_DIR}/framework.py generate asclass <xsl:value-of select="@name"/>
	DEPENDS ${DESIGN_FILE} ${PROJECT_SOURCE_DIR}/AddressSpace/designToClassHeader.xslt Configuration.hxx validateDesign
	)
	
	</xsl:template>
	<xsl:template name="simpleFileList">
	${PROJECT_SOURCE_DIR}/AddressSpace/include/<xsl:value-of select="fnc:ASClassName(@name)"/>.h
	${PROJECT_SOURCE_DIR}/AddressSpace/src/<xsl:value-of select="fnc:ASClassName(@name)"/>.cpp
	</xsl:template>
	<xsl:template match="/">
	<xsl:for-each select="/d:design/d:class">
	<xsl:call-template name="commands"/>
	</xsl:for-each>
	
	set(ADDRESSSPACE_CLASSES 
	<xsl:for-each select="/d:design/d:class">
	<xsl:call-template name="simpleFileList"/>
	</xsl:for-each>
	)
	
	set(ADDRESSSPACE_HEADERS
	<xsl:for-each select="/d:design/d:class">
	<xsl:value-of select="fnc:ASClassName(@name)"/>.h
	</xsl:for-each>
	)
	
	</xsl:template>
</xsl:transform>
