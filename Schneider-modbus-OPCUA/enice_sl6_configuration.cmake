# @author bfarnham
# @date 07-Sep-2015
# The purpose of this file is to set parameters for building the CAEN OPC-UA server on EN/ICE machines.

# The approach is to satisfy the requirements as much as possible.

message("using build configuration from enice_sl6_configuration.cmake")
message(STATUS "environment vars: BOOST_HOME [$ENV{BOOST_HOME}] UNIFIED_AUTOMATION_HOME [$ENV{UNIFIED_AUTOMATION_HOME}]")


#-------
#Boost
#-------
if( NOT DEFINED ENV{BOOST_HOME} )
	message(FATAL_ERROR "environment variable BOOST_HOME not defined - please set this to a 64bit boost installation")
else()	
	SET( BOOST_PATH_HEADERS $ENV{BOOST_HOME}/include )
	SET( BOOST_PATH_LIBS $ENV{BOOST_HOME}/lib )
endif()
message(STATUS "BOOST - include [${BOOST_PATH_HEADERS}] libs [${BOOST_PATH_LIBS}] BOOST_LIB_SUFFIX [$ENV{BOOST_LIB_SUFFIX}] (compiler info boost builds decorate library names with, often this can be ommitted)")

if(NOT TARGET libboostprogramoptions)
        add_library(libboostprogramoptions STATIC IMPORTED)
        set_property(TARGET libboostprogramoptions PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_program_options$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostsystem)
        add_library(libboostsystem STATIC IMPORTED)
        set_property(TARGET libboostsystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_system$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostfilesystem)
        add_library(libboostfilesystem STATIC IMPORTED)
        set_property(TARGET libboostfilesystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_filesystem$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostchrono) 
        add_library(libboostchrono STATIC IMPORTED)
        set_property(TARGET libboostchrono PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_chrono$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostdatetime) 
        add_library(libboostdatetime STATIC IMPORTED)
        set_property(TARGET libboostdatetime PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_date_time$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostthread) 
        add_library(libboostthread STATIC IMPORTED)
        set_property(TARGET libboostthread PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_thread$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlog)
        add_library(libboostlog STATIC IMPORTED)
        set_property(TARGET libboostlog PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlogsetup)
        add_library(libboostlogsetup STATIC IMPORTED)
        set_property(TARGET libboostlogsetup PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log_setup$ENV{BOOST_LIB_SUFFIX}.a)
endif()

set( BOOST_LIBS  libboostlogsetup libboostlog libboostsystem libboostfilesystem libboostthread libboostprogramoptions libboostchrono libboostdatetime -lrt)

#------
#OPCUA
#------
add_definitions( -DBACKEND_UATOOLKIT )
if( NOT DEFINED ENV{UNIFIED_AUTOMATION_HOME} )
	message(FATAL_ERROR "environment variable UNIFIED_AUTOMATION_HOME not defined - please set this to a 64bit unified automation toolkit installation")
else()
	SET( OPCUA_TOOLKIT_PATH $ENV{UNIFIED_AUTOMATION_HOME} )		
endif()
message(STATUS "UA TOOLKIT - OPC-UA toolkit path [${OPCUA_TOOLKIT_PATH}]" )

if(NOT TARGET libuamodule)
	add_library(libuamodule STATIC IMPORTED)
	set_property(TARGET libuamodule PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuamodule.a)
endif()
if(NOT TARGET libcoremodule)
	add_library(libcoremodule STATIC IMPORTED)
	set_property(TARGET libcoremodule PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libcoremodule.a)
endif()
if(NOT TARGET libuabase)
	add_library(libuabase STATIC IMPORTED)
	set_property(TARGET libuabase PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuabase.a)
endif()
if(NOT TARGET libuastack)
	add_library(libuastack STATIC IMPORTED)
	set_property(TARGET libuastack PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuastack.a)
endif()
if(NOT TARGET libuapki)
	add_library(libuapki STATIC IMPORTED)
	set_property(TARGET libuapki PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuapki.a)
endif()
if(NOT TARGET libxmlparser)
	add_library(libxmlparser STATIC IMPORTED)
	set_property(TARGET libxmlparser PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libxmlparser.a)
endif()

if(NOT TARGET libuamoduled)
	add_library(libuamoduled STATIC IMPORTED)
	set_property(TARGET libuamoduled PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuamoduled.a)
endif()
if(NOT TARGET libcoremoduled)
	add_library(libcoremoduled STATIC IMPORTED)
	set_property(TARGET libcoremoduled PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libcoremoduled.a)
endif()
if(NOT TARGET libuabased)
	add_library(libuabased STATIC IMPORTED)
	set_property(TARGET libuabased PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuabased.a)
endif()
if(NOT TARGET libuastackd)
	add_library(libuastackd STATIC IMPORTED)
	set_property(TARGET libuastackd PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuastackd.a)
endif()
if(NOT TARGET libuapkid)
	add_library(libuapkid STATIC IMPORTED)
	set_property(TARGET libuapkid PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libuapkid.a)
endif()
if(NOT TARGET libxmlparserd)
	add_library(libxmlparserd STATIC IMPORTED)
	set_property(TARGET libxmlparserd PROPERTY IMPORTED_LOCATION ${OPCUA_TOOLKIT_PATH}/lib/libxmlparserd.a)
endif()

SET( OPCUA_TOOLKIT_LIBS_DEBUG libuamoduled libcoremoduled libuabased libuastackd libuapkid libxmlparserd "-lxml2 -lssl -lcrypto -lpthread") 
SET( OPCUA_TOOLKIT_LIBS_RELEASE libuamodule libcoremodule libuabase libuastack libuapki libxmlparser "-lxml2 -lssl -lcrypto -lpthread")

#SET( OPCUA_TOOLKIT_LIBS_DEBUG "-luamoduled -lcoremoduled -luabased -luastackd -luapkid -lxmlparserd -lxml2 -lssl -lcrypto -lpthread" ) 
#SET( OPCUA_TOOLKIT_LIBS_RELEASE "-luamodule -lcoremodule -luabase -luastack -luapki -lxmlparser -lxml2 -lssl -lcrypto -lpthread" )

#-----
# LogIt
#-----
SET( LOGIT_HAS_STDOUTLOG TRUE )
SET( LOGIT_HAS_BOOSTLOG TRUE )
SET( LOGIT_HAS_UATRACE FALSE )
MESSAGE( STATUS "LogIt build options: stdout [${LOGIT_HAS_STDOUTLOG}] boost [${LOGIT_HAS_BOOSTLOG}] uaTrace [${LOGIT_HAS_UATRACE}]" )

#-----
#XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested
SET( XML_LIBS "-lxerces-c" ) 

#-----
#GoogleTest
#-----
include_directories(
	${PROJECT_SOURCE_DIR}/GoogleTest/gtest/src/gtest/include
)

# TODO: split between Win / MSVC, perhaps MSVC has different notation for these
add_definitions(-Wall -Wno-deprecated -std=gnu++0x -Wno-literal-suffix) 
