/*
 * TypeDefinitions.h
 *
 *  Created on: Jun 12, 2015
 *      Author: bfarnham
 */

#ifndef TYPEDEFINITIONS_H_
#define TYPEDEFINITIONS_H_

#include <boost/shared_ptr.hpp>
#include "State.h"

typedef  boost::shared_ptr<State> StateSharedPtr;

#endif /* TYPEDEFINITIONS_H_ */
