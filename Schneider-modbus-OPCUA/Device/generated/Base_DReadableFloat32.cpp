
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
    Authors(from Quasar team): Piotr Nikiel

    This file is part of Quasar.

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

    This file was completely generated by Quasar (additional info: using transform designToDeviceBaseBody.xslt)
    on 2020-02-24T16:17:51.175+01:00
 */




#include <boost/foreach.hpp>

#include <Configuration.hxx>


#include <Base_DReadableFloat32.h>







namespace Device
{



void Base_DReadableFloat32::linkAddressSpace( AddressSpace::ASReadableFloat32 *addressSpaceLink, const std::string & stringAddress)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
    m_stringAddress.assign( stringAddress );
}

/* add/remove */


//! Disconnects AddressSpace part from the Device logic, and does the same for all children
//! Returns number of unlinked objects including self
unsigned int Base_DReadableFloat32::unlinkAllChildren ()
{
    unsigned int objectCounter=1;  // 1 is for self
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */

    return objectCounter;

}


/* find methods for children */






/* Constructor */
Base_DReadableFloat32::Base_DReadableFloat32 (
    const Configuration::ReadableFloat32 & config,
    Parent_DReadableFloat32 * parent
):

    m_parent(parent),
    m_addressSpaceLink(0),
    m_stringAddress("**NB**")


{

}

Base_DReadableFloat32::~Base_DReadableFloat32 ()
{
    /* remove children */

}



std::list<DReadableFloat32*> Base_DReadableFloat32::s_orphanedObjects;




}


