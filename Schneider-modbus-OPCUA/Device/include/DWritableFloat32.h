
/* This is device header stub */


#ifndef __DWritableFloat32__H__
#define __DWritableFloat32__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../../../Siemens-S7-300-400-OPCUA/Device/generated/Base_DWritableFloat32.h"


namespace Device
{




class
    DWritableFloat32
    : public Base_DWritableFloat32
{

public:
    /* sample constructor */
    explicit DWritableFloat32 ( const Configuration::WritableFloat32 & config

                                , DDevice * parent

                              ) ;
    /* sample dtr */
    ~DWritableFloat32 ();

    DDevice * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeValue ( const OpcUa_Double & v);


    /* find methods for children */


    /* getters for values which are keys */


private:
    /* Delete copy constructor and assignment operator */
    DWritableFloat32( const DWritableFloat32 & );
    DWritableFloat32& operator=(const DWritableFloat32 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

    DDevice * m_parent;


    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


};




}

#endif // include guard
