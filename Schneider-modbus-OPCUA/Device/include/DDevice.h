
/* This is device header stub */


#ifndef __DDevice__H__
#define __DDevice__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../../../Siemens-S7-300-400-OPCUA/Device/generated/Base_DDevice.h"


namespace Device
{




class
    DDevice
    : public Base_DDevice
{

public:
    /* sample constructor */
    explicit DDevice ( const Configuration::Device & config

                       , DRoot * parent

                     ) ;
    /* sample dtr */
    ~DDevice ();

    DRoot * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */

    void add ( DReadableUInt16 *);
    const std::vector<DReadableUInt16 * > & readableuint16s () const {
        return m_ReadableUInt16s;
    }

    void add ( DReadableUInt32 *);
    const std::vector<DReadableUInt32 * > & readableuint32s () const {
        return m_ReadableUInt32s;
    }

    void add ( DReadableFloat32 *);
    const std::vector<DReadableFloat32 * > & readablefloat32s () const {
        return m_ReadableFloat32s;
    }

    void add ( DWritableUInt16 *);
    const std::vector<DWritableUInt16 * > & writableuint16s () const {
        return m_WritableUInt16s;
    }

    void add ( DWritableUInt32 *);
    const std::vector<DWritableUInt32 * > & writableuint32s () const {
        return m_WritableUInt32s;
    }

    void add ( DWritableFloat32 *);
    const std::vector<DWritableFloat32 * > & writablefloat32s () const {
        return m_WritableFloat32s;
    }

    /* find methods for children */


    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DDevice( const DDevice & );
    DDevice& operator=(const DDevice &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

    DRoot * m_parent;


    AddressSpace::
    ASDevice
    *m_addressSpaceLink;

    std::vector < DReadableUInt16 * > m_ReadableUInt16s;

    std::vector < DReadableUInt32 * > m_ReadableUInt32s;

    std::vector < DReadableFloat32 * > m_ReadableFloat32s;

    std::vector < DWritableUInt16 * > m_WritableUInt16s;

    std::vector < DWritableUInt32 * > m_WritableUInt32s;

    std::vector < DWritableFloat32 * > m_WritableFloat32s;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


};




}

#endif // include guard
