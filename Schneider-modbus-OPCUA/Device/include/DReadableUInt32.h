
/* This is device header stub */


#ifndef __DReadableUInt32__H__
#define __DReadableUInt32__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../../../Siemens-S7-300-400-OPCUA/Device/generated/Base_DReadableUInt32.h"


namespace Device
{




class
    DReadableUInt32
    : public Base_DReadableUInt32
{

public:
    /* sample constructor */
    explicit DReadableUInt32 ( const Configuration::ReadableUInt32 & config

                               , DDevice * parent

                             ) ;
    /* sample dtr */
    ~DReadableUInt32 ();

    DDevice * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */
    void update(uint32_t value, std::string serverTime, int miliseconds);


    /* find methods for children */

    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DReadableUInt32( const DReadableUInt32 & );
    DReadableUInt32& operator=(const DReadableUInt32 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

    DDevice * m_parent;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


};





}

#endif // include guard
