
/* This is device header stub */


#ifndef __DWritableUInt32__H__
#define __DWritableUInt32__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../../../Siemens-S7-300-400-OPCUA/Device/generated/Base_DWritableUInt32.h"


namespace Device
{




class
    DWritableUInt32
    : public Base_DWritableUInt32
{

public:
    /* sample constructor */
    explicit DWritableUInt32 ( const Configuration::WritableUInt32 & config

                               , DDevice * parent

                             ) ;
    /* sample dtr */
    ~DWritableUInt32 ();

    DDevice * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */


    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeValue ( const OpcUa_UInt32 & v);

    /* find methods for children */


    /* getters for values which are keys */


private:
    /* Delete copy constructor and assignment operator */
    DWritableUInt32( const DWritableUInt32 & );
    DWritableUInt32& operator=(const DWritableUInt32 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

    DDevice * m_parent;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


};




}

#endif // include guard
