
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DReadableFloat32.h>
#include <ASReadableFloat32.h>

#include <uadatetime.h>
#include "LogIt.h"

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DReadableFloat32::DReadableFloat32 (const Configuration::ReadableFloat32 & config

                                    , DDevice * parent

                                   ):
    Base_DReadableFloat32( config

                           , parent

                         )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DReadableFloat32::~DReadableFloat32 ()
{
}

/* delegators for cachevariables and externalvariables */



//TODO change to util method using template for changing between uint and float

void DReadableFloat32::update(float value, std::string serverTimeStr, int miliseconds)
{
	UaDateTime serverTime = UaDateTime::fromString(serverTimeStr.c_str());
	serverTime.addMilliSecs(miliseconds);
	getAddressSpaceLink()->setValue(value, OpcUa_Good, serverTime);
}

}


