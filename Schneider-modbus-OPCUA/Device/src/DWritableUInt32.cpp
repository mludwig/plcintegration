
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DWritableUInt32.h>
#include <ASWritableUInt32.h>
#include "CommandQueue.h"

/*namespace std{
	extern s7Interface g_PLCInterface;
}*/
using namespace std;

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DWritableUInt32::DWritableUInt32 (const Configuration::WritableUInt32 & config

                                  , DDevice * parent

                                 ):
    Base_DWritableUInt32( config

                          , parent

                        )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DWritableUInt32::~DWritableUInt32 ()
{
}

/* delegators for cachevariables and externalvariables */

/* Note: never directly call this function. */

UaStatus DWritableUInt32::writeValue ( const OpcUa_UInt32 & v)
{
	UaString addr;
	getAddressSpaceLink()->getAddress(addr);
	CommandQueue::commandPut(CommandItem(addr.toUtf8(), static_cast<uint32_t>(v)));
	return OpcUa_Good;
}

/* find methods for children */


}


