
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DReadableUInt16.h>
#include <ASReadableUInt16.h>

#include <uadatetime.h>

namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DReadableUInt16::DReadableUInt16 (const Configuration::ReadableUInt16 & config

                                  , DDevice * parent

                                 ):
    Base_DReadableUInt16( config

                          , parent

                        )
    /* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DReadableUInt16::~DReadableUInt16 ()
{
}

/* delegators for cachevariables and externalvariables */



void DReadableUInt16::update(uint16_t value, std::string serverTimeStr, int miliseconds)
{
	UaDateTime serverTime = UaDateTime::fromString(serverTimeStr.c_str());
	serverTime.addMilliSecs(miliseconds);
	getAddressSpaceLink()->setValue(value, OpcUa_Good, serverTime);
}


}


