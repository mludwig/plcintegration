#include "ModbusWriteMultipleRegistersPacket.h"

#include "ModbusWrapper.h"
#include <iostream>
#include "LogIt.h"
#include "ModbusBinAnaTSPPPacket.h"
#include "ModbusEventTSPPPacket.h"
#include "DriverUtils.h"

//Size in bytes of a Modbus response (WriteMultipleRegisters)
const uint8_t g_sModbusResponseSize = 12;
//Size in bytes of a Modbus query header (WriteMultipleRegisters)
const uint8_t g_sModbusHeaderSize = 13;
//TSPP Header identifier of a Binary Analog frame
const uint8_t g_sTSPPBinAnaId = 01;
//TSPP Header identifier of an Event frame
const uint8_t g_sTSPPEventId = 17;

const int g_sModbusHeaderTransIdOffset = 0;//IN BYTES
const int g_sModbusHeaderProtocolIdOffset = 2;//IN BYTES
const int g_sModbusHeaderLenghtOffset = 4;//IN BYTES
const int g_sModbusHeaderUnitIdOffset = 6;//IN BYTES
const int g_sModbusHeaderFunctionCodeOffset = 7;//IN BYTES
const int g_sModbusHeaderReferenceOffset = 8;//IN BYTES
const int g_sModbusHeaderWordCountOffset = 10;//IN BYTES
const int g_sModbusHeaderByteCountOffset = 12;//IN BYTES
const int g_sModbusBodyOffset = 13;//IN BYTES


ModbusWriteMultipleRegistersPacket::ModbusWriteMultipleRegistersPacket(uint16_t transactionIdentifier, uint16_t protocolIdentifier, uint16_t length, uint8_t unitIdentifier, uint8_t functionCode, uint16_t referenceNumber, uint16_t wordCount, uint8_t byteCount, const uint8_t buffer[]):
	m_transactionIdentifier(transactionIdentifier), m_protocolIdentifier(protocolIdentifier), m_lenght(length), m_unitIdentifier(unitIdentifier),
	m_functionCode(functionCode), m_referenceNumber(referenceNumber), m_wordCount(wordCount), m_byteCount(byteCount), m_register(wordCount)
{
	for(int i = 0; i < wordCount; i++)
	{
		m_register[i] = DriverUtils::getWordFromBytes(buffer[i*2], buffer[i*2 + 1]);
	}
}
//Build the ModbusWriteMultipleRegistersPacket from the header. setRegisters method should be called in order to have a proper packet.
ModbusWriteMultipleRegistersPacket::ModbusWriteMultipleRegistersPacket(const uint8_t header[], const size_t headerLengt):
	m_register(DriverUtils::getWordFromBytes(header[g_sModbusHeaderWordCountOffset] , header[g_sModbusHeaderWordCountOffset + 1]))
{
	if(headerLengt != g_sModbusHeaderSize)
	{
		LOG(Log::ERR) << "ModbusWriteMultipleRegistersPacket::ModbusWriteMultipleRegistersPacket : header of wrong size: [" << headerLengt << "] bytes, should be [" << g_sModbusHeaderSize << "]";
	}

	m_transactionIdentifier = DriverUtils::getWordFromBytes(header[g_sModbusHeaderTransIdOffset] , header[g_sModbusHeaderTransIdOffset + 1]);
	m_protocolIdentifier = DriverUtils::getWordFromBytes(header[g_sModbusHeaderProtocolIdOffset] , header[g_sModbusHeaderProtocolIdOffset + 1]);
	m_lenght = DriverUtils::getWordFromBytes(header[g_sModbusHeaderLenghtOffset] , header[g_sModbusHeaderLenghtOffset + 1]);
	m_unitIdentifier = header[g_sModbusHeaderUnitIdOffset];
	m_functionCode = header[g_sModbusHeaderFunctionCodeOffset];
	m_referenceNumber = DriverUtils::getWordFromBytes(header[g_sModbusHeaderReferenceOffset] , header[g_sModbusHeaderReferenceOffset + 1]);
	m_wordCount = DriverUtils::getWordFromBytes(header[g_sModbusHeaderWordCountOffset] , header[g_sModbusHeaderWordCountOffset + 1]);
	m_byteCount = header[g_sModbusHeaderByteCountOffset];
}
int ModbusWriteMultipleRegistersPacket::decodeRegisterIntoTSPPPacket()
{
	const int packetId = DriverUtils::getByte1(m_register[0]);
	if(packetId == g_sTSPPEventId)
	{
		ModbusEventTSPPPacket TSPPPacket(m_register);
		TSPPPacket.pushInfo();
	}
	else if(packetId == g_sTSPPBinAnaId)
	{
		ModbusBinAnaTSPPPacket TSPPPacket(m_register);
		TSPPPacket.pushInfo();
	}
	else
	{
		LOG(Log::ERR) << "Unrecognized TSPP Frame type: [" << packetId << "]";
		return -1;
	}
	return 0;
}
int ModbusWriteMultipleRegistersPacket::setRegisters(const boost::shared_array<uint8_t> body, const size_t bodyLength)
{
	for (int i = 0; i < m_wordCount; i++)
	{
		const size_t firstBytePosition = i * 2;
		const size_t secondBytePosition = firstBytePosition + 1;

		if(firstBytePosition > bodyLength-1 || secondBytePosition > bodyLength-1)
		{
			LOG(Log::ERR) << "ModbusWriteMultipleRegistersPacket::setRegisters : position [" << firstBytePosition << "] exceeded max length [" << bodyLength - 1 << "]. Skipping packet due having wrong format. WordCount = " << m_wordCount << "; ByteCount = " << static_cast<int>(m_byteCount);
			break;
		}
		//LOG(Log::DBG) << "Setting register: First byte = [" << static_cast<int>(body[firstBytePosition]) << "], second byte = [" << static_cast<int>(body[secondBytePosition]) << "]";
		const uint8_t firstByte = body[firstBytePosition];
		const uint8_t secondByte = body[secondBytePosition];
		
		m_register[i] = DriverUtils::getWordFromBytes(firstByte , secondByte);
	}
	//When this method is called it means that we are the server, and hence, that the packet that we are receiving is a TSPP packet.
	return decodeRegisterIntoTSPPPacket();
}

boost::shared_array<uint8_t> ModbusWriteMultipleRegistersPacket::generateResponse()
{
	uint16_t len = 6;
	boost::shared_array<uint8_t> response(new uint8_t[g_sModbusResponseSize]());
	LOG(Log::DBG) << "GeneratingResponse. Size: [" << g_sModbusResponseSize << "]";
	response[g_sModbusHeaderTransIdOffset] = DriverUtils::getByte1(m_transactionIdentifier);
	response[g_sModbusHeaderTransIdOffset + 1] = DriverUtils::getByte2(m_transactionIdentifier);
	response[g_sModbusHeaderProtocolIdOffset] = DriverUtils::getByte1(m_protocolIdentifier);
	response[g_sModbusHeaderProtocolIdOffset + 1] = DriverUtils::getByte2(m_protocolIdentifier);
	response[g_sModbusHeaderLenghtOffset] = DriverUtils::getByte1(len);
	response[g_sModbusHeaderLenghtOffset + 1] = DriverUtils::getByte2(len);
	response[g_sModbusHeaderUnitIdOffset] = m_unitIdentifier;
	response[g_sModbusHeaderFunctionCodeOffset] = m_functionCode;
	response[g_sModbusHeaderReferenceOffset] = DriverUtils::getByte1(m_referenceNumber);
	response[g_sModbusHeaderReferenceOffset + 1] = DriverUtils::getByte2(m_referenceNumber);
	response[g_sModbusHeaderWordCountOffset] = DriverUtils::getByte1(m_wordCount);
	response[g_sModbusHeaderWordCountOffset + 1] = DriverUtils::getByte2(m_wordCount);
	for(int i = 0; i<g_sModbusResponseSize ; i++)
	{
		LOG(Log::DBG) << "Response byte [" << i << "] = [" << static_cast<uint32_t>(response[i]) << "]";
	}

	return response;
}

boost::shared_array<uint8_t> ModbusWriteMultipleRegistersPacket::generateQuery()
{
	boost::shared_array<uint8_t> query(new uint8_t[g_sModbusHeaderSize + m_wordCount * 2]());
	LOG(Log::DBG) << "GeneratingQuery. Size: [" << g_sModbusHeaderSize + m_wordCount * 2 << "]";
	query[g_sModbusHeaderTransIdOffset] = DriverUtils::getByte1(m_transactionIdentifier);
	query[g_sModbusHeaderTransIdOffset + 1] = DriverUtils::getByte2(m_transactionIdentifier);
	query[g_sModbusHeaderProtocolIdOffset] = DriverUtils::getByte1(m_protocolIdentifier);
	query[g_sModbusHeaderProtocolIdOffset + 1] = DriverUtils::getByte2(m_protocolIdentifier);
	query[g_sModbusHeaderLenghtOffset] = DriverUtils::getByte1(m_lenght);
	query[g_sModbusHeaderLenghtOffset + 1] = DriverUtils::getByte2(m_lenght);
	query[g_sModbusHeaderUnitIdOffset] = m_unitIdentifier;
	query[g_sModbusHeaderFunctionCodeOffset] = m_functionCode;
	query[g_sModbusHeaderReferenceOffset] = DriverUtils::getByte1(m_referenceNumber);
	query[g_sModbusHeaderReferenceOffset + 1] = DriverUtils::getByte2(m_referenceNumber);
	query[g_sModbusHeaderWordCountOffset] = DriverUtils::getByte1(m_wordCount);
	query[g_sModbusHeaderWordCountOffset + 1] = DriverUtils::getByte2(m_wordCount);
	query[g_sModbusHeaderByteCountOffset] = m_byteCount;
	for (int i = 0; i < m_wordCount; i++)
	{
		query[g_sModbusBodyOffset + i*2] = DriverUtils::getByte1(m_register[i]);
		query[g_sModbusBodyOffset + i*2 + 1] = DriverUtils::getByte2(m_register[i]);
	}
	for(int i = 0; i < (g_sModbusHeaderSize + m_wordCount * 2) ; i++)
	{
		LOG(Log::DBG) << "Query byte [" << i << "] = [" << static_cast<uint32_t>(query[i]) << "]";
	}
	return query;
}
