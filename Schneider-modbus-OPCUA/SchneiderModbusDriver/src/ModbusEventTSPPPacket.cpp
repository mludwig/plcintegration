/*
 * ModbusEventTSPPPacket.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: dabalo
 */

#include "ModbusEventTSPPPacket.h"
#include "LogIt.h"
#include <iostream>
#include <sstream>
#include "DriverUtils.h"

const uint8_t g_sModbusTSPPHeaderPosition = 0;
const uint8_t g_sModbusTSPPEventPositionStart = 1;
const uint8_t g_sModbusTSPPEventLength = 5;

ModbusEventTSPPPacket::ModbusEventTSPPPacket(std::vector<uint16_t>& packet):
m_header(packet[g_sModbusTSPPHeaderPosition]), m_events(((packet.size() - g_sModbusTSPPEventPositionStart) / g_sModbusTSPPEventLength), TSPPEvent())
{
	m_packetId = DriverUtils::getByte1(m_header);
	m_year = DriverUtils::getByte2(m_header);
	for (int i = 0; i < m_events.size(); i++)
	{
		std::vector<uint16_t> eventBytes(g_sModbusTSPPEventLength);
		int thisEventOffset = g_sModbusTSPPEventPositionStart + (g_sModbusTSPPEventLength * i);
		for(int o = 0; o< g_sModbusTSPPEventLength; o++)
		{
			eventBytes[o] = packet[thisEventOffset + o];
		}
		m_events[i].setInfo(m_year, eventBytes);
	}
}

void ModbusEventTSPPPacket::printInfo()
{
	LOG(Log::INF) << "m_header = [" << std::dec << m_header << "]";
	for (int i = 0; i < m_events.size(); i++)
	{
		m_events[i].printInfo();
	}
}

void ModbusEventTSPPPacket::pushInfo()
{
	LOG(Log::DBG) << "Starting push info (EventPacket)";
	for (int i = 0; i < m_events.size(); i++)
	{
		m_events[i].pushInfo();
	}
	LOG(Log::DBG) << "Finishing push info (EventPacket)";
}
