/*
 * ModbusBinAnaTSPPPacket.cpp
 *
 *  Created on: Mar 20, 2015
 *      Author: dabalo
 */

#include "ModbusBinAnaTSPPPacket.h"
#include "LogIt.h"
#include <iostream>
#include <sstream>
#include "PropertiesMap.h"
#include "DriverUtils.h"

const uint8_t g_sModbusTSPPHeaderPosition = 0;
const uint8_t g_sModbusTSPPDateLowPosition = 1;
const uint8_t g_sModbusTSPPDateHighPosition = 2;
const uint8_t g_sModbusTSPPOffsetPosition = 3;
const uint8_t g_sModbusTSPPDataPositionStart = 4;

ModbusBinAnaTSPPPacket::ModbusBinAnaTSPPPacket(std::vector<uint16_t>& packet):
m_header(packet[g_sModbusTSPPHeaderPosition]), m_dateLow(packet[g_sModbusTSPPDateLowPosition]), m_dateHigh(packet[g_sModbusTSPPDateHighPosition]),
m_offset(packet[g_sModbusTSPPOffsetPosition]), m_data(static_cast<uint16_t>(packet.size() - g_sModbusTSPPDataPositionStart)),
m_packetId(DriverUtils::getByte1(m_header)), m_year(DriverUtils::getByte2(m_header)), m_ptimeDate(ModbusBinAnaTSPPPacket::getPlcTime(m_year, m_dateLow, m_dateHigh)),
m_PLCms(ModbusBinAnaTSPPPacket::getMs(m_dateLow, m_dateHigh)), m_PLCTime(ModbusBinAnaTSPPPacket::getISO8691(m_ptimeDate))
{
	for (int i = g_sModbusTSPPDataPositionStart; i < packet.size(); i++)
	{	
		m_data[i - g_sModbusTSPPDataPositionStart] = packet[i];
	}
}

boost::posix_time::ptime ModbusBinAnaTSPPPacket::getPlcTime(const uint8_t& year, const uint16_t& dateLow, const uint16_t& dateHigh)
{
	//This number represents second cenths from the begining of the year
	const long CentisecondsFromStartOfTheYear = DriverUtils::getLongFromWords(dateLow , dateHigh);
	LOG(Log::DBG) << "Centiseconds from the start of the year: " << CentisecondsFromStartOfTheYear;
	std::stringstream yearStringStream;
	//This will stop working on the year 2100...but it is not avoidable with the data that I am receiving.
	//In case of society collapse, complain to whoever designed the protocol
	yearStringStream << "20" << static_cast<int>(year) << "0101T000000";//TODO extract the first 2 digits of the year from the current year

	boost::posix_time::ptime plcTime(boost::posix_time::from_iso_string(yearStringStream.str()));

	for(int i = 0; i<10; i++)//This strange approach, adding 10 times the centiseconds instead of adding once the milliseconds, is due the function expecting a long
	{//If we use a long to express milliseconds from the start of the year it will easily overflow. Instead of this, the centiseconds (milliseconds/10) can be holded in a long
		plcTime += boost::posix_time::milliseconds(CentisecondsFromStartOfTheYear);
	}
	LOG(Log::DBG) << "Date of the packet = " << to_iso_string(plcTime);
	return plcTime;
}

const int ModbusBinAnaTSPPPacket::getMs(const uint16_t& dateLow, const uint16_t& dateHigh)
{
	//This number represents second cenths from the beggining of the year
	const long CentisecondsFromStartOfTheYear = DriverUtils::getLongFromWords(dateLow, dateHigh);
	LOG(Log::DBG) << "CentisecondsFromStartOfTheYear = " << CentisecondsFromStartOfTheYear;
	//We convert it to milliseconds
	//const long MsFromStartOfTheYear = CentisecondsFromStartOfTheYear * 10;
	//LOG(Log::DBG) << "MsFromStartOfTheYear = " << MsFromStartOfTheYear;
	LOG(Log::DBG) << "MS = " << static_cast<int>(CentisecondsFromStartOfTheYear % 100) * 10;
	return static_cast<int>(CentisecondsFromStartOfTheYear % 100) * 10;
}

const std::string ModbusBinAnaTSPPPacket::getISO8691(const boost::posix_time::ptime posixTime)
{
	const int month = posixTime.date().month().as_number();

	std::stringstream monthStringStream;
	monthStringStream << posixTime.date().year() << "-";
	if(month<=9)
	{
		monthStringStream << "0" << month;
	}
	else
	{
		monthStringStream << month;
	}

	monthStringStream << "-";
	const int day = posixTime.date().day().as_number();
	if(day<=9)
	{
		monthStringStream << "0" << day << "T";
	}
	else
	{
		monthStringStream << day << "T";
	}

	if(posixTime.time_of_day().hours() < 10)
	{
		monthStringStream << "0" << posixTime.time_of_day().hours() << ":";
	}
	else
	{
		monthStringStream << posixTime.time_of_day().hours() << ":";
	}
	if(posixTime.time_of_day().minutes() < 10)
	{
		monthStringStream << "0" << posixTime.time_of_day().minutes() << ":";
	}
	else
	{
		monthStringStream << posixTime.time_of_day().minutes() << ":";
	}
	if(posixTime.time_of_day().seconds() < 10)
	{
		monthStringStream << "0" << posixTime.time_of_day().seconds() << "Z";
	}
	else
	{
		monthStringStream << posixTime.time_of_day().seconds() << "Z";
	}
	return monthStringStream.str();
}

void ModbusBinAnaTSPPPacket::printInfo()
{
	LOG(Log::INF) << "m_header = [" << std::dec << m_header << "]";
	LOG(Log::INF) << "m_packetId = [" << std::dec << static_cast<int>(m_packetId) << "]";
	LOG(Log::INF) << "m_year = [" << std::dec << static_cast<int>(m_year) << "]";
	LOG(Log::INF) << "m_dateLow = [" << std::dec << m_dateLow << "]";
	LOG(Log::INF) << "m_dateHigh = [" << std::dec << m_dateHigh << "]";
	LOG(Log::INF) << "m_ptimeDate = [" << m_ptimeDate << "]";
	LOG(Log::INF) << "m_PLCTime = [" << m_PLCTime << "]";
	LOG(Log::INF) << "m_PLCms = [" << std::dec << m_PLCms << "]";
	LOG(Log::INF) << "m_offset = [" << std::dec << m_offset << "]";
}

void ModbusBinAnaTSPPPacket::pushInfo()
{
	LOG(Log::DBG) << "Starting push info. PLC time = [" << m_PLCTime << "]; PLC ms = [" << m_PLCms << "]; Address Starting Point = [" << m_offset << "]; Packet Size (in Words) = [" << m_data.size() << "]";
	for (int i = 0; i < m_data.size(); i++)
	{
		int intAddress = m_offset + i;
		std::stringstream ss;
		ss<<intAddress;
		std::string address = ss.str();

		//LOG(Log::INF) << address;

		Device::DReadableUInt16* uint16pointer =  PropertiesMap::searchUint16(address);
		if(uint16pointer != 0)
		{
			LOG(Log::DBG) << "The packet frame is a uint16";
			uint16pointer->update(static_cast<uint16_t>(m_data[i]), m_PLCTime, m_PLCms);
			continue;
		}
		Device::DReadableUInt32* uint32pointer =  PropertiesMap::searchUint32(address);
		if(uint32pointer != 0)
		{
			LOG(Log::DBG) << "The packet frame is a uint32";
			uint32pointer->update(static_cast<int>(DriverUtils::getLongFromWords(m_data[i], m_data[i+1])), m_PLCTime, m_PLCms);
			i++;
			continue;
		}
		Device::DReadableFloat32* float32pointer =  PropertiesMap::searchFloat32(address);
		if(float32pointer != 0)
		{
			LOG(Log::DBG) << "The packet frame is a float, value = [" << DriverUtils::getFloatFromWords(m_data[i], m_data[i+1]) << "]. Word 0 = [" << m_data[i] << "], Word 1 = [" << m_data[i+1] << "].";
			float32pointer->update(DriverUtils::getFloatFromWords(m_data[i], m_data[i+1]), m_PLCTime, m_PLCms);
			i++;
			continue;
		}
		LOG(Log::DBG) << "Address [" << address << "], that was provided by the PLC, is not listed.";
	}
}
