/*
 * TSPPEvent.cpp
 *
 *  Created on: Mar 24, 2015
 *      Author: dabalo
 */
#include "TSPPEvent.h"
#include "LogIt.h"
#include <iostream>
#include <sstream>
#include "PropertiesMap.h"
#include "DriverUtils.h"

const uint8_t g_sModbusEventTSPPDateLowPosition = 0;
const uint8_t g_sModbusEventTSPPDateHighPosition = 1;
const uint8_t g_sModbusEventTSPPOffsetPosition = 2;
const uint8_t g_sModbusEventTSPPNewValuePosition = 3;
const uint8_t g_sModbusEventTSPPOldValuePosition = 4;

TSPPEvent::TSPPEvent(uint8_t year,std::vector<uint16_t>& packet):
m_year(year), m_dateLow(packet[g_sModbusEventTSPPDateLowPosition]), m_dateHigh(packet[g_sModbusEventTSPPDateHighPosition]),
m_offset(packet[g_sModbusEventTSPPOffsetPosition]), m_newValue(packet[g_sModbusEventTSPPNewValuePosition]), m_oldValue(packet[g_sModbusEventTSPPOldValuePosition])
{
	processDate();
}
void TSPPEvent::setInfo(uint8_t year, std::vector<uint16_t>& packet)
{
	m_year = year;
	m_dateLow = packet[g_sModbusEventTSPPDateLowPosition];
	m_dateHigh = packet[g_sModbusEventTSPPDateHighPosition];
	m_offset = packet[g_sModbusEventTSPPOffsetPosition];
	m_newValue = packet[g_sModbusEventTSPPNewValuePosition];
	m_oldValue = packet[g_sModbusEventTSPPOldValuePosition];
	processDate();
}
void TSPPEvent::processDate()
{
	//This number represents second cenths from the beggining of the year
	long CsFromStartOfTheYear = DriverUtils::getLongFromWords(m_dateLow , m_dateHigh);

	std::stringstream ss1;
	//This will stop working on the year 2100...but it is not avoidable with the data that I am receiving.
	//In case of society collapse, complain to whoever designed the protocol
	ss1 << "20" << static_cast<int>(m_year) << "0101T000000";
	boost::posix_time::ptime temp(boost::posix_time::from_iso_string(ss1.str()));
	m_ptimeDate = temp;

	for(int i = 0; i<10; i++)//This strange approach, adding 10 times the centiseconds instead of adding once the milliseconds, is due the function expecting a long
	{//If we use a long to express milliseconds from the start of the year it will easily overflow. Instead of this, the centiseconds (milliseconds/10) can be holded in a long
		m_ptimeDate += boost::posix_time::milliseconds(CsFromStartOfTheYear);
	}

	//Output format expected by OPCUA: ISO8691
	//Example: 2015-03-20T11:13:33Z
	int month = m_ptimeDate.date().month().as_number();

	std::stringstream ss2;
	ss2 << m_ptimeDate.date().year() << "-";
	if(month<=9)
	{
		ss2 << "0" << month;
	}
	else
	{
		ss2 << month;
	}
	ss2 << "-";
	const int day = m_ptimeDate.date().day().as_number();
	if(day<=9)
	{
		ss2 << "0" << day << "T";
	}
	else
	{
		ss2 << day << "T";
	}
	if(m_ptimeDate.time_of_day().hours() < 10)
	{
		ss2 << "0" << m_ptimeDate.time_of_day().hours() << ":";
	}
	else
	{
		ss2 << m_ptimeDate.time_of_day().hours() << ":";
	}
	if(m_ptimeDate.time_of_day().minutes() < 10)
	{
		ss2 << "0" << m_ptimeDate.time_of_day().minutes() << ":";
	}
	else
	{
		ss2 << m_ptimeDate.time_of_day().minutes() << ":";
	}
	if(m_ptimeDate.time_of_day().seconds() < 10)
	{
		ss2 << "0" << m_ptimeDate.time_of_day().seconds() << "Z";
	}
	else
	{
		ss2 << m_ptimeDate.time_of_day().seconds() << "Z";
	}
	m_PLCTime = ss2.str();

	//Miliseconds are processed separately
	m_PLCms = (CsFromStartOfTheYear % 100) * 10;

	LOG(Log::DBG) << "Event date: [" << m_PLCTime << "] + [" << m_PLCms << "] ms";
}

void TSPPEvent::printInfo()
{
	LOG(Log::INF) << "m_year = [" << std::dec << static_cast<int>(m_year) << "]";
	LOG(Log::INF) << "m_dateLow = [" << std::dec << m_dateLow << "]";
	LOG(Log::INF) << "m_dateHigh = [" << std::dec << m_dateHigh << "]";
	LOG(Log::INF) << "m_ptimeDate = [" << m_ptimeDate << "]";
	LOG(Log::INF) << "m_PLCTime = [" << m_PLCTime << "]";
	LOG(Log::INF) << "m_PLCms = [" << std::dec << m_PLCms << "]";
	LOG(Log::INF) << "m_offset = [" << std::dec << m_offset << "]";
	LOG(Log::INF) << "m_newValue = [" << std::dec << m_newValue << "]";
	LOG(Log::INF) << "m_oldValue = [" << std::dec << m_oldValue << "]";
}

void TSPPEvent::pushInfo()
{
	LOG(Log::DBG) << "Starting push info (Event): address = [" << static_cast<int>(m_offset) <<"], new value = [" << m_newValue << "], old value = [" << m_oldValue << "]";
	std::stringstream ss;
	ss<<static_cast<int>(m_offset);
	std::string address = ss.str();

	/*Device::DReadableUInt16* uint16pointer =  PropertiesMap::searchUint16(address);
	if(uint16pointer != 0)
	{
		uint16pointer->update(static_cast<int>(m_newValue), m_PLCTime, m_PLCms);
		LOG(Log::DBG) << "Finishing push info";
		return;
	}*/
	Device::DReadableUInt32* uint32pointer =  PropertiesMap::searchEventUint32(address);
	if(uint32pointer != 0)
	{
		uint32pointer->update(static_cast<int>(m_newValue), m_PLCTime, m_PLCms);
		LOG(Log::DBG) << "Finishing push info";
		return;
	}
	LOG(Log::DBG) << "Address [" << address << "], that was provided by the PLC, is not listed.";
}



