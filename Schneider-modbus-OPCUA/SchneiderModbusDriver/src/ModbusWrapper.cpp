/*
 * ModbusWrapper.cpp
 *
 *  Created on: Mar 12, 2015
 *      Author: dabalo
 */
#include "ModbusWrapper.h"
#include <boost/asio/buffer.hpp>
#include <boost/asio/error.hpp>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include <iostream>
#include "ModbusWriteMultipleRegistersPacket.h"
#include <vector>
#include <string>
#include <boost/shared_array.hpp>
#include <boost/algorithm/string.hpp>
#include "LogIt.h"
#include "CommandQueue.h"
#include "CommandItem.h"
#include "DriverUtils.h"

using std::cin;
using std::string;
using std::exception;

const string g_sListenIp = "127.0.0.1";
const uint8_t g_sEmptyIP[4] = {0, 0, 0, 0};
const uint16_t g_sHeartBeatMsgId = 1;
const uint16_t g_sCommandQueueMsgId = 2;
const uint16_t g_sUint16ByteLength = 2;
const uint16_t g_sUint32ByteLength = 4;
const uint16_t g_sFloatByteLength = 4;
const uint16_t g_sHeartBeatPLCAddr = 70;
const uint8_t g_sHeartBeatMsgLengthBytes = 4;
const int g_sHeartBeatHalfCycleMs = 2000;
//This will be always 0, as it is in the modbus protocol, but it is not used by Schneider PLC, so it will be ignored
const uint8_t g_sModbusUnitIdentifier = 0;
//Function code of the Modbus operation that we will be using (WriteMultipleRegisters)
const uint8_t g_sModbusFunctionWriteMultipleRegister = 16;
//Size in bytes of a Modbus response (WriteMultipleRegisters)
const uint8_t g_sModbusResponseSize = 12;
//Size in bytes of a Modbus query header (WriteMultipleRegisters)
const uint8_t g_sModbusHeaderSize = 13;
//Time to wait in the command queue polling thread when there is nothing in the queue
const int g_sCommandPollingWaitMs = 50;

uint8_t g_sHostMachineIP[4];

ModbusWrapper::ModbusWrapper()
:m_shutdownThreads(false), m_readyToShutDown1(false), m_readyToShutDown2(false), m_readyToShutDown3(false),
 m_tsppConfig(DriverUtils::parseConfiguration()), m_reconnect(false)
{
	std::vector <string> fields;
	boost::split( fields, m_tsppConfig.getHostMachineExternalIp(), boost::is_any_of( "." ) );
	g_sHostMachineIP[0] = static_cast<uint8_t>(std::stoi(fields[2]));
	g_sHostMachineIP[1] = static_cast<uint8_t>(std::stoi(fields[3]));
	g_sHostMachineIP[2] = static_cast<uint8_t>(std::stoi(fields[0]));
	g_sHostMachineIP[3] = static_cast<uint8_t>(std::stoi(fields[1]));
}

void ModbusWrapper::writeAsyncData(const uint8_t data[], uint8_t totalLength, boost::shared_ptr< boost::asio::ip::tcp::socket > sock)
{
	LOG(Log::DBG) << "Send Query";
	boost::asio::write(*sock, boost::asio::buffer(data, totalLength));
	LOG(Log::DBG) << "Receiveing response";
	uint8_t responseBuffer[g_sModbusResponseSize];
	size_t size = boost::asio::read(*sock, boost::asio::buffer(responseBuffer));
	LOG(Log::DBG) << "Received " << size << "bytes";
}
void ModbusWrapper::writeMultipleRegister(uint16_t id, uint16_t startAddress, uint8_t byteLength, const uint8_t values[],  boost::shared_ptr< boost::asio::ip::tcp::socket > sock)
{
	if (byteLength % 2 > 0)
	{
		byteLength++;
	}
	ModbusWriteMultipleRegistersPacket queryPacket(id, (uint16_t)(0), (7 + byteLength), g_sModbusUnitIdentifier, g_sModbusFunctionWriteMultipleRegister, startAddress, (byteLength / 2), byteLength, values);
	boost::shared_array<uint8_t> queryBytes = queryPacket.generateQuery();
	writeAsyncData(queryBytes.get(), g_sModbusHeaderSize + byteLength, sock);
}

void ModbusWrapper::heartBeat(boost::shared_ptr< boost::asio::ip::tcp::socket > sock)
{	try // We include a try catch protection that just sets the flag readyToShutDown to false and throws the exception again, so it can be handled later
{
	while (!m_shutdownThreads && !m_reconnect)
	{
		LOG(Log::DBG) << "Write IP";
		writeMultipleRegister(g_sHeartBeatMsgId, g_sHeartBeatPLCAddr, g_sHeartBeatMsgLengthBytes, g_sHostMachineIP, sock);

		boost::this_thread::sleep(boost::posix_time::milliseconds( g_sHeartBeatHalfCycleMs ));

		LOG(Log::DBG) << "Clear IP";
		writeMultipleRegister(g_sHeartBeatMsgId, g_sHeartBeatPLCAddr, g_sHeartBeatMsgLengthBytes, g_sEmptyIP, sock);

		boost::this_thread::sleep(boost::posix_time::milliseconds( g_sHeartBeatHalfCycleMs ));
	}
	m_readyToShutDown1 = true;
}
catch(const boost::system::system_error & ex)
{
	LOG(Log::WRN) << "Aborted thread Heart Beat because of an error = [" << ex.what() << "]";
	m_readyToShutDown1 = true;
	//If we are not reconnecting yet, an exception is thrown so the reconection can start.
	if(!m_reconnect)
	{
		throw ex;
	}
}
}

void ModbusWrapper::pollModbusPackets(boost::shared_ptr< boost::asio::ip::tcp::socket > sock)
{	try // We include a try catch protection that just sets the flag readyToShutDown to false and throws the exception again, so it can be handled later
{
	while (!m_shutdownThreads && !m_reconnect)
	{
		while(sock->available() == 0)
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds( 50 ));
			if(m_shutdownThreads || m_reconnect)
			{
				m_readyToShutDown2 = true;
				return;
			}
		}
		//Read the header to understand how much data is there to get
		uint8_t header[g_sModbusHeaderSize];
		size_t messageSize = boost::asio::read(*sock, boost::asio::buffer(header, g_sModbusHeaderSize));
		LOG(Log::DBG) << "Read [" << messageSize << "] Bytes as header.";

		ModbusWriteMultipleRegistersPacket packet(header, messageSize);

		const int bodyBytes = packet.getByteCount();
		//Knowing the size of the body, we retrieve the rest of the packet
		//uint8_t* bodytemp = new uint8_t[bodyBytes]();
		boost::shared_array<uint8_t> body(new uint8_t[bodyBytes]);
		messageSize = boost::asio::read(*sock, boost::asio::buffer(body.get(), bodyBytes));
		LOG(Log::DBG) << "Read [" << messageSize << "] Bytes as body, intended to read [ " << bodyBytes << "].";
		
		LOG(Log::DBG) << "Writing the information in packet object";
		int ret = packet.setRegisters(body, messageSize);
		LOG(Log::DBG) << "Done writing the information";
		if(ret == -1)
		{
			LOG(Log::ERR) << "Corrupt packet detected. Skipping response";
			continue;
		}
		//delete[] body;//DONE: transform new and delete into boost shared array.
		
		//Having the proper packet in memory, we now build a response.
		boost::shared_array<uint8_t> response(packet.generateResponse());//SegmentationFault?
		LOG(Log::DBG) << "Sending a response";
		messageSize = boost::asio::write(*sock, boost::asio::buffer(response.get(), g_sModbusResponseSize));
		LOG(Log::DBG) << "Wrote [" << messageSize << "] Bytes as response.";
	}
	m_readyToShutDown2 = true;
}
catch(const boost::system::system_error & ex)
{
	LOG(Log::WRN) << "Aborted thread poll modbus packets because of an error = [" << ex.what() << "]";
	m_readyToShutDown2 = true;
	//If we are not reconnecting yet, an exception is thrown so the reconection can start.
	if(!m_reconnect)
	{
		throw ex;
	}
}
}

void ModbusWrapper::processCommandQueue(boost::shared_ptr< boost::asio::ip::tcp::socket > sock)
{	try // We include a try catch protection that just sets the flag readyToShutDown to false and throws the exception again, so it can be handled later
{
	while (!m_shutdownThreads && !m_reconnect)
	{
		if(CommandQueue::commandQueueSize()==0)
		{
			boost::this_thread::sleep(boost::posix_time::milliseconds( g_sCommandPollingWaitMs ));
		}
		else
		{
			size_t size = 0;
			do
			{//While there is commands we process them one at a time.
				//extract the command
				CommandItem command = CommandQueue::commandBlockingTake();
				//get the size of the queue after the operation
				size = CommandQueue::commandQueueSize();
				//process the command
				switch(command.m_type)
				{
				case (CommandItem::UINT16):
					{
						//Converting the usingned int into 2 bytes
						uint16_t value = static_cast<uint16_t>(command.m_value_i16);
						//Splitting the 2 bytes into a byte array
						uint8_t byteValue[g_sUint16ByteLength] = {DriverUtils::getByte1(value), DriverUtils::getByte2(value)};
						LOG(Log::DBG) << "Writing uint16 = [" << value << "] in address [" << command.m_address << "]";
						//Converting the command address (stored as a string into a char*, then into an int, and then into 2 bytes
						uint16_t numericAddress = static_cast<uint16_t>(atoi(command.m_address.c_str()));
						//We use all the information gathered to write into the PLC memory
						writeMultipleRegister(g_sCommandQueueMsgId, numericAddress, g_sUint16ByteLength, byteValue, sock);
					}
					break;
				case (CommandItem::UINT32):
					{
						//Converting the usingned int into 4 bytes
						uint32_t value = static_cast<uint32_t>(command.m_value_i32);
						//Splitting the 4 bytes into a byte array
						uint8_t byteValue[g_sUint32ByteLength] = {DriverUtils::getByte3(value), DriverUtils::getByte4(value), DriverUtils::getByte1(value), DriverUtils::getByte2(value)};
						LOG(Log::DBG) << "Writing uint32 = [" << value << "] in address [" << command.m_address.c_str() << "]";
						//Converting the command address (stored as a string into a char*, then into an int, and then into 2 bytes
						uint16_t numericAddress = static_cast<uint16_t>(atoi(command.m_address.c_str()));
						//We use all the information gathered to write into the PLC memory
						writeMultipleRegister(g_sCommandQueueMsgId, numericAddress, g_sUint32ByteLength, byteValue, sock);
					}
					break;
				case (CommandItem::FLOAT32):
					{
						float value = command.m_value_f;
						//Splitting the 4 bytes of the float into a byte array
						uint8_t byteValue[g_sFloatByteLength] = {static_cast<uint8_t>(0) , static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(0)};

						LOG(Log::DBG) << "Bytes : " << (int)(DriverUtils::getByte2(value)) << ";" << (int)(DriverUtils::getByte1(value)) << ";"
									<< (int)(DriverUtils::getByte4(value)) << ";" << (int)(DriverUtils::getByte3(value)) << " ; Word 0 = " <<
									DriverUtils::getWordFromBytes(DriverUtils::getByte2(value), DriverUtils::getByte1(value)) << "; Word1 = " << DriverUtils::getWordFromBytes(DriverUtils::getByte4(value), DriverUtils::getByte3(value));
						byteValue[0] = DriverUtils::getByte2(value);
						byteValue[1] = DriverUtils::getByte1(value);
						byteValue[2] = DriverUtils::getByte4(value);
						byteValue[3] = DriverUtils::getByte3(value);

						LOG(Log::DBG) << "Writing float32 = [" << value << "] in address [" << command.m_address.c_str() << "]";
						//Converting the command address (stored as a string into a char*, then into an int, and then into 2 bytes
						uint16_t numericAddress = static_cast<uint16_t>(atoi(command.m_address.c_str()));
						//We use all the information gathered to write into the PLC memory
						writeMultipleRegister(g_sCommandQueueMsgId, numericAddress, g_sFloatByteLength, byteValue, sock);
					}
					break;
				default:
					{
						LOG(Log::ERR) << "Unexpected type inside the command queue = [" << command.m_type << "]";
					}
					break;
				};
			}
			while(size != 0);
		}
	}
	m_readyToShutDown3 = true;
}
catch(const boost::system::system_error & ex)
{
	LOG(Log::WRN) << "Aborted thread process command queue because of an error = [" << ex.what() << "]";
	m_readyToShutDown3 = true;
	//If we are not reconnecting yet, an exception is thrown so the reconection can start.
	if(!m_reconnect)
	{
		throw ex;
	}
}
}

void ModbusWrapper::workerThreadFunction( boost::shared_ptr< boost::asio::io_service > io_service )
{
	LOG(Log::INF) << "[" << boost::this_thread::get_id() << "] Thread Start";

	while( true )
	{
		try
		{
			boost::system::error_code ec;
			io_service->run( ec );
			if( ec )
			{
				LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Error: " << ec;
			}
			break;
		}
		catch(const boost::system::system_error& ex )
		{
			LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Known exception: [" << ex.what() << "] code: [" << ex.code() << "]. Trying to reconnect...";
			m_reconnect = true;
		}
		catch(exception & ex )
		{
			LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Unexpected exception: [" << ex.what() << "]";
		}
		catch(...)
		{
			LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Non standard unexpected exception";
		}
	}
	LOG(Log::INF) << "[" << boost::this_thread::get_id() << "] Thread Finish";
}

void ModbusWrapper::onAccept( const boost::system::error_code & ec, boost::shared_ptr< boost::asio::ip::tcp::socket > sock )
{
	if( ec )
	{
		LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Error: " << ec;
	}
	else
	{
		LOG(Log::INF) << "[" << boost::this_thread::get_id() << "] Accepted!";
		//m_readyToShutDown2 = false;
		pollModbusPackets(sock);
	}
}

void ModbusWrapper::onConnect( const boost::system::error_code & ec, boost::shared_ptr< boost::asio::ip::tcp::socket > sock )
{
	if( ec )
	{
		LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Error: " << ec;
	}
	else
	{
		LOG(Log::INF) << "[" << boost::this_thread::get_id() << "] Connected!";
		//m_readyToShutDown3 = false;
		boost::thread thrd(&ModbusWrapper::processCommandQueue, this, sock);
		//m_readyToShutDown1 = false;
		heartBeat(sock);
		thrd.join();
	}
}

void ModbusWrapper::userInputThread()
{
	LOG(Log::INF) << "[" << boost::this_thread::get_id() << "] Press [return] to exit.";
	cin.get();
	m_shutdownThreads = true;
}
void ModbusWrapper::connection()
{
	boost::shared_ptr< boost::asio::io_service > ioService(new boost::asio::io_service);
	boost::shared_ptr< boost::asio::io_service::work > work(new boost::asio::io_service::work( *ioService ));

	boost::thread_group workerThreads;
	for( int x = 0; x < m_workerThreadsNum; ++x )
	{
		workerThreads.create_thread( boost::bind( &ModbusWrapper::workerThreadFunction, this, ioService ) );
	}

	boost::shared_ptr< boost::asio::ip::tcp::acceptor > acceptor(new boost::asio::ip::tcp::acceptor( *ioService, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), m_tsppConfig.getHostMachinePort()) ));
	boost::shared_ptr< boost::asio::ip::tcp::socket > clientSocket(	new boost::asio::ip::tcp::socket( *ioService ));
	boost::shared_ptr< boost::asio::ip::tcp::socket > serverSocket(	new boost::asio::ip::tcp::socket( *ioService ));

	try
	{
		//Connect and Start Client socket, that will send queries to the PLC
		boost::asio::ip::tcp::resolver resolver( *ioService );
		boost::asio::ip::tcp::resolver::query query(m_tsppConfig.getPlcIp(), boost::lexical_cast< string >( m_tsppConfig.getPlcPort() ));
		boost::asio::ip::tcp::endpoint endpoint = *resolver.resolve( query );
		LOG(Log::INF) << "Connecting to: " << endpoint;
		clientSocket->async_connect(endpoint, boost::bind(&ModbusWrapper::onConnect, this, _1, clientSocket));

		//Connect and Start Server socket, that will receive queries from the PLC
		acceptor->listen( boost::asio::socket_base::max_connections );
		LOG(Log::INF) << "Accepting connection to port : " << m_tsppConfig.getHostMachinePort();
		acceptor->async_accept( *serverSocket, boost::bind( &ModbusWrapper::onAccept, this, _1, serverSocket ) );
	}
	catch( exception & ex )
	{
		LOG(Log::ERR) << "[" << boost::this_thread::get_id() << "] Exception: " << ex.what();
	}


	bool firstFlag = true;
	while(!(m_readyToShutDown1 && m_readyToShutDown2 && m_readyToShutDown3))
	{
		boost::this_thread::sleep(boost::posix_time::milliseconds( 50 ));
		//LOG(Log::INF) << "1: " << m_readyToShutDown1 << " 2: " << m_readyToShutDown2 << " 3: " << m_readyToShutDown3;
		//If we need to reconnect we shut down all the threads and connections normally at first
		if(m_reconnect && firstFlag)
		{
			LOG(Log::INF) << "Reconnection: Shutting down threads and sockets";
			firstFlag = false;
			m_shutdownThreads = true;
		}
	}
	LOG(Log::INF) << "Joining threads and closing sockets";
	boost::system::error_code ec;
	acceptor->close( ec );
	clientSocket->shutdown( boost::asio::ip::tcp::socket::shutdown_both, ec );
	clientSocket->close( ec );
	serverSocket->shutdown( boost::asio::ip::tcp::socket::shutdown_both, ec );
	serverSocket->close( ec );
	ioService->stop();
	workerThreads.join_all();
	if(m_reconnect)
	{
		LOG(Log::INF) << "Reconnection: Starting a new set of threads and sockets";
		m_reconnect = false;
		m_readyToShutDown1 = false;
		m_readyToShutDown2 = false;
		m_readyToShutDown3 = false;
		m_shutdownThreads = false;
		connection();
	}
}
void ModbusWrapper::connectAndStartClientAndServer()
{
	boost::thread userInputThrd(&ModbusWrapper::userInputThread, this);
	connection();
	userInputThrd.join();
}


