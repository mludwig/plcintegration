#ifndef MODBUSEVENTTSPPPACKET_H_
#define MODBUSEVENTTSPPPACKET_H_
#include <string>
#include <vector>
#include <stdint.h>
#include "TSPPEvent.h"


/*
 * Class EventTSPPPacket is the object representation of an Event packet.
 * A event packet can contain from 1 to 19 events, stored in memory as TSPPEvent objects
 */
class ModbusEventTSPPPacket {
public:
	/*
	 * Default constructor of the class. Takes as input a set of words that contain the packet, which will be decoded into data.
	 */
	ModbusEventTSPPPacket(std::vector<uint16_t>&);

	const std::vector<TSPPEvent>& getEvents() const {
		return m_events;
	}
	uint16_t getHeader() const {
		return m_header;
	}
	/*
	 * Prints the information of the events using LogIt library
	 */
	void printInfo();
	/*
	 * Pushes the events information to the OPC UA server using PropertiesMap
	 */
	void pushInfo();

	uint8_t getPacketId() const {
		return m_packetId;
	}

	uint8_t getYear() const {
		return m_year;
	}

private:
	//Header of the TSPP Packet. Contains the packet identifier and the year.
	uint16_t m_header;
	//Packet identifier
	uint8_t m_packetId;
	//Year
	uint8_t m_year;
	//Array of TSPPEvents. A packet can have from 1 to 19 events
	std::vector<TSPPEvent> m_events;
};

#endif /* MODBUSEVENTTSPPPACKET_H_ */
