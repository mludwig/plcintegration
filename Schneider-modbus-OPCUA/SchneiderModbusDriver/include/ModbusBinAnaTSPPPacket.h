#ifndef MODBUSBINANATSPPPACKET_H_
#define MODBUSBINANATSPPPACKET_H_
#include <string>
#include <vector>
#include "boost/date_time/posix_time/posix_time.hpp"


/*
 * Class ModbusBinAnaTSPPPacket is the object representation of an Analog or Binary TSPP packet.
 */
class ModbusBinAnaTSPPPacket {
public:
	/*
	 * Default constructor of the class. Takes as input a set of words that contain the packet, which will be decoded into data.
	 */
	ModbusBinAnaTSPPPacket(std::vector<uint16_t>&);

	const std::vector<uint16_t>& getData() const {
		return m_data;
	}

	uint16_t getDateHigh() const {
		return m_dateHigh;
	}

	uint16_t getDateLow() const {
		return m_dateLow;
	}

	uint16_t getHeader() const {
		return m_header;
	}

	uint16_t getOffset() const {
		return m_offset;
	}

	const std::string& getPlcTime() const {
		return m_PLCTime;
	}

	uint8_t getYear() const {
		return m_year;
	}

	int getPlCms() const {
		return m_PLCms;
	}
	/*
	 * Prints the information of the frame using LogIt library
	 */
	void printInfo();
	/*
	 * Pushes the frame information to the OPC UA server using PropertiesMap
	 */
	void pushInfo();
private:
	//Header of the TSPP Packet. Contains the packet identifier and the year.
	const uint16_t m_header;
	//Packet identifier
	const uint8_t m_packetId;
	//Year
	const uint8_t m_year;
	//Lower word of the data-time
	const uint16_t m_dateLow;
	//Higher word of the data-time
	const uint16_t m_dateHigh;
	//Date-time in posix time format
	boost::posix_time::ptime m_ptimeDate;
	//Date-time in string format, following ISO8691 (Does not include milliseconds)
	std::string m_PLCTime;
	//Milliseconds of the time
	int m_PLCms;
	//Offset of the frame
	const uint16_t m_offset;
	//Set of data to be updated
	std::vector<uint16_t> m_data;
	//Gets the plc time as a posix time object from the year (1 byte), and the date (4 bytes separated into 2 variables)
	static boost::posix_time::ptime getPlcTime(const uint8_t& year, const uint16_t& dateLow, const uint16_t& dateHigh);
	//Gets the plc milliseconds from the date (4 bytes separated into 2 variables)
	static const int getMs(const uint16_t& dateLow, const uint16_t& dateHigh);
	//Gets the date as a string in the format ISO8691 from a posix time object (WARNING: does not include milliseconds)
	static const std::string getISO8691(const boost::posix_time::ptime posixTime);
};

#endif /* MODBUSBINANATSPPPACKET_H_ */
