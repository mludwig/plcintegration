/*
 * PropertiesMap.h
 *
 *  Created on: Sep 17, 2014
 *      Author: dabalo
 */

#ifndef PROPERTIESMAP_H_
#define PROPERTIESMAP_H_
#include "DReadableUInt32.h"
#include "DReadableUInt16.h"
#include "DReadableFloat32.h"

#include <map>
#include <boost/algorithm/string.hpp>


/*
 * Stores 3 static maps of DReadableUInt16, DReadableUInt32 and DReadableFloat32 so they can be found by other classes using the address as the identifier.
 * This maps are private for protection, but there are available public static wrapper functions, so they can be accessed.
 * It also contains a SharedQueue containing CommandItems, also private and with public static wrapper functions available.
 */
class PropertiesMap
{

public:
	//Inserts a DReadableUInt16 inside the corresponding map
	static void pushUint16(std::string, Device::DReadableUInt16*);
	//Inserts a DReadableUInt32 inside the corresponding map
	static void pushUint32(std::string, Device::DReadableUInt32*);
	//Inserts a DReadableUInt32 inside the event map; Only applicable for schneider PLCs
	static void pushEventUint32(std::string, Device::DReadableUInt32*);
	//Inserts a DReadableFloat32 inside the corresponding map
	static void pushFloat32(std::string, Device::DReadableFloat32*);
	//Searches for a DReadableUInt16 inside the corresponding map. If it is not found, 0 is returned.
	static Device::DReadableUInt16* searchUint16(std::string);
	//Searches for a DReadableUInt32 inside the corresponding map. If it is not found, 0 is returned.
	static Device::DReadableUInt32* searchUint32(std::string);
	//Searches for a DReadableUInt32 inside the event map. If it is not found, 0 is returned.
	static Device::DReadableUInt32* searchEventUint32(std::string);
	//Searches for a DReadableFloat32 inside the corresponding map. If it is not found, 0 is returned.
	static Device::DReadableFloat32* searchFloat32(std::string);
private:
	//DReadableUInt16 map, using the address as the identifier
	static std::map<std::string, Device::DReadableUInt32*> g_uInt32MyHashMap;
	//DReadableUInt32 event map, using the address as the identifier
	static std::map<std::string, Device::DReadableUInt32*> g_uInt32EventHashMap;
	//DReadableUInt32 map, using the address as the identifier
	static std::map<std::string, Device::DReadableUInt16*> g_uInt16MyHashMap;
	//DReadableFloat32 map, using the address as the identifier
	static std::map<std::string, Device::DReadableFloat32*> g_float32MyHashMap;
	//Private constructor (all the methods are static, it would be pointless to create an object of this class)
	PropertiesMap();
};

#endif /* PROPERTIESMAP_H_ */
