/*
 * ModbusWrapper.h
 *
 *  Created on: Mar 12, 2015
 *      Author: dabalo
 */

#ifndef MODBUSWRAPPER_H_
#define MODBUSWRAPPER_H_
#include <boost/asio.hpp>
#include <string>
#include <stdint.h>
#include "TSPPConfiguration.h"
/*
 * The ModbusWrapper class handles the communication with a Schneider PLC. As you create the object, it will start several threads to handle the
 * communication with the PLC in several ways.
 * One of the connections that is opened, has the PLC as a server, and this software as a client. This connection will be used to write in the
 * memory of the PLC, as for doing the HeartBeat, since we send the Write querys to the PLC.
 * The other connection that is opened has the PLC as a client and this software as a server. This connection will be used by the PLC in order to
 * push data to us (the PLC does the write querys).
 */
class ModbusWrapper {
public:
	/*
	 * Constructor of the class.
	 * Inside the constructor the ip and the port will be assigned, and a call to the method connectAndStartClientAndServer will be done,
	 * blocking the thread until the user presses return but starting several other threads to handle the connections.
	 */
	ModbusWrapper();
	/*
	 * As described above, this method blocks the thread until the user presses return, but before doing that it will start several different threads
	 * that will handle the 2 connections that will be established with the PLC.
	 */
	void connectAndStartClientAndServer();
private:
	void connection();
	const TSPPConfiguration m_tsppConfig;
	//Flag that tells the communication threads to shut down
	bool m_shutdownThreads;
	//Flag that tells the main thread that the communication thread 1 has finished
	bool m_readyToShutDown1;
	//Flag that tells the main thread that the communication thread 2 has finished
	bool m_readyToShutDown2;
	//Flag that tells the main thread that the communication thread 2 has finished
	bool m_readyToShutDown3;
	//Flag that tells the main thread to reconnect the communication and restart the threads
	bool m_reconnect;
	//Number of working threads that will handle the communication. At the moment only 3 will be needed.
	static const int m_workerThreadsNum = 2;

	/*
	 * Thread that waits for the user to press return. When this happens, it will set the flag m_shutdownThreads to true
	 */
	void userInputThread();
	/*
	 * Function that encapsulates a message exchange with the PLC. It sends a query and retrieves the reply.
	 */
	void writeAsyncData(const uint8_t data[], uint8_t totalLength, boost::shared_ptr< boost::asio::ip::tcp::socket > sock);
	/*
	 * Function that encapsulates a writeMultipleRegister Modbus operation. It generates the query and then calls the function writeAsyncData
	 */
	void writeMultipleRegister(uint16_t id, uint16_t startAddress, uint8_t byteLength, const uint8_t values[],  boost::shared_ptr< boost::asio::ip::tcp::socket > sock);
	/*
	 * HeartBeat operation, that will write in address 70 of the PLC the IP and then 0 once each 4 seconds. This is the keepalive operation,
	 * to let the PLC know that we are still there. The loop will go on until the flag m_shutdownThreads is set to true, and after finishing
	 * it will set to true the flag m_readyToShutDown1.
	 * This function will block the thread, so it is very important to call it from it's own independent thread.
	 */
	void heartBeat(boost::shared_ptr< boost::asio::ip::tcp::socket > sock);
	/*
	 * Function that polls the socket in order to see if the PLC is sending any write query. Taking into account the configuration of the PLC,
	 * this should be happening with any change in the data, if we are doing the Heart Beat. The loop will go on until the flag m_shutdownThreads
	 * is set to true, and after finishing it will set to true the flag m_readyToShutDown2.
	 * This function will block the thread, so it is very important to call it from it's own independent thread.
	 */
	void pollModbusPackets(boost::shared_ptr< boost::asio::ip::tcp::socket > sock);
	/*
	 * Function that polls the command queue, that will be filled by the OPC UA thread. If there is some command in the queue it will be extracted
	 * and executed by this function, making the changed in the namespace reach the PLC.
	 */
	void processCommandQueue(boost::shared_ptr< boost::asio::ip::tcp::socket > sock);
	/*
	 * Asynchronous function that will start a worker thread, following the logic of the boost::asio library. For this program each worker thread will
	 * handle a connection.
	 */
	void workerThreadFunction( boost::shared_ptr< boost::asio::io_service > io_service );
	/*
	 * Asynchronous function that will be called when the PLC connects to our server socket, and hence it can start sending us querys.
	 * It will call the function pollModbusPackets, setting this thread to handle the server socket connection.
	 */
	void onAccept( const boost::system::error_code & ec, boost::shared_ptr< boost::asio::ip::tcp::socket > sock );
	/*
	 * Asynchronous function that will be called when our client socket connects to the PLC, and hence it can start sending querys to the PLC.
	 * It will call the function heartBeat, setting this thread to handle the client socket connection.
	 */
	void onConnect( const boost::system::error_code & ec, boost::shared_ptr< boost::asio::ip::tcp::socket > sock );

};

#endif /* MODBUSWRAPPER_H_ */
