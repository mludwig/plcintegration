/*
 * ModbusWriteMultipleRegistersPacket.h
 *
 *  Created on: Mar 11, 2015
 *      Author: dabalo
 */

#ifndef MODBUSWRITEMULTIPLEREGISTERSPACKET_H_
#define MODBUSWRITEMULTIPLEREGISTERSPACKET_H_

#include <vector>
#include <stdint.h>
#include <boost/shared_array.hpp>
/*
 * The class ModbusWriteMultipleRegistersPacket represents a packet in the modbus protocol for doing the operation WriteMultipleRegisters.
 *
 * The packet can be built manually, in case we want to send the query of this operation to the PLC (and hence write in its memory). In this case we need
 * to use the first constructor, in which we specify the different parameters of the header, and the body inside the attribute buffer. After
 * creating the object using this constructor, the method generateQuery should be called in order to translate the information to the form of a byte array,
 * so it is possible to send the packet to the PLC via a TCP socket.
 *
 * If the PLC is the one sending us the query, we should use the second constructor. The buffer taken as parameter in the second constructor should contain
 * the header and only the header, as in the TCP communication you will get the header first since you don't know the length of the body. Having built
 * the object with the header (you should be careful since the packet has no body and hence it is not complete yet, so you should avoid generating the
 * response for the moment) you can check the length in bytes of the body  (getByteCount()) and use this information to receive the body in the socket.
 * After this, method setRegisters should be called, filling the body inside the package object and making it complete this way.
 * Since the PLC is the one that is sending the query, after building the package a response should be sent. This can be generated using the method
 * generateResponse().
 */
class ModbusWriteMultipleRegistersPacket
{
public:
	/*
	 * First Constructor of the class. This constructor is used to create manually the packet, and as explained in the class description, this should be used
	 * when you are sending querys to the PLC. After creating the object, generateQuery() should be called in order to transform the packet into an array
	 * of bytes, so it can be sent through a socket.
	 */
	ModbusWriteMultipleRegistersPacket(uint16_t transactionIdentifier, uint16_t protocolIdentifier, uint16_t length, uint8_t unitIdentifier, uint8_t functionCode, uint16_t referenceNumber, uint16_t wordCount, uint8_t byteCount, const uint8_t buffer[]);
	/*
	 * Second constructor of the class. As explained in the class description, this constructor takes an array of bytes that contains the header of a query
	 * (That you have previously received from the PLC) and builds the packet (without any information in the body) with this information. After doing this
	 * you can proceed to receive the body of the query, since you know now the length of the body. Once you have the body the method setRegisters should be
	 * called in order to fill this information in the package object.
	 * Since in this situation the PLC sent you the query, you will be interested in calling the method generateResponse(), in order to generate a response
	 * that can be sent back to the PLC as an array of bytes.
	 */
	ModbusWriteMultipleRegistersPacket(const uint8_t header[], const size_t headerLength);
	/*
	 * Method that takes as an attribute the body of a packet in the form of an array of bytes, and fills this information into the packet object.
	 * returns 0 for OK, -1 for corrupt packet
	 */
	int setRegisters(const boost::shared_array<uint8_t> body, const size_t bodyLength);
	/*
	 * Uses the header information to build a response as a byte array that can be sent to the PLC using a TCP socket.
	 * The body of the packet is not used to build the response, so it is not necessary that this information exists within the object.
	 */
	boost::shared_array<uint8_t> generateResponse();
	/*
	 * Uses the packet information to build a query as a byte array that can be sent to the PLC using a TCP socket.
	 * Uses both the header and the body information, so this information is necessary within the object in order to generate a valid query.
	 */
	boost::shared_array<uint8_t> generateQuery();
	/*
	 * Gets the number  of bytes of the body.
	 */
	uint8_t getByteCount() const {
		return m_byteCount;
	}
	/*
	 * Gets the code that represents the modbus function (In this case the function is WriteMultipleRegisters, whose code should be 16(dec)).
	 */
	uint8_t getFunctionCode() const {
		return m_functionCode;
	}
	/*
	 * Gets the length of the packet.
	 */
	uint16_t getLength() const {
		return m_lenght;
	}
	/*
	 * Gets the protocol identifier.
	 * This can be 0, as it is not used for this project.
	 */
	uint16_t getProtocolIdentifier() const {
		return m_protocolIdentifier;
	}
	/*
	 * Gets the reference number.
	 * This is usually the address in the memory of the PLC where the data being sent starts.
	 */
	uint16_t getReferenceNumber() const {
		return m_referenceNumber;
	}
	/*
	 * Gets the vector of words that contains the registers.
	 * Each one of this registers represents a memory address of the PLC, starting in the one referenced in referenceNumber.
	 */
	const std::vector<uint16_t>& getRegister() const {
		return m_register;
	}
	/*
	 * Gets the transaction identifier.
	 * This acts as the identifier of the packet.
	 */
	uint16_t getTransactionIdentifier() const {
		return m_transactionIdentifier;
	}
	/*
	 * Gets the unit identifier.
	 * This can be 0, as it is not used for this project.
	 */
	uint8_t getUnitIdentifier() const {
		return m_unitIdentifier;
	}
	/*
	 * Gets the number of words in the body.
	 */
	uint16_t getWordCount() const {
		return m_wordCount;
	}

private:
	int decodeRegisterIntoTSPPPacket();
	uint16_t m_transactionIdentifier;
	uint16_t m_protocolIdentifier;
	uint16_t m_lenght;
	uint8_t m_unitIdentifier;
	uint8_t m_functionCode;
	uint16_t m_referenceNumber;
	uint16_t m_wordCount;
	uint8_t m_byteCount;
	std::vector<uint16_t> m_register;
	//ModbusTSPPPacket m_TSPPPacket;
};



#endif /* MODBUSWRITEMULTIPLEREGISTERSPACKET_H_ */
