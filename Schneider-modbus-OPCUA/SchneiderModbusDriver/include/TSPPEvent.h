/*
 * TSPPEvent.h
 *
 *  Created on: Mar 24, 2015
 *      Author: dabalo
 */

#ifndef TSPPEVENT_H_
#define TSPPEVENT_H_

#include <stdint.h>
#include <string>
#include <vector>
#include "boost/date_time/posix_time/posix_time.hpp"
/*
 * Class TSPP Event is the object representation of an Event.
 * A TSPP Event frame will contain from 1 to 19 Events, and it will be stored as instances of this class
 */
class TSPPEvent{
public:
	/*
	 * Main constructor, taking as parameters the year and the set of words that will be decoded
	 */
	TSPPEvent(uint8_t, std::vector<uint16_t>&);
	/*
	 * Empty constructor, for memory allocation purposes. setInfo should be called after in order to fill the information
	 */
	TSPPEvent(){}
	/*
	 * Method to fill the information, needs to be called if the object was created using the empty constructor.
	 * Params: year and set of words to be decoded
	 */
	void setInfo(uint8_t, std::vector<uint16_t>&);
	/*
	 * Prints the information of the event using LogIt library
	 */
	void printInfo();
	/*
	 * Pushes the event information to the OPC UA server using PropertiesMap
	 */
	void pushInfo();

	uint16_t getDateHigh() const {
		return m_dateHigh;
	}

	uint16_t getDateLow() const {
		return m_dateLow;
	}

	uint16_t getNewValue() const {
		return m_newValue;
	}

	uint16_t getOffset() const {
		return m_offset;
	}

	uint16_t getOldValue() const {
		return m_oldValue;
	}

	void setOldValue(uint16_t oldValue) {
		m_oldValue = oldValue;
	}

	int getPlCms() const {
		return m_PLCms;
	}

	const std::string& getPlcTime() const {
		return m_PLCTime;
	}

	const boost::posix_time::ptime& getPtimeDate() const {
		return m_ptimeDate;
	}

	uint8_t getYear() const {
		return m_year;
	}

private:
	//Year of the date
	uint8_t m_year;
	//Lower word of the data-time
	uint16_t m_dateLow;
	//Higher word of the data-time
	uint16_t m_dateHigh;
	//Date-time in posix time format
	boost::posix_time::ptime m_ptimeDate;
	//Date-time in string format, following ISO8691 (Does not include milliseconds)
	std::string m_PLCTime;
	//Milliseconds of the time
	int m_PLCms;
	//Offset of the event
	uint16_t m_offset;
	//New value of the event
	uint16_t m_newValue;
	//Old value of the event
	uint16_t m_oldValue;

	//Process m_year m_dateLow and m_dateHigh into m_ptimeDate, m_PLCTime and m_PLCms
	void processDate();
};



#endif /* TSPPEVENT_H_ */
