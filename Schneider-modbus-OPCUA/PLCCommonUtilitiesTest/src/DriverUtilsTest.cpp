/*
 * DriverUtilsTest.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: dabalo
 */

#include "DriverUtilsTest.h"
#include <stdint.h>
//#include <list>
#include <LogIt.h>
#include <LogLevels.h>
/*	#include <boost/thread.hpp>
#include <boost/foreach.hpp>*/

TEST(DriverUtilsTest, getByteFrom16bits)
{
	uint16_t item = static_cast<uint16_t>(1);

	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte2(item));
	
	item = static_cast<uint16_t>(256);

	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte2(item));
	
	item = static_cast<uint16_t>(257);

	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte2(item));
	
	item = static_cast<uint16_t>(300);

	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(44), DriverUtils::getByte2(item));
	
	item = static_cast<uint16_t>(5555);

	EXPECT_EQ(static_cast<uint8_t>(21), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(179), DriverUtils::getByte2(item));
	
	item = static_cast<uint16_t>(65535);	
	
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte2(item));
}

TEST(DriverUtilsTest, getByteFrom32bits)
{
	uint32_t item = static_cast<uint32_t>(1);
		
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(256);	
	
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(257);	
	
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(300);	
	
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(44), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(5555);	
	
	EXPECT_EQ(static_cast<uint8_t>(21), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(179), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(65536);	
	
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(65535);	
	
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(16777216);	
	
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(1), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte4(item));
	
	item = static_cast<uint32_t>(16777215);	
	
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte1(item));
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte2(item));
	EXPECT_EQ(static_cast<uint8_t>(0), DriverUtils::getByte3(item));
	EXPECT_EQ(static_cast<uint8_t>(255), DriverUtils::getByte4(item));
	
}
union floatUnion {
    float number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} FloatUnionTest;
TEST(DriverUtilsTest, getByteFromFloat)
{	
	FloatUnionTest.number = 42.42;
	EXPECT_EQ(FloatUnionTest.byteContents[0], DriverUtils::getByte1(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[1], DriverUtils::getByte2(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[2], DriverUtils::getByte3(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[3], DriverUtils::getByte4(FloatUnionTest.number));
	
	FloatUnionTest.number = 34142.54342;
	EXPECT_EQ(FloatUnionTest.byteContents[0], DriverUtils::getByte1(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[1], DriverUtils::getByte2(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[2], DriverUtils::getByte3(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[3], DriverUtils::getByte4(FloatUnionTest.number));
	
	FloatUnionTest.number = -42.42;
	EXPECT_EQ(FloatUnionTest.byteContents[0], DriverUtils::getByte1(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[1], DriverUtils::getByte2(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[2], DriverUtils::getByte3(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[3], DriverUtils::getByte4(FloatUnionTest.number));
	
	FloatUnionTest.number = -34990042.434321435632;
	EXPECT_EQ(FloatUnionTest.byteContents[0], DriverUtils::getByte1(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[1], DriverUtils::getByte2(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[2], DriverUtils::getByte3(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[3], DriverUtils::getByte4(FloatUnionTest.number));
	
	FloatUnionTest.number = 42.424224424242244242424224;
	EXPECT_EQ(FloatUnionTest.byteContents[0], DriverUtils::getByte1(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[1], DriverUtils::getByte2(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[2], DriverUtils::getByte3(FloatUnionTest.number));
	EXPECT_EQ(FloatUnionTest.byteContents[3], DriverUtils::getByte4(FloatUnionTest.number));
}

TEST(DriverUtilsTest, getWordFromBytes)
{
	EXPECT_EQ(static_cast<uint16_t>(1), DriverUtils::getWordFromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(1)));	
	EXPECT_EQ(static_cast<uint16_t>(256), DriverUtils::getWordFromBytes(static_cast<uint8_t>(1), static_cast<uint8_t>(0)));		
	EXPECT_EQ(static_cast<uint16_t>(257), DriverUtils::getWordFromBytes(static_cast<uint8_t>(1), static_cast<uint8_t>(1)));		
	EXPECT_EQ(static_cast<uint16_t>(300), DriverUtils::getWordFromBytes(static_cast<uint8_t>(1), static_cast<uint8_t>(44)));		
	EXPECT_EQ(static_cast<uint16_t>(5555), DriverUtils::getWordFromBytes(static_cast<uint8_t>(21), static_cast<uint8_t>(179)));		
	EXPECT_EQ(static_cast<uint16_t>(65535), DriverUtils::getWordFromBytes(static_cast<uint8_t>(255), static_cast<uint8_t>(255)));	
}

TEST(DriverUtilsTest, getUint32FromBytes)
{
	EXPECT_EQ(static_cast<uint32_t>(1), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(1)));	
	EXPECT_EQ(static_cast<uint32_t>(256), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(1), static_cast<uint8_t>(0)));	
	EXPECT_EQ(static_cast<uint32_t>(257), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(1), static_cast<uint8_t>(1)));	
	EXPECT_EQ(static_cast<uint32_t>(300), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(1), static_cast<uint8_t>(44)));	
	EXPECT_EQ(static_cast<uint32_t>(5555), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(21), static_cast<uint8_t>(179)));	
	EXPECT_EQ(static_cast<uint32_t>(65536), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(1), static_cast<uint8_t>(0), static_cast<uint8_t>(0)));	
	EXPECT_EQ(static_cast<uint32_t>(65535), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(255), static_cast<uint8_t>(255)));	
	EXPECT_EQ(static_cast<uint32_t>(16777216), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(1), static_cast<uint8_t>(0), static_cast<uint8_t>(0), static_cast<uint8_t>(0)));	
	EXPECT_EQ(static_cast<uint32_t>(16777215), DriverUtils::getUint32FromBytes(static_cast<uint8_t>(0), static_cast<uint8_t>(255), static_cast<uint8_t>(255), static_cast<uint8_t>(255)));		
}

TEST(DriverUtilsTest, getLongFromWords)
{
	EXPECT_EQ(static_cast<long>(1), DriverUtils::getLongFromWords(static_cast<uint16_t>(0), static_cast<uint16_t>(1)));
	EXPECT_EQ(static_cast<long>(65535), DriverUtils::getLongFromWords(static_cast<uint16_t>(0), static_cast<uint16_t>(65535)));
	EXPECT_EQ(static_cast<long>(65536), DriverUtils::getLongFromWords(static_cast<uint16_t>(1), static_cast<uint16_t>(0)));
	EXPECT_EQ(static_cast<long>(16777215), DriverUtils::getLongFromWords(static_cast<uint16_t>(255), static_cast<uint16_t>(65535)));
	EXPECT_EQ(static_cast<long>(16777216), DriverUtils::getLongFromWords(static_cast<uint16_t>(256), static_cast<uint16_t>(0)));
}

TEST(DriverUtilsTest, getFloatFromWords)
{
	EXPECT_EQ(static_cast<float>(9.18355e-041), DriverUtils::getFloatFromWords(static_cast<uint16_t>(0), static_cast<uint16_t>(1)));
	EXPECT_EQ(static_cast<float>(1.4013e-045), DriverUtils::getFloatFromWords(static_cast<uint16_t>(1), static_cast<uint16_t>(0)));
	EXPECT_EQ(static_cast<float>(3.58732e-043), DriverUtils::getFloatFromWords(static_cast<uint16_t>(256), static_cast<uint16_t>(0)));
}

TEST(DriverUtilsTest, parseConfiguration)
{
	const TSPPConfiguration myConfig = DriverUtils::parseConfiguration();
	EXPECT_FALSE(myConfig.getHostMachineExternalIp() == "empty");
	EXPECT_FALSE(myConfig.getHostMachinePort() == -1);
	EXPECT_FALSE(myConfig.getPlcIp() == "empty");
	EXPECT_FALSE(myConfig.getPlcPort() == -1);
	EXPECT_FALSE(myConfig.getLogLevel() == "empty");
}

DriverUtilsTest::DriverUtilsTest()
{
	Log::initializeLogging(Log::INF);
}

DriverUtilsTest::~DriverUtilsTest()
{}
