/*
 * CommandQueueTest.h
 *
 *  Created on: Oct 8, 2015
 *      Author: dabalo
 */

#ifndef COMMANDQUEUETEST_H_
#define COMMANDQUEUETEST_H_

#include "gtest/gtest.h"

#include "CommandQueue.h"

class CommandQueueTest;

class CommandQueueTest  : public ::testing::Test
{
public:

	CommandQueueTest();
	virtual ~CommandQueueTest();
};

#endif /* COMMANDQUEUETEST_H_ */