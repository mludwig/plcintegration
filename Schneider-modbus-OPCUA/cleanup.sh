#!/bin/bash
rm -rf ./CMakeFiles
find ./ -name "*.generated*" -exec rm -rf {} \;
find ./ -name "*.orig" -exec rm -rf {} \;
find ./ -name "cmake_*.cmake" -exec rm -rf {} \;
find ./ -name "Makefile" -exec rm -rf {} \;
find ./ -name "CMakeCache.txt" -exec rm -rf {} \;
find ./ -name "OpcUaServer.*.ua" -exec rm -rvf {} \;
find ./ -name "CommonUtilitiesTest.bin" -exec rm -rvf {} \;
find ./ -name "PLCCommonUtilitiesTest.bin" -exec rm -rvf {} \;
find ./ -name "CMakeFiles" -exec rm -rvf {} \;
find ./ -name "*.o" -exec rm -rf {} \;


