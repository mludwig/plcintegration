#ifndef __main__H__
#define __main__H__
#include <string>
/*
 * Class containing the start routine of the server
 */
class Main
{
public:
	static int OpcServerMain(const std::string szAppPath, const char* configFileName, bool onlyCreateCertificate);
	static int printServerMsg(const std::string);
};
#endif // include guard