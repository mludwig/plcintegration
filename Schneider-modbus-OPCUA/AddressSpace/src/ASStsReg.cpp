
/***********************************
This file was initially generated by Generic OPC UA Server framework (readme: Documentation/GenericServer.html)
Generated from:  designToClassBody.xslt using project's design file.
(C) CERN 2014
***********************************/



#include <iostream>




#include "ASStsReg.h"


#include "DStsReg.h"


namespace AddressSpace
{



/*ctr*/
ASStsReg::ASStsReg (
    UaNodeId parentNodeId,
    const UaNodeId& typeNodeId,
    ASNodeManager *nm,
    const Configuration::StsReg & config):
    OpcUa::BaseObjectType (
    /*nodeId*/nm->makeChildNodeId(parentNodeId,config.name().c_str()), /*name*/config.name().c_str(), nm->getNameSpaceIndex(), nm),
    m_typeNodeId (typeNodeId)


    ,
    m_address (new

               OpcUa::BaseDataVariableType


               (nm->makeChildNodeId(this->nodeId(),UaString("address")), UaString("address"), nm->getNameSpaceIndex(), UaVariant(

                    config.address().c_str()
                ),

                OpcUa_AccessLevels_CurrentRead
                , nm))

    ,
    m_value (new

             OpcUa::BaseDataVariableType


             (nm->makeChildNodeId(this->nodeId(),UaString("value")), UaString("value"), nm->getNameSpaceIndex(), UaVariant(


              ),

              OpcUa_AccessLevels_CurrentRead
              , nm))

    ,
    m_deviceLink (0)


{
    UaStatus s;

    s = nm->addNodeAndReference(this, m_address, OpcUaId_HasComponent);
    if (!s.isGood())
    {
        std::cout << "While addNodeAndReference from " << this->nodeId().toString().toUtf8() << " to " << m_address->nodeId().toString().toUtf8() << " : " << std::endl;
        ASSERT_GOOD(s);
    }


    m_value->setValue(/*pSession*/0, UaDataValue(UaVariant(

                                      (OpcUa_UInt32)(0)


                                  ),
                                  OpcUa_BadWaitingForInitialData, UaDateTime::now(), UaDateTime::now() ), /*check access level*/OpcUa_False);

    s = nm->addNodeAndReference(this, m_value, OpcUaId_HasComponent);
    if (!s.isGood())
    {
        std::cout << "While addNodeAndReference from " << this->nodeId().toString().toUtf8() << " to " << m_value->nodeId().toString().toUtf8() << " : " << std::endl;
        ASSERT_GOOD(s);
    }


}





ASStsReg::~ASStsReg ()
{

    if (m_deviceLink != 0)
    {
        PRINT("deviceLink not zero!!");
        PRINT_LOCATION();

    }

}




/* generate setters and getters (if requested) */

UaStatus ASStsReg::setAddress (const UaString & value, OpcUa_StatusCode statusCode, const UaDateTime & srcTime )
{
    return m_address->setValue (0, UaDataValue (UaVariant(value), statusCode, srcTime, UaDateTime::now()), /*check access*/OpcUa_False  ) ;

}

UaStatus ASStsReg::getAddress (UaString & r) const
{
    UaVariant v (* (m_address->value(/*session*/0).value()));

    if (v.type() == OpcUaType_String)
    {
        r = v.toString();
        return OpcUa_Good;
    }
    else
        return OpcUa_Bad;

}


/* short getter (possible because this variable will never be null) */
UaString ASStsReg::getAddress () const
{
    UaVariant v (* m_address->value (0).value() );
    UaString v_value;
    v_value = v.toString();
    return v_value;
}

UaStatus ASStsReg::setValue (const OpcUa_UInt32 & value, OpcUa_StatusCode statusCode, const UaDateTime & srcTime )
{
    return m_value->setValue (0, UaDataValue (UaVariant(value), statusCode, srcTime, UaDateTime::now()), /*check access*/OpcUa_False  ) ;

}

UaStatus ASStsReg::getValue (OpcUa_UInt32 & r) const
{
    UaVariant v (* (m_value->value(/*session*/0).value()));
    return v.toUInt32 ( r );
}


/* short getter (possible because this variable will never be null) */
OpcUa_UInt32 ASStsReg::getValue () const
{
    UaVariant v (* m_value->value (0).value() );
    OpcUa_UInt32 v_value;
    v.toUInt32 ( v_value );
    return v_value;
}





/* generate delegates (if requested) */





/* generate device logic link */


void ASStsReg::linkDevice( Device::DStsReg *deviceLink)
{
    if (m_deviceLink != 0)
    {
        /* This is an error -- device can be linked at most in the object's lifetime. */
        //TODO After error handling is discussed, abort in smarter way
        abort();

    }
    else
        m_deviceLink = deviceLink;
}


void ASStsReg::unlinkDevice ()
{
    PRINT_LOCATION();
    m_deviceLink = 0;
}


}

