
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
    Authors(from Quasar team): Piotr Nikiel

    This file is part of Quasar.

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

    This file was completely generated by Quasar (additional info: using transform designToClassHeader.xslt)
    on 2020-02-24T16:17:58.733+01:00
 */



#ifndef __ASWritableUInt32__H__
#define __ASWritableUInt32__H__


#include <opcua_baseobjecttype.h>
#include <opcua_basedatavariabletype.h>

#include <Configuration.hxx>

#include <ASNodeManager.h>
#include <ASDelegatingVariable.h>
#include <ASSourceVariable.h>


/* forward declaration */
namespace Device {
class DWritableUInt32;
}


namespace AddressSpace
{
//! Fully auto-generated class to represent WritableUInt32 in the OPC UA AddressSpace
class ASWritableUInt32: public OpcUa::BaseObjectType
{
    UA_DISABLE_COPY(ASWritableUInt32);
public:



    //! Constructor. Used in Configurator.cpp. You NEVER use it directly.
    ASWritableUInt32 (
        UaNodeId parentNodeId,
        const UaNodeId& typeNodeId,
        ASNodeManager *nm,
        const Configuration::WritableUInt32 &config);



    /*dtor*/
    ~ASWritableUInt32 ();



    /* setters and getters for variables */



    UaStatus getAddress (UaString &) const ;
    UaStatus setAddress (const UaString & value, OpcUa_StatusCode statusCode,const UaDateTime & srcTime = UaDateTime::now()) ;



    /* short getter (possible because nullPolicy=nullForbidden) */
    UaString getAddress () const;



    UaStatus getValue (OpcUa_UInt32 &) const ;
    UaStatus setValue (const OpcUa_UInt32 value, OpcUa_StatusCode statusCode,const UaDateTime & srcTime = UaDateTime::now()) ;



    /* short getter (possible because nullPolicy=nullForbidden) */
    OpcUa_UInt32 getValue () const;




    /* delegators for cachevariables  */

    //! This function will be called when OPC UA write request comes. Never directly call it.

    /* NOTE: This function is not intended to be used by human being. */
    UaStatus

    writeValue (Session* pSession, const UaDataValue& dataValue, OpcUa_Boolean checkAccessLevel

                = OpcUa_True

               )
    ;




    /* Device Logic setter (if requested) */

    void linkDevice( Device::DWritableUInt32 *deviceLink);
    void unlinkDevice ();
    Device::DWritableUInt32 * getDeviceLink () const {
        return m_deviceLink;
    }

    /* OPC UA Type Information provider for this class. */
    virtual UaNodeId typeDefinitionId () const {
        return m_typeNodeId;
    }
private:
    UaNodeId m_typeNodeId;
    /* Variables */
    OpcUa::BaseDataVariableType
    * m_address;
    ASDelegatingVariable<ASWritableUInt32>
    * m_value;


    /* Device Logic link (if requested) */

    Device::DWritableUInt32 *m_deviceLink;




};

}



#endif // include guard

