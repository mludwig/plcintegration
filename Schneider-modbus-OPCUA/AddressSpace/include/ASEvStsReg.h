
/***********************************
This file was initially generated by Generic OPC UA Server framework (readme: Documentation/GenericServer.html)
Generated from:  designToClassHeader.xslt using project's design file.
(C) CERN 2014
***********************************/




#ifndef __ASEvStsReg__H__
#define __ASEvStsReg__H__


#include <opcua_baseobjecttype.h>
#include <opcua_basedatavariabletype.h>

#include <Configuration.hxx>

#include <ASNodeManager.h>
#include <ASDelegatingVariable.h>


/* forward declaration */
namespace Device {
class DEvStsReg;
}


namespace AddressSpace
{
class ASEvStsReg: public OpcUa::BaseObjectType
{
    UA_DISABLE_COPY(ASEvStsReg);
public:



    /*ctr*/
    ASEvStsReg (
        UaNodeId parentNodeId,
        const UaNodeId& typeNodeId,
        ASNodeManager *nm,
        const Configuration::EvStsReg &config);



    /*dtor*/
    ~ASEvStsReg ();



    /* setters and getters for variables */



    UaStatus getAddress (UaString &) const ;
    UaStatus setAddress (const UaString & value, OpcUa_StatusCode statusCode, const UaDateTime & srcTime ) ;


    /* short getter (possible because this variable will never be null) */
    UaString getAddress () const;



    UaStatus getValue (OpcUa_UInt32 &) const ;
    UaStatus setValue (const OpcUa_UInt32 & value, OpcUa_StatusCode statusCode, const UaDateTime & srcTime ) ;


    /* short getter (possible because this variable will never be null) */
    OpcUa_UInt32 getValue () const;




    /* delegators for cachevariables and externalvariables */




    /* Device Logic setter (if requested) */

    void linkDevice( Device::DEvStsReg *deviceLink);
    void unlinkDevice ();

    /* OPC UA Type Information provider for this class. */
    virtual UaNodeId typeDefinitionId () const {
        return m_typeNodeId;
    }
private:
    UaNodeId m_typeNodeId;
    /* Variables */
    OpcUa::BaseDataVariableType
    * m_address;
    OpcUa::BaseDataVariableType
    * m_value;


    /* Device Logic link (if requested) */

    Device::DEvStsReg *m_deviceLink;




};

}



#endif // include guard

