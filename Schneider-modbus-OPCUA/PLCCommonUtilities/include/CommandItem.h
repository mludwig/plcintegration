
#ifndef COMMANDITEM_H_
#define COMMANDITEM_H_

#include <string>
#include <stdint.h>
/*
 * This Class represents a Command. It will be created and queued in the OPC UA Thread after the Server receives a WriteRequest.
 * After this, the queue will be polled and processed to send the commands to the PLC.
 */
class CommandItem
{
public:
	//The different Types of commands supported, based on the datatype.
	enum Type { UINT16, UINT32, FLOAT32};
	//Constructor of the class using an unsigned int of 16 bits
    CommandItem(const std::string& address, const uint16_t value);
	//Constructor of the class using an unsigned int of 32 bits
	CommandItem(const std::string& address, const uint32_t value);
    //Constructor of the class using a float. The type is assigned automatically.
    CommandItem(const std::string& address, const float value);
    //Overloaded assignment operator.
    CommandItem & operator=(const CommandItem &rhs);
    //Virtual destructor
    virtual ~CommandItem();
    //The address of this value in the PLC memory, needed to push the command to the PLC.
    std::string m_address;
	//The value, when the type is an unsigned integer of 16 bits
    uint16_t m_value_i16;
    //The value, when the type is an unsigned integer of 32 bits
    uint32_t m_value_i32;
    //The value, when the type is a float
	float m_value_f;
	//The type.
	Type m_type;
};

#endif /* COMMANDITEM_H_ */
