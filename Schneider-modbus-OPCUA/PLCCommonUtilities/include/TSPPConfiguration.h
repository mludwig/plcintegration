
#ifndef TSPPCONFIG_H_
#define TSPPCONFIG_H_

#include <string>
/*
 * Class representation of the file TSPPConfig.xml as a container of information
 */
class TSPPConfiguration
{
public:
	/*
	 * Main constructor of the class
	 */
	TSPPConfiguration(const std::string hostMachineExternalIp, const unsigned int hostMachinePort,
			const std::string PlcIP, const unsigned int PlcPort, const std::string LogLevel):
				m_hostMachineExternalIP(hostMachineExternalIp), m_hostMachinePort(hostMachinePort),
				m_PlcIP(PlcIP), m_PlcPort(PlcPort), m_LogLevel(LogLevel)
{}
	/*
	 * External IP of the current machine. Used for the heartbeat message.
	 */
	const std::string& getHostMachineExternalIp() const
	{
		return m_hostMachineExternalIP;
	}
	/*
	 * Host machine port used for modbus. It will be 502 by default, as it is the protocol standard, but it is adviced to redirect the messages
	 * to a different port in order to avoid using ports below 1000 in Linux (they are system reserved).
	 */
	const unsigned int getHostMachinePort() const
	{
		return m_hostMachinePort;
	}
	/*
	 * LogIt log level.
	 */
	const std::string& getLogLevel() const
	{
		return m_LogLevel;
	}
	/*
	 * PLC Ip
	 */
	const std::string& getPlcIp() const
	{
		return m_PlcIP;
	}
	/*
	 * PLC port used in modbus communication, usually 502.
	 */
	const unsigned int getPlcPort() const
	{
		return m_PlcPort;
	}

private:
	const std::string m_hostMachineExternalIP;
	const unsigned int m_hostMachinePort;
	const std::string m_PlcIP;
	const unsigned int m_PlcPort;
	const std::string m_LogLevel;
};

#endif /* TSPPCONFIG_H_ */
