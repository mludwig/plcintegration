#ifndef __DriverUtils__H__
#define __DriverUtils__H__

#include <string>
#include <stdint.h>
#include "TSPPConfiguration.h"

/*
 * Utility class, containing some getAddress methods (set here to avoid having custom code on the generated classes) and several utility methods
 * with byte level operations, needed all around the component.
 */
class DriverUtils
{
public:
	//Gets the first byte of a word
	static const uint8_t getByte1(const uint16_t);
	//Gets the second byte of a word
	static const uint8_t getByte2(const uint16_t);
	//Gets the first byte of 2 words (with byte swapping technique)
	static const uint8_t getByte1(const uint32_t);
	//Gets the second byte of 2 words (with byte swapping technique)
	static const uint8_t getByte2(const uint32_t);
	//Gets the third byte of 2 words (with byte swapping technique)
	static const uint8_t getByte3(const uint32_t);
	//Gets the forth byte of 2 words (with byte swapping technique)
	static const uint8_t getByte4(const uint32_t);
	//Gets the first byte of 2 words (with byte swapping technique)
	static const uint8_t getByte1(const float);
	//Gets the second byte of 2 words (with byte swapping technique)
	static const uint8_t getByte2(const float);
	//Gets the third byte of 2 words (with byte swapping technique)
	static const uint8_t getByte3(const float);
	//Gets the forth byte of 2 words (with byte swapping technique)
	static const uint8_t getByte4(const float);
	//Builds a word from 2 bytes
	static const uint16_t getWordFromBytes(const uint8_t, const uint8_t);
	//Builds a unsigned int (2 words) from 4 bytes
	static const uint32_t getUint32FromBytes(const uint8_t, const uint8_t, const uint8_t, const uint8_t);
	//Builds a long from 2 words
	static const long getLongFromWords(const uint16_t, const uint16_t);
	//Builds a float from 2 words
	static const float getFloatFromWords(const uint16_t, const uint16_t);
	/*
	 * Parses the TSPPConfig.xml file and gets the information into a TSPPConfiguration object, which is returned.
	 */
	static const TSPPConfiguration parseConfiguration();
};

#endif // include guard
