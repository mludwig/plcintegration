/*
 * Data.h
 *
 *  Created on: Apr 25, 2014
 *      Author: bfarnham
 */

#ifndef DATA_H_
#define DATA_H_

#include <string>
#include <boost/shared_array.hpp>

typedef boost::shared_array<const unsigned char> bufferPtr;
/*
 * Items that will be stored on the Shared Packet Queue.
 * This items contain a byte buffer (m_buffer) and it's length (m_bufferLength)
 */
class PacketItem
{
public:
	/*
	 * Main constructor. Creates an item from a buffer of bytes and it's length
	 */
    PacketItem(const int bufferLength, const unsigned char* buffer);
    /*
     * Virtual destructor
     */
    virtual ~PacketItem();
    /*
     * The length of the byte buffer
     */
    const int m_bufferLength;
    /*
     * A pointer to the byte buffer
     */
    const bufferPtr m_buffer;

private:
    static bufferPtr clonebuffer(const unsigned char* buffer);
};

typedef boost::shared_ptr<PacketItem> PacketItemPtr;

#endif /* DATA_H_ */
