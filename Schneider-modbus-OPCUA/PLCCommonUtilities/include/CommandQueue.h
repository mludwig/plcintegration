/*
 * CommandQueue.h
 *
 *  Created on: Sep 17, 2014
 *      Author: dabalo
 */
#include "CommandItem.h"
#include "SharedQueue.h"

typedef SharedQueue<CommandItem> SharedCommandQueue;

class CommandQueue
{

public:
	//Inserts a CommandItem in the queue
	static void commandPut(CommandItem);
	//Takes a CommandItem from the queue syncronously.
	static CommandItem commandBlockingTake();
	//Gets the current size of the command queue
	static size_t commandQueueSize();
private:
	//CommandItem queue
	static SharedCommandQueue* m_commandQueue;
	//Private constructor (all the methods are static, it would be pointless to create an object of this class)
	CommandQueue();
};