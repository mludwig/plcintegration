/*
 * StateMachine.cpp
 *
 *  Created on: Oct 1, 2014
 *      Author: bfarnham
 */
#include "StateMachine.h"
#include <LogIt.h>
#include <boost/thread.hpp>

StateMachine::StateMachine(StateFactory& stateFactory)
:m_stateFactory(stateFactory), m_stopRunning(false), m_isRunning(false)
{}

StateMachine::~StateMachine()
{}

void StateMachine::run()
{
	m_isRunning = true;
	LOG(Log::DBG) << "state machine starting";

	for(STATE_IDS::STATE_ID currentStateId = STATE_IDS::CONNECT; !m_stopRunning;)
	{
		const bool stateCompleted = executeState(currentStateId);

		if(!stateCompleted)
		{
			handleMessyExit(currentStateId);
		}

		currentStateId = getNextStateId(currentStateId);
	}

	m_isRunning = false;
	LOG(Log::DBG) << "state machine exiting, stopRunning ["<<m_stopRunning<<"]";
}

enum STATE_IDS::STATE_ID StateMachine::getNextStateId(const enum STATE_IDS::STATE_ID& currentStateId) const
{
	if(currentStateId == STATE_IDS::DISCONNECT)
	{
		return STATE_IDS::CONNECT;
	}
	else
	{
		return static_cast<STATE_IDS::STATE_ID>(currentStateId+1);
	}
}

bool StateMachine::executeState(const enum STATE_IDS::STATE_ID& stateId) const
{
	boost::shared_ptr<State> state = m_stateFactory.getState(stateId);

	LOG(Log::DBG) << "starting execution for state id ["<<STATE_IDS::toString(stateId)<<"] object ["<<state->toString()<<"]";

	boost::this_thread::interruption_point();
	const bool stateCompleted = state->execute();
	boost::this_thread::interruption_point();

	if(!stateCompleted)
	{
		LOG(Log::WRN) << " failure during execution of state ["<<state->toString()<<"]";
	}
	else
	{
		LOG(Log::DBG) << "state id ["<<STATE_IDS::toString(stateId)<<"] object ["<<state->toString()<<"] completed, OK ["<<(stateCompleted?"Y":"N")<<"]";
	}

	return stateCompleted;
}

void StateMachine::stop()
{
	m_stopRunning = true;
}

bool StateMachine::isRunning() const
{
	return m_isRunning;
}

void StateMachine::handleMessyExit(enum STATE_IDS::STATE_ID& currentStateId)
{
	LOG(Log::WRN) << __FUNCTION__ << " handling a messy exit from failed state ["<<STATE_IDS::toString(currentStateId)<<"]";

	if(currentStateId != STATE_IDS::DISCONNECT)
	{
		LOG(Log::WRN) << __FUNCTION__ << " running disconnect state, trying to make clean exit";
		currentStateId = STATE_IDS::DISCONNECT;
		const bool disconnected = executeState(currentStateId);
		LOG(Log::WRN) << __FUNCTION__ << " disconnect state success? ["<<(disconnected?'Y':'N')<<"]";
	}
}
