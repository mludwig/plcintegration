# PLCintegration


integration TSPP (time stamp push protocol from CERN) for these PLCs into WinCC-OA/scada, by providing OPCUA quasar servers

* Siemens S7-300 and S7-400 TSPP integration as OPCUA adress space (Damien, Ben)
  https://ics-svn.web.cern.ch/repo/trunk/tools/OPC/SiemensS7OPCUA

* Schneider TSPP integration modbus as OPCUA adress space(Damien, Ben)
  https://ics-svn.web.cern.ch/repo/trunk/tools/OPC/SchneiderModbusOPCUA

* Siemens S7-1500 TSPP integration as OPCUA adress space (Michael, new)
