<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0"
xmlns:xml="http://www.w3.org/XML/1998/namespace" 
xmlns:xs="http://www.w3.org/2001/XMLSchema" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
xmlns:d="http://www.example.org/Design"
xmlns:fnc="http://MyFunctions"
xsi:schemaLocation="http://www.w3.org/1999/XSL/Transform schema-for-xslt20.xsd ">
	<xsl:output method="text"></xsl:output>
	<xsl:include href="Design/CommonFunctions.xslt" />


	<xsl:template match="/">
	
	set(ADDRESSSPACE_HEADERS
	<xsl:for-each select="/d:design/d:class">
	<xsl:value-of select="fnc:ASClassName(@name)"/>.h
	</xsl:for-each>
	)
	
	</xsl:template>
</xsl:transform>
