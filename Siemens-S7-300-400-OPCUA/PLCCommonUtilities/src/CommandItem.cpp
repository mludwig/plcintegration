#include "CommandItem.h"

CommandItem::CommandItem(const std::string& address, const uint16_t value):
    	m_address(address), m_value_i32(NULL), m_value_i16(value), m_value_f(NULL), m_type(UINT16)
{}

CommandItem::CommandItem(const std::string& address, const uint32_t value):
    	m_address(address), m_value_i16(NULL), m_value_i32(value), m_value_f(NULL), m_type(UINT32)
{}

CommandItem::CommandItem(const std::string& address, const float value):
	m_address(address), m_value_f(value), m_value_i16(NULL), m_value_i32(NULL), m_type(FLOAT32)
{}

CommandItem & CommandItem::operator=(const CommandItem &rhs)
{
	CommandItem::m_address = rhs.m_address;
	CommandItem::m_value_f = rhs.m_value_f;
	CommandItem::m_value_i16 = rhs.m_value_i16;
	CommandItem::m_value_i32 = rhs.m_value_i32;
	return *this;
}

CommandItem::~CommandItem()
{}
