/*
 * Data.cpp
 *
 *  Created on: Apr 25, 2014
 *      Author: bfarnham
 */

#include "../include/PacketItem.h"

PacketItem::PacketItem(const int bufferLength, const unsigned char* buffer)
:m_bufferLength(bufferLength), m_buffer(buffer)
{}

PacketItem::~PacketItem()
{}

