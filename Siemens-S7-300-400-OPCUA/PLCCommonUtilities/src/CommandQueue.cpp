/*
 * CommandQueue.cpp
 *
 *  Created on: Sep 18, 2014
 *      Author: dabalo
 */
#include "CommandQueue.h"

SharedCommandQueue* CommandQueue::m_commandQueue = new SharedCommandQueue();

void CommandQueue::commandPut(CommandItem c)
{
	m_commandQueue->put(c);
}
CommandItem CommandQueue::commandBlockingTake()
{
	size_t size = 0;
	CommandItem command = m_commandQueue->blockingTake(size);
	return command;
}
size_t CommandQueue::commandQueueSize()
{
	return m_commandQueue->getSize();
}
