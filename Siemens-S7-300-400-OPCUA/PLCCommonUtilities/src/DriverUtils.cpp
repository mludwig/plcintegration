#include "DriverUtils.h"
/*#include "ASReadableUInt16.h"
#include "ASReadableUInt32.h"
#include "ASReadableFloat32.h"
#include "ASWritableUInt32.h"
#include "ASWritableUInt16.h"
#include "ASWritableFloat32.h"*/
#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include "LogIt.h"

using namespace xercesc;
using std::map;
using std::string;
using std::pair;

const string g_sTSPPConfigFileName = "TSPPConfig.xml";
const string g_sconfigHostIp = "tns:HostMachineExternalIP";
const string g_sconfigHostPort = "tns:HostMachinePort";
const string g_sconfigPlcIp = "tns:PlcIP";
const string g_sconfigPlcPort = "tns:PlcPort";
const string g_sconfigLogLevel = "tns:LogLevel";

union floatUnion {
    float number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} FloatUnion;
union uint32union {
    uint32_t number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} Uint32union;
const TSPPConfiguration DriverUtils::parseConfiguration()
{
	try
	{
		map<string,pair<int,int> > myData;
		XMLPlatformUtils::Initialize();
		// Create parser, set parser values for validation
		XercesDOMParser* parser = new XercesDOMParser();
		parser->setValidationScheme(XercesDOMParser::Val_Always);
		parser->setDoNamespaces(true);
		parser->setDoSchema(true);
		parser->setValidationConstraintFatal(true);
		parser->parse(XMLString::transcode(g_sTSPPConfigFileName.c_str()));
		DOMElement* docRootNode;
		xercesc::DOMDocument* doc;
		DOMNodeIterator * walker;
		doc = parser->getDocument();
		docRootNode = doc->getDocumentElement();
		// Create the node iterator, that will walk through each element.
		walker = doc->createNodeIterator(docRootNode,DOMNodeFilter::SHOW_ELEMENT,NULL,true);
		// Some declarations
		DOMNode * current_node = NULL;
		string currentNodeName;
		string hostMachineIp = "empty";
		unsigned int hostMachinePort = -1;
		string plcIp = "empty";
		unsigned int plcPort = -1;
		string logLevel = "empty";
		for (current_node = walker->nextNode(); current_node != 0; current_node = walker->nextNode())
		{
			currentNodeName = XMLString::transcode(current_node->getNodeName());
			//We store the value of the xml entry depending on the xml node
			if(currentNodeName == g_sconfigHostIp)
			{
				hostMachineIp = XMLString::transcode(current_node->getFirstChild()->getNodeValue());
			}
			else if(currentNodeName == g_sconfigHostPort)
			{
				hostMachinePort = XMLString::parseInt(current_node->getFirstChild()->getNodeValue());
			}
			else if(currentNodeName == g_sconfigPlcIp)
			{
				plcIp = XMLString::transcode(current_node->getFirstChild()->getNodeValue());
			}
			else if(currentNodeName == g_sconfigPlcPort)
			{
				plcPort = XMLString::parseInt(current_node->getFirstChild()->getNodeValue());
			}
			else if(currentNodeName == g_sconfigLogLevel)
			{
				logLevel = XMLString::transcode(current_node->getFirstChild()->getNodeValue());
			}
		}
		return TSPPConfiguration(hostMachineIp, hostMachinePort, plcIp, plcPort, logLevel);
	}
	catch (...) {
		LOG(Log::ERR) << "There was a problem parsing your XML configuration file (TSPPConfig.xml). The application will shut down now.";
	}

	return TSPPConfiguration("empty", -1, "empty", -1, "empty");
}

const uint8_t DriverUtils::getByte1(const uint16_t word)
{
	return static_cast<uint8_t>(word >> 8);
}
const uint8_t DriverUtils::getByte2(const uint16_t word)
{
	return static_cast<uint8_t>(word & 0xFF);
}
const uint8_t DriverUtils::getByte1(const uint32_t doubleWord)
{
	return static_cast<uint8_t>(doubleWord >> 8);
}
const uint8_t DriverUtils::getByte2(const uint32_t doubleWord)
{
	return static_cast<uint8_t>(doubleWord & 0xFF);
}
const uint8_t DriverUtils::getByte3(const uint32_t doubleWord)
{
	return static_cast<uint8_t>(doubleWord >> 24);
}
const uint8_t DriverUtils::getByte4(const uint32_t doubleWord)
{
	return static_cast<uint8_t>(doubleWord >> 16);
}
const uint8_t DriverUtils::getByte1(const float doubleWord)
{
	FloatUnion.number = doubleWord;
	return FloatUnion.byteContents[0];
}
const uint8_t DriverUtils::getByte2(const float doubleWord)
{
	FloatUnion.number = doubleWord;
	return FloatUnion.byteContents[1];
}
const uint8_t DriverUtils::getByte3(const float doubleWord)
{
	FloatUnion.number = doubleWord;
	return FloatUnion.byteContents[2];
}
const uint8_t DriverUtils::getByte4(const float doubleWord)
{
	FloatUnion.number = doubleWord;
	return FloatUnion.byteContents[3];
}
const uint16_t DriverUtils::getWordFromBytes(const uint8_t byte1, const uint8_t byte2)
{
	return static_cast<uint16_t>((byte1 << 8) | byte2);
}
const uint32_t DriverUtils::getUint32FromBytes(const uint8_t byte1, const uint8_t byte2, const uint8_t byte3, const uint8_t byte4)
{
	return static_cast<uint32_t>((byte1 << 24) | (byte2 << 16) | (byte3 << 8) | byte4);
}
const long DriverUtils::getLongFromWords(const uint16_t word1, const uint16_t word2)
{
	return static_cast<long>((word1 << 16) | word2);
}
const float DriverUtils::getFloatFromWords(const uint16_t word1, const uint16_t word2)
{
	FloatUnion.byteContents[1] = getByte1(word1);
	FloatUnion.byteContents[0] = getByte2(word1);
	FloatUnion.byteContents[3] = getByte1(word2);
	FloatUnion.byteContents[2] = getByte2(word2);
	return FloatUnion.number;
}
