#include(${PROJECT_SOURCE_DIR}/generated_headers_list.cmake)
#
#add_library (PLCCommonUtilities OBJECT
#	 src/CommandItem.cpp
#	 src/PacketItem.cpp
#	 src/DriverUtils.cpp
#	 src/PropertiesMap.cpp	 
#	)
#
#add_dependencies (PLCCommonUtilities AddressSpaceGeneratedHeaders DeviceBase)


file(GLOB PLCCOMMON_SRCS src/*.cpp)

add_library (PLCCommonUtilities OBJECT ${PLCCOMMON_SRCS})

add_dependencies (PLCCommonUtilities Configuration Device)