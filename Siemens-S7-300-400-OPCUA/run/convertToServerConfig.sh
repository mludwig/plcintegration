#!/bin/bash
#
# $1 = UAB driver config file with adresses in text format
# generate a config.xml OPCUA population file from this
# for all the different devices and their address structure. Only a few devices
# are known here to cover the testing needs. More devices exist in the generic world
# of course
#
# the script is basically a switch on text function which inserts xml snippets according to known
# device names, types and addresses


# ------------
# functions


#Config Line : CPC_AnalogInput;deviceNumber;Alias[,DeviceLinkList];Description - ElectricalDiagram;Diagnostics;WWWLink;Synoptic;Domain;Nature;WidgetType;Unit;Format;RangeMax;RangeMin;HHLimit;HLimit;LLimit;LLLimit;AlarmActive;DriverDeadbandValue;DriverDeadbandType;ArchiveMode;TimeFilter;SMSCat;AlarmMessage;AlarmAck;addr_StsReg01;addr_EvStsReg01;addr_PosSt;addr_HFSt;addr_ManReg01;addr_MPosR;BooleanArchive;AnalogArchive;EventArchive;MaskEvent;Parameters;Master;Parents;children;Type;SecondAlias;
# get the adresses in that order from the config line. These fields start with "addr_"
function getAddrArraysFromConfigLine() {
	#echo "function getAddrArraysFromConfigLine _getAddrArraysFromConfigLine_line= "$_getAddrArraysFromConfigLine_line
	__cc=`echo $_getAddrArraysFromConfigLine_line | sed "s/#Config //g" | sed -nr 's/\[(.*)\]//p'` 
	#echo "__cc= "$__cc
	ar_addr=()
	ar_index=()
	_i=1
	for fd in $__cc
	do
	    if [[ "$fd" == "addr_"* ]]; then
		    echo "addr= "$fd
		    ar_addr[ $_i]=$fd
		    #ar_index[ $_i]=$_i
		    ar_index[ $_i]=$_i
		#else
		#    echo "fd= "$fd
		fi
	_i=$((_i+1))
	done
	echo "adresses: "${ar_addr[@]}
	echo "indices: "${ar_index[@]}
}

# for devices AnalogInput, i.e.
function device_AnalogInput_xml() {
	#echo "function _device_AnalogInput_xml_line _device_AnalogInput_xml_line= "$_device_AnalogInput_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogInput_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function _device_AnalogInput_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:WritableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}

# for devices AnalogInputReal
function device_AnalogInputReal_xml() {
	#echo "function device_AnalogInputReal_xml _device_AnalogInputReal_xml_line= "$_device_AnalogInputReal_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogInputReal_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AnalogInputReal_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:WritableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}

# for devices AnalogOutput
function device_AnalogOutput_xml() {
	#echo "function device_AnalogOutput_xml _device_AnalogOutput_xml_line= "$_device_AnalogOutput_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogOutput_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AnalogOutput_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:WritableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}
# for devices AnalogOutputReal
function device_AnalogOutputReal_xml() {
	#echo "function device_AnalogOutputReal_xml _device_AnalogOutputReal_xml_line= "$_device_AnalogOutputReal_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogOutputReal_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AnalogOutputReal_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:WritableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}
# for devices DigitalParameter
function device_DigitalParameter_xml() {
	#echo "function device_DigitalParameter_xml _device_DigitalParameter_xml_line= "$_device_DigitalParameter_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_DigitalParameter_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_DigitalParameter_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}
# for devices AnalogParameter
function device_AnalogParameter_xml() {
	#echo "function device_AnalogParameter_xml _device_AnalogParameter_xml_line= "$_device_AnalogParameter_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogParameter_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AnalogParameter_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:WritableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:WritableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}

# for devices Process Control Object
function device_PCO_xml() {
	#echo "function device_AnalogParameter_xml _device_AnalogParameter_xml_line= "$_device_AnalogParameter_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AnalogParameter_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AnalogParameter_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			echo "   <tns:ReadableUInt16 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "3" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "4" ]; then
			echo "   <tns:ReadableUInt32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "5" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "6" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "7" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}

# analog status
function device_AS_xml() {
	#echo "function device_AS_xml _device_AS_xml_line= "$_device_AS_xml_line
	varcount=1
	for adindex in ${ar_index[@]}
	do
		addressValue=`echo $_device_AS_xml_line | awk -v ii="$adindex" '{ print $ii }'`
		addressName=` echo ${ar_addr[$adindex]} | sed "s/addr_//g" `
		#echo "function device_AS_xml adindex= "$adindex" has address= "$addressValue" name= "$addressName
		if [ $varcount == "1" ]; then
			echo "   <tns:ReadableFloat32 address="\"$addressValue\"" name="\"$addressName"\"/>">> $fout       
		fi
		if [ $varcount == "2" ]; then
			varcount=$((varcount+1))
		else
			varcount=1
		fi
	done
}

# watchdog always at the same address
function watchdog_xml() {
	echo "<tns:Device name="\"WATCHDOG"\">" >> $fout       
	echo "   <tns:ReadableUInt16 address="\"DB100.DBW4\"" name="\"counter"\"></tns:ReadableUInt16>" >> $fout       
	echo "</tns:Device>" >> $fout       
}
# ------------

filename=$1
n=1
fout="config.xml"
rm -f $fout; touch $fout
echo $fout" generated from "$1 
cat opcua_config_header.txt >> $fout 
while read line; do
	#echo "line $n: $line"
	
	# format line to correspond nicely to the header, replace missing field values with null
	# eliminate optional device link list [] as well
	# this works here, but on a broader and more generic scope this quite likely does not work any more, because
	# more rules and syntax of UNICOS register configuration have to be taken into account.
	# this is a quick hack to make the performance test program & OPCUA server work 
	line0=`echo $line | sed "s/#Config\ Line\ :/#Config;/g" | sed "s/ //g" | sed "s/;;/;null;/g"  | sed "s/;;/;null;/g" | sed "s/;/ /g" | sed "s/,/ /g" `
	type0=` echo $line0 | awk '{printf "%s", $1}' `
	count0=` echo $line0 | awk '{printf "%04d", $2}' `
	name0=` echo $line0 | awk '{printf "%s", $3}' `
	type1=` echo $line0 | awk '{printf "%s", $9}' `
	
	#echo "line0=" $line0
	#echo "type0="$type0
	#echo "count0="$count0
	#echo "name0="$name0
	#echo "type1="$type1
	
	# ------------------------------------------------------------------------------------------
	# if we have a configuration header line, all the lines below follow the format as specified
	# ------------------------------------------------------------------------------------------
	if [[ "$type0" == "#Config"* ]] 
	then
		# echo "config line= "$line0
		_getAddrArraysFromConfigLine_line=$line0
		getAddrArraysFromConfigLine
	fi
	
	# ----------------------------
	# the various types of devices
	# ----------------------------
	#	if [ "$type0" == "CPC_AnalogInput" ] && [ "$type1" == "AI" ]
	if [ "$type1" == "AI" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog input integer"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AnalogInput_xml_line=$line0 
		device_AnalogInput_xml  
		echo "</tns:Device>" >> $fout
	fi

	#	if [ "$type0" == "CPC_AnalogInput" ] && [ "$type1" == "AIR" ]
	if [ "$type1" == "AIR" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog input real"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AnalogInputReal_xml_line=$line0 
		device_AnalogInputReal_xml  
		echo "</tns:Device>" >> $fout
	fi

	# AnalogOutput
	if [ "$type1" == "AO" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog output integer"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AnalogOutput_xml_line=$line0 >> $fout    
		echo "</tns:Device>" >> $fout
	fi
	
	# AnalogOutputReal
	if [ "$type1" == "AOR" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog output real"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AnalogOutputReal_xml_line=$line0
		device_AnalogOutputReal_xml 
		echo "</tns:Device>" >> $fout
	fi
	
	# DigitalParameter
	if [ "$type1" == "DPar" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> digital parameter"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_DigitalParameter_xml_line=$line0
		device_DigitalParameter_xml 
		echo "</tns:Device>" >> $fout
	fi
	
	# AnalogParameter
	if [ "$type1" == "APar" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog parameter"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AnalogParameter_xml_line=$line0
		device_AnalogParameter_xml 
		echo "</tns:Device>" >> $fout
	fi
	
	# Process Control Object
	if [ "$type1" == "PCO" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> process control object"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_PCO_xml_line=$line0
		device_PCO_xml 
		echo "</tns:Device>" >> $fout
	fi

	# Analog Status
	if [ "$type1" == "AS" ]
	then
		echo "line"$n" found "$type0" count0= "$count0" ==> analog status"
		echo "<tns:Device name=\"$name0\">" >> $fout
		_device_AS_xml_line=$line0
		device_AS_xml 
		echo "</tns:Device>" >> $fout
	fi
	
	# ... other devices as needed
	
	n=$((n+1))
done < $filename

# add the watchdog
watchdog_xml

echo "</tns:configuration>"  >> $fout
echo "output written to "$fout



