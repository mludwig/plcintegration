#include <iostream>
#include <boost/foreach.hpp>
#include <DRoot.h>
#include "stdio.h"
#include "stdlib.h"
#include <iostream>
#include <termios.h>
#include <string>
#include "TSPPProcessingThread.h"
#include "sapi_s7.h"
#include "TSPPPacket.h"
#include "PropertiesMap.h"
#include "LogIt.h"

const char* configFile = "config.xml";

TSPPProcessingThread::TSPPProcessingThread(SharedPacketQueue* TSPPQueue):
		m_exitFlag(0), m_xmlConfig(configFile), m_running(false)
{
	m_TSPPQueue = TSPPQueue;
}

void TSPPProcessingThread::run()
{
	m_running = true;
    do
    {
    	size_t size;
    	PacketItemPtr itemPtr = m_TSPPQueue->blockingTake(size);
    	boost::shared_array<const unsigned char> buffer = itemPtr->m_buffer;
    	TSPPProcessingThread::processTSPPPacket(static_cast<const unsigned int>(itemPtr->m_bufferLength),
    			static_cast<const uint8_t*>(itemPtr->m_buffer.get())
    			);
		LOG(Log::DBG) << "Package processed";
    }
    while(!m_exitFlag);
    m_running = false;
}

void TSPPProcessingThread::processTSPPPacket(const unsigned int rec_buf_len, const uint8_t* buffer)
{
	LOG(Log::DBG) << "Received ["<< rec_buf_len <<"] bytes to process";

	/**
	 * all the processing is done in the constructor on the stack.
	 * advantages:
	 * 		simple code, safe at runtime for mem management
	 * 		threadsafe in an OO way
	 *
	 * disadvantages:
	 * 		unsafe at runtime because constructors should never fail,
	 *           and if they do, it is impossible to do error handling
	 *      also it is much slower on the OS for alloc/dealloc and mem garbage collection, especially on high frequencies
	 *
	 * Since there is one TSPP thread per PLC the TSPP processing itself is serialized, therefore parallel
	 * execution is not an issue here anyway. Therefore we can allocate a singleton object and re-use it
	 * in the thread.
	 */
	TSPPPacket tspp(rec_buf_len, buffer, m_xmlConfig);
	//tspp->printInfo(m_xmlConfig);
	LOG(Log::DBG) << "Pushing info!";
	tspp.pushInfo(m_xmlConfig);
}
