#include "s7interface.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include <iostream>
#include <fstream>
#include <termios.h>
#include <ctime>
#include "LogIt.h"
#include <boost/algorithm/string.hpp>

using std::vector;

//Defines
//StatisticMode
const int STATISTICS_MODE = 0;
const char* STATISTICS_FILE = "statistics.csv";
const int MAXERRMSGLEN = 100;
//Initial value of m_rId
const ord32 initialRId = 0;
//Initial receive timeout
const int msRecieveTimeoutInit = 5000;
//Standard receive timeout
const int msRecieveTimeout = 1000;
using std::string;
//IP to write to the plc
const uint8_t ip1 = 188;
const uint8_t ip2 = 184;
const uint8_t ip3 = 7;
const uint8_t ip4 = 48;

void waitMs(int ms)
{
    clock_t begin_time = clock ();
    do
    {
        int busy = 1;
    }
    while((float( clock () - begin_time ) /  CLOCKS_PER_SEC)<(float(ms)/1000));
}

s7Interface::s7Interface(string plcName, string h1_file, string VFDname, SharedPacketQueue* TSPPQueue)
{
	m_TSPPQueue = TSPPQueue;
    // initialize s7
	initialize(h1_file, VFDname);
    // get reference for connection 'TEST'
    getCRefFromS7(plcName);
    // initiate connection
    initiateRequest();
    // receive message. This starts the cyclic reading of a variable
    receiveInit(S7_INITIATE_CNF, msRecieveTimeoutInit);
    initiateBlockReceive();
}

s7Interface::~s7Interface()
{
    s7Interface::shutdown();
}

void s7Interface::initiateBlockReceive()
{
    m_rId = initialRId;
    /* BRCV initialize */
	int ret = s7_brcv_init(m_cpDescr,m_cref,m_rId);
	LOG(Log::DBG) << "Starting block receive init: s7_brcv_init return = [" << ret << "], r_id = [" << m_rId << "]";
	if(ret != S7_OK)
	{
		LOG(Log::ERR) << "There was an error when starting block receive: [" << ret << "]";
		shutdown();
	}
}

void s7Interface::stopBlockReceive()
{
	LOG(Log::DBG) << "Stopping brcv";
    //We close the BRCV connection, in order to open a new one later
    int ret = s7_brcv_stop(m_cpDescr, m_cref, m_rId);
    if(ret != S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 block receive stop returned: [" << ret << "]";
    }
}

void s7Interface::blockReceive()
{
	//We recieve the confirmation when the PLC is ready.
	receiveRead(S7_BRCV_IND, msRecieveTimeout);
}

void s7Interface::writeIP(ord16 orderid, std::string s7Adress)
{
	LOG(Log::TRC) << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " "
			<< " s7adress= " << s7Adress
			<< " ip1= " << (int) ip1
			<< " ip2= " << (int) ip2
			<< " ip3= " << (int) ip3
			<< " ip4= " << (int) ip4;

	//std::string newAddress;
	//transformAddress(s7Adress, newAddress);

	struct S7_WRITE_PARA_LONG write_para;
    write_para.access=S7_ACCESS_SYMB_ADDRESS;
    strcpy(write_para.var_name, s7Adress.c_str());
    write_para.var_length=4;
    write_para.value[0] = ip1;
    write_para.value[1] = ip2;
    write_para.value[2] = ip3;
    write_para.value[3] = ip4;

    int ret=s7_write_long_req(m_cpDescr, m_cref, orderid, &write_para, NULL);
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 write long request returned: [" << ret << "]";
    	//shutdown();
    }
    //Wait for confirmation
    receiveWrite(S7_WRITE_CNF, msRecieveTimeout);
}
//Used for splitting uint16 into bytes
union uint16union {
    uint16_t number; // occupies 2 bytes
    uint8_t byteContents[2]; // occupies 2 bytes
} Uint16union;
void s7Interface::writeUInt16(uint16_t value, std::string s7Adress)
{
	LOG(Log::TRC) << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " " << s7Adress;
	std::string newAddress;
	transformAddress(s7Adress, newAddress);
	struct S7_WRITE_PARA write_para;
	write_para.access=S7_ACCESS_SYMB_ADDRESS;
	strcpy(write_para.var_name,newAddress.c_str());
	write_para.var_length=2;
	Uint16union.number = value;
	write_para.value[1] = Uint16union.byteContents[0];
	write_para.value[0] = Uint16union.byteContents[1];
	LOG(Log::DBG) << "Trying to write " << static_cast<int>(value) << " in address " << newAddress;
	int ret=s7_write_req(m_cpDescr, m_cref, 0, &write_para, NULL);
	if(ret!=S7_OK)
	{
		LOG(Log::ERR) << "ERROR: s7 write request returned: [" << ret << "]";
		//shutdown();
	}
	//Wait for confirmation
	receiveWrite(S7_WRITE_CNF, msRecieveTimeout);
}
//Used for splitting uint32 into bytes
union int32union {
    uint32_t number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} Int32union;
void s7Interface::writeUInt32(uint32_t value, std::string s7Adress)
{
	LOG(Log::TRC) << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " s7Adress= " << s7Adress;
	std::string newAddress;
	transformAddress(s7Adress, newAddress);
	LOG(Log::TRC) << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " newAddress= " << newAddress;
	struct S7_WRITE_PARA_LONG write_para;
	write_para.access=S7_ACCESS_SYMB_ADDRESS;
	strcpy(write_para.var_name,newAddress.c_str());
	write_para.var_length=4;
	Int32union.number = value;
	write_para.value[0] = Int32union.byteContents[3];
	write_para.value[1] = Int32union.byteContents[2];
	write_para.value[2] = Int32union.byteContents[1];
	write_para.value[3] = Int32union.byteContents[0];
	int ret=s7_write_long_req(m_cpDescr, m_cref, 0, &write_para, NULL);
	if(ret!=S7_OK)
	{
		LOG(Log::ERR) << "ERROR: s7 write long request returned: [" << ret << "]";
		//shutdown();
	}
	//Wait for confirmation
	receiveWrite(S7_WRITE_CNF, msRecieveTimeout);
}
//Used for splitting float into bytes
union fltUnion {
    float number; // occupies 4 bytes
    uint8_t byteContents[4]; // occupies 4 bytes
} FltUnion;
void s7Interface::writeFloat(float value, std::string s7Adress)//CORRUPT!
{
	LOG(Log::TRC) << __FILE__ <<  " " << __LINE__ << " " << __FUNCTION__ << " " << s7Adress;

	std::string newAddress;
	transformAddress(s7Adress, newAddress);
	struct S7_WRITE_PARA_LONG write_para;
	write_para.access=S7_ACCESS_SYMB_ADDRESS;
	strcpy(write_para.var_name,newAddress.c_str());
	write_para.var_length=4;
	FltUnion.number = value;
	write_para.value[0] = FltUnion.byteContents[3];
	write_para.value[1] = FltUnion.byteContents[2];
	write_para.value[2] = FltUnion.byteContents[1];
	write_para.value[3] = FltUnion.byteContents[0];
	int ret=s7_write_long_req(m_cpDescr, m_cref, 0, &write_para, NULL);
	if(ret!=S7_OK)
	{
		LOG(Log::ERR) << "ERROR: s7 write long request returned: [" << ret << "]";
		//shutdown();
	}
	//Wait for confirmation
	receiveWrite(S7_WRITE_CNF, msRecieveTimeout);
}
/* start cyclic read */
void s7Interface::cyclicRead(ord16 orderid, std::string s7Adress)
{
    struct S7_READ_PARA read_para;
    read_para.access=S7_ACCESS_SYMB_ADDRESS;
    strcpy(read_para.var_name,s7Adress.c_str());
    int ret=s7_cycl_read(m_cpDescr,m_cref,orderid,200,1,&read_para);//200 is the cycle time
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 cyclic read returned: [" << ret << "]";
		shutdown();
    }
    //Wait for the confirmation when the PLC is ready
    receiveRead(S7_CYCL_READ_IND, msRecieveTimeout);
    //After reading, close the connection
    cyclicReadDeleteRequest(orderid);
    receiveRead(S7_CYCL_READ_DELETE_CNF, msRecieveTimeout);
}

/* delete cyclic read */
void s7Interface::cyclicReadDeleteRequest(ord16 orderid)
{
    int ret=s7_cycl_read_delete_req(m_cpDescr,m_cref,orderid);
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 cyclic read delete request returned: [" << ret << "]";
		shutdown();
    }
}

/* get cyclic read delete confirmation */
void s7Interface::getCyclicReadDeleteConfirmation()
{
    int ret=s7_get_cycl_read_delete_cnf();
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 cyclic read delete confirmation returned: [" << ret << "]";
		shutdown();
    }
}

/* get cyclic read indication */
void s7Interface::getCyclicReadInd()
{
    ord16 var_length=10,result;
    unsigned char data_buffer[10];
    unsigned char *value_array[1];
    value_array[0]=data_buffer;
    int ret=s7_get_cycl_read_ind(( void *)0, &result, &var_length, (void *)value_array);
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << "ERROR: s7 cyclic read ind returned: [" << ret << "]";
		shutdown();
    }
    else
    {
        if(result==S7_RESULT_OK)
        {
			LOG(Log::DBG) << "Received cycle read. Variable length: [" << var_length << "]";
			LOG(Log::DBG) << "Data: [" << (unsigned int) data_buffer[0] << "] , [" << (unsigned int) data_buffer[1] << "] , [" << (unsigned int) data_buffer[2] << "] , [" << (int) data_buffer[3] << "]...";
        }
    }
}

/* get initiate confirmation */
void s7Interface::getInitializeConfirmation()
{
    LOG(Log::TRC) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    		<< " Getting cref. Cp despriptor= " << getCpDescr();

    int ret=s7_get_initiate_cnf();
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    			<< " ERROR: s7 get initiate confirmation returned: [" << ret << "] Cp despriptor= " << getCpDescr();
		shutdown();
    }
    else
    {
    	LOG(Log::DBG) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    			<< "Initiate confirmation received, Cp despriptor= " << getCpDescr();
    }
}

/* initiate connection "TEST" */
void s7Interface::initiateRequest()
{
	LOG(Log::TRC) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__;
	int ret=s7_initiate_req(m_cpDescr,m_cref);
	switch( ret ) {
	case S7_OK:{
		LOG(Log::TRC) << "s7_initiate_req OK";
		break;
	}
	case S7_ERR_RETRY:{
		LOG(Log::ERR) << "s7_initiate_req retry";
		break;
	}
	case S7_ERR:{
		LOG(Log::ERR) << "s7_initiate_req error";
		break;
	}
	}
	//	if(ret!=S7_OK)
	//{
	//LOG(Log::ERR) << "ERROR: s7 get initiate request returned: [" << ret << "]";
	//shutdown();
	//}

}

/* receive any message from communication system */
void s7Interface::receiveInit(int32 last_event_expected, int timeoutMs)
{
	LOG(Log::TRC) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__;

	ord16 orderid;
	int32 msg;
	int noMessageCounter = 0;
    ord16 cref = m_cref;
	clock_t begin_time = clock();
	do
	{
		msg=s7_receive(m_cpDescr,&cref,&orderid);
		switch(msg)
		{
		case S7_NO_MSG:
			noMessageCounter++;
			break;
		case S7_INITIATE_CNF:
			getInitializeConfirmation();
			break;
		default:
			s7_discard_msg();
			break;
		}

	}
	while(msg!=last_event_expected && ((float( clock () - begin_time ) /  CLOCKS_PER_SEC)<(float(timeoutMs)/1000)));
	if(last_event_expected != msg)
	{
		LOG(Log::DBG) << "Timeout!" << std::endl;
	}
	else
	{
		LOG(Log::DBG) << "Message received!"<< std::endl;
	}

}

/* receive any message from communication system */
void s7Interface::receiveRead(int32 last_event_expected, int timeoutMs)
{
	ord16 orderid;
	int32 msg;
	int noMessageCounter = 0;
	ord32 buflenTspp = 0;
	ord8 buffer[S7_MAX_BSEND_BRCV_DATA];
	memset(buffer, 0, S7_MAX_BSEND_BRCV_DATA);
    ord16 cref = m_cref;
	clock_t begin_time = clock();
	do
	{
		msg=s7_receive(m_cpDescr,&cref,&orderid);
		switch(msg)
		{
		case S7_NO_MSG:
		{
			noMessageCounter++;
			break;
		}
		case S7_INITIATE_CNF:
		{
			getInitializeConfirmation();
			break;
		}
		case S7_CYCL_READ_IND:
		{
			getCyclicReadInd();
			break;
		}
		case S7_CYCL_READ_DELETE_CNF:
		{
			getCyclicReadDeleteConfirmation();
			return;
			break;
		}
		case S7_BRCV_IND:
		{
			int ret = s7_get_brcv_ind(buffer,S7_MAX_BSEND_BRCV_DATA,&m_rId,&buflenTspp);
			if (ret == S7_OK)
			{
				ord8* bufferClone = new ord8[S7_MAX_BSEND_BRCV_DATA]; // allocated heap memory! Needs cleaned up by someone.
				std::memcpy(bufferClone, buffer, S7_MAX_BSEND_BRCV_DATA);
				m_TSPPQueue->put(PacketItemPtr(new PacketItem(S7_MAX_BSEND_BRCV_DATA, bufferClone)));
			}
			else
			{
				LOG(Log::ERR) << "ERROR: s7 get block receive indication returned [" << ret << "]";
			}
			break;
		}
		default:
		{
			s7_discard_msg();
			break;
		}
		}
	}
	while(msg!=last_event_expected && ((float( clock () - begin_time ) /  CLOCKS_PER_SEC)<(float(timeoutMs)/1000)));
	if(last_event_expected != msg)
	{
		LOG(Log::TRC)<< "Timeout!";
	}
	else
	{
		LOG(Log::TRC) << "Message received!";
	}
}

/* receive any message from communication system */
void s7Interface::receiveWrite(int32 last_event_expected, int timeoutMs)
{
    ord16 orderid;
    int32 msg;
    int ret;
    int counter = 0;
    int noMessageCounter = 0;
    ord16 cref = m_cref;
    clock_t begin_time = clock();
    do
    {
        msg=s7_receive(m_cpDescr,&cref,&orderid);
        switch(msg)
        {
        case S7_NO_MSG:
            noMessageCounter++;
            break;
        case S7_WRITE_CNF:
            ret = s7_get_write_cnf();
            if (ret !=0)
			{
            	LOG(Log::ERR) << "Get write config returned [" << ret << "]";
			}
            break;
        default:
            s7_discard_msg();
            break;
        }
        counter++;
    }
    while(msg!=last_event_expected && ((static_cast<float>( clock () - begin_time ) /  CLOCKS_PER_SEC)<(static_cast<float>(timeoutMs)/1000)));
    if(last_event_expected != msg)
    {
    	LOG(Log::DBG) << "(Timeout: no message received)";
    }
    else
    {
    	LOG(Log::DBG) << "Message received!";
    }
}

/* get reference for connection to PLC */
void s7Interface::getCRefFromS7(string plcName)
{
    char* nonConstPlcName = const_cast<char*>(plcName.c_str()); // use const_cast
    LOG(Log::DBG) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    		<< " Getting cref. Cp despriptor = [" << m_cpDescr << "], name = [" << nonConstPlcName << "]";
    m_cref = -1;
    int ret=s7_get_cref(m_cpDescr, nonConstPlcName, &m_cref);
    if(ret!=S7_OK)
    {
    	LOG(Log::ERR) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    			<< "ERROR: s7 get cref returned: [" << ret << "]";
		shutdown();
    }
    else
    {
    	LOG(Log::DBG) << __FILE__ << " " << __LINE__ << " " << __FUNCTION__
    			<< "Cref successfully obtained: [" << m_cref << "]";
    }
}
/* initialize s7 */
void s7Interface::initialize(string h1_file, string VFDname)
{
    ord16 number;
    /* initialize s7 */
    int ret=s7_init(const_cast<char*>(h1_file.c_str()) ,const_cast<char*>(VFDname.c_str()) ,&m_cpDescr);
    if(ret!=S7_OK)
    {
        /* something has gone wrong */
    	LOG(Log::ERR) <<  "Error in s7_init, return code [" << ret << "], number = [" << number << "], name = [" << VFDname << "]";
        shutdown();
    }
    else
    {
        /* s7 properly initialized */
    	LOG(Log::DBG) << "S7 successfully initialized; CpDescriptor: " << m_cpDescr << "]";
    }
}

void s7Interface::generateStatistics()
{
	if(STATISTICS_MODE == 0)
	{
		return;
	}
	std::ofstream myfile;
	myfile.open (STATISTICS_FILE);
	myfile << "Package time in server (ms).\n";
	double total = 0;
	int counter = 0;
	for(vector<double>::iterator it = m_statisticsMsPerPackett.begin(); it != m_statisticsMsPerPackett.end(); it++)
	{
		double ms = static_cast<double> (*it);
		myfile << ms << "\n";
		counter++;
		total += ms;
	}
	myfile << "Mean time: " << total/counter << "; Total ms: " << total << " ; Counter: " << counter << std::endl;
	myfile.close();
}

/* end communication */
void s7Interface::shutdown()
{
    stopBlockReceive();

    int ret=s7_shut(m_cpDescr);
    if(ret!=S7_OK)
    {
        /* error has occurred -> exit */
    	LOG(Log::ERR) << "An Error has occurred -> [" << ret << "]";
    }
    else
    {
        /* no error has occurred */
    	LOG(Log::INF) <<  "Connection shut down";
    }
}

ord32 s7Interface::getCpDescr()
{
    return m_cpDescr;
}
ord16 s7Interface::getCref()
{
    return m_cref;
}


/**
 * transform addresses from UNICOS "." notation to siemens s7 "," notation, plus adapt the
 * addressing format.
 * i.e. source= DB100.DBD0 destination= DB100,D0
 */
void s7Interface::transformAddress(std::string source, std::string &destination)
{
	vector <string> fields;
	boost::split( fields, source, boost::is_any_of( "." ) );
	string address = fields[1];
	boost::erase_all(address, "DB");

	if (address.find("F") != std::string::npos)
	{
		boost::erase_all(address, "F");
		boost::erase_all(address, "D");
		string rets = fields[0] + ",REAL" + address;
		destination = rets.c_str();
	}
	else if (address.find("D") != std::string::npos)
	{
		boost::erase_all(address, "D");
		string rets = fields[0] + ",D" + address;
		destination = rets.c_str();
	}
	else if (address.find("W") != std::string::npos)
	{
		boost::erase_all(address, "W");
		string rets = fields[0] + ",W" + address;
		destination = rets.c_str();
	}
	else
	{
		LOG(Log::ERR) << "Error: Invalid address format.";
		destination = "Error";
	}
	LOG(Log::TRC) <<  "transformAddress destination= " << destination;

}
