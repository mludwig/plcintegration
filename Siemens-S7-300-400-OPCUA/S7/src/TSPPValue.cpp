#include "TSPPValue.h"
#include <iostream>
#include <sstream>
#include "PropertiesMap.h"
#include <ctime>
#include "LogIt.h"

using std::dec;
using std::string;

TSPPValue::TSPPValue(uint16_t value, uint16_t dBnumber, uint16_t address)
{
    m_valueUInt16 = value;
    m_valueUInt32 = -1;
    m_valueFloat32 = -1;
    m_type = UINT16;
    m_dBnumber = dBnumber;
    m_address = address;
}

TSPPValue::TSPPValue(uint32_t value, uint16_t dBnumber, uint16_t address)
{
    m_valueUInt16 = -1;
    m_valueUInt32 = value;
    m_valueFloat32 = -1;
    m_type = UINT32;
    m_dBnumber = dBnumber;
    m_address = address;
}

TSPPValue::TSPPValue(float value, uint16_t dBnumber, uint16_t address)
{
    m_valueUInt16 = -1;
    m_valueUInt32 = -1;
    m_valueFloat32 = value;
    m_type = FLOAT32;
    m_dBnumber = dBnumber;
    m_address = address;
}

TSPPValue::~TSPPValue()
{
    //dtor
}

void TSPPValue::printInfo(const XMLConfigObject& xmlConfig)
{
	if(m_type == UINT16)
	{
		LOG(Log::INF) << "Item [" << xmlConfig.findItem(m_dBnumber, m_address) << "] with value [" << m_valueUInt16 << "] (Address: [" << xmlConfig.getFullAddress(m_dBnumber, m_address) << "], db [" << dec << m_dBnumber << "], addr [" << dec << m_address << "])";
	}
	if(m_type == UINT32)
	{
		LOG(Log::INF) << "Item [" << xmlConfig.findItem(m_dBnumber, m_address) << "] with value [" << m_valueUInt32 << "] (Address: [" << xmlConfig.getFullAddress(m_dBnumber, m_address) << "], db [" << dec << m_dBnumber << "], addr [" << dec << m_address << "])";
	}
	if(m_type == FLOAT32)
	{
		LOG(Log::INF) << "Item [" << xmlConfig.findItem(m_dBnumber, m_address) << "] with value [" << m_valueFloat32 << "] (Address: [" << xmlConfig.getFullAddress(m_dBnumber, m_address) << "], db [" << dec << m_dBnumber << "], addr [" << dec << m_address << "])";
	}
}

void TSPPValue::pushInfo(const XMLConfigObject& xmlConfig, string serverTime, int miliseconds)
{
	string address = xmlConfig.getFullAddress(m_dBnumber, m_address);
	if(address.find("F") != string::npos)
	{
		//its a PropertyFloat32
		PropertiesMap::searchFloat32(address)->update(m_valueFloat32, serverTime, miliseconds);
	}
	else if(address.find("DBD") != string::npos)
	{
		//its a UInt32
		PropertiesMap::searchUint32(address)->update(m_valueUInt32, serverTime, miliseconds);
	}
	else
	{
		if(PropertiesMap::searchUint16(address) != 0)
		{
			/*if(m_dBnumber == 100 && m_address == 4)
			{
				LOG(Log::INF) << "Watchdog updated with = " << m_valueUInt16;
			}*/
			//its a UInt16
			PropertiesMap::searchUint16(address)->update(m_valueUInt16, serverTime, miliseconds);
		}
		else
		{
			LOG(Log::DBG) << "Address [" << m_address << "] not found in OPC UA namespace";
		}
	}
}
