#include "TSPPTimestamp.h"
#include <iostream>
#include <sstream>
#include "LogIt.h"

//We define the mapping of the different contents of the buffer (in bytes)
#define YEAR_MAPPING 0
#define MONTH_MAPPING 1
#define DAY_MAPPING 2
#define HOUR_MAPPING 3
#define MINUTE_MAPPING 4
#define SECOND_MAPPING 5
#define MS_MAPPING 6
#define MS_WD_MAPPING 7

using std::string;
using std::stringstream;
using std::dec;
using std::hex;

TSPPTimestamp::TSPPTimestamp(const uint8_t* buffer)
{
    m_year = buffer[YEAR_MAPPING];
    m_month = buffer[MONTH_MAPPING];
    m_day = buffer[DAY_MAPPING];
    m_hour = buffer[HOUR_MAPPING];
    m_minute = buffer[MINUTE_MAPPING];
    m_second = buffer[SECOND_MAPPING];
    uint8_t raw_milisecond = buffer[MS_MAPPING];
    uint8_t raw_msWeekDay = buffer[MS_WD_MAPPING];
    //The last digit of the millisecond is contained in the msWeekDay byte. We extract it.
    std::stringstream msss;
    msss << hex << (raw_milisecond << 4) + (raw_msWeekDay >> 4);
    msss >> dec >> m_milisecond;
    //Extracting the second digit, that contains information about the weekday
    m_weekDay = buffer[MS_WD_MAPPING] & 15;//To extract it we apply an AND bitmask with 00001111
}

TSPPTimestamp::~TSPPTimestamp()
{
    //dtor
}

void TSPPTimestamp::printInfo()
{
	LOG(Log::INF) << "Date: [" << hex << static_cast<int>(m_day) << "/" << hex << static_cast<int>(m_month) << "/" << hex << static_cast<int>(m_year) << " Week day: " << hex << m_weekDay << "]";
	LOG(Log::INF) << "Time: [" << hex << static_cast<int>(m_hour) << "h " << hex << static_cast<int>(m_minute)  << "m " << hex << static_cast<int>(m_second) << "s " << hex << m_milisecond << "ms]";
}

string TSPPTimestamp::getISO8601Time()
{
	stringstream ss;
	if(m_year > 9)
		ss << "20" << hex << static_cast<int>(m_year) << "-";//I hope no1 time travels and uses this server in the past, before the year 2000.
	else
		ss << "200" << hex << static_cast<int>(m_year) << "-";
	if(m_month > 9)
		ss << hex << static_cast<int>(m_month) << "-";
	else
		ss << "0" << hex << static_cast<int>(m_month) << "-";
	if(m_day > 9)
		ss << hex << static_cast<int>(m_day) << "T";
	else
		ss << "0" << hex << (int) m_day << "T";
	if(m_hour > 9)
		ss << hex << static_cast<int>(m_hour) << ":";
	else
		ss << "0" << hex << static_cast<int>(m_hour) << ":";
	if(m_minute > 9)
		ss << hex << static_cast<int>(m_minute) << ":";
	else
		ss << "0" << hex << static_cast<int>(m_minute) << ":";
	if(m_second > 9)
		ss << hex << static_cast<int>(m_second) << "Z";
	else
		ss << "0" << hex << static_cast<int>(m_second) << "Z";
	return ss.str();
}

