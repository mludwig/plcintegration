/*
 * PropertiesMap.cpp
 *
 *  Created on: Sep 18, 2014
 *      Author: dabalo
 */
#include "PropertiesMap.h"

using namespace std;

map<string, Device::DReadableFloat32*> PropertiesMap::g_float32MyHashMap;
map<string, Device::DReadableUInt32*> PropertiesMap::g_uInt32MyHashMap;
map<string, Device::DReadableUInt32*> PropertiesMap::g_uInt32EventHashMap;
map<string, Device::DReadableUInt16*> PropertiesMap::g_uInt16MyHashMap;

PropertiesMap::PropertiesMap()
{}

void PropertiesMap::pushUint16(std::string address, Device::DReadableUInt16* device)
{
	g_uInt16MyHashMap[address] = device;
}
void PropertiesMap::pushUint32(std::string address, Device::DReadableUInt32* device)
{
	g_uInt32MyHashMap[address] = device;
}
void PropertiesMap::pushEventUint32(std::string address, Device::DReadableUInt32* device)
{
	g_uInt32EventHashMap[address] = device;
}
void PropertiesMap::pushFloat32(std::string address, Device::DReadableFloat32* device)
{
	g_float32MyHashMap[address] = device;
}
Device::DReadableUInt16* PropertiesMap::searchUint16(std::string address)
{
	if(g_uInt16MyHashMap.find(address) != g_uInt16MyHashMap.end())
	{
		return g_uInt16MyHashMap[address];
	}
	else
	{
		return 0;
	}
}
Device::DReadableUInt32* PropertiesMap::searchUint32(std::string address)
{
	if(g_uInt32MyHashMap.find(address) != g_uInt32MyHashMap.end())
	{
		return g_uInt32MyHashMap[address];
	}
	else
	{
		return 0;
	}
}
Device::DReadableUInt32* PropertiesMap::searchEventUint32(std::string address)
{
	if(g_uInt32EventHashMap.find(address) != g_uInt32EventHashMap.end())
	{
		return g_uInt32EventHashMap[address];
	}
	else
	{
		return 0;
	}
}
Device::DReadableFloat32* PropertiesMap::searchFloat32(std::string address)
{
	if(g_float32MyHashMap.find(address) != g_float32MyHashMap.end())
	{
		return g_float32MyHashMap[address];
	}
	else
	{
		return 0;
	}
}