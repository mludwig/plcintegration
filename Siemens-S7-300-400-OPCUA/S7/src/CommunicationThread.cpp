#include <iostream>
#include <DRoot.h>
#include "stdio.h"
#include "stdlib.h"
#include <iostream>
#include <termios.h>
#include <string>
#include "CommunicationThread.h"
#include "CommandQueue.h"
#include "CommandItem.h"
#include "LogIt.h"
//Constants
//PLC interface name (for one PLC)
// plc read
const char* plcName0 = "S7_TSPP";
const char* vfd0 = "VFD";

// plc write
const char* plcName1 = "S7_TSPP2"; //
const char* vfd1 = "VFD2";
//XML config file
const char* xmlFile = "config.xml";
//Handshake address
const char* handshakeAddress = "DB100,D0,1";
//time between handshakes (seconds)
const int handshakeDelaySecs = 5;

CommunicationThread::CommunicationThread(SharedPacketQueue* TSPPQueue):
		m_exitFlag(0), m_running(false)
{
	m_TSPPQueue = TSPPQueue;
	m_state = State::HANDSHAKE;
	m_lastHandshakeTime = clock();
}

void sendCommand(int num, char* address, s7Interface &myPLC)
{
    std::string formatedAddr;
    std::string& refFormatedAddr = formatedAddr;
    std::string source(address) ;
	myPLC.transformAddress(source,refFormatedAddr);
	myPLC.writeUInt16(num,formatedAddr);
}

void CommunicationThread::run()
{
	m_running = true;
	s7Interface* PLCInterfaceBRCV = new s7Interface(plcName0, "h1_0", vfd0, m_TSPPQueue);
	s7Interface* PLCInterfaceWrite = new s7Interface(plcName1, "h1_0", vfd1, m_TSPPQueue);
	do
	{
		switch(m_state)
		{
		case State::HANDSHAKE:
			handShake(PLCInterfaceWrite);
			break;
		case State::TSPP_READ:
			TSPPRead(PLCInterfaceBRCV);
			break;
		case State::COMMAND_WRITE:
			commandWrite(PLCInterfaceWrite);
			break;
		default:
			LOG(Log::ERR) << "Error in CommunicationThread::updateState: Unexpected state.";
			break;
		};
		updateState();
	}
	while(!m_exitFlag);
	PLCInterfaceBRCV->generateStatistics();
	PLCInterfaceWrite->generateStatistics();
	delete(PLCInterfaceBRCV);
	delete(PLCInterfaceWrite);
	m_running = false;
}

void CommunicationThread::TSPPRead(s7Interface* myPLCInterface)
{
	LOG(Log::DBG) << "CommunicationThread::TSPPRead Listening...";
	myPLCInterface->blockReceive();
}

void CommunicationThread::handShake(s7Interface* myPLCInterface)
{
	LOG(Log::DBG) << "CommunicationThread::handShake Starting Handshake";
	m_lastHandshakeTime = clock();
	myPLCInterface->writeIP(1, handshakeAddress);
}

void CommunicationThread::commandWrite(s7Interface* myPLCInterface)
{

	if(CommandQueue::commandQueueSize()==0)
		return;//No commands to process
	size_t size = 42;
	LOG(Log::DBG) << "CommunicationThread::commandWrite Polling command queue.";
	do
	{//While there is commands we process them one at a time.
		//extract the command
		CommandItem command = CommandQueue::commandBlockingTake();
		size = CommandQueue::commandQueueSize();
		//process the command
		switch(command.m_type)
		{
		case (CommandItem::UINT16):
		{
			myPLCInterface->writeUInt16((uint16_t)command.m_value_i16, command.m_address);
			LOG(Log::DBG) << "CommunicationThread::commandWrite Writing uint16. Writing [" << command.m_value_i16 << "] in address [" << command.m_address << "]";
			break;
		}
		case (CommandItem::UINT32):
		{
			myPLCInterface->writeUInt32((uint32_t)command.m_value_i32, command.m_address);
			LOG(Log::DBG) << "CommunicationThread::commandWrite Writing uint32. Writing [" << command.m_value_i32 << "] in address [" << command.m_address << "]";
			break;
		}
		case (CommandItem::FLOAT32):
		{
			myPLCInterface->writeFloat(command.m_value_f, command.m_address);
			LOG(Log::DBG) << "CommunicationThread::commandWrite Writing float32. Writing [" << command.m_value_f << "] in address [" << command.m_address << "]";
			break;
		}
		};
	}
	while(size != 0);
}

/**
 * simple state machine implementation
 * 3 states:
 * state_command: empty the command queue, send to PLC
 * state_handshake: tell PLC the server is alive by writing the IP address into a certain mem location each 4 secs
 * state_tspp: pushes tspp frames to the shared queue to be picked up tby the tspp thread
 *     which then decodes and digests any TSPP frames, publish to AS
 *
 * transitions:
 * state_handshake->state_command
 * state_tspp->state_command
 * state_command->[state_handshake (if timeout) OR state_tspp]
 *
 * the resulting behavior is:
 * at init, after a successful handshake, a command is read from the queue. If the command queue is empty after processing has finished
 * either another handshake is done (if the last handshake is older than 4 sec) or any arriving tspp frames are processed.
 * Effectively, tspp frames are processed all the time, but they are preceded by any commands and a regular timeout.
 */
void CommunicationThread::updateState()
{
	switch(m_state)
	{
	case State::HANDSHAKE:
	{
		m_state = State::COMMAND_WRITE;
		break;
	}
	case State::TSPP_READ:
	{
		m_state = State::COMMAND_WRITE;
		break;
	}
	case State::COMMAND_WRITE:
	{
		if((float( clock() - m_lastHandshakeTime ) / CLOCKS_PER_SEC)>=(float)handshakeDelaySecs)
		{
			m_state = State::HANDSHAKE;
		}
		else
		{
			m_state = State::TSPP_READ;
		}
		break;
	}
	default:
	{
		LOG(Log::ERR) << "Error in CommunicationThread::updateState: Unexpected state.";
		break;
	}
	};
}
