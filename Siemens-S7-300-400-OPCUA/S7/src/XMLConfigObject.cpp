#include "XMLConfigObject.h"
#include <iostream>
#include <iterator>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <vector>
#include "LogIt.h"

using std::string;
using std::stringstream;
using std::istringstream;
using std::vector;

using boost::split;
using boost::is_any_of;
using boost::erase_all;

XMLConfigObject::XMLConfigObject(string xmlPath)
{
    //The device list is instantiated from the xml file
    try
    {
        m_devLis = Configuration::configuration(xmlPath);
        ::Configuration::Configuration::Device_sequence& ds (m_devLis-> Device ());

		for (::Configuration::Configuration::Device_iterator i (ds.begin ()); i != ds.end (); ++i)
		{
			::Configuration::Configuration::Device_type& temp_device = *i;

			::Configuration::Configuration::Device_type::ReadableUInt32_sequence& seq1 (temp_device.ReadableUInt32());
			for (::Configuration::Configuration::Device_type::ReadableUInt32_sequence::iterator o (seq1.begin ()); o != seq1.end (); ++o)
			{
				::Configuration::Configuration::Device_type::ReadableUInt32_type& temp_property = *o;
				AddressKey ak(getDbNumber(temp_property.address()),getAddress(temp_property.address()));
				m_addressMap[ak] = temp_property.address();
			}

			::Configuration::Configuration::Device_type::ReadableUInt16_sequence& seq2 (temp_device.ReadableUInt16());
			for (::Configuration::Configuration::Device_type::ReadableUInt16_sequence::iterator o (seq2.begin ()); o != seq2.end (); ++o)
			{
				::Configuration::Configuration::Device_type::ReadableUInt16_type& temp_property = *o;
				AddressKey ak(getDbNumber(temp_property.address()),getAddress(temp_property.address()));
				m_addressMap[ak] = temp_property.address();
			}

			::Configuration::Configuration::Device_type::ReadableFloat32_sequence& seq3 (temp_device.ReadableFloat32());
			for (::Configuration::Configuration::Device_type::ReadableFloat32_sequence::iterator o (seq3.begin ()); o != seq3.end (); ++o)
			{
				::Configuration::Configuration::Device_type::ReadableFloat32_type& temp_property = *o;
				AddressKey ak(getDbNumber(temp_property.address()),getAddress(temp_property.address()));
				m_addressMap[ak] = temp_property.address();
			}
		}
    }
    catch (const xml_schema::exception& e)
    {
    	LOG(Log::ERR) << "An error has occurred [" << e << "]";
    }
}

XMLConfigObject::~XMLConfigObject()
{
    //dtor
}

string XMLConfigObject::getFullAddress(uint16_t dBnumber, uint16_t address) const
{
	AddressKey ak(dBnumber, address);
	if(m_addressMap.find(ak) != m_addressMap.end())
	{
		return m_addressMap.find(ak)->second;
	}
	else
	{
		LOG(Log::DBG) << "Address not found: DBNumber = [" << dBnumber << "] , data block address = [" << address << "]";
		return "Error";
	}
}

string XMLConfigObject::findItem(uint16_t dBnumber, uint16_t address) const
{
    ::Configuration::Configuration::Device_sequence& ds (m_devLis-> Device ());
    for (::Configuration::Configuration::Device_iterator i (ds.begin ()); i != ds.end (); ++i)
    {

        ::Configuration::Configuration::Device_type& temp_device = *i;

        ::Configuration::Configuration::Device_type::ReadableUInt32_sequence& seq1 (temp_device.ReadableUInt32());
        for (::Configuration::Configuration::Device_type::ReadableUInt32_sequence::iterator o (seq1.begin ()); o != seq1.end (); ++o)
        {
            ::Configuration::Configuration::Device_type::ReadableUInt32_type& temp_property = *o;
            if(dBnumber == getDbNumber(temp_property.address()) && address == getAddress(temp_property.address()))
            {
                stringstream ss;
                ss << temp_device.name() << "." << temp_property.name();
                return ss.str();
            }
        }

        ::Configuration::Configuration::Device_type::ReadableUInt16_sequence& seq2 (temp_device.ReadableUInt16());
        for (::Configuration::Configuration::Device_type::ReadableUInt16_sequence::iterator o (seq2.begin ()); o != seq2.end (); ++o)
        {
            ::Configuration::Configuration::Device_type::ReadableUInt16_type& temp_property = *o;
            if(dBnumber == getDbNumber(temp_property.address()) && address == getAddress(temp_property.address()))
            {
                stringstream ss;
                ss << temp_device.name() << "." << temp_property.name();
                return ss.str();
            }
        }

        ::Configuration::Configuration::Device_type::ReadableFloat32_sequence& seq3 (temp_device.ReadableFloat32());
        for (::Configuration::Configuration::Device_type::ReadableFloat32_sequence::iterator o (seq3.begin ()); o != seq3.end (); ++o)
        {
            ::Configuration::Configuration::Device_type::ReadableFloat32_type& temp_property = *o;
            if(dBnumber == getDbNumber(temp_property.address()) && address == getAddress(temp_property.address()))
            {
                stringstream ss;
                ss << temp_device.name() << "." << temp_property.name();
                return ss.str();
            }
        }
    }
    LOG(Log::ERR) << "Address not found: DBNumber = [" << dBnumber << "] , data block address = [" << address << "]";
    return "-1";
}

void XMLConfigObject::printInfo()
{
    ::Configuration::Configuration::Device_sequence& ds (m_devLis-> Device ());
    for (::Configuration::Configuration::Device_iterator i (ds.begin ()); i != ds.end (); ++i)
        {

            ::Configuration::Configuration::Device_type& temp_device = *i;
            LOG(Log::DBG) << "DEVICE = [" << temp_device.name() << "]";

            ::Configuration::Configuration::Device_type::ReadableUInt32_sequence& seq1 (temp_device.ReadableUInt32());
            for (::Configuration::Configuration::Device_type::ReadableUInt32_sequence::iterator o (seq1.begin ()); o != seq1.end (); ++o)
            {
                ::Configuration::Configuration::Device_type::ReadableUInt32_type& temp_property = *o;
                LOG(Log::DBG) << "[" << temp_property.name() << "] address is [" << temp_property.address() << "]";
            }

            ::Configuration::Configuration::Device_type::ReadableUInt16_sequence& seq2 (temp_device.ReadableUInt16());
            for (::Configuration::Configuration::Device_type::ReadableUInt16_sequence::iterator o (seq2.begin ()); o != seq2.end (); ++o)
            {
                ::Configuration::Configuration::Device_type::ReadableUInt16_type& temp_property = *o;
                LOG(Log::DBG) << "[" << temp_property.name() << "] address is [" << temp_property.address() << "]";
            }

            ::Configuration::Configuration::Device_type::ReadableFloat32_sequence& seq3 (temp_device.ReadableFloat32());
            for (::Configuration::Configuration::Device_type::ReadableFloat32_sequence::iterator o (seq3.begin ()); o != seq3.end (); ++o)
            {
                ::Configuration::Configuration::Device_type::ReadableFloat32_type& temp_property = *o;
                LOG(Log::DBG) << "[" << temp_property.name() << "] address is [" << temp_property.address() << "]";
            }
        }
}

int XMLConfigObject::getDbNumber(string fullAddress) const
{
    vector <string> fields;
    split( fields, fullAddress, is_any_of( "." ) );
    string number = fields[0];
    erase_all(number, "DB");
    int ret;
    istringstream ( number ) >> ret;
    return ret;
}
int XMLConfigObject::getAddress(string fullAddress) const
{
    vector <string> fields;
    split( fields, fullAddress, is_any_of( "." ) );
    string address = fields[1];
    erase_all(address, "DB");
    erase_all(address, "D" );
    erase_all(address, "W" );
    erase_all(address, "F" );
    int ret;
    istringstream ( address ) >> ret;
    return ret;
}
XMLConfigObject::Type XMLConfigObject::getType(string fullAddress) const
{
    vector <string> fields;
    split( fields, fullAddress, is_any_of( "." ) );
    string address = fields[1];
    erase_all(address, "DB");

    if (address.find("F") != std::string::npos)
    {
        return FLOAT32;
    }
    else if (address.find("D") != std::string::npos)
    {
        return UINT32;
    }
    else if (address.find("W") != std::string::npos)
    {
        return UINT16;
    }
    else
    {
    	LOG(Log::ERR) << "Error: Address in wrong format";
        return UNKNOWN;
    }
}
