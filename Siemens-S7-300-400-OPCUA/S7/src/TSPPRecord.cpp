#include "TSPPRecord.h"
#include <iostream>
#include <iterator>
#include "LogIt.h"
#include "DriverUtils.h"

//We define the mapping of the different contents of the buffer (in bytes)
#define g_sTimestampMapping 0
#define g_sDBNumberMapping 8
#define g_sStartAddressMapping 10
#define g_sFirstValueMapping 12

using std::string;
using std::vector;
using std::dec;

TSPPRecord::TSPPRecord(const uint8_t wordsPerRecord, const uint16_t recordLength/*IN BYTES*/, const uint8_t* buffer, const XMLConfigObject& xmlConfig)
{
	m_values.reserve(static_cast<int>(wordsPerRecord));
    //We create a new timestamp, passing the buffer position where the timestamp starts as a parameter.
    const uint8_t* bufferTimeStamp = buffer + g_sTimestampMapping;
    m_timeStamp = new TSPPTimestamp(bufferTimeStamp);
    //We extract DB Number, taking its 2 bytes into account
    m_dBnumber = DriverUtils::getWordFromBytes(buffer[g_sDBNumberMapping] , buffer[g_sDBNumberMapping + 1]);
    //We extract Start Address, taking its 2 bytes into account
    m_startAddress = DriverUtils::getWordFromBytes(buffer[g_sStartAddressMapping] , buffer[g_sStartAddressMapping + 1]);
    //We point currentRecordBuffer to the adress of the first value, in orderr to start the loop
    const uint8_t* currentRecordBuffer = buffer + g_sFirstValueMapping;
    for(int i = 0; i < wordsPerRecord; i++)
    {
    	string address = xmlConfig.getFullAddress(m_dBnumber, m_startAddress+(i*2));
    	if(address.find("Error") != std::string::npos)
    	{
			continue;
    	}
    	XMLConfigObject::Type type = xmlConfig.getType(address);
    	switch(type)
    	{
    	case XMLConfigObject::Type::UINT16:
    	{
    		//We extract the value of the current word
			uint16_t tempValue = DriverUtils::getWordFromBytes(currentRecordBuffer[0] , currentRecordBuffer[1]);
			//We create a new TSPP Value, and store it on the vector m_values
			TSPPValue* value16 = new TSPPValue(tempValue, m_dBnumber, m_startAddress+(i*2));
			m_values.push_back(value16);
			//We move the pointer to the next WORD
			currentRecordBuffer += 2;
			break;
    	}
    	case XMLConfigObject::Type::UINT32:
    	{
    		//We extract the value of the current word
			uint32_t tempValue32 = DriverUtils::getUint32FromBytes(currentRecordBuffer[0] , currentRecordBuffer[1] , currentRecordBuffer[2] , currentRecordBuffer[3]);
			//We create a new TSPP Value, and store it on the vector m_values
			TSPPValue* value32 = new TSPPValue(tempValue32, m_dBnumber, m_startAddress+(i*2));
			m_values.push_back(value32);
			//We move the pointer to the next WORD
			currentRecordBuffer += 4;
			i++;
    		break;
    	}
    	case XMLConfigObject::Type::FLOAT32:
    	{
    		//We extract the value of the current word
    		uint32_t tempValue32 = DriverUtils::getUint32FromBytes(currentRecordBuffer[0] , currentRecordBuffer[1] , currentRecordBuffer[2] , currentRecordBuffer[3]);
    		float tempValueFloat = 0;
    		memcpy(&tempValueFloat, &tempValue32, sizeof(tempValue32));
			//We create a new TSPP Value, and store it on the vector m_values
			TSPPValue* valuef = new TSPPValue(tempValueFloat, m_dBnumber, m_startAddress+(i*2));
			m_values.push_back(valuef);
			//We move the pointer to the next WORD
			currentRecordBuffer += 4;
			i++;
			break;
    	}
    	case XMLConfigObject::Type::UNKNOWN:
    	{
    		LOG(Log::ERR) << "Error: Unknown address format";
			break;
    	}
    	}
    }

}

TSPPRecord::~TSPPRecord()
{
    delete m_timeStamp;
    for(vector<TSPPValue*>::iterator it = m_values.begin(); it != m_values.end(); it++)
    {
        TSPPValue* value = *it;
		delete value;
    }
}
void TSPPRecord::printInfo(const XMLConfigObject& xmlConfig)
{
	LOG(Log::INF) << "DB Number: [" << dec << m_dBnumber << "]";
	LOG(Log::INF) << "Start Address: [" << dec << m_startAddress << "]";
    m_timeStamp->printInfo();

    for(vector<TSPPValue*>::iterator it = m_values.begin(); it != m_values.end(); it++)
    {
        TSPPValue* value = *it;
        value->printInfo(xmlConfig);
    }
}

void TSPPRecord::pushInfo(const XMLConfigObject& xmlConfig)
{
    for(vector<TSPPValue*>::iterator it = m_values.begin(); it != m_values.end(); it++)
    {
        TSPPValue* value = *it;
        value->pushInfo(xmlConfig, m_timeStamp->getISO8601Time(), m_timeStamp->getMilisecond());
    }
}
