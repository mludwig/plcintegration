#include "TSPPPacket.h"
#include <iostream>
#include <iterator>
#include "LogIt.h"
#include "DriverUtils.h"

//We define the mapping of the different contents of the buffer (in bytes)
const unsigned int g_sTMapping = 0;
const unsigned int g_sSMapping = 1;
const unsigned int g_sPMapping = 2;
const unsigned int g_sWordsPerRecordMapping = 3;
const unsigned int g_sPacketLengthMapping = 4;
const unsigned int g_sFirstRecordMapping = 6;

using std::vector;

/**
 * creator does all the processing
 */
TSPPPacket::TSPPPacket(const unsigned int rec_buf_len, const uint8_t* buffer, const XMLConfigObject& xmlConfig)
{
    //We check the header to see if the packet is corrupt or not
    if(!(buffer[g_sTMapping] == 'T' || buffer[g_sSMapping] == 'S' || buffer[g_sPMapping] == 'P'))//The first 3 bytes should be TSP. If not something werid happened.
    {
    	LOG(Log::ERR) << "ERROR! Corrupt TSPP Packet";
        m_corrupt = true;
    }
    else
    {
    	LOG(Log::DBG) << "Creating TSPP Packet from a buffer of bytes";
        m_corrupt = false;
        //The data is not corrupt, so we decode it.
        //---HEADER---//
        m_wordsPerRecord = buffer[g_sWordsPerRecordMapping];
        m_packetLength = DriverUtils::getWordFromBytes(buffer[g_sPacketLengthMapping] , buffer[g_sPacketLengthMapping + 1]);
        //We calculate the number of words each record has, and multiply it for 2 to get the bytes per record
        m_recordLengthBytes = (6 + m_wordsPerRecord) * 2;
        //We divide the block length (not counting the header) for the length of each record to get the number of records
        m_numberOfRecords = m_packetLength / (6 + m_wordsPerRecord);

        //---BODY---//
        //We do an iteration of this loop per number of records.
        const uint8_t* currentRecordBuffer = buffer + g_sFirstRecordMapping;
        for(int i = 0; i < m_numberOfRecords; i++)
        {
            //We create a new TSPP Record, giving as parameters the length of the block and the buffer, starting in the position where the record starts
            TSPPRecord* record = new TSPPRecord(m_wordsPerRecord, m_packetLength * 2, currentRecordBuffer, xmlConfig);
            m_records.push_back(record);
            currentRecordBuffer += m_recordLengthBytes;
        }
        LOG(Log::DBG) << "[" << m_numberOfRecords << "] Records created. Individual length: [" << (int) m_wordsPerRecord << "]";
    }
}

TSPPPacket::~TSPPPacket()
{
    for(vector<TSPPRecord*>::iterator it = m_records.begin(); it != m_records.end(); it++)
    {
        TSPPRecord* record = *it;
		delete record;
    }
}

void TSPPPacket::printInfo(const XMLConfigObject& xmlConfig)
{
    if(!m_corrupt)
    {
    	LOG(Log::INF) << "Record Lenght: [" << (int) m_wordsPerRecord << "]";
    	LOG(Log::INF) << "Block Lenght: [" << m_packetLength << "]";
    	LOG(Log::INF) << "Number of Records in the block: " << m_numberOfRecords << "]";
        //We do an iteration of this loop per number of records.
        for(vector<TSPPRecord*>::iterator it = m_records.begin(); it != m_records.end(); it++)
        {
            TSPPRecord* record = *it;
            record->printInfo(xmlConfig);
        }
    }
    else
    {
    	LOG(Log::ERR) << "Error: Corrupt TSPP Packet";
    }
}

void TSPPPacket::pushInfo(const XMLConfigObject& xmlConfig)
{
    if(!m_corrupt)
    {
    	LOG(Log::DBG) << "Pushing TSPP Packet";
        for(vector<TSPPRecord*>::iterator it = m_records.begin(); it != m_records.end(); it++)
        {
            TSPPRecord* record = *it;
            record->pushInfo(xmlConfig);
        }
    }
    else
    {
    	LOG(Log::ERR) << "Error: Corrupt TSPP Packet";
    }
}
