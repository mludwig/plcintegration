#ifndef S7INTERFACE_H
#define S7INTERFACE_H

#include "s7interface.h"
#include "sapi_s7.h"
#include "TSPPPacket.h"
#include "SharedQueue.h"
#include "PacketItem.h"
#include <queue>
#include <vector>
#include <string>

typedef SharedQueue<PacketItemPtr> SharedPacketQueue;

/**
* Class s7Interface
* Intended as a wrapper of some the functionalities of the Softnet library, the connection with the PLC
* is stablished in the constructor, and calling the public methods in the correct order will let you use
* the Softnet library functionality with ease.
**/
class s7Interface
{
    public:
        //Constructor of the class. Receives the plc name as a parameter and initializes the connection
        s7Interface(std::string plcName, std::string h1_file, std::string VFDname, SharedPacketQueue* TSPPQueue);
        //Destructor of the class.
        virtual ~s7Interface();
        //Do a cyclic read in the specified address. receive needs to be called afterwards.
        void cyclicRead(ord16 orderid, std::string s7Adress);
        //Start the block receive.
        void blockReceive();
        //Write an IP on the provided address. receive needs to be called afterwards.
        void writeIP(ord16 orderid, std::string s7Adress);
        //Write an unsigned integer of 2 bytes in the address specified
        void writeUInt16(uint16_t value, std::string s7Adress);
        //Write an unsigned integer of 4 bytes in the address specified
        void writeUInt32(uint32_t value, std::string s7Adress);
        //Write a float of 4 bytes in the address specified
        void writeFloat(float value, std::string s7Adress);
        //Generates Statistics about the time spent doing different operations
        void generateStatistics();
        //Transforms an address to the format used by softnet
        static void transformAddress(std::string source, std::string& destination);
        ord32 getCpDescr();
        ord16 getCref();

    protected:
    private:
        std::queue<std::string> m_queue;
        std::vector<double> m_statisticsMsPerPackett;

        //Internal parameter used by softnet library to identify the connection
        ord32 m_cpDescr;
        //Internal parameter used by softnet library to identify the connection
        ord16 m_cref;
        //Internal parameter used by softnet library to identify the connection
        ord32 m_rId;
        //Shared queue to store raw TSPP packets and then process them in a different thread.
        SharedPacketQueue* m_TSPPQueue;

        //This should be called after every other public method to collect the result, with the exception of my abort
		void receiveInit(int32 last_event_expected, int timeoutMs);
		void receiveRead(int32 last_event_expected, int timeoutMs);
		void receiveWrite(int32 last_event_expected, int timeoutMs);

        void initiateBlockReceive();
        void stopBlockReceive();

        void processBlockReceive(const unsigned int rec_buf_len, const uint8_t* buffer);

        void initialize(std::string h1_file, std::string VFDname);
        void shutdown();
        void initiateRequest();
        void getCRefFromS7(const std::string plcName);

        void getInitializeConfirmation();

        void cyclicReadDeleteRequest(ord16 orderid);
        void getCyclicReadDeleteConfirmation();
        void getCyclicReadInd();
};

#endif // S7INTERFACE_H
