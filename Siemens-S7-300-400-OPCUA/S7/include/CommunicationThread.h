/*
 * s7protmain.h
 *
 *  Created on: Sep 12, 2014
 *      Author: dabalo
 */

#ifndef COMMUNICATIONTHREAD_H_
#define COMMUNICATIONTHREAD_H_
#include "XMLConfigObject.h"
#include "s7interface.h"
#include "SharedQueue.h"
/*
 * Thread in charge of Communicating with the PLC
 * This thread has 3 different states.
 * - In the state TSPP_READ it will receive TSPP packets from the PLC and push them into a Shared Queue, so a different thread can process them.
 * 		- This state will be triggered when the other 2 states release the thread
 * - In the state HANDSHAKE there is a handshake communication with the PLC, writing our IP in a certain memory address, so the PLC knows that we are still there
 * 		- This state is triggered once each ~~4 seconds
 * - In the state COMMAND_WRITE all the commands inserted in the command queue (by the OPC UA thread) will be executed
 * 		- This state is triggered whenever the command queue is not empty
 */
class CommunicationThread
{
public:
	/*
	 * State that lets you know what the thread is doing at the moment
	 */
	enum State { TSPP_READ, HANDSHAKE, COMMAND_WRITE};
	/*
	 * Main constructor
	 */
	CommunicationThread(SharedPacketQueue*);
	/*
	 * Main loop of the thread. It will loop until exit() is called doing the different operations and updating the sate accordingly
	 */
	void run();
	/*
	 * The thread will exit after this method is called
	 */
	void exit() {m_exitFlag=1;}
	/*
	 * Is the thread still running?
	 */
	bool running(){return m_running;}
private:
	/*
	 * Updates the current state of the thread to decide which one will be the next operation
	 */
	void updateState();
	/*
	 * Receives data from the PLC and puts it into the packet queue
	 */
	void TSPPRead(s7Interface*);
	/*
	 * Handshake communication with the PLC, writing our IP in a certain memory address, so the PLC knows that we are still there
	 */
	void handShake(s7Interface*);
	/*
	 * All the commands inserted in the command queue (by the OPC UA thread) will be executed on this method.
	 */
	void commandWrite(s7Interface*);
	volatile bool m_exitFlag;
	bool m_running;
	SharedPacketQueue* m_TSPPQueue;
	//State that lets you know what the thread is doing at the moment
	State m_state;
	//Time when the last handshake happened.
	clock_t m_lastHandshakeTime;
};

#endif /* COMMUNICATIONTHREAD_H_ */
