/*
 * AddressKey.h
 *
 *  Created on: Oct 1, 2014
 *      Author: dabalo
 */

#ifndef ADDRESSKEY_H_
#define ADDRESSKEY_H_

/**
 * Class used by XMLConfigObject that represents a full address in the siemens PLC memory.
 * This address is represented by 2 numbers, DBNumber and Address
 */
class AddressKey
{
public:
	/*
	 * Main constructor of the class
	 */
	AddressKey(uint16_t dBnumber, uint16_t address)
	{
		m_dBnumber = dBnumber;
		m_address = address;
	}
	uint16_t getDBnumber() const { return m_dBnumber; }
	uint16_t getAddress() const { return m_address; }
	/*
	 * Overload of the == operator. It will return true if both numbers are equals
	 * in the 2 objects that are being compared.
	 */
	inline bool operator==(const AddressKey& other) const
	{
		return (getDBnumber() == other.getDBnumber() && getAddress() == other.getAddress());
	}
	/*
	 * Overload of the operator !=. It will be true when == is false
	 */
	inline bool operator!=(const AddressKey& other) const
	{
		return !operator == (other);
	}
	/*
	 * Overload of operator <. If the DBNumber is equal in the two object the address is compared.
	 * Otherwise, the DBNumber is compared.
	 */
	inline bool operator< (const AddressKey& other) const
	{
		if(this->getDBnumber() == other.getDBnumber())
		{
			return this->getAddress() < other.getAddress();
		}
		return this->getDBnumber() < other.getDBnumber();
	}
	/*
	 * Overload of operator >. If the DBNumber is equal in the two object the address is compared.
	 * Otherwise, the DBNumber is compared.
	 */
	inline bool operator> (const AddressKey& other) const
	{
		if(this->getDBnumber() == other.getDBnumber())
		{
			return this->getAddress() > other.getAddress();
		}
		return this->getDBnumber() > other.getDBnumber();
	}
	/*
	 * Overload of operator <=, negating operator >
	 */
	inline bool operator<=(const AddressKey& other) const
	{
		return !operator> (other);
	}
	/*
	 * Overload of operator >=, negating operator <
	 */
	inline bool operator>=(const AddressKey& other) const
	{
		return !operator< (other);
	}
private:
	//DBNumner (Part of the full address)
	uint16_t m_dBnumber;
	//Address inside the Data Block
	uint16_t m_address;
};


#endif /* ADDRESSKEY_H_ */
