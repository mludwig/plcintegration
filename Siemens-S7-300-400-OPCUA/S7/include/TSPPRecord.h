#ifndef TSPPRECORD_H
#define TSPPRECORD_H
#include <stdint.h>
#include <vector>
#include "TSPPValue.h"
#include "TSPPTimestamp.h"

/**
* Class TSPPRecord
* Encapsulates a TSPP Record. The constructor receives a buffer of bytes and it's length, and it decodes it.
* With that, it extracts the following information: Data Block Number, Starting Address and Time Stamp.
* Apart from that, It also creates a list of TSPPValue objects, as one record may contain several Values.
**/
class TSPPRecord
{
    public:
        //Constructor of the class, receives a buffer of bytes, which gets decoded into different class parameters
        TSPPRecord(const uint8_t wordsPerRecord, const uint16_t recordLength/*IN BYTES*/, const uint8_t* buffer, const XMLConfigObject& xmlConfig);
        //Destructor of the class
        virtual ~TSPPRecord();
        uint16_t getDBnumber() { return m_dBnumber; }
        uint16_t getStartAddress() { return m_startAddress; }
        //Prints on screen the decoded content of the record.
        void printInfo(const XMLConfigObject& xmlConfig);
        //Pushes the data to the OPC UA Namespace
        void pushInfo(const XMLConfigObject& xmlConfig);
    protected:
    private:
        //Data Block Number. This is the datablock that contains the information that we are receiveing.
        //Must be used to identify what is the data contained by the TSPP Values
        uint16_t m_dBnumber;
        //Starting address inside the datablock.
        //Must be used to identify what is the data contained by the TSPP Values
        uint16_t m_startAddress;
        //Timestamp of the values, containing date and time.
        TSPPTimestamp* m_timeStamp;
        //List of the values contained in the Record
        std::vector<TSPPValue*> m_values;
};

#endif // TSPPRECORD_H
