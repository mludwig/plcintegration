/*
 * s7protmain.h
 *
 *  Created on: Sep 12, 2014
 *      Author: dabalo
 */

#ifndef TSPPPROCESSINGTHREAD_H_
#define TSPPPROCESSINGTHREAD_H_
#include "SharedQueue.h"
#include "XMLConfigObject.h"
#include "PacketItem.h"

typedef SharedQueue<PacketItemPtr> SharedPacketQueue;

/*
 * Thread in charge of Processing the TSPP Packets.
 * The packets will be stored in TSPPQueue by the communication thread, and processed in this one to lower the work load
 * of the communication thread.
 */
class TSPPProcessingThread
{
public:
	/*
	 * Main constructor
	 */
	TSPPProcessingThread(SharedPacketQueue* TSPPQueue);
	/*
	 * Process an array of bytes, converting it into a TSPPPacket object and pushing the information that it contains into
	 * the OPC UA namespace.
	 */
	void processTSPPPacket(const unsigned int rec_buf_len, const uint8_t* buffer);
	/*
	 * Main loop of the thread.
	 * Polls the TSPPQueue constantly, and process packets when they are available.
	 */
	void run();
	/*
	 * Ends the thread
	 */
	void exit() {m_exitFlag=1;}
	//Is the thread still running?
	bool running(){return m_running;}
private:
	//When it is set to 1 the main loop will stop
	volatile bool m_exitFlag;
	//Is the thread still running?
	bool m_running;
	const XMLConfigObject m_xmlConfig;
	//Packet queue that will be processed
	SharedPacketQueue* m_TSPPQueue;
};

#endif /* TSPPPROCESSINGTHREAD_H_ */
