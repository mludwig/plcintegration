#ifndef TSPPVALUE_H
#define TSPPVALUE_H
#include <stdint.h>
#include "XMLConfigObject.h"
/**
* Class TSPPValue
* Encapsulates a TSPP Value. It receives a word of information (2 bytes) in the form of a uint_16.
**/
class TSPPValue
{
    public:
		//The different Types of values supported, based on the datatype.
		enum Type { UINT16, UINT32, FLOAT32};
        //Constructor of the class
        TSPPValue(uint16_t value, uint16_t dBnumber, uint16_t address);
        TSPPValue(uint32_t value, uint16_t dBnumber, uint16_t address);
        TSPPValue(float value, uint16_t dBnumber, uint16_t address);
        //Destructor of the class
        virtual ~TSPPValue();
        uint16_t getValueUInt16() { return m_valueUInt16; }
        uint32_t getValueUInt32() { return m_valueUInt32; }
        float getValueFloat32() { return m_valueFloat32; }
        char getType() {return m_type;}
        uint16_t getDBnumber() { return m_dBnumber; }
        uint16_t getAddress() { return m_address; }
        //Prints on screen the decoded content of the value.
        void printInfo(const XMLConfigObject& xmlConfig);
        //Pushes data into the OPC UA Namespace
        void pushInfo(const XMLConfigObject& xmlConfig, std::string serverTime, int miliseconds);
    protected:
    private:
        //Value if the type is uint16
        uint16_t m_valueUInt16;
        //Value if the type is uint32
        uint32_t m_valueUInt32;
        //Value if the type is Float32
        float m_valueFloat32;
        //Type of the value
        Type m_type;
        //DBNumber
        uint16_t m_dBnumber;
        //PLC memory address
        uint16_t m_address;
};

#endif // TSPPVALUE_H
