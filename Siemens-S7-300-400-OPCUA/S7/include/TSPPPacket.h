#ifndef TSPPPACKET_H
#define TSPPPACKET_H
#include "sapi_s7.h"
#include "TSPPRecord.h"
#include <stdint.h>
#include <vector>

/**
* Class TSPPPacket
* Encapsulates a full TSPP Packet. The constructor receives a buffer of bytes and it's length, and it decodes it.
* With that, it extracts the following information: Words per record, packet length, number of records, and record length (in bytes).
* Apart from that, It also creates a list of TSPPRecord objects, as one packet may contain several Records.
**/
class TSPPPacket
{
    public:
        //Constructor of the class, receives a buffer of bytes, which gets decoded into different class parameters
        TSPPPacket(const unsigned int rec_buf_len, const uint8_t* buffer, const XMLConfigObject& xmlConfig);
        //Destructor of the class.
        virtual ~TSPPPacket();
        //Prints on screen the decoded content of the packet.
        void printInfo(const XMLConfigObject& xmlConfig);
        //Pushes information of the packet into the OPC UA Server
        void pushInfo(const XMLConfigObject& xmlConfig);
    protected:
    private:
        //How many WORDs has each record?
        uint8_t  m_wordsPerRecord;
        //packet lenght in WORDS not counting the header
        uint16_t m_packetLength;
        //How many recods does the message have?
        uint16_t m_numberOfRecords;
        //Record length in bytes.
        uint16_t m_recordLengthBytes;
        //List of Records inside the packet
        std::vector<TSPPRecord*> m_records;
        //Is the packet corrupt?
        bool m_corrupt;
};

#endif // TSPPPACKET_H
