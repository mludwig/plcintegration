#ifndef TSPPTIMESTAMP_H
#define TSPPTIMESTAMP_H
#include <stdint.h>
#include <string>

/**
* Class TSPPTimestamp
* Encapsulates a TSPP Timestamp. The constructor receives a buffer of bytes and it decodes it.
* With that, it extracts the following information: Year, month, day, hour, minute, second, milisecond and weekday.
**/
class TSPPTimestamp
{
    public:
        //Constructor of the class, receives a buffer of bytes, which gets decoded into different class parameters
        TSPPTimestamp(const uint8_t* buffer);
        //Destructor of the class
        virtual ~TSPPTimestamp();
        uint8_t getYear() { return m_year; }
        uint8_t getMonth() { return m_month; }
        uint8_t getDay() { return m_day; }
        uint8_t getHour() { return m_hour; }
        uint8_t getMinute() { return m_minute; }
        uint8_t getSecond() { return m_second; }
        int getMilisecond() { return m_milisecond; }
        int getWeekDay() { return m_weekDay; }
        /*
         * Get date and time in format ISO8601 (Does not include milliseconds
         */
        std::string getISO8601Time();
        //Prints on screen the decoded content of the timestamp.
        void printInfo();
    protected:
    private:
        //Timestamp's year
        uint8_t m_year;
        //Timestamp's month
        uint8_t m_month;
        //Timestamp's day
        uint8_t m_day;
        //Timestamp's hour
        uint8_t m_hour;
        //Timestamp's minute
        uint8_t m_minute;
        //Timestamp's second
        uint8_t m_second;
        //Timestamp's milisecond
        int m_milisecond;
        //Timestamp's weekday
        int m_weekDay;
};

#endif // TSPPTIMESTAMP_H
