/******************************************************************************
** demo_uniontest.h
**
** Copyright (c) 2006-2016 Unified Automation GmbH. All rights reserved.
**
** Software License Agreement ("SLA") Version 2.5
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.5, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.5/
**
** Project: C++ OPC Server SDK information model for namespace http://www.unifiedautomation.com/DemoServer/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/


#ifndef __DEMO_UNIONTEST_H__
#define __DEMO_UNIONTEST_H__

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uastring.h"
#include "uaarraytemplates.h"
#include "demo_identifiers.h"
#include "demo_datatypes.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

// Namespace for the UA information model http://www.unifiedautomation.com/DemoServer/
namespace Demo {

class CPP_DEMO_EXPORT UnionTestPrivate;

/**
 *  @brief Wrapper class for the UA stack structure Demo_UnionTest.
 *
 *  This class encapsulates the native Demo_UnionTest structure
 *  and handles memory allocation and cleanup for you.
 *  UnionTest uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared UnionTest it creates a copy for that (copy-on-write).
 *  So assigning another UnionTest or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class CPP_DEMO_EXPORT UnionTest
{
    // Structure is defined as Union
    // Auto generating code for unions is a not released feature
    // Manual editing of the code is required

    DEMO_DECLARE_PRIVATE(UnionTest)
public:
    enum Type
    {
        Type_Null = Demo_UnionTest_Null,
        Type_Int32 = Demo_UnionTest_Int32,
        Type_String = Demo_UnionTest_String
    };
    UnionTest();
    UnionTest(const UnionTest &other);
    UnionTest(const Demo_UnionTest &other);
    UnionTest(const UaExtensionObject &extensionObject);
    UnionTest(const OpcUa_ExtensionObject &extensionObject);
    UnionTest(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UnionTest(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UnionTest();

    Type type() const;
    bool isNull() const;

    void clear();

    bool operator==(const UnionTest &other) const;
    bool operator!=(const UnionTest &other) const;

    UnionTest& operator=(const UnionTest &other);

    Demo_UnionTest* copy() const;
    void copyTo(Demo_UnionTest *pDst) const;

    static Demo_UnionTest* clone(const Demo_UnionTest& source);
    static void cloneTo(const Demo_UnionTest& source, Demo_UnionTest& copy);

    void attach(const Demo_UnionTest *pValue);
    Demo_UnionTest* detach(Demo_UnionTest* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setUnionTest(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setUnionTest(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setUnionTest(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUnionTest(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_Int32 getInt32(bool *ok = 0) const;
    UaString getString(bool *ok = 0) const;

    void setInt32(OpcUa_Int32 Int32);
    void setString(const UaString& String);
};

class UnionTests
{
public:
    UnionTests();
    UnionTests(const UnionTests &other);
    UnionTests(OpcUa_Int32 length, Demo_UnionTest* data);
    virtual ~UnionTests();

    UnionTests& operator=(const UnionTests &other);
    Demo_UnionTest& operator[](OpcUa_UInt32 index);
    const Demo_UnionTest& operator[](OpcUa_UInt32 index) const;

    void attach(OpcUa_UInt32 length, Demo_UnionTest* data);
    void attach(OpcUa_Int32 length, Demo_UnionTest* data);
    Demo_UnionTest* detach();

    bool operator==(const UnionTests &other) const;
    bool operator!=(const UnionTests &other) const;

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const Demo_UnionTest* rawData() const {return m_data;}
    inline Demo_UnionTest* rawData() {return m_data;}
    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setUnionTests(const UaVariant &variant);
    OpcUa_StatusCode setUnionTests(const OpcUa_Variant &variant);
    OpcUa_StatusCode setUnionTests(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUnionTests(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUnionTests(OpcUa_Int32 length, Demo_UnionTest* data);

    static UnionTests empty;
private:
    OpcUa_UInt32 m_noOfElements;
    Demo_UnionTest* m_data;
};

} // namespace Demo

#endif // __DEMO_UNIONTEST_H__

