var classUaBase_1_1VariableType =
[
    [ "VariableType", "classUaBase_1_1VariableType.html#aea97f8b17dc18579da27e7e1f5d9603a", null ],
    [ "VariableType", "classUaBase_1_1VariableType.html#a5cbd369445cf5907bc2365d707c68452", null ],
    [ "~VariableType", "classUaBase_1_1VariableType.html#ae0f2b7347d941403d44331ce2d5871a4", null ],
    [ "arrayDimensions", "classUaBase_1_1VariableType.html#abbb34f679607c6e14cf1dbc627226222", null ],
    [ "dataTypeId", "classUaBase_1_1VariableType.html#a52d548a9bd9c322246a5f1039fcb2a06", null ],
    [ "nodeClass", "classUaBase_1_1VariableType.html#a6894c53e1f5cb16d44f74b382685615b", null ],
    [ "setArrayDimensions", "classUaBase_1_1VariableType.html#a64261fd087173d368cacedeb1ee5b95b", null ],
    [ "setDataType", "classUaBase_1_1VariableType.html#ac3868783b723de2a0c5c0ef6440704ec", null ],
    [ "setValue", "classUaBase_1_1VariableType.html#a7283420f50494d68b96db445c3b9235b", null ],
    [ "setValueRank", "classUaBase_1_1VariableType.html#ab2188c3f67b258001f0d7e3ec557ffd1", null ],
    [ "value", "classUaBase_1_1VariableType.html#af33a9d312d336639d20bd40636a0b19b", null ],
    [ "valueRank", "classUaBase_1_1VariableType.html#afba57e0d9b1cd55b05bc1b8132f3ba4e", null ]
];