var classIOManager =
[
    [ "TransactionType", "group__ServerCoreInterfaces.html#gabf06d4995bae765430ac833402793d15", [
      [ "TransactionRead", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a378a62008ea530fcf23bd53aadde18a6", null ],
      [ "TransactionWrite", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15af02a18425dea0272acca3a514603f72f", null ],
      [ "TransactionMonitorBegin", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15afa3d9f34bd81c6137316ae4925c78af0", null ],
      [ "TransactionMonitorModify", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ac4173b056e6a8962579643d42459841c", null ],
      [ "TransactionMonitorStop", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a9329c2e912c5d2a200299e9c2f09afbd", null ],
      [ "TransactionInvalid", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ad8e7c08c9d329c9d5180eb71ab4e258d", null ]
    ] ],
    [ "IOManager", "classIOManager.html#afce14d2f016545728fee1e48b74f431c", null ],
    [ "~IOManager", "classIOManager.html#afc81e305edf44159e87d17347751e5a9", null ],
    [ "beginModifyMonitoring", "classIOManager.html#a411aec9192de3702f2ab08f95a4604ea", null ],
    [ "beginRead", "classIOManager.html#a55a7e546253da06c62d43cabb09cfb5b", null ],
    [ "beginStartMonitoring", "classIOManager.html#a43f14cb4e647ed4352cfe26c046bb0fb", null ],
    [ "beginStopMonitoring", "classIOManager.html#a5ae8c42c15bdc85692c64caba2de050b", null ],
    [ "beginTransaction", "classIOManager.html#a16c80044cd51e124335752f8bdc4bdf6", null ],
    [ "beginWrite", "classIOManager.html#a50e8fbd883a5c825c54b74e8e0dddf5b", null ],
    [ "finishTransaction", "classIOManager.html#a8dd9fc501ce76817cb1b53b4297fe1f2", null ]
];