var classUaDataTypeDictionary =
[
    [ "DefinitionType", "classUaDataTypeDictionary.html#af65dcfb2fe58ff81016f95432db001b7", null ],
    [ "definitionType", "classUaDataTypeDictionary.html#a4268171a80c0358d8e0c61d564938f4e", null ],
    [ "definitionType", "classUaDataTypeDictionary.html#ab25e005bc85be2df6aa510f214ab1277", null ],
    [ "definitionType", "classUaDataTypeDictionary.html#a59be202b7119c50a53e5f33419398570", null ],
    [ "enumDefinition", "classUaDataTypeDictionary.html#a5e1289469849f703ccff90927d043b6f", null ],
    [ "optionSetDefinition", "classUaDataTypeDictionary.html#a6c5d417483556aa5055e6222025a856f", null ],
    [ "structureDefinition", "classUaDataTypeDictionary.html#a9c182757e6c3d3a45fe476ed7e0327ee", null ],
    [ "structureDefinition", "classUaDataTypeDictionary.html#af6b64016d1c59257925066f4e6e5078b", null ],
    [ "structureDefinition", "classUaDataTypeDictionary.html#a5c6aeb1ba5c8a90a808b89427a829d1a", null ]
];