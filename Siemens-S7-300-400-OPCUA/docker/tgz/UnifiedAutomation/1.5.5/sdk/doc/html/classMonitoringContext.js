var classMonitoringContext =
[
    [ "MonitoringContext", "classMonitoringContext.html#a5c5ad3a7126d2f9c19b09556cee618d6", null ],
    [ "~MonitoringContext", "classMonitoringContext.html#a1bb707313538d7b16a5c8035f127bbc3", null ],
    [ "dataChangeTrigger", "classMonitoringContext.html#ac16c1075ea2818368f500823958b529e", null ],
    [ "deadband", "classMonitoringContext.html#a24191fdbe0dff241e3442bafc7f3f24e", null ],
    [ "isAbsoluteDeadband", "classMonitoringContext.html#af5c14fc6d0b83c34ba44652f0c70d0ff", null ],
    [ "pDataEncoding", "classMonitoringContext.html#aa9f343f63b38bfc0edf283740e07ec38", null ],
    [ "pIndexRange", "classMonitoringContext.html#ab9e06e23aa9edce92bee6fc052eb21a2", null ],
    [ "queueSize", "classMonitoringContext.html#aae644d36674c9588b3c3fb36de7504b6", null ],
    [ "samplingInterval", "classMonitoringContext.html#a4b1a5e0280950eabd0ce28d2887eb786", null ],
    [ "sdkMustHandleAbsoluteDeadband", "classMonitoringContext.html#a7c05b42f1e8950cba210c74438ed60f2", null ]
];