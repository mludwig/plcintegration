var classUaVariableType =
[
    [ "~UaVariableType", "classUaVariableType.html#a3676519d8bc634d6543c6928fd6c9377", null ],
    [ "UaVariableType", "classUaVariableType.html#a573f969952ea4bbb5581b5a315136108", null ],
    [ "arrayDimensions", "classUaVariableType.html#a2a50bf4086044504eb6909d8a204cc1c", null ],
    [ "dataType", "classUaVariableType.html#a4635fb0b5c145c24eaf4baa4588eab98", null ],
    [ "getAttributeValue", "classUaVariableType.html#a34b93d77901440b2c2e6bb30bc93a95d", null ],
    [ "isAbstract", "classUaVariableType.html#a17068153c1ff182b137bb7bc7890e533", null ],
    [ "isArrayDimensionsSupported", "classUaVariableType.html#a00802fcc683e71458aa62e03b4612ef5", null ],
    [ "isValueSupported", "classUaVariableType.html#a61f2703a2e2e873a673397d1b3791510", null ],
    [ "nodeClass", "classUaVariableType.html#a810653a9dfde1a0c6083b6ac830f9bb3", null ],
    [ "value", "classUaVariableType.html#a48aacdf1d4679d3cab91304aa9a25ddf", null ],
    [ "valueRank", "classUaVariableType.html#ac7d2a8484ed114ec5bbaab7b8d8bd1ff", null ]
];