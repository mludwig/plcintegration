var classUaDoubleArray =
[
    [ "UaDoubleArray", "classUaDoubleArray.html#a0653a52bda014b80659f16e4722689e5", null ],
    [ "UaDoubleArray", "classUaDoubleArray.html#a1cd57e825d766f1b114764c9188029f9", null ],
    [ "UaDoubleArray", "classUaDoubleArray.html#ac7e038e56f8c4711fac5ee519ae89e38", null ],
    [ "~UaDoubleArray", "classUaDoubleArray.html#af01225ff15d765a7c9ce3fd7816d1622", null ],
    [ "attach", "classUaDoubleArray.html#aebc119249bc1915f84755ab323143f9e", null ],
    [ "attach", "classUaDoubleArray.html#aba65d07d4b1ed2d6c4e0fa65d2d429cb", null ],
    [ "clear", "classUaDoubleArray.html#a6ec63461f4001035bcc6f8b593468f43", null ],
    [ "create", "classUaDoubleArray.html#a1e321c2a9a913f84727867dc403f1d49", null ],
    [ "detach", "classUaDoubleArray.html#a60cfc1efb707ee5856789c8ecdae3a50", null ],
    [ "operator!=", "classUaDoubleArray.html#af43c23f83776debcb53c9581c0964c6b", null ],
    [ "operator=", "classUaDoubleArray.html#ae50d11b5ea7d43baf245b7263d89614c", null ],
    [ "operator==", "classUaDoubleArray.html#a232fddf934eb334f8fc5766c0b4fd6b0", null ],
    [ "operator[]", "classUaDoubleArray.html#a486bbdb921876bf3aae37944af925af9", null ],
    [ "operator[]", "classUaDoubleArray.html#ab117028e20ed9091b993da31a950e8ed", null ],
    [ "resize", "classUaDoubleArray.html#abcb13d4b5bdb2276c37929a414fb1d6e", null ]
];