var classEventManager =
[
    [ "EventTransactionType", "group__ServerCoreInterfaces.html#gad028aaf46f546e841a7e9ddd454f8cb6", [
      [ "MONITOR_BEGIN", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2bbf320ef39238b799c72376b79d1549", null ],
      [ "MONITOR_MODIFY", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2db1edefaedc6c547d85aaaf0dca449a", null ],
      [ "MONITOR_STOP", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a78bc6cc9193eff9c679f59958444d4b0", null ],
      [ "INVALID", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a444aee1d9503d934c3a2d10ab51efd33", null ]
    ] ],
    [ "EventManager", "classEventManager.html#a89099b22114f158b5c530edfea52371d", null ],
    [ "~EventManager", "classEventManager.html#afc6d01d2924c433f62afc9156351ea86", null ],
    [ "beginConditionRefresh", "classEventManager.html#ad30e8283eeda0eeb5b2f8d561c41b68b", null ],
    [ "beginEventTransaction", "classEventManager.html#af63a8d1ca228de1686c507c292bd8cd6", null ],
    [ "beginModifyMonitoring", "classEventManager.html#a3e3c794a81f265c64eebfc57c6103031", null ],
    [ "beginStartMonitoring", "classEventManager.html#a1734770b69986526745385fc8749ddf3", null ],
    [ "beginStopMonitoring", "classEventManager.html#a4662a50dbd99406fbb4bd7ee0138937b", null ],
    [ "finishEventTransaction", "classEventManager.html#ae0a33401961d790c56ed23597672c3cf", null ],
    [ "sendRefreshRequired", "classEventManager.html#a33d6f7dd876ca404757369115d144849", null ]
];