var structOpcUa__RegisteredServer =
[
    [ "DiscoveryUrls", "group__UaStackTypes.html#gafbdcf5f21c1ca36cc5f7ca0c4a68aa4b", null ],
    [ "GatewayServerUri", "group__UaStackTypes.html#gab6dbc96e73abcb557a1eb38f6928ea68", null ],
    [ "IsOnline", "group__UaStackTypes.html#gae5e4cc1b3a56864a33934c0a128f29b8", null ],
    [ "ProductUri", "group__UaStackTypes.html#ga9a5dba95cd9fc34f84c2d34373c7e03d", null ],
    [ "SemaphoreFilePath", "group__UaStackTypes.html#ga42f1fae39569083bb5bff78a0de2277b", null ],
    [ "ServerNames", "group__UaStackTypes.html#ga90cf983d20c77ef9582d58a39852fd39", null ],
    [ "ServerType", "group__UaStackTypes.html#ga1efa0a96a110877b696ce483de3c1546", null ],
    [ "ServerUri", "group__UaStackTypes.html#gaaa25ddf5445c621a3c961a8138d70d9c", null ]
];