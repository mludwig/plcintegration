var Compile3rdParty =
[
    [ "Compiling OpenSSL with Visual Studio", "CompileOpenSSLVS.html", [
      [ "Requirements", "CompileOpenSSLVS.html#OpenSSLVS2008Requirements", null ],
      [ "Debug and Release", "CompileOpenSSLVS.html#OpenSSLVS2008DebugRelease", null ],
      [ "Download and Unpack OpenSSL", "CompileOpenSSLVS.html#OpenSSLVS2008St1", null ],
      [ "Configure", "CompileOpenSSLVS.html#OpenSSLVS2008St2", null ],
      [ "Create Makefiles", "CompileOpenSSLVS.html#OpenSSLVS2008St3", null ],
      [ "Compile", "CompileOpenSSLVS.html#OpenSSLVS2008St4", null ],
      [ "Copy the Libraries to the SDK Folder Structure", "CompileOpenSSLVS.html#OpenSSLVS2008St5", null ]
    ] ],
    [ "Cross Compiling OpenSSL", "OpenSSLXCompile.html", [
      [ "Requirements", "OpenSSLXCompile.html#OpenSSLXCompileRequirements", null ],
      [ "Add Cross Compiler to Path Variable", "OpenSSLXCompile.html#OpenSSLXCompileSt1", null ],
      [ "Download and Unpack OpenSSL", "OpenSSLXCompile.html#OpenSSLXCompileSt2", null ],
      [ "Configure", "OpenSSLXCompile.html#OpenSSLXCompileSt3", null ],
      [ "Build and Install", "OpenSSLXCompile.html#OpenSSLXCompileSt4", null ]
    ] ],
    [ "Compiling OpenSSL for Windows CE", "CompileOpenSSLCE.html", [
      [ "Requirements", "CompileOpenSSLCE.html#OpenSSLCERequirements", null ],
      [ "Compile wcecompat", "CompileOpenSSLCE.html#OpenSSLCE_wcecompat", null ],
      [ "Debug and Release", "CompileOpenSSLCE.html#OpenSSLCEDebugRelease", null ],
      [ "Download and Patch OpenSSL", "CompileOpenSSLCE.html#OpenSSLCESt1", null ],
      [ "Configure", "CompileOpenSSLCE.html#OpenSSLCESt2", null ],
      [ "Create Makefiles", "CompileOpenSSLCE.html#OpenSSLCESt3", null ],
      [ "Compile", "CompileOpenSSLCE.html#OpenSSLCESt4", null ],
      [ "Copy the Libraries to the SDK Folder Structure", "CompileOpenSSLCE.html#OpenSSLCESt5", null ]
    ] ],
    [ "Compiling LibXml2  with Visual Studio", "CompileLibXml2VS.html", [
      [ "Requirements", "CompileLibXml2VS.html#LibXml2VS2008Requirements", null ],
      [ "Debug and Release", "CompileLibXml2VS.html#LibXml2VS2008DebugRelease", null ],
      [ "Download and Unpack LibXml2", "CompileLibXml2VS.html#LibXml2VS2008St1", null ],
      [ "Configure", "CompileLibXml2VS.html#LibXml2VS2008St2", null ],
      [ "Compile", "CompileLibXml2VS.html#LibXml2VS2008St3", null ],
      [ "Copy the Libraries to the SDK Folder Structure", "CompileLibXml2VS.html#LibXml2VS2008St4", null ]
    ] ],
    [ "Cross Compiling LibXml2", "LibXml2XCompile.html", [
      [ "Requirements", "LibXml2XCompile.html#LibXml2XCompileRequirements", null ],
      [ "Download and Unpack LibXml2", "LibXml2XCompile.html#LibXml2XCompileSt1", null ],
      [ "Set Environment Variables", "LibXml2XCompile.html#LibXml2XCompileSt2", null ],
      [ "Configure", "LibXml2XCompile.html#LibXml2XCompileSt3", null ],
      [ "Build and Install", "LibXml2XCompile.html#LibXml2XCompileSt4", null ]
    ] ],
    [ "Compiling LibXml2 for Windows CE", "CompileLibXml2CE.html", [
      [ "Requirements", "CompileLibXml2CE.html#LibXml2CERequirements", null ],
      [ "Debug and Release", "CompileLibXml2CE.html#LibXml2CEDebugRelease", null ],
      [ "Download and Unpack LibXml2", "CompileLibXml2CE.html#LibXml2CESt1", null ],
      [ "Create a Visual Studio Solution", "CompileLibXml2CE.html#LibXml2CESt2", null ],
      [ "Disable iconv", "CompileLibXml2CE.html#LibXml2CESt3", null ],
      [ "Copy the Libraries to the SDK Folder Structure", "CompileLibXml2CE.html#LibXml2CESt4", null ]
    ] ]
];