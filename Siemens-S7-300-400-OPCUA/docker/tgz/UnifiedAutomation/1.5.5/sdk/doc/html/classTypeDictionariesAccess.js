var classTypeDictionariesAccess =
[
    [ "TypeDictionariesAccess", "classTypeDictionariesAccess.html#abc5d40270f83b7fbbc54f92b75897fcf", null ],
    [ "~TypeDictionariesAccess", "classTypeDictionariesAccess.html#a73d38aea566e91dbc943a8e85191ce47", null ],
    [ "addEnumeratedTypeDefinition", "classTypeDictionariesAccess.html#aac03b90778c9d770ae9b283a5366d8a2", null ],
    [ "addStructuredTypeDefinition", "classTypeDictionariesAccess.html#ac5290863f04c537f3f8f2004c7b0c0fa", null ],
    [ "definitionType", "classTypeDictionariesAccess.html#a31ff779bfdbe815c17effb9121411e35", null ],
    [ "enumDefinition", "classTypeDictionariesAccess.html#ab10a3b8f359909069956c178b6d3b19c", null ],
    [ "optionSetDefinition", "classTypeDictionariesAccess.html#aecbb68d9031196e04ffac2602093a25b", null ],
    [ "structureDefinition", "classTypeDictionariesAccess.html#af2c28575f4903d7d9015f3035d412ecb", null ]
];