var classUaOptionSetDefinition =
[
    [ "UaOptionSetDefinition", "classUaOptionSetDefinition.html#a82679b66ab7f94756cf6a8bf306a40f3", null ],
    [ "UaOptionSetDefinition", "classUaOptionSetDefinition.html#a7c81ea384efe0281bb7b43a04a0a0c11", null ],
    [ "addChild", "classUaOptionSetDefinition.html#a69fe00eaf8d58dd36db4969da69fa5e3", null ],
    [ "baseType", "classUaOptionSetDefinition.html#a7abaacf8ec9e5298a658e5a896a57e53", null ],
    [ "binaryEncodingId", "classUaOptionSetDefinition.html#a1fe3447a5412e57603665dd6b2d47072", null ],
    [ "child", "classUaOptionSetDefinition.html#a18f755dd2165af6e69caae961f7c1e8c", null ],
    [ "children", "classUaOptionSetDefinition.html#a0fab2d487f5655727b9004bb904ada31", null ],
    [ "childrenCount", "classUaOptionSetDefinition.html#af98fb91ec212aaee8cb2dcee96b88c29", null ],
    [ "clear", "classUaOptionSetDefinition.html#a2b191109ece2b526fbe12c27dfcc6bd0", null ],
    [ "operator!=", "classUaOptionSetDefinition.html#a1fefc29374438aab2192d063afbafc6c", null ],
    [ "operator=", "classUaOptionSetDefinition.html#ad1ca64590bfc45d87d02def87498264a", null ],
    [ "operator==", "classUaOptionSetDefinition.html#ae79d4950627a0d4d8fd55085edeb8ab7", null ],
    [ "setBaseType", "classUaOptionSetDefinition.html#af90343b73a596f4f14ee2b5b4d43e509", null ],
    [ "setBinaryEncodingId", "classUaOptionSetDefinition.html#a0cff93b7fd01ca2c03e5d2a54f5fb0bf", null ],
    [ "setChildren", "classUaOptionSetDefinition.html#a96957759e4de771a6d5b72194ef364b0", null ],
    [ "setDataTypeId", "classUaOptionSetDefinition.html#a8cbe76ab00aee11c46ba758048dffcca", null ],
    [ "setDocumentation", "classUaOptionSetDefinition.html#a9aed9c9ff350d6a9f8334c1ea0fae1ef", null ],
    [ "setName", "classUaOptionSetDefinition.html#a9e785dd477bdd6c4af0aa19219e730a7", null ],
    [ "setNamespace", "classUaOptionSetDefinition.html#a459c12661b808c94532cc91ca3a24b7f", null ],
    [ "setXmlEncodingId", "classUaOptionSetDefinition.html#a290e463545b4df8e49182d94d23867ef", null ],
    [ "xmlEncodingId", "classUaOptionSetDefinition.html#a34e056dcaae66ae0fc4c57161cc187ff", null ]
];