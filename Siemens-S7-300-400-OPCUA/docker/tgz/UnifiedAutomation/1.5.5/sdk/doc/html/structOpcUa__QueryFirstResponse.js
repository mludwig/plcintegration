var structOpcUa__QueryFirstResponse =
[
    [ "ContinuationPoint", "group__UaStackTypes.html#gaa4c860aaab9b7a05f14208da0aebbcb3", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga89a8b46caf8027109c58c4691f95537d", null ],
    [ "FilterResult", "group__UaStackTypes.html#gac30154e53deeefda7b2bc1a8ea3d1307", null ],
    [ "ParsingResults", "group__UaStackTypes.html#ga1b00803c29e242db5abaebd311eadaa2", null ],
    [ "QueryDataSets", "group__UaStackTypes.html#gac0580420bd29a3a3edd9aa963607b478", null ]
];