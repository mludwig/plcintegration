var structOpcUa__FindServersRequest =
[
    [ "EndpointUrl", "group__UaStackTypes.html#ga2e5ef21252a58b177025d0954656f216", null ],
    [ "LocaleIds", "group__UaStackTypes.html#gad40764e0a7b7d5dd8b12651ce9365e6d", null ],
    [ "ServerUris", "group__UaStackTypes.html#ga92ba58bfe7e16a7924e392309fc46c82", null ]
];