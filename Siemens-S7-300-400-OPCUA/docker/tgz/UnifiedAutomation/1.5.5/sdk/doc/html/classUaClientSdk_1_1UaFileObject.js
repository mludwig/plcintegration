var classUaClientSdk_1_1UaFileObject =
[
    [ "UaFileObject", "classUaClientSdk_1_1UaFileObject.html#a2ad5c31f6f310e017a6a9ff0303166ac", null ],
    [ "~UaFileObject", "classUaClientSdk_1_1UaFileObject.html#aba283e38ea127eaf8e43621d3d562e69", null ],
    [ "attachToOpenFile", "classUaClientSdk_1_1UaFileObject.html#afb88f4be6a421cfc6f31a76cf3fbbbbb", null ],
    [ "close", "classUaClientSdk_1_1UaFileObject.html#af10c83df61f93fc68f5b042b3458cfc7", null ],
    [ "fileHandle", "classUaClientSdk_1_1UaFileObject.html#ae1b43b573a17a46cdaff0e2317d88477", null ],
    [ "fileObjectNodeId", "classUaClientSdk_1_1UaFileObject.html#a2aaab2225090d8f1249da1c288a66c60", null ],
    [ "getPosition", "classUaClientSdk_1_1UaFileObject.html#adbac7f9102fdc5a5482097a6ebeea07d", null ],
    [ "open", "classUaClientSdk_1_1UaFileObject.html#abf1d0744d7cfbb84f417c8625d45a10c", null ],
    [ "read", "classUaClientSdk_1_1UaFileObject.html#a5f9f9cfb7083190f2c67ae2b7a1558df", null ],
    [ "setPosition", "classUaClientSdk_1_1UaFileObject.html#a682c1e591da1b7534ac29af22e506456", null ],
    [ "write", "classUaClientSdk_1_1UaFileObject.html#a1a105b4f591c07c83e45e8c711df6d76", null ]
];