var hierarchy =
[
    [ "OpcUa::AddressSpaceFileCallback", "classOpcUa_1_1AddressSpaceFileCallback.html", null ],
    [ "AggregateCalculatorInterface", "classAggregateCalculatorInterface.html", [
      [ "AggregateCalculator", "classAggregateCalculator.html", [
        [ "AggregateCalculatorAverage", "classAggregateCalculatorAverage.html", null ],
        [ "AggregateCalculatorCount", "classAggregateCalculatorCount.html", null ],
        [ "AggregateCalculatorMinMax", "classAggregateCalculatorMinMax.html", null ],
        [ "AggregateCalculatorStartEnd", "classAggregateCalculatorStartEnd.html", null ],
        [ "AggregateCalculatorStatus", "classAggregateCalculatorStatus.html", null ],
        [ "AggregateCalculatorStdDev", "classAggregateCalculatorStdDev.html", null ]
      ] ]
    ] ],
    [ "Aggregators", "classAggregators.html", null ],
    [ "OpcUaGds::ApplicationRecordDataType", "classOpcUaGds_1_1ApplicationRecordDataType.html", null ],
    [ "UaBase::BaseNode", "classUaBase_1_1BaseNode.html", [
      [ "UaBase::Method", "classUaBase_1_1Method.html", null ],
      [ "UaBase::Object", "classUaBase_1_1Object.html", null ],
      [ "UaBase::TypeNode", "classUaBase_1_1TypeNode.html", [
        [ "UaBase::DataType", "classUaBase_1_1DataType.html", null ],
        [ "UaBase::ObjectType", "classUaBase_1_1ObjectType.html", null ],
        [ "UaBase::ReferenceType", "classUaBase_1_1ReferenceType.html", null ],
        [ "UaBase::VariableType", "classUaBase_1_1VariableType.html", null ]
      ] ],
      [ "UaBase::Variable", "classUaBase_1_1Variable.html", null ]
    ] ],
    [ "UaBase::BaseNodeFactory", "classUaBase_1_1BaseNodeFactory.html", null ],
    [ "UaClientSdk::BrowseContext", "classUaClientSdk_1_1BrowseContext.html", null ],
    [ "BrowseContext", "classBrowseContext.html", null ],
    [ "BrowseUaNodeCallback", "classBrowseUaNodeCallback.html", null ],
    [ "CacheSignal", "classCacheSignal.html", [
      [ "CacheVariableConnector", "classCacheVariableConnector.html", null ]
    ] ],
    [ "UaClientSdk::CallIn", "classUaClientSdk_1_1CallIn.html", null ],
    [ "UaClientSdk::CallOut", "classUaClientSdk_1_1CallOut.html", null ],
    [ "CertificateCreateSettings", "classCertificateCreateSettings.html", null ],
    [ "UaClientSdk::CertificateValidationCallback", "classUaClientSdk_1_1CertificateValidationCallback.html", [
      [ "UaClientSdk::UaSession", "classUaClientSdk_1_1UaSession.html", null ]
    ] ],
    [ "UaClientSdk::ClientSecurityInfo", "classUaClientSdk_1_1ClientSecurityInfo.html", [
      [ "UaClientSdk::SessionSecurityInfo", "classUaClientSdk_1_1SessionSecurityInfo.html", null ]
    ] ],
    [ "ComAeAttributeMapping", "classComAeAttributeMapping.html", null ],
    [ "ConditionBranchList", "classConditionBranchList.html", null ],
    [ "OpcUa::ConditionTypeBase", "classOpcUa_1_1ConditionTypeBase.html", [
      [ "OpcUa::ConditionType", "classOpcUa_1_1ConditionType.html", [
        [ "OpcUa::AcknowledgeableConditionTypeBase", "classOpcUa_1_1AcknowledgeableConditionTypeBase.html", [
          [ "OpcUa::AcknowledgeableConditionType", "classOpcUa_1_1AcknowledgeableConditionType.html", [
            [ "OpcUa::AlarmConditionTypeBase", "classOpcUa_1_1AlarmConditionTypeBase.html", [
              [ "OpcUa::AlarmConditionType", "classOpcUa_1_1AlarmConditionType.html", [
                [ "OpcUa::DiscreteAlarmTypeBase", "classOpcUa_1_1DiscreteAlarmTypeBase.html", [
                  [ "OpcUa::DiscreteAlarmType", "classOpcUa_1_1DiscreteAlarmType.html", [
                    [ "OpcUa::OffNormalAlarmTypeBase", "classOpcUa_1_1OffNormalAlarmTypeBase.html", [
                      [ "OpcUa::OffNormalAlarmType", "classOpcUa_1_1OffNormalAlarmType.html", [
                        [ "OpcUa::SystemOffNormalAlarmType", "classOpcUa_1_1SystemOffNormalAlarmType.html", [
                          [ "OpcUa::CertificateExpirationAlarmType", "classOpcUa_1_1CertificateExpirationAlarmType.html", null ]
                        ] ],
                        [ "OpcUa::TripAlarmType", "classOpcUa_1_1TripAlarmType.html", null ]
                      ] ]
                    ] ]
                  ] ]
                ] ],
                [ "OpcUa::LimitAlarmTypeBase", "classOpcUa_1_1LimitAlarmTypeBase.html", [
                  [ "OpcUa::LimitAlarmType", "classOpcUa_1_1LimitAlarmType.html", [
                    [ "OpcUa::ExclusiveLimitAlarmTypeBase", "classOpcUa_1_1ExclusiveLimitAlarmTypeBase.html", [
                      [ "OpcUa::ExclusiveLimitAlarmType", "classOpcUa_1_1ExclusiveLimitAlarmType.html", [
                        [ "OpcUa::ExclusiveDeviationAlarmType", "classOpcUa_1_1ExclusiveDeviationAlarmType.html", null ],
                        [ "OpcUa::ExclusiveLevelAlarmType", "classOpcUa_1_1ExclusiveLevelAlarmType.html", null ],
                        [ "OpcUa::ExclusiveRateOfChangeAlarmType", "classOpcUa_1_1ExclusiveRateOfChangeAlarmType.html", null ]
                      ] ]
                    ] ],
                    [ "OpcUa::NonExclusiveLimitAlarmTypeBase", "classOpcUa_1_1NonExclusiveLimitAlarmTypeBase.html", [
                      [ "OpcUa::NonExclusiveLimitAlarmType", "classOpcUa_1_1NonExclusiveLimitAlarmType.html", [
                        [ "OpcUa::NonExclusiveDeviationAlarmType", "classOpcUa_1_1NonExclusiveDeviationAlarmType.html", null ],
                        [ "OpcUa::NonExclusiveLevelAlarmType", "classOpcUa_1_1NonExclusiveLevelAlarmType.html", null ],
                        [ "OpcUa::NonExclusiveRateOfChangeAlarmType", "classOpcUa_1_1NonExclusiveRateOfChangeAlarmType.html", null ]
                      ] ]
                    ] ]
                  ] ]
                ] ]
              ] ]
            ] ]
          ] ]
        ] ],
        [ "OpcUa::DialogConditionTypeBase", "classOpcUa_1_1DialogConditionTypeBase.html", [
          [ "OpcUa::DialogConditionType", "classOpcUa_1_1DialogConditionType.html", null ]
        ] ]
      ] ]
    ] ],
    [ "ContinuationPointUserDataBase", "classContinuationPointUserDataBase.html", null ],
    [ "ContinuationPointWrapper", "classContinuationPointWrapper.html", null ],
    [ "CoreModule", "classCoreModule.html", null ],
    [ "DataMonitoredItemSpec", "classDataMonitoredItemSpec.html", null ],
    [ "OpcUaGds::DataTypes", "classOpcUaGds_1_1DataTypes.html", null ],
    [ "OpcUaDi::DataTypes", "classOpcUaDi_1_1DataTypes.html", null ],
    [ "OpcUaPlc::DataTypes", "classOpcUaPlc_1_1DataTypes.html", null ],
    [ "UaClientSdk::DeleteAtTimeDetail", "classUaClientSdk_1_1DeleteAtTimeDetail.html", null ],
    [ "UaClientSdk::DeleteEventDetail", "classUaClientSdk_1_1DeleteEventDetail.html", null ],
    [ "UaClientSdk::DeleteRawModifiedDetail", "classUaClientSdk_1_1DeleteRawModifiedDetail.html", null ],
    [ "EventCallback", "classEventCallback.html", [
      [ "UaMonitoredItemEvent", "classUaMonitoredItemEvent.html", null ]
    ] ],
    [ "EventFilter", "classEventFilter.html", null ],
    [ "EventFilterElement", "classEventFilterElement.html", null ],
    [ "EventFilterOperand", "classEventFilterOperand.html", null ],
    [ "EventItem", "classEventItem.html", null ],
    [ "EventManager", "classEventManager.html", [
      [ "EventManagerBase", "classEventManagerBase.html", [
        [ "EventManagerUaNode", "classEventManagerUaNode.html", [
          [ "NodeManagerBase", "classNodeManagerBase.html", [
            [ "NodeManagerNodeSetXml", "classNodeManagerNodeSetXml.html", null ],
            [ "NodeManagerNS1", "classNodeManagerNS1.html", null ],
            [ "NodeManagerRoot", "classNodeManagerRoot.html", null ],
            [ "OpcUaDi::NodeManagerDevicesBase", "classOpcUaDi_1_1NodeManagerDevicesBase.html", [
              [ "OpcUaDi::NodeManagerDevices", "classOpcUaDi_1_1NodeManagerDevices.html", null ]
            ] ],
            [ "OpcUaPlc::NodeManagerPLCopenBase", "classOpcUaPlc_1_1NodeManagerPLCopenBase.html", [
              [ "OpcUaPlc::NodeManagerPLCopen", "classOpcUaPlc_1_1NodeManagerPLCopen.html", null ]
            ] ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "EventManagerCallback", "classEventManagerCallback.html", [
      [ "UaSubscription", "classUaSubscription.html", null ]
    ] ],
    [ "EventManagerSubset", "classEventManagerSubset.html", null ],
    [ "EventMonitoringContext", "classEventMonitoringContext.html", [
      [ "EventMonitoringContextCreate", "classEventMonitoringContextCreate.html", null ]
    ] ],
    [ "EventNotifierTreeElement", "classEventNotifierTreeElement.html", null ],
    [ "EventTransactionContext", "classEventTransactionContext.html", null ],
    [ "EventTypeTreeElement", "classEventTypeTreeElement.html", null ],
    [ "UaBase::Extension", "classUaBase_1_1Extension.html", null ],
    [ "OpcUaDi::FetchResultDataDataType", "classOpcUaDi_1_1FetchResultDataDataType.html", null ],
    [ "OpcUaDi::FetchResultDataType", "classOpcUaDi_1_1FetchResultDataType.html", null ],
    [ "OpcUaDi::FetchResultErrorDataType", "classOpcUaDi_1_1FetchResultErrorDataType.html", null ],
    [ "OpcUa::FileDirectoryTypeCallback", "classOpcUa_1_1FileDirectoryTypeCallback.html", null ],
    [ "UaBase::FullReference", "classUaBase_1_1FullReference.html", null ],
    [ "HandleManager< T >", "classHandleManager.html", null ],
    [ "HandleManager< CoreMonitoredItem >", "classHandleManager.html", null ],
    [ "HandleManager< EventItem >", "classHandleManager.html", null ],
    [ "HandleManager< FileHandleData >", "classHandleManager.html", null ],
    [ "HandleManager< RegisteredNode >", "classHandleManager.html", null ],
    [ "HandleManager< SamplingItem >", "classHandleManager.html", null ],
    [ "HandleManager< TransactionContext >", "classHandleManager.html", null ],
    [ "HandleManager< UaBaseChangeMonitorTypeContext >", "classHandleManager.html", null ],
    [ "HandleManager< UaMonitoredItem >", "classHandleManager.html", null ],
    [ "HandleManager< UaTMBaseContext >", "classHandleManager.html", null ],
    [ "HashTable", "classHashTable.html", null ],
    [ "HistoryManager", "classHistoryManager.html", [
      [ "HistoryManagerBase", "classHistoryManagerBase.html", null ]
    ] ],
    [ "HistoryManagerCallback", "classHistoryManagerCallback.html", [
      [ "UaTransactionManager", "classUaTransactionManager.html", null ]
    ] ],
    [ "HistoryManagerSubset", "classHistoryManagerSubset.html", null ],
    [ "UaClientSdk::HistoryReadAtTimeContext", "classUaClientSdk_1_1HistoryReadAtTimeContext.html", null ],
    [ "HistoryReadCPUserDataBase", "classHistoryReadCPUserDataBase.html", null ],
    [ "UaClientSdk::HistoryReadDataResult", "classUaClientSdk_1_1HistoryReadDataResult.html", null ],
    [ "UaClientSdk::HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html", null ],
    [ "UaClientSdk::HistoryReadEventResult", "classUaClientSdk_1_1HistoryReadEventResult.html", null ],
    [ "UaClientSdk::HistoryReadProcessedContext", "classUaClientSdk_1_1HistoryReadProcessedContext.html", null ],
    [ "UaClientSdk::HistoryReadRawModifiedContext", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html", null ],
    [ "HistoryTransactionContext", "classHistoryTransactionContext.html", null ],
    [ "HistoryUpdateEventsJob", "classHistoryUpdateEventsJob.html", null ],
    [ "OpcUa_NodeId::Identifier", "unionOpcUa__NodeId_1_1Identifier.html", null ],
    [ "IOManager", "classIOManager.html", [
      [ "IOManagerUaNode", "classIOManagerUaNode.html", [
        [ "NodeManagerBase", "classNodeManagerBase.html", null ]
      ] ]
    ] ],
    [ "IOManager2", "classIOManager2.html", null ],
    [ "IOManager2Callback", "classIOManager2Callback.html", null ],
    [ "IOManagerCallback", "classIOManagerCallback.html", [
      [ "UaSubscription", "classUaSubscription.html", null ],
      [ "UaTransactionManager", "classUaTransactionManager.html", null ]
    ] ],
    [ "IOManagerSubset", "classIOManagerSubset.html", null ],
    [ "IOTransactionContext", "classIOTransactionContext.html", null ],
    [ "IOVariableCallback", "classIOVariableCallback.html", [
      [ "UaMonitoredItemData", "classUaMonitoredItemData.html", null ]
    ] ],
    [ "OpcUaDi::LockingServicesCallback", "classOpcUaDi_1_1LockingServicesCallback.html", null ],
    [ "MethodManager", "classMethodManager.html", [
      [ "OpcUa::BaseObjectType", "classOpcUa_1_1BaseObjectType.html", [
        [ "OpcUa::AggregateConfigurationTypeBase", "classOpcUa_1_1AggregateConfigurationTypeBase.html", [
          [ "OpcUa::AggregateConfigurationType", "classOpcUa_1_1AggregateConfigurationType.html", null ]
        ] ],
        [ "OpcUa::BaseObjectTypeGeneric", "classOpcUa_1_1BaseObjectTypeGeneric.html", null ],
        [ "OpcUa::CertificateGroupType", "classOpcUa_1_1CertificateGroupType.html", null ],
        [ "OpcUa::DataTypeEncodingTypeBase", "classOpcUa_1_1DataTypeEncodingTypeBase.html", [
          [ "OpcUa::DataTypeEncodingType", "classOpcUa_1_1DataTypeEncodingType.html", null ]
        ] ],
        [ "OpcUa::FileTypeBase", "classOpcUa_1_1FileTypeBase.html", [
          [ "OpcUa::FileType", "classOpcUa_1_1FileType.html", [
            [ "OpcUa::AddressSpaceFileType", "classOpcUa_1_1AddressSpaceFileType.html", null ],
            [ "OpcUa::TrustListTypeBase", "classOpcUa_1_1TrustListTypeBase.html", [
              [ "OpcUa::TrustListType", "classOpcUa_1_1TrustListType.html", null ]
            ] ]
          ] ]
        ] ],
        [ "OpcUa::FiniteStateMachineType", "classOpcUa_1_1FiniteStateMachineType.html", [
          [ "OpcUa::ExclusiveLimitStateMachineType", "classOpcUa_1_1ExclusiveLimitStateMachineType.html", null ],
          [ "OpcUa::ProgramStateMachineTypeBase", "classOpcUa_1_1ProgramStateMachineTypeBase.html", [
            [ "OpcUa::ProgramStateMachineType", "classOpcUa_1_1ProgramStateMachineType.html", null ]
          ] ],
          [ "OpcUa::ShelvedStateMachineTypeBase", "classOpcUa_1_1ShelvedStateMachineTypeBase.html", [
            [ "OpcUa::ShelvedStateMachineType", "classOpcUa_1_1ShelvedStateMachineType.html", null ]
          ] ]
        ] ],
        [ "OpcUa::FolderTypeBase", "classOpcUa_1_1FolderTypeBase.html", [
          [ "OpcUa::FolderType", "classOpcUa_1_1FolderType.html", [
            [ "OpcUa::CertificateGroupFolderType", "classOpcUa_1_1CertificateGroupFolderType.html", null ],
            [ "OpcUa::FileDirectoryTypeBase", "classOpcUa_1_1FileDirectoryTypeBase.html", [
              [ "OpcUa::FileDirectoryType", "classOpcUa_1_1FileDirectoryType.html", null ]
            ] ],
            [ "OpcUaDi::FunctionalGroupTypeBase", "classOpcUaDi_1_1FunctionalGroupTypeBase.html", [
              [ "OpcUaDi::FunctionalGroupType", "classOpcUaDi_1_1FunctionalGroupType.html", null ]
            ] ]
          ] ]
        ] ],
        [ "OpcUa::HistoricalDataConfigurationTypeBase", "classOpcUa_1_1HistoricalDataConfigurationTypeBase.html", [
          [ "OpcUa::HistoricalDataConfigurationType", "classOpcUa_1_1HistoricalDataConfigurationType.html", null ]
        ] ],
        [ "OpcUa::HistoryServerCapabilitiesType", "classOpcUa_1_1HistoryServerCapabilitiesType.html", null ],
        [ "OpcUa::NamespaceMetadataType", "classOpcUa_1_1NamespaceMetadataType.html", null ],
        [ "OpcUa::ServerConfigurationTypeBase", "classOpcUa_1_1ServerConfigurationTypeBase.html", [
          [ "OpcUa::ServerConfigurationType", "classOpcUa_1_1ServerConfigurationType.html", null ]
        ] ],
        [ "OpcUa::StateMachineType", "classOpcUa_1_1StateMachineType.html", null ],
        [ "OpcUa::StateType", "classOpcUa_1_1StateType.html", [
          [ "OpcUa::InitialStateType", "classOpcUa_1_1InitialStateType.html", null ]
        ] ],
        [ "OpcUa::TransitionType", "classOpcUa_1_1TransitionType.html", null ],
        [ "OpcUaDi::ConfigurableObjectTypeBase", "classOpcUaDi_1_1ConfigurableObjectTypeBase.html", [
          [ "OpcUaDi::ConfigurableObjectType", "classOpcUaDi_1_1ConfigurableObjectType.html", null ]
        ] ],
        [ "OpcUaDi::LockingServicesTypeBase", "classOpcUaDi_1_1LockingServicesTypeBase.html", [
          [ "OpcUaDi::LockingServicesType", "classOpcUaDi_1_1LockingServicesType.html", null ]
        ] ],
        [ "OpcUaDi::NetworkTypeBase", "classOpcUaDi_1_1NetworkTypeBase.html", [
          [ "OpcUaDi::NetworkType", "classOpcUaDi_1_1NetworkType.html", null ]
        ] ],
        [ "OpcUaDi::ProtocolTypeBase", "classOpcUaDi_1_1ProtocolTypeBase.html", [
          [ "OpcUaDi::ProtocolType", "classOpcUaDi_1_1ProtocolType.html", null ]
        ] ],
        [ "OpcUaDi::TopologyElementTypeBase", "classOpcUaDi_1_1TopologyElementTypeBase.html", [
          [ "OpcUaDi::TopologyElementType", "classOpcUaDi_1_1TopologyElementType.html", [
            [ "OpcUaDi::BlockTypeBase", "classOpcUaDi_1_1BlockTypeBase.html", [
              [ "OpcUaDi::BlockType", "classOpcUaDi_1_1BlockType.html", [
                [ "OpcUaPlc::CtrlProgramOrganizationUnitTypeBase", "classOpcUaPlc_1_1CtrlProgramOrganizationUnitTypeBase.html", [
                  [ "OpcUaPlc::CtrlProgramOrganizationUnitType", "classOpcUaPlc_1_1CtrlProgramOrganizationUnitType.html", [
                    [ "OpcUaPlc::CtrlFunctionBlockTypeBase", "classOpcUaPlc_1_1CtrlFunctionBlockTypeBase.html", [
                      [ "OpcUaPlc::CtrlFunctionBlockType", "classOpcUaPlc_1_1CtrlFunctionBlockType.html", null ]
                    ] ],
                    [ "OpcUaPlc::CtrlProgramTypeBase", "classOpcUaPlc_1_1CtrlProgramTypeBase.html", [
                      [ "OpcUaPlc::CtrlProgramType", "classOpcUaPlc_1_1CtrlProgramType.html", null ]
                    ] ]
                  ] ]
                ] ]
              ] ]
            ] ],
            [ "OpcUaDi::ConnectionPointTypeBase", "classOpcUaDi_1_1ConnectionPointTypeBase.html", [
              [ "OpcUaDi::ConnectionPointType", "classOpcUaDi_1_1ConnectionPointType.html", null ]
            ] ],
            [ "OpcUaDi::DeviceTypeBase", "classOpcUaDi_1_1DeviceTypeBase.html", [
              [ "OpcUaDi::DeviceType", "classOpcUaDi_1_1DeviceType.html", [
                [ "OpcUaPlc::CtrlConfigurationTypeBase", "classOpcUaPlc_1_1CtrlConfigurationTypeBase.html", [
                  [ "OpcUaPlc::CtrlConfigurationType", "classOpcUaPlc_1_1CtrlConfigurationType.html", null ]
                ] ],
                [ "OpcUaPlc::CtrlResourceTypeBase", "classOpcUaPlc_1_1CtrlResourceTypeBase.html", [
                  [ "OpcUaPlc::CtrlResourceType", "classOpcUaPlc_1_1CtrlResourceType.html", null ]
                ] ]
              ] ]
            ] ]
          ] ]
        ] ],
        [ "OpcUaDi::TransferServicesTypeBase", "classOpcUaDi_1_1TransferServicesTypeBase.html", [
          [ "OpcUaDi::TransferServicesType", "classOpcUaDi_1_1TransferServicesType.html", null ]
        ] ],
        [ "OpcUaPlc::CtrlTaskTypeBase", "classOpcUaPlc_1_1CtrlTaskTypeBase.html", [
          [ "OpcUaPlc::CtrlTaskType", "classOpcUaPlc_1_1CtrlTaskType.html", null ]
        ] ],
        [ "OpcUaPlc::SFCTypeBase", "classOpcUaPlc_1_1SFCTypeBase.html", [
          [ "OpcUaPlc::SFCType", "classOpcUaPlc_1_1SFCType.html", null ]
        ] ]
      ] ]
    ] ],
    [ "MethodManagerCallback", "classMethodManagerCallback.html", [
      [ "UaCallContext", "classUaCallContext.html", null ]
    ] ],
    [ "MonitoringContext", "classMonitoringContext.html", null ],
    [ "NamespaceMappingInterface", "classNamespaceMappingInterface.html", null ],
    [ "NewEventContext", "classNewEventContext.html", null ],
    [ "NodeManagerConfig", "classNodeManagerConfig.html", [
      [ "NodeManagerUaNode", "classNodeManagerUaNode.html", [
        [ "NodeManagerBase", "classNodeManagerBase.html", null ]
      ] ]
    ] ],
    [ "NodeManagerCrossReferences", "classNodeManagerCrossReferences.html", [
      [ "NodeManagerUaNode", "classNodeManagerUaNode.html", null ]
    ] ],
    [ "NodeManagerList", "classNodeManagerList.html", [
      [ "NodeManagerRoot", "classNodeManagerRoot.html", null ]
    ] ],
    [ "NodeManagerNodeSetXmlCreator", "classNodeManagerNodeSetXmlCreator.html", null ],
    [ "OpcServerPrivate", "classOpcServerPrivate.html", null ],
    [ "OpcUa_ActivateSessionRequest", "structOpcUa__ActivateSessionRequest.html", null ],
    [ "OpcUa_ActivateSessionResponse", "structOpcUa__ActivateSessionResponse.html", null ],
    [ "OpcUa_AddNodesItem", "structOpcUa__AddNodesItem.html", null ],
    [ "OpcUa_AddNodesRequest", "structOpcUa__AddNodesRequest.html", null ],
    [ "OpcUa_AddNodesResponse", "structOpcUa__AddNodesResponse.html", null ],
    [ "OpcUa_AddNodesResult", "structOpcUa__AddNodesResult.html", null ],
    [ "OpcUa_AddReferencesItem", "structOpcUa__AddReferencesItem.html", null ],
    [ "OpcUa_AddReferencesRequest", "structOpcUa__AddReferencesRequest.html", null ],
    [ "OpcUa_AddReferencesResponse", "structOpcUa__AddReferencesResponse.html", null ],
    [ "OpcUa_AggregateConfiguration", "structOpcUa__AggregateConfiguration.html", null ],
    [ "OpcUa_Annotation", "structOpcUa__Annotation.html", null ],
    [ "OpcUa_AnonymousIdentityToken", "structOpcUa__AnonymousIdentityToken.html", null ],
    [ "OpcUa_ApplicationDescription", "structOpcUa__ApplicationDescription.html", null ],
    [ "OpcUa_Argument", "structOpcUa__Argument.html", null ],
    [ "OpcUa_ArrayTestType", "structOpcUa__ArrayTestType.html", null ],
    [ "OpcUa_AttributeOperand", "structOpcUa__AttributeOperand.html", null ],
    [ "OpcUa_AxisInformation", "structOpcUa__AxisInformation.html", null ],
    [ "OpcUa_BrowseDescription", "structOpcUa__BrowseDescription.html", null ],
    [ "OpcUa_BrowseNextRequest", "structOpcUa__BrowseNextRequest.html", null ],
    [ "OpcUa_BrowseNextResponse", "structOpcUa__BrowseNextResponse.html", null ],
    [ "OpcUa_BrowsePath", "structOpcUa__BrowsePath.html", null ],
    [ "OpcUa_BrowsePathResult", "structOpcUa__BrowsePathResult.html", null ],
    [ "OpcUa_BrowsePathTarget", "structOpcUa__BrowsePathTarget.html", null ],
    [ "OpcUa_BrowseRequest", "structOpcUa__BrowseRequest.html", null ],
    [ "OpcUa_BrowseResponse", "structOpcUa__BrowseResponse.html", null ],
    [ "OpcUa_BrowseResult", "structOpcUa__BrowseResult.html", null ],
    [ "OpcUa_BuildInfo", "structOpcUa__BuildInfo.html", null ],
    [ "OpcUa_ByteString", "structOpcUa__ByteString.html", [
      [ "UaByteString", "classUaByteString.html", null ]
    ] ],
    [ "OpcUa_CallMethodRequest", "structOpcUa__CallMethodRequest.html", null ],
    [ "OpcUa_CallMethodResult", "structOpcUa__CallMethodResult.html", null ],
    [ "OpcUa_CallRequest", "structOpcUa__CallRequest.html", null ],
    [ "OpcUa_CallResponse", "structOpcUa__CallResponse.html", null ],
    [ "OpcUa_CancelRequest", "structOpcUa__CancelRequest.html", null ],
    [ "OpcUa_CancelResponse", "structOpcUa__CancelResponse.html", null ],
    [ "OpcUa_ChannelSecurityToken", "structOpcUa__ChannelSecurityToken.html", null ],
    [ "OpcUa_CloseSecureChannelRequest", "structOpcUa__CloseSecureChannelRequest.html", null ],
    [ "OpcUa_CloseSecureChannelResponse", "structOpcUa__CloseSecureChannelResponse.html", null ],
    [ "OpcUa_CloseSessionRequest", "structOpcUa__CloseSessionRequest.html", null ],
    [ "OpcUa_CloseSessionResponse", "structOpcUa__CloseSessionResponse.html", null ],
    [ "OpcUa_ContentFilter", "structOpcUa__ContentFilter.html", null ],
    [ "OpcUa_ContentFilterElement", "structOpcUa__ContentFilterElement.html", null ],
    [ "OpcUa_ContentFilterElementResult", "structOpcUa__ContentFilterElementResult.html", null ],
    [ "OpcUa_ContentFilterResult", "structOpcUa__ContentFilterResult.html", null ],
    [ "OpcUa_CreateMonitoredItemsRequest", "structOpcUa__CreateMonitoredItemsRequest.html", null ],
    [ "OpcUa_CreateMonitoredItemsResponse", "structOpcUa__CreateMonitoredItemsResponse.html", null ],
    [ "OpcUa_CreateSessionRequest", "structOpcUa__CreateSessionRequest.html", null ],
    [ "OpcUa_CreateSessionResponse", "structOpcUa__CreateSessionResponse.html", null ],
    [ "OpcUa_CreateSubscriptionRequest", "structOpcUa__CreateSubscriptionRequest.html", null ],
    [ "OpcUa_CreateSubscriptionResponse", "structOpcUa__CreateSubscriptionResponse.html", null ],
    [ "OpcUa_DataTypeAttributes", "structOpcUa__DataTypeAttributes.html", [
      [ "UaDataTypeAttributes", "classUaDataTypeAttributes.html", null ]
    ] ],
    [ "OpcUa_DataValue", "structOpcUa__DataValue.html", null ],
    [ "OpcUa_DateTime", "structOpcUa__DateTime.html", null ],
    [ "OpcUa_DeleteAtTimeDetails", "structOpcUa__DeleteAtTimeDetails.html", null ],
    [ "OpcUa_DeleteEventDetails", "structOpcUa__DeleteEventDetails.html", null ],
    [ "OpcUa_DeleteMonitoredItemsRequest", "structOpcUa__DeleteMonitoredItemsRequest.html", null ],
    [ "OpcUa_DeleteMonitoredItemsResponse", "structOpcUa__DeleteMonitoredItemsResponse.html", null ],
    [ "OpcUa_DeleteNodesItem", "structOpcUa__DeleteNodesItem.html", null ],
    [ "OpcUa_DeleteNodesRequest", "structOpcUa__DeleteNodesRequest.html", null ],
    [ "OpcUa_DeleteNodesResponse", "structOpcUa__DeleteNodesResponse.html", null ],
    [ "OpcUa_DeleteRawModifiedDetails", "structOpcUa__DeleteRawModifiedDetails.html", null ],
    [ "OpcUa_DeleteReferencesItem", "structOpcUa__DeleteReferencesItem.html", null ],
    [ "OpcUa_DeleteReferencesRequest", "structOpcUa__DeleteReferencesRequest.html", null ],
    [ "OpcUa_DeleteReferencesResponse", "structOpcUa__DeleteReferencesResponse.html", null ],
    [ "OpcUa_DeleteSubscriptionsRequest", "structOpcUa__DeleteSubscriptionsRequest.html", null ],
    [ "OpcUa_DeleteSubscriptionsResponse", "structOpcUa__DeleteSubscriptionsResponse.html", null ],
    [ "OpcUa_DiagnosticInfo", "structOpcUa__DiagnosticInfo.html", null ],
    [ "OpcUa_ElementOperand", "structOpcUa__ElementOperand.html", null ],
    [ "OpcUa_EncodeableType", "structOpcUa__EncodeableType.html", null ],
    [ "OpcUa_EncodeableTypeTable", "structOpcUa__EncodeableTypeTable.html", null ],
    [ "OpcUa_EndpointDescription", "structOpcUa__EndpointDescription.html", null ],
    [ "OpcUa_EndpointUrlListDataType", "structOpcUa__EndpointUrlListDataType.html", null ],
    [ "OpcUa_EnumValueType", "structOpcUa__EnumValueType.html", null ],
    [ "OpcUa_EUInformation", "structOpcUa__EUInformation.html", null ],
    [ "OpcUa_EventFilter", "structOpcUa__EventFilter.html", null ],
    [ "OpcUa_EventFilterResult", "structOpcUa__EventFilterResult.html", null ],
    [ "OpcUa_ExpandedNodeId", "structOpcUa__ExpandedNodeId.html", [
      [ "UaExpandedNodeId", "classUaExpandedNodeId.html", null ]
    ] ],
    [ "OpcUa_ExtensionObject", "structOpcUa__ExtensionObject.html", [
      [ "UaExtensionObject", "classUaExtensionObject.html", null ]
    ] ],
    [ "OpcUa_FindServersRequest", "structOpcUa__FindServersRequest.html", null ],
    [ "OpcUa_FindServersResponse", "structOpcUa__FindServersResponse.html", null ],
    [ "OpcUa_GetEndpointsRequest", "structOpcUa__GetEndpointsRequest.html", null ],
    [ "OpcUa_GetEndpointsResponse", "structOpcUa__GetEndpointsResponse.html", null ],
    [ "OpcUa_Guid", "structOpcUa__Guid.html", [
      [ "UaGuid", "classUaGuid.html", null ]
    ] ],
    [ "OpcUa_HistoryEvent", "structOpcUa__HistoryEvent.html", null ],
    [ "OpcUa_HistoryReadRequest", "structOpcUa__HistoryReadRequest.html", null ],
    [ "OpcUa_HistoryReadResponse", "structOpcUa__HistoryReadResponse.html", null ],
    [ "OpcUa_HistoryReadResult", "structOpcUa__HistoryReadResult.html", null ],
    [ "OpcUa_HistoryReadValueId", "structOpcUa__HistoryReadValueId.html", null ],
    [ "OpcUa_HistoryUpdateRequest", "structOpcUa__HistoryUpdateRequest.html", null ],
    [ "OpcUa_HistoryUpdateResponse", "structOpcUa__HistoryUpdateResponse.html", null ],
    [ "OpcUa_HistoryUpdateResult", "structOpcUa__HistoryUpdateResult.html", null ],
    [ "OpcUa_IssuedIdentityToken", "structOpcUa__IssuedIdentityToken.html", null ],
    [ "OpcUa_LocalizedText", "structOpcUa__LocalizedText.html", [
      [ "UaLocalizedText", "classUaLocalizedText.html", null ]
    ] ],
    [ "OpcUa_MdnsDiscoveryConfiguration", "structOpcUa__MdnsDiscoveryConfiguration.html", null ],
    [ "OpcUa_MethodAttributes", "structOpcUa__MethodAttributes.html", [
      [ "UaMethodAttributes", "classUaMethodAttributes.html", null ]
    ] ],
    [ "OpcUa_MethodNode", "structOpcUa__MethodNode.html", null ],
    [ "OpcUa_ModelChangeStructureDataType", "structOpcUa__ModelChangeStructureDataType.html", null ],
    [ "OpcUa_ModifyMonitoredItemsRequest", "structOpcUa__ModifyMonitoredItemsRequest.html", null ],
    [ "OpcUa_ModifyMonitoredItemsResponse", "structOpcUa__ModifyMonitoredItemsResponse.html", null ],
    [ "OpcUa_ModifySubscriptionRequest", "structOpcUa__ModifySubscriptionRequest.html", null ],
    [ "OpcUa_ModifySubscriptionResponse", "structOpcUa__ModifySubscriptionResponse.html", null ],
    [ "OpcUa_MonitoredItemCreateRequest", "structOpcUa__MonitoredItemCreateRequest.html", null ],
    [ "OpcUa_MonitoredItemCreateResult", "structOpcUa__MonitoredItemCreateResult.html", null ],
    [ "OpcUa_MonitoredItemModifyRequest", "structOpcUa__MonitoredItemModifyRequest.html", null ],
    [ "OpcUa_MonitoredItemModifyResult", "structOpcUa__MonitoredItemModifyResult.html", null ],
    [ "OpcUa_MonitoringParameters", "structOpcUa__MonitoringParameters.html", null ],
    [ "OpcUa_NetworkGroupDataType", "structOpcUa__NetworkGroupDataType.html", null ],
    [ "OpcUa_Node", "structOpcUa__Node.html", null ],
    [ "OpcUa_NodeAttributes", "structOpcUa__NodeAttributes.html", null ],
    [ "OpcUa_NodeId", "structOpcUa__NodeId.html", null ],
    [ "OpcUa_NodeTypeDescription", "structOpcUa__NodeTypeDescription.html", null ],
    [ "OpcUa_ObjectAttributes", "structOpcUa__ObjectAttributes.html", [
      [ "UaObjectAttributes", "classUaObjectAttributes.html", null ]
    ] ],
    [ "OpcUa_ObjectNode", "structOpcUa__ObjectNode.html", null ],
    [ "OpcUa_ObjectTypeAttributes", "structOpcUa__ObjectTypeAttributes.html", [
      [ "UaObjectTypeAttributes", "classUaObjectTypeAttributes.html", null ]
    ] ],
    [ "OpcUa_ObjectTypeNode", "structOpcUa__ObjectTypeNode.html", null ],
    [ "OpcUa_OpenSecureChannelRequest", "structOpcUa__OpenSecureChannelRequest.html", null ],
    [ "OpcUa_OpenSecureChannelResponse", "structOpcUa__OpenSecureChannelResponse.html", null ],
    [ "OpcUa_OptionSet", "structOpcUa__OptionSet.html", null ],
    [ "OpcUa_ParsingResult", "structOpcUa__ParsingResult.html", null ],
    [ "OpcUa_PublishRequest", "structOpcUa__PublishRequest.html", null ],
    [ "OpcUa_PublishResponse", "structOpcUa__PublishResponse.html", null ],
    [ "OpcUa_QualifiedName", "structOpcUa__QualifiedName.html", [
      [ "UaQualifiedName", "classUaQualifiedName.html", null ]
    ] ],
    [ "OpcUa_QueryDataDescription", "structOpcUa__QueryDataDescription.html", null ],
    [ "OpcUa_QueryFirstRequest", "structOpcUa__QueryFirstRequest.html", null ],
    [ "OpcUa_QueryFirstResponse", "structOpcUa__QueryFirstResponse.html", null ],
    [ "OpcUa_QueryNextRequest", "structOpcUa__QueryNextRequest.html", null ],
    [ "OpcUa_QueryNextResponse", "structOpcUa__QueryNextResponse.html", null ],
    [ "OpcUa_Range", "structOpcUa__Range.html", null ],
    [ "OpcUa_ReadAtTimeDetails", "structOpcUa__ReadAtTimeDetails.html", null ],
    [ "OpcUa_ReadEventDetails", "structOpcUa__ReadEventDetails.html", null ],
    [ "OpcUa_ReadProcessedDetails", "structOpcUa__ReadProcessedDetails.html", null ],
    [ "OpcUa_ReadRawModifiedDetails", "structOpcUa__ReadRawModifiedDetails.html", null ],
    [ "OpcUa_ReadRequest", "structOpcUa__ReadRequest.html", null ],
    [ "OpcUa_ReadResponse", "structOpcUa__ReadResponse.html", null ],
    [ "OpcUa_ReadValueId", "structOpcUa__ReadValueId.html", null ],
    [ "OpcUa_RedundantServerDataType", "structOpcUa__RedundantServerDataType.html", null ],
    [ "OpcUa_ReferenceDescription", "structOpcUa__ReferenceDescription.html", null ],
    [ "OpcUa_ReferenceNode", "structOpcUa__ReferenceNode.html", null ],
    [ "OpcUa_ReferenceTypeAttributes", "structOpcUa__ReferenceTypeAttributes.html", [
      [ "UaReferenceTypeAttributes", "classUaReferenceTypeAttributes.html", null ]
    ] ],
    [ "OpcUa_ReferenceTypeNode", "structOpcUa__ReferenceTypeNode.html", null ],
    [ "OpcUa_RegisteredServer", "structOpcUa__RegisteredServer.html", null ],
    [ "OpcUa_RegisterNodesRequest", "structOpcUa__RegisterNodesRequest.html", null ],
    [ "OpcUa_RegisterNodesResponse", "structOpcUa__RegisterNodesResponse.html", null ],
    [ "OpcUa_RegisterServerRequest", "structOpcUa__RegisterServerRequest.html", null ],
    [ "OpcUa_RegisterServerResponse", "structOpcUa__RegisterServerResponse.html", null ],
    [ "OpcUa_RelativePath", "structOpcUa__RelativePath.html", null ],
    [ "OpcUa_RelativePathElement", "structOpcUa__RelativePathElement.html", null ],
    [ "OpcUa_RepublishRequest", "structOpcUa__RepublishRequest.html", null ],
    [ "OpcUa_RepublishResponse", "structOpcUa__RepublishResponse.html", null ],
    [ "OpcUa_RequestHeader", "structOpcUa__RequestHeader.html", null ],
    [ "OpcUa_ResponseHeader", "structOpcUa__ResponseHeader.html", null ],
    [ "OpcUa_SamplingIntervalDiagnosticsDataType", "structOpcUa__SamplingIntervalDiagnosticsDataType.html", null ],
    [ "OpcUa_ScalarTestType", "structOpcUa__ScalarTestType.html", null ],
    [ "OpcUa_SemanticChangeStructureDataType", "structOpcUa__SemanticChangeStructureDataType.html", null ],
    [ "OpcUa_ServerDiagnosticsSummaryDataType", "structOpcUa__ServerDiagnosticsSummaryDataType.html", null ],
    [ "OpcUa_ServerStatusDataType", "structOpcUa__ServerStatusDataType.html", null ],
    [ "OpcUa_ServiceCounterDataType", "structOpcUa__ServiceCounterDataType.html", null ],
    [ "OpcUa_ServiceFault", "structOpcUa__ServiceFault.html", null ],
    [ "OpcUa_SessionDiagnosticsDataType", "structOpcUa__SessionDiagnosticsDataType.html", null ],
    [ "OpcUa_SessionSecurityDiagnosticsDataType", "structOpcUa__SessionSecurityDiagnosticsDataType.html", null ],
    [ "OpcUa_SetMonitoringModeRequest", "structOpcUa__SetMonitoringModeRequest.html", null ],
    [ "OpcUa_SetMonitoringModeResponse", "structOpcUa__SetMonitoringModeResponse.html", null ],
    [ "OpcUa_SetPublishingModeRequest", "structOpcUa__SetPublishingModeRequest.html", null ],
    [ "OpcUa_SetPublishingModeResponse", "structOpcUa__SetPublishingModeResponse.html", null ],
    [ "OpcUa_SetTriggeringRequest", "structOpcUa__SetTriggeringRequest.html", null ],
    [ "OpcUa_SetTriggeringResponse", "structOpcUa__SetTriggeringResponse.html", null ],
    [ "OpcUa_SignatureData", "structOpcUa__SignatureData.html", null ],
    [ "OpcUa_SignedSoftwareCertificate", "structOpcUa__SignedSoftwareCertificate.html", null ],
    [ "OpcUa_SimpleAttributeOperand", "structOpcUa__SimpleAttributeOperand.html", null ],
    [ "OpcUa_SoftwareCertificate", "structOpcUa__SoftwareCertificate.html", null ],
    [ "OpcUa_StatusCode", "structOpcUa__StatusCode.html", null ],
    [ "OpcUa_String", "structOpcUa__String.html", null ],
    [ "OpcUa_SubscriptionAcknowledgement", "structOpcUa__SubscriptionAcknowledgement.html", null ],
    [ "OpcUa_SubscriptionDiagnosticsDataType", "structOpcUa__SubscriptionDiagnosticsDataType.html", null ],
    [ "OpcUa_TimeZoneDataType", "structOpcUa__TimeZoneDataType.html", null ],
    [ "OpcUa_TransferResult", "structOpcUa__TransferResult.html", null ],
    [ "OpcUa_TransferSubscriptionsRequest", "structOpcUa__TransferSubscriptionsRequest.html", null ],
    [ "OpcUa_TransferSubscriptionsResponse", "structOpcUa__TransferSubscriptionsResponse.html", null ],
    [ "OpcUa_TranslateBrowsePathsToNodeIdsRequest", "structOpcUa__TranslateBrowsePathsToNodeIdsRequest.html", null ],
    [ "OpcUa_TranslateBrowsePathsToNodeIdsResponse", "structOpcUa__TranslateBrowsePathsToNodeIdsResponse.html", null ],
    [ "OpcUa_TrustListDataType", "structOpcUa__TrustListDataType.html", null ],
    [ "OpcUa_UnregisterNodesRequest", "structOpcUa__UnregisterNodesRequest.html", null ],
    [ "OpcUa_UnregisterNodesResponse", "structOpcUa__UnregisterNodesResponse.html", null ],
    [ "OpcUa_UpdateDataDetails", "structOpcUa__UpdateDataDetails.html", null ],
    [ "OpcUa_UpdateEventDetails", "structOpcUa__UpdateEventDetails.html", null ],
    [ "OpcUa_UpdateStructureDataDetails", "structOpcUa__UpdateStructureDataDetails.html", null ],
    [ "OpcUa_UserIdentityToken", "structOpcUa__UserIdentityToken.html", null ],
    [ "OpcUa_UserNameIdentityToken", "structOpcUa__UserNameIdentityToken.html", null ],
    [ "OpcUa_UserTokenPolicy", "structOpcUa__UserTokenPolicy.html", null ],
    [ "OpcUa_VariableAttributes", "structOpcUa__VariableAttributes.html", [
      [ "UaVariableAttributes", "classUaVariableAttributes.html", null ]
    ] ],
    [ "OpcUa_VariableNode", "structOpcUa__VariableNode.html", null ],
    [ "OpcUa_VariableTypeAttributes", "structOpcUa__VariableTypeAttributes.html", [
      [ "UaVariableTypeAttributes", "classUaVariableTypeAttributes.html", null ]
    ] ],
    [ "OpcUa_VariableTypeNode", "structOpcUa__VariableTypeNode.html", null ],
    [ "OpcUa_Variant", "structOpcUa__Variant.html", null ],
    [ "OpcUa_ViewAttributes", "structOpcUa__ViewAttributes.html", [
      [ "UaViewAttributes", "classUaViewAttributes.html", null ]
    ] ],
    [ "OpcUa_ViewDescription", "structOpcUa__ViewDescription.html", null ],
    [ "OpcUa_WriteRequest", "structOpcUa__WriteRequest.html", null ],
    [ "OpcUa_WriteResponse", "structOpcUa__WriteResponse.html", null ],
    [ "OpcUa_WriteValue", "structOpcUa__WriteValue.html", null ],
    [ "OpcUa_X509IdentityToken", "structOpcUa__X509IdentityToken.html", null ],
    [ "OpcUa_XmlElement", "structOpcUa__XmlElement.html", null ],
    [ "OpcUa_XVType", "structOpcUa__XVType.html", null ],
    [ "OpcUaDi::ParameterResultDataType", "classOpcUaDi_1_1ParameterResultDataType.html", null ],
    [ "OpcUa::ProgramStateMachineCallback", "classOpcUa_1_1ProgramStateMachineCallback.html", null ],
    [ "QueryContext", "classQueryContext.html", null ],
    [ "UaBase::Reference", "classUaBase_1_1Reference.html", null ],
    [ "ReferenceCounter", "classReferenceCounter.html", [
      [ "CertificateConfiguration", "classCertificateConfiguration.html", null ],
      [ "CertificateStoreConfiguration", "classCertificateStoreConfiguration.html", null ],
      [ "ChainEntry", "classChainEntry.html", [
        [ "UaNode", "classUaNode.html", [
          [ "UaDataType", "classUaDataType.html", [
            [ "UaDataTypeNS0", "classUaDataTypeNS0.html", null ],
            [ "UaGenericDataType", "classUaGenericDataType.html", null ]
          ] ],
          [ "UaMethod", "classUaMethod.html", [
            [ "OpcUa::BaseMethod", "classOpcUa_1_1BaseMethod.html", null ],
            [ "UaGenericMethod", "classUaGenericMethod.html", null ],
            [ "UaMethodGeneric", "classUaMethodGeneric.html", null ]
          ] ],
          [ "UaObject", "classUaObject.html", [
            [ "OpcUa::BaseObjectType", "classOpcUa_1_1BaseObjectType.html", null ],
            [ "UaGenericObject", "classUaGenericObject.html", null ],
            [ "UaObjectBase", "classUaObjectBase.html", [
              [ "UaFolder", "classUaFolder.html", [
                [ "UaAreaFolder", "classUaAreaFolder.html", null ]
              ] ],
              [ "UaModelingRule", "classUaModelingRule.html", null ],
              [ "UaObjectServer", "classUaObjectServer.html", null ],
              [ "UaObjectServerCapabilities", "classUaObjectServerCapabilities.html", null ],
              [ "UaObjectServerDiagnostic", "classUaObjectServerDiagnostic.html", null ],
              [ "UaObjectVendorServerInfo", "classUaObjectVendorServerInfo.html", null ]
            ] ]
          ] ],
          [ "UaObjectType", "classUaObjectType.html", [
            [ "UaGenericObjectType", "classUaGenericObjectType.html", null ],
            [ "UaObjectTypeSimple", "classUaObjectTypeSimple.html", null ]
          ] ],
          [ "UaReferenceType", "classUaReferenceType.html", [
            [ "UaGenericReferenceType", "classUaGenericReferenceType.html", null ],
            [ "UaReferenceTypeNS0", "classUaReferenceTypeNS0.html", null ]
          ] ],
          [ "UaVariable", "classUaVariable.html", [
            [ "UaDataVariableDevice", "classUaDataVariableDevice.html", null ],
            [ "UaVariableCache", "classUaVariableCache.html", [
              [ "OpcUa::BaseVariableType", "classOpcUa_1_1BaseVariableType.html", [
                [ "OpcUa::BaseDataVariableType", "classOpcUa_1_1BaseDataVariableType.html", [
                  [ "OpcUa::ConditionVariableType", "classOpcUa_1_1ConditionVariableType.html", null ],
                  [ "OpcUa::DataItemType", "classOpcUa_1_1DataItemType.html", [
                    [ "OpcUa::AnalogItemType", "classOpcUa_1_1AnalogItemType.html", null ],
                    [ "OpcUa::ArrayItemType", "classOpcUa_1_1ArrayItemType.html", [
                      [ "OpcUa::CubeItemType", "classOpcUa_1_1CubeItemType.html", null ],
                      [ "OpcUa::ImageItemType", "classOpcUa_1_1ImageItemType.html", null ],
                      [ "OpcUa::NDimensionArrayItemType", "classOpcUa_1_1NDimensionArrayItemType.html", null ],
                      [ "OpcUa::XYArrayItemType", "classOpcUa_1_1XYArrayItemType.html", null ],
                      [ "OpcUa::YArrayItemType", "classOpcUa_1_1YArrayItemType.html", null ]
                    ] ],
                    [ "OpcUa::DiscreteItemType", "classOpcUa_1_1DiscreteItemType.html", [
                      [ "OpcUa::MultiStateDiscreteType", "classOpcUa_1_1MultiStateDiscreteType.html", null ],
                      [ "OpcUa::MultiStateValueDiscreteType", "classOpcUa_1_1MultiStateValueDiscreteType.html", null ],
                      [ "OpcUa::TwoStateDiscreteType", "classOpcUa_1_1TwoStateDiscreteType.html", null ]
                    ] ]
                  ] ],
                  [ "OpcUa::DataTypeDescriptionType", "classOpcUa_1_1DataTypeDescriptionType.html", null ],
                  [ "OpcUa::DataTypeDictionaryType", "classOpcUa_1_1DataTypeDictionaryType.html", null ],
                  [ "OpcUa::OptionSetType", "classOpcUa_1_1OptionSetType.html", null ],
                  [ "OpcUa::ProgramDiagnosticType", "classOpcUa_1_1ProgramDiagnosticType.html", null ],
                  [ "OpcUa::StateVariableType", "classOpcUa_1_1StateVariableType.html", [
                    [ "OpcUa::FiniteStateVariableType", "classOpcUa_1_1FiniteStateVariableType.html", null ],
                    [ "OpcUa::TwoStateVariableType", "classOpcUa_1_1TwoStateVariableType.html", null ]
                  ] ],
                  [ "OpcUa::TransitionVariableType", "classOpcUa_1_1TransitionVariableType.html", [
                    [ "OpcUa::FiniteTransitionVariableType", "classOpcUa_1_1FiniteTransitionVariableType.html", null ]
                  ] ]
                ] ],
                [ "OpcUa::BaseVariableTypeGeneric", "classOpcUa_1_1BaseVariableTypeGeneric.html", [
                  [ "OpcUa::BaseVariableTypeLocalizedTextValue", "classOpcUa_1_1BaseVariableTypeLocalizedTextValue.html", null ]
                ] ],
                [ "OpcUa::PropertyType", "classOpcUa_1_1PropertyType.html", null ],
                [ "OpcUaDi::UIElementType", "classOpcUaDi_1_1UIElementType.html", null ]
              ] ],
              [ "UaDataVariableCache", "classUaDataVariableCache.html", [
                [ "UaAnalogItemCache", "classUaAnalogItemCache.html", null ],
                [ "UaPropertyCache", "classUaPropertyCache.html", null ]
              ] ],
              [ "UaGenericVariable", "classUaGenericVariable.html", null ],
              [ "UaPropertyMethodArgument", "classUaPropertyMethodArgument.html", null ]
            ] ]
          ] ],
          [ "UaVariableType", "classUaVariableType.html", [
            [ "UaGenericVariableType", "classUaGenericVariableType.html", null ],
            [ "UaVariableTypeSimple", "classUaVariableTypeSimple.html", null ]
          ] ],
          [ "UaView", "classUaView.html", [
            [ "UaGenericView", "classUaGenericView.html", null ],
            [ "UaViewSimple", "classUaViewSimple.html", null ]
          ] ]
        ] ]
      ] ],
      [ "HistoryVariableHandle", "classHistoryVariableHandle.html", [
        [ "HistoryVariableHandleNodeId", "classHistoryVariableHandleNodeId.html", null ],
        [ "HistoryVariableHandleUaNode", "classHistoryVariableHandleUaNode.html", null ]
      ] ],
      [ "MethodHandle", "classMethodHandle.html", [
        [ "MethodHandleNodeId", "classMethodHandleNodeId.html", null ],
        [ "MethodHandleUaNode", "classMethodHandleUaNode.html", null ]
      ] ],
      [ "NodeAccessInfo", "classNodeAccessInfo.html", null ],
      [ "OpcUa::ConditionTypeData", "classOpcUa_1_1ConditionTypeData.html", [
        [ "OpcUa::AcknowledgeableConditionTypeData", "classOpcUa_1_1AcknowledgeableConditionTypeData.html", [
          [ "OpcUa::AlarmConditionTypeData", "classOpcUa_1_1AlarmConditionTypeData.html", [
            [ "OpcUa::DiscreteAlarmTypeData", "classOpcUa_1_1DiscreteAlarmTypeData.html", [
              [ "OpcUa::OffNormalAlarmTypeData", "classOpcUa_1_1OffNormalAlarmTypeData.html", [
                [ "OpcUa::SystemOffNormalAlarmTypeData", "classOpcUa_1_1SystemOffNormalAlarmTypeData.html", [
                  [ "OpcUa::CertificateExpirationAlarmTypeData", "classOpcUa_1_1CertificateExpirationAlarmTypeData.html", null ]
                ] ],
                [ "OpcUa::TripAlarmTypeData", "classOpcUa_1_1TripAlarmTypeData.html", null ]
              ] ]
            ] ],
            [ "OpcUa::LimitAlarmTypeData", "classOpcUa_1_1LimitAlarmTypeData.html", [
              [ "OpcUa::ExclusiveLimitAlarmTypeData", "classOpcUa_1_1ExclusiveLimitAlarmTypeData.html", [
                [ "OpcUa::ExclusiveDeviationAlarmTypeData", "classOpcUa_1_1ExclusiveDeviationAlarmTypeData.html", null ],
                [ "OpcUa::ExclusiveLevelAlarmTypeData", "classOpcUa_1_1ExclusiveLevelAlarmTypeData.html", null ],
                [ "OpcUa::ExclusiveRateOfChangeAlarmTypeData", "classOpcUa_1_1ExclusiveRateOfChangeAlarmTypeData.html", null ]
              ] ],
              [ "OpcUa::NonExclusiveLimitAlarmTypeData", "classOpcUa_1_1NonExclusiveLimitAlarmTypeData.html", [
                [ "OpcUa::NonExclusiveDeviationAlarmTypeData", "classOpcUa_1_1NonExclusiveDeviationAlarmTypeData.html", null ],
                [ "OpcUa::NonExclusiveLevelAlarmTypeData", "classOpcUa_1_1NonExclusiveLevelAlarmTypeData.html", null ],
                [ "OpcUa::NonExclusiveRateOfChangeAlarmTypeData", "classOpcUa_1_1NonExclusiveRateOfChangeAlarmTypeData.html", null ]
              ] ]
            ] ]
          ] ]
        ] ],
        [ "OpcUa::DialogConditionTypeData", "classOpcUa_1_1DialogConditionTypeData.html", null ]
      ] ],
      [ "OpcUa::TwoStateDisplayNames", "classOpcUa_1_1TwoStateDisplayNames.html", null ],
      [ "Session", "classSession.html", [
        [ "UaSession", "classUaSession.html", null ]
      ] ],
      [ "SessionUserContext", "classSessionUserContext.html", null ],
      [ "UaSubscription", "classUaSubscription.html", null ],
      [ "UserDataReferenceCounted", "classUserDataReferenceCounted.html", null ],
      [ "VariableHandle", "classVariableHandle.html", [
        [ "VariableHandleUaNode", "classVariableHandleUaNode.html", null ]
      ] ]
    ] ],
    [ "RegisteredNode", "classRegisteredNode.html", null ],
    [ "RingBuffer", "classRingBuffer.html", null ],
    [ "SamplingExecution", "classSamplingExecution.html", [
      [ "CacheVariableConnector", "classCacheVariableConnector.html", null ]
    ] ],
    [ "ServerConfig", "classServerConfig.html", [
      [ "ServerConfigData", "classServerConfigData.html", [
        [ "ServerConfigIni", "classServerConfigIni.html", null ],
        [ "ServerConfigXml", "classServerConfigXml.html", null ]
      ] ]
    ] ],
    [ "ServerManager", "classServerManager.html", null ],
    [ "ServiceContext", "classServiceContext.html", null ],
    [ "UaClientSdk::ServiceSettings", "classUaClientSdk_1_1ServiceSettings.html", null ],
    [ "SessionCallback", "classSessionCallback.html", null ],
    [ "UaClientSdk::SessionConnectInfo", "classUaClientSdk_1_1SessionConnectInfo.html", null ],
    [ "SessionManager", "classSessionManager.html", null ],
    [ "SessionUserDataBase", "classSessionUserDataBase.html", null ],
    [ "AggregateCalculator::SubRegion", "structAggregateCalculator_1_1SubRegion.html", null ],
    [ "UaClientSdk::SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html", null ],
    [ "AggregateCalculator::TimeSlice", "structAggregateCalculator_1_1TimeSlice.html", null ],
    [ "OpcUaDi::TransferServicesCallback", "classOpcUaDi_1_1TransferServicesCallback.html", null ],
    [ "UaAbstractDecoder", "classUaAbstractDecoder.html", [
      [ "UaBinaryDecoder", "classUaBinaryDecoder.html", null ]
    ] ],
    [ "UaAbstractDefinition", "classUaAbstractDefinition.html", [
      [ "UaOptionSetDefinition", "classUaOptionSetDefinition.html", null ]
    ] ],
    [ "UaAbstractDefinitionPrivate", "classUaAbstractDefinitionPrivate.html", null ],
    [ "UaAbstractDictionaryReader", "classUaAbstractDictionaryReader.html", null ],
    [ "UaAbstractEncoder", "classUaAbstractEncoder.html", [
      [ "UaBinaryEncoder", "classUaBinaryEncoder.html", null ]
    ] ],
    [ "UaAbstractGenericValue", "classUaAbstractGenericValue.html", [
      [ "UaAbstractGenericStructureValue", "classUaAbstractGenericStructureValue.html", [
        [ "UaGenericStructureValue", "classUaGenericStructureValue.html", null ],
        [ "UaGenericUnionValue", "classUaGenericUnionValue.html", null ]
      ] ],
      [ "UaGenericOptionSetValue", "classUaGenericOptionSetValue.html", null ]
    ] ],
    [ "UaAddNodesItem", "classUaAddNodesItem.html", null ],
    [ "UaAddNodesItems", "classUaAddNodesItems.html", null ],
    [ "UaAddNodesResults", "classUaAddNodesResults.html", null ],
    [ "UaAddReferencesItem", "classUaAddReferencesItem.html", null ],
    [ "UaAddReferencesItems", "classUaAddReferencesItems.html", null ],
    [ "UaAggregateConfiguration", "classUaAggregateConfiguration.html", null ],
    [ "UaAggregateConfigurations", "classUaAggregateConfigurations.html", null ],
    [ "UaAnnotation", "classUaAnnotation.html", null ],
    [ "UaAnnotations", "classUaAnnotations.html", null ],
    [ "UaApplicationControlCallback", "classUaApplicationControlCallback.html", [
      [ "UaServerApplication", "classUaServerApplication.html", [
        [ "OpcServer", "classOpcServer.html", null ]
      ] ]
    ] ],
    [ "UaApplicationDescription", "classUaApplicationDescription.html", null ],
    [ "UaApplicationDescriptions", "classUaApplicationDescriptions.html", null ],
    [ "UaArgument", "classUaArgument.html", null ],
    [ "UaArguments", "classUaArguments.html", null ],
    [ "UaAxisInformation", "classUaAxisInformation.html", null ],
    [ "UaAxisInformations", "classUaAxisInformations.html", null ],
    [ "UaBooleanArray", "classUaBooleanArray.html", null ],
    [ "UaBrowseDescriptions", "classUaBrowseDescriptions.html", null ],
    [ "UaBrowsePathResults", "classUaBrowsePathResults.html", null ],
    [ "UaBrowsePaths", "classUaBrowsePaths.html", null ],
    [ "UaBrowsePathTargets", "classUaBrowsePathTargets.html", null ],
    [ "UaBrowseResults", "classUaBrowseResults.html", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html", null ],
    [ "UaBuildInfos", "classUaBuildInfos.html", null ],
    [ "UaByteArray", "classUaByteArray.html", null ],
    [ "UaByteRef", "classUaByteRef.html", null ],
    [ "UaByteStringArray", "classUaByteStringArray.html", null ],
    [ "UaCallMethodRequests", "classUaCallMethodRequests.html", null ],
    [ "UaCallMethodResults", "classUaCallMethodResults.html", null ],
    [ "UaClientSdk::UaCertificateDirectoryObject", "classUaClientSdk_1_1UaCertificateDirectoryObject.html", null ],
    [ "UaChar", "classUaChar.html", null ],
    [ "UaClientSdk::UaClient", "classUaClientSdk_1_1UaClient.html", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html", null ],
    [ "UaComplexNumberTypes", "classUaComplexNumberTypes.html", null ],
    [ "UaContentFilter", "classUaContentFilter.html", null ],
    [ "UaContentFilterElement", "classUaContentFilterElement.html", null ],
    [ "UaContentFilterElementResult", "classUaContentFilterElementResult.html", null ],
    [ "UaContentFilterResult", "classUaContentFilterResult.html", null ],
    [ "UaCoreServerApplication", "classUaCoreServerApplication.html", [
      [ "UaServerApplication", "classUaServerApplication.html", null ]
    ] ],
    [ "UaDataTypeDictionary", "classUaDataTypeDictionary.html", [
      [ "NodeManagerUaNode", "classNodeManagerUaNode.html", null ],
      [ "TypeDictionariesAccess", "classTypeDictionariesAccess.html", [
        [ "TypeDictionaries", "classTypeDictionaries.html", null ]
      ] ],
      [ "UaClientSdk::UaSession", "classUaClientSdk_1_1UaSession.html", null ]
    ] ],
    [ "UaDataValue", "classUaDataValue.html", null ],
    [ "UaDataValues", "classUaDataValues.html", null ],
    [ "UaDateTime", "classUaDateTime.html", null ],
    [ "UaDateTimeArray", "classUaDateTimeArray.html", null ],
    [ "UaDeleteNodesItem", "classUaDeleteNodesItem.html", null ],
    [ "UaDeleteNodesItems", "classUaDeleteNodesItems.html", null ],
    [ "UaDeleteReferencesItem", "classUaDeleteReferencesItem.html", null ],
    [ "UaDeleteReferencesItems", "classUaDeleteReferencesItems.html", null ],
    [ "UaDiagnosticInfo", "classUaDiagnosticInfo.html", null ],
    [ "UaDir", "classUaDir.html", null ],
    [ "UaClientSdk::UaDiscovery", "classUaClientSdk_1_1UaDiscovery.html", null ],
    [ "UaDoubleArray", "classUaDoubleArray.html", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html", null ],
    [ "UaDoubleComplexNumberTypes", "classUaDoubleComplexNumberTypes.html", null ],
    [ "UaEndpointConfiguration", "classUaEndpointConfiguration.html", null ],
    [ "UaEndpointConfigurations", "classUaEndpointConfigurations.html", null ],
    [ "UaEndpointDescription", "classUaEndpointDescription.html", null ],
    [ "UaEndpointDescriptions", "classUaEndpointDescriptions.html", null ],
    [ "UaEndpointSecuritySetting", "classUaEndpointSecuritySetting.html", null ],
    [ "UaEndpointUrlListDataType", "classUaEndpointUrlListDataType.html", null ],
    [ "UaEndpointUrlListDataTypes", "classUaEndpointUrlListDataTypes.html", null ],
    [ "UaEnumDefinition", "classUaEnumDefinition.html", null ],
    [ "UaEnumValue", "classUaEnumValue.html", null ],
    [ "UaEnumValueType", "classUaEnumValueType.html", null ],
    [ "UaEnumValueTypes", "classUaEnumValueTypes.html", null ],
    [ "UaEUInformation", "classUaEUInformation.html", null ],
    [ "UaEUInformations", "classUaEUInformations.html", null ],
    [ "UaEventData", "classUaEventData.html", [
      [ "BaseEventTypeData", "classBaseEventTypeData.html", [
        [ "OpcUa::AuditEventTypeData", "classOpcUa_1_1AuditEventTypeData.html", [
          [ "OpcUa::AuditNodeManagementEventTypeData", "classOpcUa_1_1AuditNodeManagementEventTypeData.html", [
            [ "OpcUa::AuditAddNodesEventTypeData", "classOpcUa_1_1AuditAddNodesEventTypeData.html", null ],
            [ "OpcUa::AuditAddReferencesEventTypeData", "classOpcUa_1_1AuditAddReferencesEventTypeData.html", null ],
            [ "OpcUa::AuditDeleteNodesEventTypeData", "classOpcUa_1_1AuditDeleteNodesEventTypeData.html", null ],
            [ "OpcUa::AuditDeleteReferencesEventTypeData", "classOpcUa_1_1AuditDeleteReferencesEventTypeData.html", null ]
          ] ],
          [ "OpcUa::AuditSecurityEventTypeData", "classOpcUa_1_1AuditSecurityEventTypeData.html", [
            [ "OpcUa::AuditCertificateEventTypeData", "classOpcUa_1_1AuditCertificateEventTypeData.html", [
              [ "OpcUa::AuditCertificateDataMismatchEventTypeData", "classOpcUa_1_1AuditCertificateDataMismatchEventTypeData.html", null ],
              [ "OpcUa::AuditCertificateExpiredEventTypeData", "classOpcUa_1_1AuditCertificateExpiredEventTypeData.html", null ],
              [ "OpcUa::AuditCertificateInvalidEventTypeData", "classOpcUa_1_1AuditCertificateInvalidEventTypeData.html", null ],
              [ "OpcUa::AuditCertificateMismatchEventTypeData", "classOpcUa_1_1AuditCertificateMismatchEventTypeData.html", null ],
              [ "OpcUa::AuditCertificateRevokedEventTypeData", "classOpcUa_1_1AuditCertificateRevokedEventTypeData.html", null ],
              [ "OpcUa::AuditCertificateUntrustedEventTypeData", "classOpcUa_1_1AuditCertificateUntrustedEventTypeData.html", null ]
            ] ],
            [ "OpcUa::AuditChannelEventTypeData", "classOpcUa_1_1AuditChannelEventTypeData.html", [
              [ "OpcUa::AuditOpenSecureChannelEventTypeData", "classOpcUa_1_1AuditOpenSecureChannelEventTypeData.html", null ]
            ] ],
            [ "OpcUa::AuditSessionEventTypeData", "classOpcUa_1_1AuditSessionEventTypeData.html", [
              [ "OpcUa::AuditActivateSessionEventTypeData", "classOpcUa_1_1AuditActivateSessionEventTypeData.html", null ],
              [ "OpcUa::AuditCancelEventTypeData", "classOpcUa_1_1AuditCancelEventTypeData.html", null ],
              [ "OpcUa::AuditCreateSessionEventTypeData", "classOpcUa_1_1AuditCreateSessionEventTypeData.html", [
                [ "OpcUa::AuditUrlMismatchEventTypeData", "classOpcUa_1_1AuditUrlMismatchEventTypeData.html", null ]
              ] ]
            ] ]
          ] ],
          [ "OpcUa::AuditUpdateEventTypeData", "classOpcUa_1_1AuditUpdateEventTypeData.html", [
            [ "OpcUa::AuditHistoryUpdateEventTypeData", "classOpcUa_1_1AuditHistoryUpdateEventTypeData.html", [
              [ "OpcUa::AuditHistoryDeleteEventTypeData", "classOpcUa_1_1AuditHistoryDeleteEventTypeData.html", [
                [ "OpcUa::AuditHistoryAtTimeDeleteEventTypeData", "classOpcUa_1_1AuditHistoryAtTimeDeleteEventTypeData.html", null ],
                [ "OpcUa::AuditHistoryEventDeleteEventTypeData", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html", null ],
                [ "OpcUa::AuditHistoryRawModifyDeleteEventTypeData", "classOpcUa_1_1AuditHistoryRawModifyDeleteEventTypeData.html", null ]
              ] ],
              [ "OpcUa::AuditHistoryEventUpdateEventTypeData", "classOpcUa_1_1AuditHistoryEventUpdateEventTypeData.html", null ],
              [ "OpcUa::AuditHistoryValueUpdateEventTypeData", "classOpcUa_1_1AuditHistoryValueUpdateEventTypeData.html", null ]
            ] ],
            [ "OpcUa::AuditWriteUpdateEventTypeData", "classOpcUa_1_1AuditWriteUpdateEventTypeData.html", null ]
          ] ],
          [ "OpcUa::AuditUpdateMethodEventTypeData", "classOpcUa_1_1AuditUpdateMethodEventTypeData.html", [
            [ "OpcUa::AuditConditionEventTypeData", "classOpcUa_1_1AuditConditionEventTypeData.html", [
              [ "OpcUa::AuditConditionAcknowledgeEventTypeData", "classOpcUa_1_1AuditConditionAcknowledgeEventTypeData.html", null ],
              [ "OpcUa::AuditConditionCommentEventTypeData", "classOpcUa_1_1AuditConditionCommentEventTypeData.html", null ],
              [ "OpcUa::AuditConditionConfirmEventTypeData", "classOpcUa_1_1AuditConditionConfirmEventTypeData.html", null ],
              [ "OpcUa::AuditConditionEnableEventTypeData", "classOpcUa_1_1AuditConditionEnableEventTypeData.html", null ],
              [ "OpcUa::AuditConditionRespondEventTypeData", "classOpcUa_1_1AuditConditionRespondEventTypeData.html", null ],
              [ "OpcUa::AuditConditionShelvingEventTypeData", "classOpcUa_1_1AuditConditionShelvingEventTypeData.html", null ]
            ] ],
            [ "OpcUa::AuditUpdateStateEventTypeData", "classOpcUa_1_1AuditUpdateStateEventTypeData.html", [
              [ "OpcUa::AuditProgramTransitionEventTypeData", "classOpcUa_1_1AuditProgramTransitionEventTypeData.html", null ],
              [ "OpcUa::ProgramTransitionAuditEventTypeData", "classOpcUa_1_1ProgramTransitionAuditEventTypeData.html", null ]
            ] ],
            [ "OpcUa::CertificateUpdatedAuditEventTypeData", "classOpcUa_1_1CertificateUpdatedAuditEventTypeData.html", null ],
            [ "OpcUa::TrustListUpdatedAuditEventTypeData", "classOpcUa_1_1TrustListUpdatedAuditEventTypeData.html", null ]
          ] ]
        ] ],
        [ "OpcUa::BaseModelChangeEventTypeData", "classOpcUa_1_1BaseModelChangeEventTypeData.html", [
          [ "OpcUa::GeneralModelChangeEventTypeData", "classOpcUa_1_1GeneralModelChangeEventTypeData.html", null ],
          [ "OpcUa::SemanticChangeEventTypeData", "classOpcUa_1_1SemanticChangeEventTypeData.html", null ]
        ] ],
        [ "OpcUa::ConditionTypeData", "classOpcUa_1_1ConditionTypeData.html", null ],
        [ "OpcUa::SystemEventTypeData", "classOpcUa_1_1SystemEventTypeData.html", [
          [ "OpcUa::DeviceFailureEventTypeData", "classOpcUa_1_1DeviceFailureEventTypeData.html", null ],
          [ "OpcUa::SystemStatusChangeEventTypeData", "classOpcUa_1_1SystemStatusChangeEventTypeData.html", null ]
        ] ],
        [ "OpcUa::TransitionEventTypeData", "classOpcUa_1_1TransitionEventTypeData.html", [
          [ "OpcUa::ProgramTransitionEventTypeData", "classOpcUa_1_1ProgramTransitionEventTypeData.html", null ]
        ] ],
        [ "TagFileEventTypeData", "classTagFileEventTypeData.html", null ]
      ] ]
    ] ],
    [ "UaEventFieldLists", "classUaEventFieldLists.html", null ],
    [ "UaEventFilter", "classUaEventFilter.html", null ],
    [ "UaEventFilterResult", "classUaEventFilterResult.html", null ],
    [ "UaExpandedNodeIdArray", "classUaExpandedNodeIdArray.html", null ],
    [ "UaExtensionObjectArray", "classUaExtensionObjectArray.html", null ],
    [ "UaFileEngine", "classUaFileEngine.html", null ],
    [ "UaClientSdk::UaFileObject", "classUaClientSdk_1_1UaFileObject.html", null ],
    [ "UaFilterOperand", "classUaFilterOperand.html", [
      [ "UaElementOperand", "classUaElementOperand.html", null ],
      [ "UaLiteralOperand", "classUaLiteralOperand.html", null ],
      [ "UaSimpleAttributeOperand", "classUaSimpleAttributeOperand.html", null ]
    ] ],
    [ "UaFloatArray", "classUaFloatArray.html", null ],
    [ "UaGenericOptionSetArray", "classUaGenericOptionSetArray.html", null ],
    [ "UaGenericStructureArray", "classUaGenericStructureArray.html", null ],
    [ "UaGenericUnionArray", "classUaGenericUnionArray.html", null ],
    [ "UaGuidArray", "classUaGuidArray.html", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html", null ],
    [ "UaHistoryEventFieldList", "classUaHistoryEventFieldList.html", null ],
    [ "UaHistoryEventFieldLists", "classUaHistoryEventFieldLists.html", null ],
    [ "UaHistoryEvents", "classUaHistoryEvents.html", null ],
    [ "UaHistoryReadResults", "classUaHistoryReadResults.html", null ],
    [ "UaHistoryReadValueIds", "classUaHistoryReadValueIds.html", null ],
    [ "UaHistoryUpdateResults", "classUaHistoryUpdateResults.html", null ],
    [ "UaInt16Array", "classUaInt16Array.html", null ],
    [ "UaInt32Array", "classUaInt32Array.html", null ],
    [ "UaInt64Array", "classUaInt64Array.html", null ],
    [ "UaIODevice", "classUaIODevice.html", [
      [ "UaFile", "classUaFile.html", null ]
    ] ],
    [ "UaLocalizedTextArray", "classUaLocalizedTextArray.html", null ],
    [ "UaModelChangeStructureDataType", "classUaModelChangeStructureDataType.html", null ],
    [ "UaModelChangeStructureDataTypes", "classUaModelChangeStructureDataTypes.html", null ],
    [ "UaModificationInfos", "classUaModificationInfos.html", null ],
    [ "UaModule", "classUaModule.html", null ],
    [ "UaMonitoredItem", "classUaMonitoredItem.html", [
      [ "UaMonitoredItemData", "classUaMonitoredItemData.html", null ],
      [ "UaMonitoredItemEvent", "classUaMonitoredItemEvent.html", null ]
    ] ],
    [ "UaMonitoredItemCreateRequests", "classUaMonitoredItemCreateRequests.html", null ],
    [ "UaMonitoredItemCreateResults", "classUaMonitoredItemCreateResults.html", null ],
    [ "UaMonitoredItemModifyRequests", "classUaMonitoredItemModifyRequests.html", null ],
    [ "UaMonitoredItemModifyResults", "classUaMonitoredItemModifyResults.html", null ],
    [ "UaMonitoredItemNotifications", "classUaMonitoredItemNotifications.html", null ],
    [ "UaMutex", "classUaMutex.html", [
      [ "UaMutexRefCounted", "classUaMutexRefCounted.html", null ]
    ] ],
    [ "UaMutexLocker", "classUaMutexLocker.html", null ],
    [ "UaNetworkBrowser", "classUaNetworkBrowser.html", null ],
    [ "UaNetworkBrowseResult", "classUaNetworkBrowseResult.html", null ],
    [ "UaNetworkGroupDataType", "classUaNetworkGroupDataType.html", null ],
    [ "UaNetworkGroupDataTypes", "classUaNetworkGroupDataTypes.html", null ],
    [ "UaNodeId", "classUaNodeId.html", null ],
    [ "UaNodeIdArray", "classUaNodeIdArray.html", null ],
    [ "UaBase::UaNodesetXmlParser", "classUaBase_1_1UaNodesetXmlParser.html", [
      [ "UaNodeSetXmlParserUaNode", "classUaNodeSetXmlParserUaNode.html", null ]
    ] ],
    [ "UaNodeTypeDescriptions", "classUaNodeTypeDescriptions.html", null ],
    [ "UaNumericRange", "classUaNumericRange.html", null ],
    [ "UaObjectArray< T >", "classUaObjectArray.html", null ],
    [ "UaObjectArray< SamplingClassDevice >", "classUaObjectArray.html", null ],
    [ "UaObjectPointerArray< T >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< ComAeAttributeMapping >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< EventFilterOperand >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< EventNotifierTreeElement >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< EventTypeTreeElement >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaEndpoint >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaEndpointSecuritySetting >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaFilterOperand >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaNodeId >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaVariant >", "classUaObjectPointerArray.html", null ],
    [ "UaObjectPointerArray< UaVariantArray >", "classUaObjectPointerArray.html", null ],
    [ "UaOptionSet", "classUaOptionSet.html", null ],
    [ "UaOptionSets", "classUaOptionSets.html", null ],
    [ "UaParsingResults", "classUaParsingResults.html", null ],
    [ "UaPkiCertificate", "classUaPkiCertificate.html", null ],
    [ "UaPkiCertificateCollection", "classUaPkiCertificateCollection.html", null ],
    [ "UaPkiCertificateInfo", "classUaPkiCertificateInfo.html", null ],
    [ "UaPkiCSR", "classUaPkiCSR.html", null ],
    [ "UaPkiIdentity", "classUaPkiIdentity.html", null ],
    [ "UaPkiPrivateKey", "classUaPkiPrivateKey.html", null ],
    [ "UaPkiProvider", "classUaPkiProvider.html", [
      [ "UaPkiProviderNoSecurity", "classUaPkiProviderNoSecurity.html", null ],
      [ "UaPkiProviderOpenSSL", "classUaPkiProviderOpenSSL.html", null ],
      [ "UaPkiProviderWindowsStore", "classUaPkiProviderWindowsStore.html", null ]
    ] ],
    [ "UaPkiProviderCertificateValidationCallback", "classUaPkiProviderCertificateValidationCallback.html", [
      [ "CertificateConfiguration", "classCertificateConfiguration.html", null ],
      [ "UaEndpoint", "classUaEndpoint.html", [
        [ "UaEndpointIni", "classUaEndpointIni.html", null ],
        [ "UaEndpointXml", "classUaEndpointXml.html", null ]
      ] ]
    ] ],
    [ "UaPkiPublicKey", "classUaPkiPublicKey.html", null ],
    [ "UaPkiRevocationList", "classUaPkiRevocationList.html", null ],
    [ "UaPkiRsaKeyPair", "classUaPkiRsaKeyPair.html", null ],
    [ "UaPlatformLayer", "classUaPlatformLayer.html", null ],
    [ "UaPointerArray< T >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< CertificateConfiguration >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< CertificateStoreConfiguration >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< EventManager >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< OpcUa_DataValue >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< SamplingItemDevice >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< UaContentFilterElement >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< UaDiagnosticInfo >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< UaNumericRange >", "classUaPointerArray.html", null ],
    [ "UaPointerArray< UaVariable >", "classUaPointerArray.html", null ],
    [ "UaProgramDiagnosticDataType", "classUaProgramDiagnosticDataType.html", null ],
    [ "UaProgramDiagnosticDataTypes", "classUaProgramDiagnosticDataTypes.html", null ],
    [ "UaPublishContext", "classUaPublishContext.html", null ],
    [ "UaQualifiedNameArray", "classUaQualifiedNameArray.html", null ],
    [ "UaQueryDataSets", "classUaQueryDataSets.html", null ],
    [ "UaRange", "classUaRange.html", null ],
    [ "UaRanges", "classUaRanges.html", null ],
    [ "UaReadValueIds", "classUaReadValueIds.html", null ],
    [ "UaRedundantServerDataType", "classUaRedundantServerDataType.html", null ],
    [ "UaRedundantServerDataTypes", "classUaRedundantServerDataTypes.html", null ],
    [ "UaReference", "classUaReference.html", [
      [ "UaGenericReference", "classUaGenericReference.html", null ],
      [ "UaRefCrossNodeManager", "classUaRefCrossNodeManager.html", null ],
      [ "UaRefCrossNodeManagerSingle", "classUaRefCrossNodeManagerSingle.html", null ],
      [ "UaReferenceCPCopy", "classUaReferenceCPCopy.html", null ],
      [ "UaRefFromState", "classUaRefFromState.html", null ],
      [ "UaRefGeneratesEvent", "classUaRefGeneratesEvent.html", null ],
      [ "UaRefHasCause", "classUaRefHasCause.html", null ],
      [ "UaRefHasComponent", "classUaRefHasComponent.html", null ],
      [ "UaRefHasCondition", "classUaRefHasCondition.html", null ],
      [ "UaRefHasDescription", "classUaRefHasDescription.html", null ],
      [ "UaRefHasEffect", "classUaRefHasEffect.html", null ],
      [ "UaRefHasEncoding", "classUaRefHasEncoding.html", null ],
      [ "UaRefHasEventSource", "classUaRefHasEventSource.html", null ],
      [ "UaRefHasHistoricalConfiguration", "classUaRefHasHistoricalConfiguration.html", null ],
      [ "UaRefHasModellingRule", "classUaRefHasModellingRule.html", null ],
      [ "UaRefHasNotifier", "classUaRefHasNotifier.html", null ],
      [ "UaRefHasOrderedComponent", "classUaRefHasOrderedComponent.html", null ],
      [ "UaRefHasProperty", "classUaRefHasProperty.html", null ],
      [ "UaRefHasSubtype", "classUaRefHasSubtype.html", null ],
      [ "UaRefHasTypeDefinition", "classUaRefHasTypeDefinition.html", null ],
      [ "UaRefOrganizes", "classUaRefOrganizes.html", null ],
      [ "UaRefOutOfServer", "classUaRefOutOfServer.html", null ],
      [ "UaRefToState", "classUaRefToState.html", null ]
    ] ],
    [ "UaReferenceDescriptions", "classUaReferenceDescriptions.html", null ],
    [ "UaReferenceLists", "classUaReferenceLists.html", [
      [ "OpcUa::BaseMethod", "classOpcUa_1_1BaseMethod.html", null ],
      [ "OpcUa::BaseObjectType", "classOpcUa_1_1BaseObjectType.html", null ],
      [ "OpcUa::BaseVariableType", "classOpcUa_1_1BaseVariableType.html", null ],
      [ "UaDataTypeNS0", "classUaDataTypeNS0.html", null ],
      [ "UaDataVariableCache", "classUaDataVariableCache.html", null ],
      [ "UaDataVariableDevice", "classUaDataVariableDevice.html", null ],
      [ "UaGenericDataType", "classUaGenericDataType.html", null ],
      [ "UaGenericMethod", "classUaGenericMethod.html", null ],
      [ "UaGenericObject", "classUaGenericObject.html", null ],
      [ "UaGenericObjectType", "classUaGenericObjectType.html", null ],
      [ "UaGenericReferenceType", "classUaGenericReferenceType.html", null ],
      [ "UaGenericVariable", "classUaGenericVariable.html", null ],
      [ "UaGenericVariableType", "classUaGenericVariableType.html", null ],
      [ "UaGenericView", "classUaGenericView.html", null ],
      [ "UaMethodGeneric", "classUaMethodGeneric.html", null ],
      [ "UaObjectBase", "classUaObjectBase.html", null ],
      [ "UaObjectTypeSimple", "classUaObjectTypeSimple.html", null ],
      [ "UaPropertyMethodArgument", "classUaPropertyMethodArgument.html", null ],
      [ "UaReferenceTypeNS0", "classUaReferenceTypeNS0.html", null ],
      [ "UaVariableTypeSimple", "classUaVariableTypeSimple.html", null ],
      [ "UaViewSimple", "classUaViewSimple.html", null ]
    ] ],
    [ "UaRegisteredServer", "classUaRegisteredServer.html", null ],
    [ "UaRegisteredServers", "classUaRegisteredServers.html", null ],
    [ "UaRelativePath", "classUaRelativePath.html", null ],
    [ "UaRelativePathElements", "classUaRelativePathElements.html", null ],
    [ "UaRepublishContext", "classUaRepublishContext.html", null ],
    [ "UaSamplingIntervalDiagnosticsDataType", "classUaSamplingIntervalDiagnosticsDataType.html", null ],
    [ "UaSamplingIntervalDiagnosticsDataTypes", "classUaSamplingIntervalDiagnosticsDataTypes.html", null ],
    [ "UaSByteArray", "classUaSByteArray.html", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html", null ],
    [ "UaSemanticChangeStructureDataTypes", "classUaSemanticChangeStructureDataTypes.html", null ],
    [ "UaSemaphore", "classUaSemaphore.html", null ],
    [ "UaServerApplicationCallback", "classUaServerApplicationCallback.html", null ],
    [ "UaServerApplicationModule", "classUaServerApplicationModule.html", [
      [ "NodeManager", "classNodeManager.html", [
        [ "NodeManagerUaNode", "classNodeManagerUaNode.html", null ]
      ] ],
      [ "UaNodeSetXmlParserUaNode", "classUaNodeSetXmlParserUaNode.html", null ]
    ] ],
    [ "UaServerApplicationPrivate", "classUaServerApplicationPrivate.html", null ],
    [ "UaClientSdk::UaServerConfigurationObject", "classUaClientSdk_1_1UaServerConfigurationObject.html", null ],
    [ "UaServerDiagnosticsSummaryDataType", "classUaServerDiagnosticsSummaryDataType.html", null ],
    [ "UaServerDiagnosticsSummaryDataTypes", "classUaServerDiagnosticsSummaryDataTypes.html", null ],
    [ "UaServerOnNetwork", "classUaServerOnNetwork.html", null ],
    [ "UaServerOnNetworks", "classUaServerOnNetworks.html", null ],
    [ "UaServerStatusDataType", "classUaServerStatusDataType.html", null ],
    [ "UaServerStatusDataTypes", "classUaServerStatusDataTypes.html", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html", null ],
    [ "UaServiceCounterDataTypes", "classUaServiceCounterDataTypes.html", null ],
    [ "UaClientSdk::UaSessionCallback", "classUaClientSdk_1_1UaSessionCallback.html", null ],
    [ "UaSessionDiagnosticsDataType", "classUaSessionDiagnosticsDataType.html", null ],
    [ "UaSessionDiagnosticsDataTypes", "classUaSessionDiagnosticsDataTypes.html", null ],
    [ "UaSessionSecurityDiagnosticsDataType", "classUaSessionSecurityDiagnosticsDataType.html", null ],
    [ "UaSessionSecurityDiagnosticsDataTypes", "classUaSessionSecurityDiagnosticsDataTypes.html", null ],
    [ "UaSettings", "classUaSettings.html", null ],
    [ "UaSettingsSection", "classUaSettingsSection.html", null ],
    [ "UaSignedSoftwareCertificate", "classUaSignedSoftwareCertificate.html", null ],
    [ "UaSignedSoftwareCertificates", "classUaSignedSoftwareCertificates.html", null ],
    [ "UaSimpleAttributeOperands", "classUaSimpleAttributeOperands.html", null ],
    [ "UaSoftwareCertificate", "classUaSoftwareCertificate.html", null ],
    [ "UaSoftwareCertificates", "classUaSoftwareCertificates.html", null ],
    [ "UaStatusCode", "classUaStatusCode.html", [
      [ "UaStatus", "classUaStatus.html", null ]
    ] ],
    [ "UaStatusCodeArray", "classUaStatusCodeArray.html", null ],
    [ "UaString", "classUaString.html", null ],
    [ "UaStringArray", "classUaStringArray.html", null ],
    [ "UaStructureDefinition", "classUaStructureDefinition.html", null ],
    [ "UaStructureField", "classUaStructureField.html", null ],
    [ "UaClientSdk::UaSubscription", "classUaClientSdk_1_1UaSubscription.html", null ],
    [ "UaClientSdk::UaSubscriptionCallback", "classUaClientSdk_1_1UaSubscriptionCallback.html", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html", null ],
    [ "UaSubscriptionDiagnosticsDataTypes", "classUaSubscriptionDiagnosticsDataTypes.html", null ],
    [ "UaSupportedProfile", "classUaSupportedProfile.html", null ],
    [ "UaSupportedProfiles", "classUaSupportedProfiles.html", null ],
    [ "UaThread", "classUaThread.html", [
      [ "SamplingEngine", "classSamplingEngine.html", null ],
      [ "UaServer", "classUaServer.html", null ],
      [ "UaSubscriptionManager", "classUaSubscriptionManager.html", null ]
    ] ],
    [ "UaThreadPool", "classUaThreadPool.html", null ],
    [ "UaThreadPoolJob", "classUaThreadPoolJob.html", [
      [ "OpcUa::MethodCallJob", "classOpcUa_1_1MethodCallJob.html", null ],
      [ "UaBaseChangeMonitorTypeContext", "classUaBaseChangeMonitorTypeContext.html", [
        [ "AddEventManagerMonitoredItemsContext", "classAddEventManagerMonitoredItemsContext.html", null ],
        [ "UaCreateMonitoredItemsContext", "classUaCreateMonitoredItemsContext.html", null ],
        [ "UaDeleteMonitoredItemsContext", "classUaDeleteMonitoredItemsContext.html", null ],
        [ "UaModifyMonitoredItemsContext", "classUaModifyMonitoredItemsContext.html", null ],
        [ "UaSetMonitoringModeContext", "classUaSetMonitoringModeContext.html", null ]
      ] ],
      [ "UaSubscriptionContext", "classUaSubscriptionContext.html", null ],
      [ "UaTMBaseContext", "classUaTMBaseContext.html", [
        [ "UaBrowseContext", "classUaBrowseContext.html", null ],
        [ "UaBrowseNextContext", "classUaBrowseNextContext.html", null ],
        [ "UaCallContext", "classUaCallContext.html", null ],
        [ "UaHistoryReadContext", "classUaHistoryReadContext.html", null ],
        [ "UaHistoryUpdateContext", "classUaHistoryUpdateContext.html", null ],
        [ "UaQueryContext", "classUaQueryContext.html", [
          [ "UaQueryFirstContext", "classUaQueryFirstContext.html", null ],
          [ "UaQueryNextContext", "classUaQueryNextContext.html", null ]
        ] ],
        [ "UaReadContext", "classUaReadContext.html", null ],
        [ "UaTranslateBrowsePathsToNodeIdsContext", "classUaTranslateBrowsePathsToNodeIdsContext.html", null ],
        [ "UaWriteContext", "classUaWriteContext.html", null ]
      ] ]
    ] ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html", null ],
    [ "UaTimeZoneDataTypes", "classUaTimeZoneDataTypes.html", null ],
    [ "UaTrace", "classUaTrace.html", [
      [ "SrvT", "classSrvT.html", null ]
    ] ],
    [ "UaTraceHook", "classUaTraceHook.html", null ],
    [ "UaTrustListDataType", "classUaTrustListDataType.html", null ],
    [ "UaTrustListDataTypes", "classUaTrustListDataTypes.html", null ],
    [ "UaClientSdk::UaTrustListObject", "classUaClientSdk_1_1UaTrustListObject.html", null ],
    [ "UaUInt16Array", "classUaUInt16Array.html", null ],
    [ "UaUInt32Array", "classUaUInt32Array.html", null ],
    [ "UaUInt64Array", "classUaUInt64Array.html", null ],
    [ "UaUniString", "classUaUniString.html", null ],
    [ "UaUniStringList", "classUaUniStringList.html", null ],
    [ "UaUserIdentityToken", "classUaUserIdentityToken.html", [
      [ "UaUserIdentityTokenAnonymous", "classUaUserIdentityTokenAnonymous.html", null ],
      [ "UaUserIdentityTokenCertificate", "classUaUserIdentityTokenCertificate.html", null ],
      [ "UaUserIdentityTokenKerberosTicket", "classUaUserIdentityTokenKerberosTicket.html", null ],
      [ "UaUserIdentityTokenUserPassword", "classUaUserIdentityTokenUserPassword.html", null ]
    ] ],
    [ "UaUserTokenPolicy", "classUaUserTokenPolicy.html", null ],
    [ "UaUserTokenPolicys", "classUaUserTokenPolicys.html", null ],
    [ "UaVariant", "classUaVariant.html", null ],
    [ "UaVariantArray", "classUaVariantArray.html", null ],
    [ "UaVariantException", "classUaVariantException.html", null ],
    [ "UaWriteValues", "classUaWriteValues.html", null ],
    [ "UaXmlDocument", "classUaXmlDocument.html", null ],
    [ "UaXmlElement", "classUaXmlElement.html", null ],
    [ "UaXmlValue", "classUaXmlValue.html", null ],
    [ "UaXVType", "classUaXVType.html", null ],
    [ "UaXVTypes", "classUaXVTypes.html", null ],
    [ "UaClientSdk::UpdateDataDetail", "classUaClientSdk_1_1UpdateDataDetail.html", null ],
    [ "UaClientSdk::UpdateEventDetail", "classUaClientSdk_1_1UpdateEventDetail.html", null ],
    [ "UserDataBase", "classUserDataBase.html", [
      [ "UaBase::BaseNodeStandardUserData", "classUaBase_1_1BaseNodeStandardUserData.html", null ],
      [ "UserDataVariableGetValue", "classUserDataVariableGetValue.html", [
        [ "UaVariableNs1UserDataGetCounter", "classUaVariableNs1UserDataGetCounter.html", null ],
        [ "UserDataGetCounterBoolean", "classUserDataGetCounterBoolean.html", null ],
        [ "UserDataGetCounterByte", "classUserDataGetCounterByte.html", null ],
        [ "UserDataGetCounterDouble", "classUserDataGetCounterDouble.html", null ],
        [ "UserDataGetCounterNodeId", "classUserDataGetCounterNodeId.html", null ],
        [ "UserDataGetCounterUInt32", "classUserDataGetCounterUInt32.html", null ]
      ] ]
    ] ],
    [ "VersionInfoCoreModule", "classVersionInfoCoreModule.html", null ],
    [ "XmlUaNodeFactoryManager", "classXmlUaNodeFactoryManager.html", null ],
    [ "XmlUaNodeFactoryNamespace", "classXmlUaNodeFactoryNamespace.html", [
      [ "XmlUaNodeFactoryNamespace0", "classXmlUaNodeFactoryNamespace0.html", null ]
    ] ]
];