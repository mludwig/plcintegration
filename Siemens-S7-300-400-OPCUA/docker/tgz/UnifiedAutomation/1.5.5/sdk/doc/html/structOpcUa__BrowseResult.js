var structOpcUa__BrowseResult =
[
    [ "ContinuationPoint", "group__UaStackTypes.html#ga4bcf9eab29b1bcb68935f0dd3430cea4", null ],
    [ "References", "group__UaStackTypes.html#gaa71a9fc191f9eaef9b9a4fc25587a490", null ],
    [ "StatusCode", "group__UaStackTypes.html#gabc48f6536750838eab1ec585a13b097e", null ]
];