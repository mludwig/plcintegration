var L1OpcUaFundamentals =
[
    [ "OPC Unified Architecture Overview", "L2OpcUaFundamentalsOverview.html", null ],
    [ "Address Space Concepts", "L2UaAddressSpaceConcepts.html", [
      [ "Object Model", "L2UaAddressSpaceConcepts.html#L2UaAdrSpaceConceptObjectModel", null ],
      [ "Node Model", "L2UaAddressSpaceConcepts.html#L2UaAdrSpaceConceptNodeModel", [
        [ "Node Classes", "L2UaAddressSpaceConcepts.html#L3UaAdrSpaceConceptNodeModel_nodeclasses", null ],
        [ "Attributes", "L2UaAddressSpaceConcepts.html#L3UaAdrSpaceConceptNodeModel_attributes", null ],
        [ "References", "L2UaAddressSpaceConcepts.html#L3UaAdrSpaceConceptNodeModel_references", null ]
      ] ],
      [ "Variables", "L2UaAddressSpaceConcepts.html#L2UaAdrSpaceConceptVariables", [
        [ "Properties", "L2UaAddressSpaceConcepts.html#L3UaAdrSpaceConceptVariables_properties", null ],
        [ "Data Variables", "L2UaAddressSpaceConcepts.html#L3UaAdrSpaceConceptVariables_datavariables", null ]
      ] ]
    ] ],
    [ "OPC UA Node Classes", "L2UaNodeClasses.html", [
      [ "Base Node Class", "L2UaNodeClasses.html#L3UaNodeClassBase", null ],
      [ "Object", "L2UaNodeClasses.html#L3UaNodeClassObject", null ],
      [ "Variable", "L2UaNodeClasses.html#L3UaNodeClassVariable", null ],
      [ "Method", "L2UaNodeClasses.html#L3UaNodeClassMethod", null ],
      [ "ReferenceType", "L2UaNodeClasses.html#L3UaNodeClassReferenceType", null ],
      [ "ObjectType", "L2UaNodeClasses.html#L3UaNodeClassObjectType", null ],
      [ "VariableType", "L2UaNodeClasses.html#L3UaNodeClassVariableType", null ],
      [ "DataType", "L2UaNodeClasses.html#L3UaNodeClassDataType", null ],
      [ "View", "L2UaNodeClasses.html#L3UaNodeClassView", null ]
    ] ],
    [ "OPC UA NodeId Concepts", "L2UaNodeIds.html", [
      [ "Difference to “Classic” OPC DA", "L2UaNodeIds.html#UaNodeIdsClassic", null ],
      [ "OPC UA NodeIds", "L2UaNodeIds.html#UaNodeIdsConcept", null ],
      [ "XML Notation", "L2UaNodeIds.html#UaNodeIdsXml", null ]
    ] ],
    [ "OPC UA Subscription Concept", "L2UaSubscription.html", null ],
    [ "Discovery and Security Configuration", "L2UaDiscoveryConnect.html", [
      [ "SDK Specific Configuration", "L2UaDiscoveryConnect.html#SdkSpecificConfiguration", null ],
      [ "General Concept", "L2UaDiscoveryConnect.html#DiscoveryConnect_GeneralConcept", null ],
      [ "Certificates, Certificate Store and Trust List", "L2UaDiscoveryConnect.html#DiscoveryConnect_Certificates", null ],
      [ "Initial Server Configuration", "L2UaDiscoveryConnect.html#DiscoveryConnect_ServerInitialConfig", [
        [ "Endpoints", "L2UaDiscoveryConnect.html#DiscoveryConnect_ServerInitialConfig_Endpoints", null ],
        [ "Discovery Server", "L2UaDiscoveryConnect.html#DiscoveryConnect_ServerInitialConfig_DiscoveryServer", null ]
      ] ],
      [ "Initial Client Configuration", "L2UaDiscoveryConnect.html#DiscoveryConnect_ClientInitialConfig", [
        [ "List of Server Endpoints", "L2UaDiscoveryConnect.html#DiscoveryConnect_ClientInitialConfig_EndpointList", null ]
      ] ],
      [ "Connection Configuration", "L2UaDiscoveryConnect.html#DiscoveryConnect_ConnectConfig", null ],
      [ "Discovery", "L2UaDiscoveryConnect.html#DiscoveryConnect_Discovery", [
        [ "Local Discovery", "L2UaDiscoveryConnect.html#DiscoveryConnect_Discovery_Local", null ],
        [ "Multicast Subnet Discovery", "L2UaDiscoveryConnect.html#DiscoveryConnect_Discovery_Multicast", null ],
        [ "Global Discovery Server", "L2UaDiscoveryConnect.html#DiscoveryConnect_Discovery_Global", null ]
      ] ],
      [ "Certificate Management with GDS", "L2UaDiscoveryConnect.html#DiscoveryConnect_CertificateManagement", null ]
    ] ],
    [ "OPC UA Programs", "L2UaPrograms.html", null ]
];