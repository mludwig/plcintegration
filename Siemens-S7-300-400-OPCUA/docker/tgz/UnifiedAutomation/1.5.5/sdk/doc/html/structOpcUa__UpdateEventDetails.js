var structOpcUa__UpdateEventDetails =
[
    [ "EventData", "group__UaStackTypes.html#ga205c235a7e1920e0480691d270cab3d3", null ],
    [ "Filter", "group__UaStackTypes.html#ga9a7a17d4ebbb8c0484fc13ed218550d4", null ],
    [ "NodeId", "group__UaStackTypes.html#ga8aaf650a031d96b69307e7a42fa9fd66", null ],
    [ "PerformInsertReplace", "group__UaStackTypes.html#ga46cf77cf9807b530ef026ddd2f174775", null ]
];