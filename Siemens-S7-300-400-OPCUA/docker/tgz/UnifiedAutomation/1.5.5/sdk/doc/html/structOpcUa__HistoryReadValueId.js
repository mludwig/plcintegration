var structOpcUa__HistoryReadValueId =
[
    [ "ContinuationPoint", "group__UaStackTypes.html#ga04e94c656c2cf51a7608725b158db1dd", null ],
    [ "DataEncoding", "group__UaStackTypes.html#ga20a62e3aadf460d2dc579797eea3ff60", null ],
    [ "IndexRange", "group__UaStackTypes.html#gab0706ca2862b4c943781a700fa127ce6", null ],
    [ "NodeId", "group__UaStackTypes.html#ga776613d6065cf93d1300fe5e2e8f136d", null ]
];