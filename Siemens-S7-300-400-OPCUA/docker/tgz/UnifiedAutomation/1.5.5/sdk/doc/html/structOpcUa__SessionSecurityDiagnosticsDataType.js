var structOpcUa__SessionSecurityDiagnosticsDataType =
[
    [ "AuthenticationMechanism", "group__UaStackTypes.html#ga7701ce5a311a0986f44143da9b65ceff", null ],
    [ "ClientCertificate", "group__UaStackTypes.html#ga4a9b61e555ff62fa72d1ab7cbf29c9b2", null ],
    [ "ClientUserIdHistory", "group__UaStackTypes.html#ga7b15772e9703e220688068de82cda99a", null ],
    [ "ClientUserIdOfSession", "group__UaStackTypes.html#ga77b62ee9aeac3370560beb66432f0419", null ],
    [ "Encoding", "group__UaStackTypes.html#gace4b5b48c9be6d7d9365b9db9d195150", null ],
    [ "SecurityMode", "group__UaStackTypes.html#ga6627615dcc660f8019130f85ed779a44", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga5384385752c609824fe2a27f6fc47f1d", null ],
    [ "SessionId", "group__UaStackTypes.html#gad7abc52a3d50e9e626f910dc59c76e8d", null ],
    [ "TransportProtocol", "group__UaStackTypes.html#gae13835806aea8841ece342b4bea5c197", null ]
];