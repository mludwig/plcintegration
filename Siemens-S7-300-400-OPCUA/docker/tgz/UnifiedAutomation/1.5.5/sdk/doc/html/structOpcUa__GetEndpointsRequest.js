var structOpcUa__GetEndpointsRequest =
[
    [ "EndpointUrl", "group__UaStackTypes.html#ga01a2b332908eccb1b1bfb74b927b097a", null ],
    [ "LocaleIds", "group__UaStackTypes.html#gaf20bce1ec7913bcbe46ea6cc68be15ed", null ],
    [ "ProfileUris", "group__UaStackTypes.html#ga1638ca5557ca0d856698babba04db0cd", null ]
];