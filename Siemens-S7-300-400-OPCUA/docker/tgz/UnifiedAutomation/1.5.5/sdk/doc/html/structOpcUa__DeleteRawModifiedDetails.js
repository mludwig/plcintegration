var structOpcUa__DeleteRawModifiedDetails =
[
    [ "EndTime", "group__UaStackTypes.html#gad166672b5dbe0c8b1f83ce68be84cfad", null ],
    [ "IsDeleteModified", "group__UaStackTypes.html#ga88235d0faa39022b594421d18afda1a8", null ],
    [ "NodeId", "group__UaStackTypes.html#gaaccbcc6d2f9d9be4738b175c94db4121", null ],
    [ "StartTime", "group__UaStackTypes.html#gaf5177406eb275d39bcaea86a5e49d9ba", null ]
];