var structOpcUa__CreateSubscriptionRequest =
[
    [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga5c6a618a41dddce7a362e20280e30889", null ],
    [ "Priority", "group__UaStackTypes.html#ga78766f534e548b0de146ce4b8c0cd9f0", null ],
    [ "PublishingEnabled", "group__UaStackTypes.html#ga6b64a2bbe7b7e55ccc5fb5840f1b2fdd", null ],
    [ "RequestedLifetimeCount", "group__UaStackTypes.html#gad987badc435c4edd8383c4da75acb222", null ],
    [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#gaba5ea661e7b9e955f7f6d7039f498aba", null ],
    [ "RequestedPublishingInterval", "group__UaStackTypes.html#gae0125058057f4090d46ed614dbbb89b1", null ]
];