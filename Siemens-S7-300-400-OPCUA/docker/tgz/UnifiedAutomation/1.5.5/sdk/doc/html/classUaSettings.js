var classUaSettings =
[
    [ "Scope", "group__CppBaseLibraryClass.html#gab07e752b89751f93cbfc8ce71c46fe5e", [
      [ "UserScope", "group__CppBaseLibraryClass.html#ggab07e752b89751f93cbfc8ce71c46fe5eacf138d068550b5fc13cc207cf5c52956", null ],
      [ "SystemScope", "group__CppBaseLibraryClass.html#ggab07e752b89751f93cbfc8ce71c46fe5ea3a5c547d69f77819cd51944673c1235f", null ]
    ] ],
    [ "UaSettings", "classUaSettings.html#a84ed972c58c7a0dfe3859a101bc3c128", null ],
    [ "UaSettings", "classUaSettings.html#a3ea4cb805372b0d98d0f920703e6d501", null ],
    [ "~UaSettings", "classUaSettings.html#abe05fdf9032686303b104ee9772509c1", null ],
    [ "applicationName", "classUaSettings.html#a96e4c359b22173209f342f4fcc8580eb", null ],
    [ "beginGroup", "classUaSettings.html#a5a934268a98d7f3c1ba75baf36f4d0df", null ],
    [ "childGroups", "classUaSettings.html#a9509780547d6504171944a04ce4e48cd", null ],
    [ "childKeys", "classUaSettings.html#a1025a074408c8891c5561dbb26b4964f", null ],
    [ "contains", "classUaSettings.html#a26a564a509c1cb5017c359acce7f927e", null ],
    [ "endGroup", "classUaSettings.html#ad089ab6dc14e64c03e0bc3767e88c2f0", null ],
    [ "group", "classUaSettings.html#aae3c2f94b855c97b302d324bc9ff83ba", null ],
    [ "organizationName", "classUaSettings.html#a6c16f37008eda844e601544013e7124f", null ],
    [ "removeKey", "classUaSettings.html#aeceb553f9ae62ebf1e78e9fe5cdb04ce", null ],
    [ "scope", "classUaSettings.html#a1dd1db310dba65c20efc1177bad92259", null ],
    [ "setValue", "classUaSettings.html#a79b35c9d1ae02aa00af32dbe6e93db2d", null ],
    [ "sync", "classUaSettings.html#abfe38ca4e1c2b48082aab894c962fa6d", null ],
    [ "value", "classUaSettings.html#a97601964a4d82314aa8cb1f84ca40988", null ]
];