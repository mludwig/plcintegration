var classCacheVariableConnector =
[
    [ "CacheVariableConnector", "classCacheVariableConnector.html#a64b33092f80e6c1fcda7a467303007bd", null ],
    [ "~CacheVariableConnector", "classCacheVariableConnector.html#ab43de1947fa9dcde5781d77b98f78a39", null ],
    [ "getSamplingRate", "classCacheVariableConnector.html#a13bbb3b46a55f579e3f5c465f88c62ae", null ],
    [ "hIOVariable", "classCacheVariableConnector.html#adfe70c89e348150fb5ed584588ada1f1", null ],
    [ "pIOVariableCallback", "classCacheVariableConnector.html#a518fc582c99947c514c96a8f218725b4", null ],
    [ "pUaVariableCache", "classCacheVariableConnector.html#a8691579c225d0501675a1cadf1047b2a", null ],
    [ "sample", "classCacheVariableConnector.html#a9c9a11e1097bc0cc29765bf05fc10c14", null ],
    [ "setChanged", "classCacheVariableConnector.html#a583de551ea3922c83bc58f3d77f2544f", null ],
    [ "sethIOVariable", "classCacheVariableConnector.html#a020cc2fb50aa5eb33d2b8e08b115ba99", null ],
    [ "setInvalid", "classCacheVariableConnector.html#a5075e4c49466f4d57e1f51d31f56ff9c", null ],
    [ "setSamplingRate", "classCacheVariableConnector.html#aa82c7cecfe989c448a29cf1a00c189e2", null ],
    [ "setValid", "classCacheVariableConnector.html#afec01543a6c50e399abd040c1d118c5c", null ]
];