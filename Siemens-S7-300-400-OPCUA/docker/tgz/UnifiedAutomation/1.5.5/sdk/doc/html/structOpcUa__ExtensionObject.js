var structOpcUa__ExtensionObject =
[
    [ "Body", "group__UaStackTypes.html#ga2486cdc2ba5e2ecc20b0280390982360", null ],
    [ "BodySize", "group__UaStackTypes.html#ga6bfd27ad076d6d7c75f25ae381959b88", null ],
    [ "Encoding", "group__UaStackTypes.html#gacc281c31f020ec3ae619e25ee1986979", null ],
    [ "TypeId", "group__UaStackTypes.html#ga47e1863bd35cf7bc2299e3221f69200d", null ]
];