var classDataMonitoredItemSpec =
[
    [ "DataMonitoredItemSpec", "classDataMonitoredItemSpec.html#a5501cd12de86fab8bb5a0326ad2c8c00", null ],
    [ "~DataMonitoredItemSpec", "classDataMonitoredItemSpec.html#a72f67879a874ad36dabccf79b0f18027", null ],
    [ "m_createResult", "classDataMonitoredItemSpec.html#a27ffd7ff36748d4cd7bd960a08b80051", null ],
    [ "m_initialValue", "classDataMonitoredItemSpec.html#a20260b024e23ed134077b57e618699ed", null ],
    [ "m_isInitialValueProvided", "classDataMonitoredItemSpec.html#aea4993e00712180c00d911e6457b43ba", null ],
    [ "m_itemToMonitor", "classDataMonitoredItemSpec.html#aff433fd771d7265b83557144930e9496", null ],
    [ "m_monitoredItemId", "classDataMonitoredItemSpec.html#a1ce59ae7a8ae500dcc777a3547394e0d", null ],
    [ "m_pDataCallback", "classDataMonitoredItemSpec.html#a6033fb969cac3621b48210355aca50fc", null ],
    [ "m_requestedSamplingInterval", "classDataMonitoredItemSpec.html#a1db4026b9e3dc08cf53938320b6444d3", null ],
    [ "m_revisedSamplingInterval", "classDataMonitoredItemSpec.html#a363daac292f0e27a1d7c76071a6c2edd", null ]
];