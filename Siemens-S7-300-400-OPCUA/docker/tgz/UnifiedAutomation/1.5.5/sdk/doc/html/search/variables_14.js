var searchData=
[
  ['validbits',['ValidBits',['../group__UaStackTypes.html#ga9cb3005d8166b39e2a4bd3a5c7e966d3',1,'OpcUa_OptionSet']]],
  ['validtime',['validTime',['../classUaPkiCertificateInfo.html#a42dd4588651d30fe9643792ead937829',1,'UaPkiCertificateInfo']]],
  ['value',['Value',['../group__UaStackTypes.html#gab73548c3060f08143718e983701b24f4',1,'OpcUa_EnumValueType::Value()'],['../group__UaStackTypes.html#ga3ca42461df9be24803d0a50daa97b157',1,'OpcUa_OptionSet::Value()'],['../group__UaStackTypes.html#ga44e48805a3c8b6fd7289d0dfb7fa172d',1,'OpcUa_LiteralOperand::Value()'],['../group__UaStackTypes.html#gac73eef6289dbf901c65d455164a5fc45',1,'OpcUa_WriteValue::Value()'],['../group__UaStackTypes.html#ga3b182cdf58d719acc9b8911390408c0b',1,'OpcUa_XVType::Value()'],['../group__UaStackTypes.html#gad1541d8ebb3bf7b7837a3862c6266f39',1,'OpcUa_Variant::Value()'],['../group__UaStackTypes.html#ga58c8a3df4e676757166a9cb12441f8ae',1,'OpcUa_DataValue::Value()']]],
  ['valuerank',['ValueRank',['../group__UaStackTypes.html#ga3fb73033eec0aa0c8050182bf7d04ad5',1,'OpcUa_Argument']]],
  ['vendorname',['VendorName',['../group__UaStackTypes.html#gaa797675c8200f0eddc0e47260a482c89',1,'OpcUa_SoftwareCertificate']]],
  ['vendorproductcertificate',['VendorProductCertificate',['../group__UaStackTypes.html#ga1bd41d9aad3bf1c0c7b7ea7145bca9e0',1,'OpcUa_SoftwareCertificate']]],
  ['verb',['Verb',['../group__UaStackTypes.html#gaa0e81ecd74ae9ef3b23bd82643a66385',1,'OpcUa_ModelChangeStructureDataType']]],
  ['view',['View',['../group__UaStackTypes.html#ga5aef5d9f3fdcfeacf30077f5a57c4d7d',1,'OpcUa_BrowseRequest::View()'],['../group__UaStackTypes.html#ga168653f0e90a93b7298e436df937bbf6',1,'OpcUa_QueryFirstRequest::View()'],['../classUaClientSdk_1_1BrowseContext.html#ad762a621a39874ee70c5025d881e4ec5',1,'UaClientSdk::BrowseContext::view()']]],
  ['viewid',['ViewId',['../group__UaStackTypes.html#ga672bc50354b983a799f570f7743508a8',1,'OpcUa_ViewDescription']]],
  ['viewversion',['ViewVersion',['../group__UaStackTypes.html#gad1c2ea69c8f725433711ea0fdb328649',1,'OpcUa_ViewDescription']]]
];
