var classOpcUa_1_1TwoStateDiscreteType =
[
    [ "~TwoStateDiscreteType", "classOpcUa_1_1TwoStateDiscreteType.html#a554119604ebdeac87be869d0d4c34616", null ],
    [ "TwoStateDiscreteType", "classOpcUa_1_1TwoStateDiscreteType.html#a36b6c80d6c62fcf7d8ad0f2658c3aa18", null ],
    [ "TwoStateDiscreteType", "classOpcUa_1_1TwoStateDiscreteType.html#a6043aa2566b6e6ea95a4a3f4caf7e4c6", null ],
    [ "TwoStateDiscreteType", "classOpcUa_1_1TwoStateDiscreteType.html#a8495c6d2ff77075fa0e98ea7cb660b3d", null ],
    [ "getFalseState", "classOpcUa_1_1TwoStateDiscreteType.html#a66c3ff53b09fa2fd7b85b3d8a85dd37f", null ],
    [ "getFalseStateNode", "classOpcUa_1_1TwoStateDiscreteType.html#a4b11f747afde25f8e7cdd30035e16145", null ],
    [ "getTrueState", "classOpcUa_1_1TwoStateDiscreteType.html#a7a0d2b27d42f78228fb01449f5de0526", null ],
    [ "getTrueStateNode", "classOpcUa_1_1TwoStateDiscreteType.html#a4f2257bc1530e0cd415305e77ccd8425", null ],
    [ "setFalseState", "classOpcUa_1_1TwoStateDiscreteType.html#a7107eddbf8c5ce84dd92bbb49c7f03c9", null ],
    [ "setTrueState", "classOpcUa_1_1TwoStateDiscreteType.html#af0480a1f13def91c396376a3ff0312e8", null ],
    [ "typeDefinitionId", "classOpcUa_1_1TwoStateDiscreteType.html#a353dd452710104563aa51e5563b9d51c", null ],
    [ "useAccessInfoFromInstance", "classOpcUa_1_1TwoStateDiscreteType.html#ae6c7f68366568ef8d6e027b8aee9a757", null ],
    [ "useAccessInfoFromType", "classOpcUa_1_1TwoStateDiscreteType.html#af5c496f81889b246139ecee33eb672f9", null ]
];