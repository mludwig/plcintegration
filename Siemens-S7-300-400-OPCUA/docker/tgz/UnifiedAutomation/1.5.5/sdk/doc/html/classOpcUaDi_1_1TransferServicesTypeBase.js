var classOpcUaDi_1_1TransferServicesTypeBase =
[
    [ "~TransferServicesTypeBase", "classOpcUaDi_1_1TransferServicesTypeBase.html#a89bd0c71bcbfbc828eaf1a5f46cbc944", null ],
    [ "TransferServicesTypeBase", "classOpcUaDi_1_1TransferServicesTypeBase.html#a4a8b93272d4eefdb87acb58e090988d2", null ],
    [ "TransferServicesTypeBase", "classOpcUaDi_1_1TransferServicesTypeBase.html#a84ab6958bdfbbc7edd7061deec867aed", null ],
    [ "TransferServicesTypeBase", "classOpcUaDi_1_1TransferServicesTypeBase.html#afe6e3484269e0c66a61aecd02fba7168", null ],
    [ "beginCall", "classOpcUaDi_1_1TransferServicesTypeBase.html#aede21ee9e31637793650292245941487", null ],
    [ "call", "classOpcUaDi_1_1TransferServicesTypeBase.html#aff8798d0212aab8a13559507253bea44", null ],
    [ "FetchTransferResultData", "classOpcUaDi_1_1TransferServicesTypeBase.html#a6f695c12963f9ab0f2d6203edd2da003", null ],
    [ "getFetchTransferResultData", "classOpcUaDi_1_1TransferServicesTypeBase.html#a4672078d38088fec7b8c7d4f8dd630c9", null ],
    [ "getTransferFromDevice", "classOpcUaDi_1_1TransferServicesTypeBase.html#ac20a8a64ca3a4a4879800a2079540ccb", null ],
    [ "getTransferToDevice", "classOpcUaDi_1_1TransferServicesTypeBase.html#a8576a247d4cecb34519f7ae7a31219c9", null ],
    [ "TransferFromDevice", "classOpcUaDi_1_1TransferServicesTypeBase.html#a49fa9d67f1152201b749f321d051893e", null ],
    [ "TransferToDevice", "classOpcUaDi_1_1TransferServicesTypeBase.html#a3968056ec03b407536ca3a22748f05e0", null ],
    [ "typeDefinitionId", "classOpcUaDi_1_1TransferServicesTypeBase.html#a3b4a0a2ed0c80b3646b453a4616b65dc", null ],
    [ "useAccessInfoFromInstance", "classOpcUaDi_1_1TransferServicesTypeBase.html#a1ebb32b5a0b49ad46ab26e59f01a38e3", null ],
    [ "useAccessInfoFromType", "classOpcUaDi_1_1TransferServicesTypeBase.html#aa62a5ce07db4350e1e9400a58e2cf21b", null ]
];