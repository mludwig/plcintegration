var structOpcUa__DiagnosticInfo =
[
    [ "AdditionalInfo", "group__UaStackTypes.html#ga26be87f4f5998f0b1f985d356582e159", null ],
    [ "InnerDiagnosticInfo", "group__UaStackTypes.html#gabb0898b6f8ee1a7fa034b2471bb0afbe", null ],
    [ "InnerStatusCode", "group__UaStackTypes.html#ga6c227ee81cb5021424e3ab5dc99edbef", null ],
    [ "Locale", "group__UaStackTypes.html#ga6b894922e46884db6dd4941c1078c768", null ],
    [ "LocalizedText", "group__UaStackTypes.html#ga05d2adafce9f931e2ccd04a52130d46a", null ],
    [ "NamespaceUri", "group__UaStackTypes.html#gaf8248cd635a1a6c00ebe907d16f6909a", null ],
    [ "SymbolicId", "group__UaStackTypes.html#ga2b8a8b3fd6f9ee6d416fe90f71354bca", null ]
];