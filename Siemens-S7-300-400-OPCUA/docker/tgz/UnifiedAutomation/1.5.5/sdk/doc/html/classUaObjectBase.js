var classUaObjectBase =
[
    [ "~UaObjectBase", "classUaObjectBase.html#adddc79c3039c3094ed6a2b13d1d91b8a", null ],
    [ "UaObjectBase", "classUaObjectBase.html#aa1698f936df9d8045925044ca67fb490", null ],
    [ "browse", "classUaObjectBase.html#afa349bb848c63facdfbe4ef4196046bd", null ],
    [ "browseName", "classUaObjectBase.html#a7b70618cfa455a52bd2a9db62f99781e", null ],
    [ "description", "classUaObjectBase.html#aae8bf3925197fb2442055b549e23f380", null ],
    [ "displayName", "classUaObjectBase.html#a101d38105d235231c071f39c4d57feb9", null ],
    [ "getUaNode", "classUaObjectBase.html#ae8d59a17c9e15f62d3ea724c2038c971", null ],
    [ "getUaReferenceLists", "classUaObjectBase.html#ae5f0e5ed50b63959d623b864bfeaccdf", null ],
    [ "isDescriptionSupported", "classUaObjectBase.html#aa2aaa81882e76ee0e2cd0e6fb3e9c3ba", null ],
    [ "isUserWriteMaskSupported", "classUaObjectBase.html#ad4202dc7d431718e3e3be83e6f237913", null ],
    [ "isWriteMaskSupported", "classUaObjectBase.html#ae719a219f863561df4f352966860e37b", null ],
    [ "nodeId", "classUaObjectBase.html#a9970e86bae200d3e5e7f68c0c5648442", null ],
    [ "userWriteMask", "classUaObjectBase.html#aee816a0fa97d3b162b6b226de8d76f96", null ],
    [ "writeMask", "classUaObjectBase.html#ac74ad9dc466d08885de4eb91cb71420b", null ],
    [ "m_defaultLocaleId", "classUaObjectBase.html#abade2bf987529b1c360cb3ac10c9a28a", null ]
];