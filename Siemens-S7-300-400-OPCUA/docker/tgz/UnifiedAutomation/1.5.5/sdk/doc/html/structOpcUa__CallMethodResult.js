var structOpcUa__CallMethodResult =
[
    [ "InputArgumentDiagnosticInfos", "group__UaStackTypes.html#ga4bf33f93174146993e351aeb0ee7482e", null ],
    [ "InputArgumentResults", "group__UaStackTypes.html#ga3e0f1d2dbc5b89b1048306c89e827665", null ],
    [ "OutputArguments", "group__UaStackTypes.html#ga744a3f7302f37de1ebb0441c64c6c3ba", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga3c9c90e0070e660e1715abeccf9b6bf9", null ]
];