var group__UaClientLibraryHelper =
[
    [ "UaClientSdk", "namespaceUaClientSdk.html", null ],
    [ "UaClient", "classUaClientSdk_1_1UaClient.html", [
      [ "ConnectServiceType", "group__UaClientLibraryHelper.html#gaa9dcb71d5bab4f170c00684102029d48", [
        [ "CertificateValidation", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a796ffefa3b9416b680a3c1b2436f8232", null ],
        [ "OpenSecureChannel", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48ad27febe76244232b5c30f4d5bf147200", null ],
        [ "CreateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a31fca26b8ce505e46f2facbe96980813", null ],
        [ "ActivateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48afc609ea75dc746e59427ab7b26569baf", null ]
      ] ],
      [ "ReadTypeDictionaries", "group__UaClientLibraryHelper.html#ga1e8eec334edb5b41e70294a5aa2eb795", [
        [ "ReadTypeDictionaries_FirstUse", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a1e91b48ccf2e6d99f398b558bdeb72f2", null ],
        [ "ReadTypeDictionaries_Manual", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a41235ca2d748879955e1a1ad8ccda48f", null ],
        [ "ReadTypeDictionaries_Connect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a019ddec14445f37c8bd7e5b9fd81e118", null ],
        [ "ReadTypeDictionaries_Reconnect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795aefd27a02f34a7b519b923ba24b87d046", null ]
      ] ],
      [ "ServerStatus", "group__UaClientLibraryHelper.html#ga3f55bd54bbf50515656f2b9ab621dc7f", [
        [ "Disconnected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa5e1921caf43ec40c27e70de227f99be2", null ],
        [ "Connected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa3bf419f6fab7bc01b0f5b54877def6bb", null ],
        [ "ConnectionWarningWatchdogTimeout", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa24e8d658c496bbdd3130ee7f609527bd", null ],
        [ "ConnectionErrorApiReconnect", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa59e55a2bc03496fa163db95406ec78ba", null ],
        [ "ServerShutdown", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa6246b2e6f2f391874978b57880c030ef", null ],
        [ "NewSessionCreated", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fab9c69648d5ea27308f906828d9828043", null ]
      ] ]
    ] ],
    [ "SessionConnectInfo", "classUaClientSdk_1_1SessionConnectInfo.html", [
      [ "SessionConnectInfo", "classUaClientSdk_1_1SessionConnectInfo.html#a44a0ed2add93cc0d56f0f48b50393b71", null ],
      [ "~SessionConnectInfo", "classUaClientSdk_1_1SessionConnectInfo.html#a860c004c49780d618c17596b4803d878", null ],
      [ "alternativeEndpointUrls", "classUaClientSdk_1_1SessionConnectInfo.html#a1047118d4b65a2460d3ac6218716aa5d", null ],
      [ "applicationType", "classUaClientSdk_1_1SessionConnectInfo.html#ac7c00a7deb6e43e71b288d68a1b55fde", null ],
      [ "arAdditionalLocaleIds", "classUaClientSdk_1_1SessionConnectInfo.html#ad94d25d09eb698f61645efd06ee85406", null ],
      [ "auditEntryId", "classUaClientSdk_1_1SessionConnectInfo.html#af64b66e34b6fcd5af26bf8cf025886a2", null ],
      [ "bAutomaticReconnect", "classUaClientSdk_1_1SessionConnectInfo.html#a29fe4ceb166f8cf016b0ac7c62c97127", null ],
      [ "bRetryInitialConnect", "classUaClientSdk_1_1SessionConnectInfo.html#ab45fab6c2132b9a635f4c624f574e566", null ],
      [ "clientConnectionId", "classUaClientSdk_1_1SessionConnectInfo.html#aa068260ae313893e692c77dc50336a0f", null ],
      [ "endpointDescriptions", "classUaClientSdk_1_1SessionConnectInfo.html#af0149b51b012ed0d190f445ec7fd2bab", null ],
      [ "internalServiceCallTimeout", "classUaClientSdk_1_1SessionConnectInfo.html#a489b9a9175cc626fef7708f203bfbd65", null ],
      [ "nConnectTimeout", "classUaClientSdk_1_1SessionConnectInfo.html#a49624abb2a625fc5898e23b50ab48a18", null ],
      [ "nMaxOperationsPerServiceCall", "classUaClientSdk_1_1SessionConnectInfo.html#a93a2385a2cc67ff77bbd4218efbe4ded", null ],
      [ "nPublishTimeout", "classUaClientSdk_1_1SessionConnectInfo.html#aae0d22bd0b0204b1bf234def2e50c35b", null ],
      [ "nSecureChannelLifetime", "classUaClientSdk_1_1SessionConnectInfo.html#aba9b1fb9d989e8b6ebc570404c026443", null ],
      [ "nSessionTimeout", "classUaClientSdk_1_1SessionConnectInfo.html#af3cc3bbf606fceb8a6009f8c480932cf", null ],
      [ "nWatchdogTime", "classUaClientSdk_1_1SessionConnectInfo.html#a04235a7edc09097c8bda285e97c2ec99", null ],
      [ "nWatchdogTimeout", "classUaClientSdk_1_1SessionConnectInfo.html#a6dd2a23f1f557685ce603b0c9b78ea17", null ],
      [ "sApplicationName", "classUaClientSdk_1_1SessionConnectInfo.html#a379d031497a7d4d982ba4a5bfc1ae9fc", null ],
      [ "sApplicationUri", "classUaClientSdk_1_1SessionConnectInfo.html#a1b5aabdbc009bd3d051b40439915d057", null ],
      [ "sLocaleId", "classUaClientSdk_1_1SessionConnectInfo.html#accc719ed574821d156ee4f2f55f0efa9", null ],
      [ "sProductUri", "classUaClientSdk_1_1SessionConnectInfo.html#ac1a57ee1f9a383ae8051079c2bc0815c", null ],
      [ "sServerUri", "classUaClientSdk_1_1SessionConnectInfo.html#a91d14d8652132cb3b9c4a62c7bdf5aec", null ],
      [ "sSessionName", "classUaClientSdk_1_1SessionConnectInfo.html#ac62fed89aa0db8cb129708328094b931", null ],
      [ "typeDictionaryMode", "classUaClientSdk_1_1SessionConnectInfo.html#aefecac550d70c667f6972e6f45ccfcb1", null ]
    ] ],
    [ "ClientSecurityInfo", "classUaClientSdk_1_1ClientSecurityInfo.html", [
      [ "ClientSecurityInfo", "classUaClientSdk_1_1ClientSecurityInfo.html#a66fc6a8ee9c0ff313a520489f383869a", null ],
      [ "~ClientSecurityInfo", "classUaClientSdk_1_1ClientSecurityInfo.html#ae0dcc2b31911d4f54fadd0d19507c25d", null ],
      [ "initializePkiProviderHttps", "classUaClientSdk_1_1ClientSecurityInfo.html#a09369d086818dd36089879ec669fd81c", null ],
      [ "initializePkiProviderOpenSSL", "classUaClientSdk_1_1ClientSecurityInfo.html#a9102371b82f866e8851c3cc6f125e0ef", null ],
      [ "initializePkiProviderOpenSSL", "classUaClientSdk_1_1ClientSecurityInfo.html#a5e4d92252982e84206c7dfd8e6baad6b", null ],
      [ "initializePkiProviderWindows", "classUaClientSdk_1_1ClientSecurityInfo.html#a1c6ef93db15ac24e17d92bf6f50849b7", null ],
      [ "initializePkiProviderWindows", "classUaClientSdk_1_1ClientSecurityInfo.html#a76095d74227b98c4546af30aec3c2bdc", null ],
      [ "loadClientCertificateOpenSSL", "classUaClientSdk_1_1ClientSecurityInfo.html#a28e531e71e9e1d4826fdf899eea9b4dd", null ],
      [ "loadClientCertificateWindows", "classUaClientSdk_1_1ClientSecurityInfo.html#abb500f6e721fdc0250fd7c195a939e08", null ],
      [ "pkiCfg", "classUaClientSdk_1_1ClientSecurityInfo.html#ab6d67d301a3b766808a0f32834e159c7", null ],
      [ "pkiCfgHttps", "classUaClientSdk_1_1ClientSecurityInfo.html#ac8f00d949b05fc8c692c06c08cd89a4b", null ],
      [ "clientCertificate", "classUaClientSdk_1_1ClientSecurityInfo.html#a9a036b3ba8dc73262a123a78c7636e45", null ],
      [ "clientPrivateKey", "classUaClientSdk_1_1ClientSecurityInfo.html#ab1dc5b4dd07fb6a4708a4c62e7fe2deb", null ],
      [ "messageSecurityMode", "classUaClientSdk_1_1ClientSecurityInfo.html#a9f86cdda489732d773201d2d1fdec0b5", null ],
      [ "sSecurityPolicy", "classUaClientSdk_1_1ClientSecurityInfo.html#a8811b8341e1fe2277acaf0ec014a07f6", null ]
    ] ],
    [ "CertificateValidationCallback", "classUaClientSdk_1_1CertificateValidationCallback.html", [
      [ "verificationError", "classUaClientSdk_1_1CertificateValidationCallback.html#aaeaccd84c8b5623b3229da13cb3c3907", null ]
    ] ],
    [ "SessionSecurityInfo", "classUaClientSdk_1_1SessionSecurityInfo.html", [
      [ "SessionSecurityInfo", "classUaClientSdk_1_1SessionSecurityInfo.html#a287a708e79b7a877aee1c3bcc2d29dc8", null ],
      [ "~SessionSecurityInfo", "classUaClientSdk_1_1SessionSecurityInfo.html#a3e3ec4ef16dd55dbab672ec736d87293", null ],
      [ "SessionSecurityInfo", "classUaClientSdk_1_1SessionSecurityInfo.html#ad3ab9a87b35a3a37b79cf07650ae1c3b", null ],
      [ "getCurrentErrorInformationOpenSSL", "classUaClientSdk_1_1SessionSecurityInfo.html#aaeb6b8794ddfafeb98575226f950ca17", null ],
      [ "loadServerCertificateOpenSSL", "classUaClientSdk_1_1SessionSecurityInfo.html#a4b14b67e9e6021e0472bebe3e8a9416f", null ],
      [ "loadServerCertificateWindows", "classUaClientSdk_1_1SessionSecurityInfo.html#a176a387a3604785ba4dc535c1dbbb1c3", null ],
      [ "operator=", "classUaClientSdk_1_1SessionSecurityInfo.html#a544b8c1c831bf19eeef473b74be9556f", null ],
      [ "pUserIdentityToken", "classUaClientSdk_1_1SessionSecurityInfo.html#a544ceb620aec606be5718ca12a9be582", null ],
      [ "saveServerCertificate", "classUaClientSdk_1_1SessionSecurityInfo.html#ac37a85ee7426a40ed51d666b9c449ba2", null ],
      [ "setAnonymousUserIdentity", "classUaClientSdk_1_1SessionSecurityInfo.html#aa60df67203560fb19705f7acfa777045", null ],
      [ "setCertificateUserIdentity", "classUaClientSdk_1_1SessionSecurityInfo.html#ad15606b165ce81962520b6271364cfd6", null ],
      [ "setUserPasswordUserIdentity", "classUaClientSdk_1_1SessionSecurityInfo.html#a38eec167da258bf62a8e9df547eeac1b", null ],
      [ "splitCertificateChain", "classUaClientSdk_1_1SessionSecurityInfo.html#ac265245a734834c9e616149571a30c38", null ],
      [ "verifyServerCertificate", "classUaClientSdk_1_1SessionSecurityInfo.html#a44dc1136a6d98d9dc6dd9e5cba6269b5", null ],
      [ "disableApplicationUriCheck", "classUaClientSdk_1_1SessionSecurityInfo.html#ae471afefc8a4d8a3355ad2cdbe75ea50", null ],
      [ "disableEncryptedPasswordCheck", "classUaClientSdk_1_1SessionSecurityInfo.html#a3eec2f43d886681a93b143208855e98c", null ],
      [ "disableErrorCertificateHostNameInvalid", "classUaClientSdk_1_1SessionSecurityInfo.html#abfa57f3b88b2f6e5373f0ef8e711ae58", null ],
      [ "disableErrorCertificateIssuerRevocationUnknown", "classUaClientSdk_1_1SessionSecurityInfo.html#a58b262cfd5c8080a0c955d0dfca516aa", null ],
      [ "disableErrorCertificateIssuerTimeInvalid", "classUaClientSdk_1_1SessionSecurityInfo.html#a5bb6a6a78ca25dc609b22d8525892f08", null ],
      [ "disableErrorCertificateRevocationUnknown", "classUaClientSdk_1_1SessionSecurityInfo.html#a2ced1ded700def95a23a184e48214778", null ],
      [ "disableErrorCertificateTimeInvalid", "classUaClientSdk_1_1SessionSecurityInfo.html#a4488c25aa621c8e570f52c712cbc0569", null ],
      [ "disableNonceLengthCheck", "classUaClientSdk_1_1SessionSecurityInfo.html#a7592780365589e144c1a27e76232390a", null ],
      [ "doServerCertificateVerify", "classUaClientSdk_1_1SessionSecurityInfo.html#a98681511a65065074a895b488125b2c7", null ],
      [ "serverCertificate", "classUaClientSdk_1_1SessionSecurityInfo.html#a03780807fa6b62765fb0f46d5ae5379b", null ]
    ] ],
    [ "ServiceSettings", "classUaClientSdk_1_1ServiceSettings.html", [
      [ "ServiceSettings", "classUaClientSdk_1_1ServiceSettings.html#a7c2c76c66d73717da0e39376f975ec29", null ],
      [ "~ServiceSettings", "classUaClientSdk_1_1ServiceSettings.html#a9bdbb4cd8950ce661d49203d5101b4ee", null ],
      [ "auditEntryId", "classUaClientSdk_1_1ServiceSettings.html#ac67121334db488f0e64c695581f1f020", null ],
      [ "callTimeout", "classUaClientSdk_1_1ServiceSettings.html#a73af610c9be059c2bcc8fb235c48e81f", null ],
      [ "requestHandle", "classUaClientSdk_1_1ServiceSettings.html#a546a0dcbfc09f5f678a2ab419db3c17c", null ],
      [ "responseTimestamp", "classUaClientSdk_1_1ServiceSettings.html#a4358a362417903c0e34bce387d7dcd5e", null ],
      [ "returnDiagnostics", "classUaClientSdk_1_1ServiceSettings.html#aa14ffd045a02773b7838a28d9640b193", null ],
      [ "stringTable", "classUaClientSdk_1_1ServiceSettings.html#a8858833364713f3707c95bac1177643f", null ]
    ] ],
    [ "BrowseContext", "classUaClientSdk_1_1BrowseContext.html", [
      [ "BrowseContext", "classUaClientSdk_1_1BrowseContext.html#a9c8504697b13636d132ee5bd17ab1bc4", null ],
      [ "~BrowseContext", "classUaClientSdk_1_1BrowseContext.html#a646606d31e98f2166ef48784d9e741e1", null ],
      [ "browseDirection", "classUaClientSdk_1_1BrowseContext.html#ad89eb827bdf0872d238bda9d087bc98c", null ],
      [ "includeSubtype", "classUaClientSdk_1_1BrowseContext.html#a8d84facb3917ecb3f46c15f9cc4433eb", null ],
      [ "maxReferencesToReturn", "classUaClientSdk_1_1BrowseContext.html#a81833dc6e542bdaa4573aaa9394cb755", null ],
      [ "nodeClassMask", "classUaClientSdk_1_1BrowseContext.html#ace381d5e585d4c488dd02c897da712c2", null ],
      [ "referenceTypeId", "classUaClientSdk_1_1BrowseContext.html#a802ba238e44c49f32ade1695518f6fec", null ],
      [ "resultMask", "classUaClientSdk_1_1BrowseContext.html#acc1111496ea5f8099768c1f43a8c7ca9", null ],
      [ "view", "classUaClientSdk_1_1BrowseContext.html#ad762a621a39874ee70c5025d881e4ec5", null ]
    ] ],
    [ "HistoryReadRawModifiedContext", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html", [
      [ "HistoryReadRawModifiedContext", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#a89f047aebca5411292ba3128f2cd166b", null ],
      [ "~HistoryReadRawModifiedContext", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#aa0ddfc5371269d3e63f48a11d971233e", null ],
      [ "bReleaseContinuationPoints", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#a89dbfe9c6556a061bbd3848d7e99a9f6", null ],
      [ "endTime", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#a0aea2375679c2c42b5727ce70481ab72", null ],
      [ "isReadModified", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#ae12f37ab65328b60dfba144bd6a73499", null ],
      [ "numValuesPerNode", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#af12abf5e5fd16bc8b4b4b6fa2d0639af", null ],
      [ "returnBounds", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#aa6cc2c42f18db87a64e739990097d56c", null ],
      [ "startTime", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#adb06249b60d6966c2b591316bd431cf6", null ],
      [ "timeStamps", "classUaClientSdk_1_1HistoryReadRawModifiedContext.html#a10369fb3c69b4db5dd23ebf792ca2560", null ]
    ] ],
    [ "HistoryReadProcessedContext", "classUaClientSdk_1_1HistoryReadProcessedContext.html", [
      [ "HistoryReadProcessedContext", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a8e2137e66fa930dac4e76f4d7c6d694a", null ],
      [ "~HistoryReadProcessedContext", "classUaClientSdk_1_1HistoryReadProcessedContext.html#ac04c950ab93b2978ceaa4e494dab360a", null ],
      [ "aggregateConfiguration", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a5e081a7d84e31cda7ea86fa8f0950534", null ],
      [ "aggregateTypes", "classUaClientSdk_1_1HistoryReadProcessedContext.html#ab7fe71401598e060313b794a1f59abbd", null ],
      [ "bReleaseContinuationPoints", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a1a31d30017920c5b1ec6a349f0b47d7c", null ],
      [ "endTime", "classUaClientSdk_1_1HistoryReadProcessedContext.html#ad33d6192adb6266429ca4082b651f2bf", null ],
      [ "processingInterval", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a943e0d0c41e44c04a493fe5578bc82dc", null ],
      [ "startTime", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a432b71c2897004be538f0b3e495d861d", null ],
      [ "timeStamps", "classUaClientSdk_1_1HistoryReadProcessedContext.html#a032ff0b48e4966ad9fd5a2df6088f6a3", null ]
    ] ],
    [ "HistoryReadAtTimeContext", "classUaClientSdk_1_1HistoryReadAtTimeContext.html", [
      [ "HistoryReadAtTimeContext", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#a15f6a9808aaa8eb23ee762b38d9c272f", null ],
      [ "~HistoryReadAtTimeContext", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#a23353ff2334fae9a5833ced009584b13", null ],
      [ "bReleaseContinuationPoints", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#a9bafab2297fca2274d0c91e7c24b77c2", null ],
      [ "requestedTimes", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#afbddee2b22a0cf51c7a761d378cb0ed5", null ],
      [ "timeStamps", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#a89637f02b4ce575005a6c18e1ff6e5f0", null ],
      [ "useSimpleBounds", "classUaClientSdk_1_1HistoryReadAtTimeContext.html#ace74e3d9b6e2fede92ddec28ca89a614", null ]
    ] ],
    [ "HistoryReadDataResult", "classUaClientSdk_1_1HistoryReadDataResult.html", [
      [ "HistoryReadDataResult", "classUaClientSdk_1_1HistoryReadDataResult.html#a3beea427b090ecef7c68646cb1e4b348", null ],
      [ "~HistoryReadDataResult", "classUaClientSdk_1_1HistoryReadDataResult.html#a2b1f1d892a23b7ccc90218fec9e17cd8", null ],
      [ "m_continuationPoint", "classUaClientSdk_1_1HistoryReadDataResult.html#abffaad614b207c1a2814b16e2b66bcc9", null ],
      [ "m_dataValues", "classUaClientSdk_1_1HistoryReadDataResult.html#a047526f926aacfe52e0504aa239d8e2c", null ],
      [ "m_modificationInformation", "classUaClientSdk_1_1HistoryReadDataResult.html#afc673051853c66082bde4f80bcc724b8", null ],
      [ "m_status", "classUaClientSdk_1_1HistoryReadDataResult.html#afca46e512517fb495fa910b239ced21e", null ]
    ] ],
    [ "HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html", [
      [ "HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html#aa7186c3d4281fef2b131389275023f7a", null ],
      [ "~HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html#acd266005d59be85f4a24db6834992a22", null ],
      [ "bReleaseContinuationPoints", "classUaClientSdk_1_1HistoryReadEventContext.html#a9660112cb2178288bbc3179b8f438ca9", null ],
      [ "endTime", "classUaClientSdk_1_1HistoryReadEventContext.html#ae42b9637df7e21f8f9f5f80cf4297475", null ],
      [ "eventFilter", "classUaClientSdk_1_1HistoryReadEventContext.html#abf055a5c856d05a8e082b48a83b80bb7", null ],
      [ "numValuesPerNode", "classUaClientSdk_1_1HistoryReadEventContext.html#a416a0bc8fba9f8b9298543b2acd53cbc", null ],
      [ "startTime", "classUaClientSdk_1_1HistoryReadEventContext.html#a6c4b7829dc0d76e429e2e0a3a79ff285", null ],
      [ "timeStamps", "classUaClientSdk_1_1HistoryReadEventContext.html#acaa7b2043a92aa85e9daf02ba08d3d2c", null ]
    ] ],
    [ "HistoryReadEventResult", "classUaClientSdk_1_1HistoryReadEventResult.html", [
      [ "HistoryReadEventResult", "classUaClientSdk_1_1HistoryReadEventResult.html#a59b9d07e7776d1d06997616b8c22d26a", null ],
      [ "~HistoryReadEventResult", "classUaClientSdk_1_1HistoryReadEventResult.html#a2976daffa40d45be46ea8c177303d956", null ],
      [ "m_continuationPoint", "classUaClientSdk_1_1HistoryReadEventResult.html#af6677376247dfb8e2f18d6e9c7374b88", null ],
      [ "m_events", "classUaClientSdk_1_1HistoryReadEventResult.html#ad6e07799c22da9450da1048421c48b1d", null ],
      [ "m_status", "classUaClientSdk_1_1HistoryReadEventResult.html#ac6e01c3a735c324166e8faed3aa67c01", null ]
    ] ],
    [ "UpdateDataDetail", "classUaClientSdk_1_1UpdateDataDetail.html", [
      [ "UpdateDataDetail", "classUaClientSdk_1_1UpdateDataDetail.html#af6a0b8f3dd7e05806922254259cca3c3", null ],
      [ "~UpdateDataDetail", "classUaClientSdk_1_1UpdateDataDetail.html#ab6d2f4f69756e8e1505bc64ec31dd846", null ],
      [ "m_dataValues", "classUaClientSdk_1_1UpdateDataDetail.html#afd08f3a68f781fb5569cebe82a9f804c", null ],
      [ "m_isStructureUpdate", "classUaClientSdk_1_1UpdateDataDetail.html#aa6fdb94ed4035055ecd5b6e093809ee1", null ],
      [ "m_nodeId", "classUaClientSdk_1_1UpdateDataDetail.html#a034677517b10e74d05d1285aa4f83206", null ],
      [ "m_PerformInsertReplace", "classUaClientSdk_1_1UpdateDataDetail.html#a77a05a5ac5bb1258bd3b21f96dc11333", null ]
    ] ],
    [ "DeleteRawModifiedDetail", "classUaClientSdk_1_1DeleteRawModifiedDetail.html", [
      [ "DeleteRawModifiedDetail", "classUaClientSdk_1_1DeleteRawModifiedDetail.html#a280736a091d84bc4e9f16669fee7aeeb", null ],
      [ "~DeleteRawModifiedDetail", "classUaClientSdk_1_1DeleteRawModifiedDetail.html#a7f3d0f7b744c6ded1827738bc96188c4", null ]
    ] ],
    [ "DeleteAtTimeDetail", "classUaClientSdk_1_1DeleteAtTimeDetail.html", [
      [ "DeleteAtTimeDetail", "classUaClientSdk_1_1DeleteAtTimeDetail.html#a5c469d669e07dcccb0f2eb3130b14be1", null ],
      [ "~DeleteAtTimeDetail", "classUaClientSdk_1_1DeleteAtTimeDetail.html#ae1f96a71adc243742d6bc60f278d3f0c", null ]
    ] ],
    [ "UpdateEventDetail", "classUaClientSdk_1_1UpdateEventDetail.html", [
      [ "UpdateEventDetail", "classUaClientSdk_1_1UpdateEventDetail.html#a73834baa4af860b1337140c3923cba89", null ],
      [ "~UpdateEventDetail", "classUaClientSdk_1_1UpdateEventDetail.html#a04932658b9762b415bb96318bfb58cd5", null ]
    ] ],
    [ "DeleteEventDetail", "classUaClientSdk_1_1DeleteEventDetail.html", [
      [ "DeleteEventDetail", "classUaClientSdk_1_1DeleteEventDetail.html#a7e0354078a3af9502cd795e78b56bfb6", null ],
      [ "~DeleteEventDetail", "classUaClientSdk_1_1DeleteEventDetail.html#a90f120befb245af0f70d31cd22b445ad", null ]
    ] ],
    [ "CallIn", "classUaClientSdk_1_1CallIn.html", [
      [ "CallIn", "classUaClientSdk_1_1CallIn.html#ad5622d052e80b03432a93b96a4db86cc", null ],
      [ "~CallIn", "classUaClientSdk_1_1CallIn.html#a2e71b283452d00ebc3c948fe638178ea", null ]
    ] ],
    [ "CallOut", "classUaClientSdk_1_1CallOut.html", [
      [ "CallOut", "classUaClientSdk_1_1CallOut.html#ae432a780034895ae8000a83b8eb05c66", null ],
      [ "~CallOut", "classUaClientSdk_1_1CallOut.html#aa05948bc4cac2c0892ffce3441ebe568", null ]
    ] ],
    [ "SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html", [
      [ "SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html#ab03cc4512f0fc5d7c7152213e25e7082", null ],
      [ "~SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html#af909c75191f2b7b7d707ff2062bd88a1", null ],
      [ "lifetimeCount", "classUaClientSdk_1_1SubscriptionSettings.html#ac27d5468e2052beaa5b6818a69a4a949", null ],
      [ "maxKeepAliveCount", "classUaClientSdk_1_1SubscriptionSettings.html#a1753fb167c29a2c3313a2f8f2c7b17f3", null ],
      [ "maxNotificationsPerPublish", "classUaClientSdk_1_1SubscriptionSettings.html#a750cda928a8fa58ee29e8df1a290a785", null ],
      [ "priority", "classUaClientSdk_1_1SubscriptionSettings.html#a50e4fd24311c36faa821f822a8b90be9", null ],
      [ "publishingInterval", "classUaClientSdk_1_1SubscriptionSettings.html#a29ab09bbd318526adb3ffec8f7de4db6", null ]
    ] ],
    [ "UaSessionCallback", "classUaClientSdk_1_1UaSessionCallback.html", [
      [ "addNodesComplete", "classUaClientSdk_1_1UaSessionCallback.html#a2c00c0a21087170ebe2f6c38a8d96b77", null ],
      [ "addReferencesComplete", "classUaClientSdk_1_1UaSessionCallback.html#a8895cda329baae668abe17235c7be61c", null ],
      [ "callComplete", "classUaClientSdk_1_1UaSessionCallback.html#af456a65f4872b3b6c7c3a5e7801c231b", null ],
      [ "connectError", "classUaClientSdk_1_1UaSessionCallback.html#a1e10b0820a0ff8f27583a4d16b6b5b7c", null ],
      [ "connectionStatusChanged", "classUaClientSdk_1_1UaSessionCallback.html#a5cb6f8af01dcd6210b77f7012bbb49da", null ],
      [ "deleteNodesComplete", "classUaClientSdk_1_1UaSessionCallback.html#add1a7eb69aac0eb2d96737d8fdfcc724", null ],
      [ "deleteReferencesComplete", "classUaClientSdk_1_1UaSessionCallback.html#a39dc94247c7ad6b2864486efd9196c36", null ],
      [ "historyDeleteAtTimeComplete", "classUaClientSdk_1_1UaSessionCallback.html#afe6bfad34786b3f9c5dad5845677e5c9", null ],
      [ "historyDeleteEventsComplete", "classUaClientSdk_1_1UaSessionCallback.html#a053da1f83db6d69e010b475b61f031a0", null ],
      [ "historyDeleteRawModifiedComplete", "classUaClientSdk_1_1UaSessionCallback.html#a37065493d8f47b00295f46ef5a314936", null ],
      [ "historyReadAtTimeComplete", "classUaClientSdk_1_1UaSessionCallback.html#a37c02f6c23afad24e031d6d8a9321712", null ],
      [ "historyReadEventComplete", "classUaClientSdk_1_1UaSessionCallback.html#a9bb5bcf32a6ea27008fbe2d22d03ab5a", null ],
      [ "historyReadProcessedComplete", "classUaClientSdk_1_1UaSessionCallback.html#ae1d43afbf470ecf38846850d24b7050a", null ],
      [ "historyReadRawModifiedComplete", "classUaClientSdk_1_1UaSessionCallback.html#ac5a791bf1b8e22f96cda298dc39fc77b", null ],
      [ "historyUpdateDataComplete", "classUaClientSdk_1_1UaSessionCallback.html#a3771312fffe8f9a8ea67ae69e1248afb", null ],
      [ "historyUpdateEventsComplete", "classUaClientSdk_1_1UaSessionCallback.html#ad0c8d3be60a8927be71d24f03c1ea757", null ],
      [ "readComplete", "classUaClientSdk_1_1UaSessionCallback.html#a4f79cd7efb9a6ddee962774422cfb5a9", null ],
      [ "sslCertificateValidationFailed", "classUaClientSdk_1_1UaSessionCallback.html#a1d511a5f3cb2a02db44b93bf7f8faff3", null ],
      [ "writeComplete", "classUaClientSdk_1_1UaSessionCallback.html#af20ba43cff3874e6a3f2ee8a16f2942e", null ]
    ] ],
    [ "UaSubscriptionCallback", "classUaClientSdk_1_1UaSubscriptionCallback.html", [
      [ "createMonitoredItemsComplete", "classUaClientSdk_1_1UaSubscriptionCallback.html#a966077e08dcd26e93d6dc8d6b1844098", null ],
      [ "dataChange", "classUaClientSdk_1_1UaSubscriptionCallback.html#a5fce3b40521c5342f084b6ca70a8a82a", null ],
      [ "deleteMonitoredItemsComplete", "classUaClientSdk_1_1UaSubscriptionCallback.html#aa91d1ac49467368117ec4bfd868d59ed", null ],
      [ "keepAlive", "classUaClientSdk_1_1UaSubscriptionCallback.html#a8cdd423bd2d2e152545b041363fd12b1", null ],
      [ "modifyMonitoredItemsComplete", "classUaClientSdk_1_1UaSubscriptionCallback.html#ae56f1ba5a28818ee7b15c031ce9abc0d", null ],
      [ "newEvents", "classUaClientSdk_1_1UaSubscriptionCallback.html#a432a1955c78cab80328abd792e543198", null ],
      [ "notificationsMissing", "classUaClientSdk_1_1UaSubscriptionCallback.html#a6b1341655ca32eecaa9ec92eb29f0c1b", null ],
      [ "setMonitoringModeComplete", "classUaClientSdk_1_1UaSubscriptionCallback.html#a18ab63395739fc69b7d3a7e812434415", null ],
      [ "subscriptionStatusChanged", "classUaClientSdk_1_1UaSubscriptionCallback.html#a6573482cca88950b9b09f9b0671514f2", null ]
    ] ],
    [ "ConnectServiceType", "group__UaClientLibraryHelper.html#gaa9dcb71d5bab4f170c00684102029d48", [
      [ "CertificateValidation", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a796ffefa3b9416b680a3c1b2436f8232", null ],
      [ "OpenSecureChannel", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48ad27febe76244232b5c30f4d5bf147200", null ],
      [ "CreateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a31fca26b8ce505e46f2facbe96980813", null ],
      [ "ActivateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48afc609ea75dc746e59427ab7b26569baf", null ]
    ] ],
    [ "ReadTypeDictionaries", "group__UaClientLibraryHelper.html#ga1e8eec334edb5b41e70294a5aa2eb795", [
      [ "ReadTypeDictionaries_FirstUse", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a1e91b48ccf2e6d99f398b558bdeb72f2", null ],
      [ "ReadTypeDictionaries_Manual", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a41235ca2d748879955e1a1ad8ccda48f", null ],
      [ "ReadTypeDictionaries_Connect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a019ddec14445f37c8bd7e5b9fd81e118", null ],
      [ "ReadTypeDictionaries_Reconnect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795aefd27a02f34a7b519b923ba24b87d046", null ]
    ] ],
    [ "ServerStatus", "group__UaClientLibraryHelper.html#ga3f55bd54bbf50515656f2b9ab621dc7f", [
      [ "Disconnected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa5e1921caf43ec40c27e70de227f99be2", null ],
      [ "Connected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa3bf419f6fab7bc01b0f5b54877def6bb", null ],
      [ "ConnectionWarningWatchdogTimeout", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa24e8d658c496bbdd3130ee7f609527bd", null ],
      [ "ConnectionErrorApiReconnect", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa59e55a2bc03496fa163db95406ec78ba", null ],
      [ "ServerShutdown", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa6246b2e6f2f391874978b57880c030ef", null ],
      [ "NewSessionCreated", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fab9c69648d5ea27308f906828d9828043", null ]
    ] ]
];