var structOpcUa__AggregateConfiguration =
[
    [ "PercentDataBad", "group__UaStackTypes.html#ga0a926196cc135225c34511d0af8510c7", null ],
    [ "PercentDataGood", "group__UaStackTypes.html#gaace11fc8ab21f4b62d3eba9d50df0b38", null ],
    [ "TreatUncertainAsBad", "group__UaStackTypes.html#ga07a7cf674b279849b828741d7b241b60", null ],
    [ "UseServerCapabilitiesDefaults", "group__UaStackTypes.html#ga0fa5356f7bbce5945c8cca338a203727", null ],
    [ "UseSlopedExtrapolation", "group__UaStackTypes.html#ga335cf19cb04c84f22944bc120be57be1", null ]
];