var classUaHistoryEvent =
[
    [ "UaHistoryEvent", "classUaHistoryEvent.html#a19b74f20c80f1e1eb4293afe84caa85d", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html#a50a520859f31c081469882aeabbc147c", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html#a80628923f862cabe86a9941281907d74", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html#a360c7d86b4b65f6885989c8e2218a558", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html#a0e8b40b0d6482b5469c446c86a1420c4", null ],
    [ "UaHistoryEvent", "classUaHistoryEvent.html#ae378d288e85f47266d31a3cca092eab5", null ],
    [ "~UaHistoryEvent", "classUaHistoryEvent.html#a70baa402911bdccce43443a17e9856fa", null ],
    [ "attach", "classUaHistoryEvent.html#a11b7383bc6210f6160ea07064d038674", null ],
    [ "clear", "classUaHistoryEvent.html#a15eeba33576197bde570b47e8937557b", null ],
    [ "copy", "classUaHistoryEvent.html#aa4ba8213327bc16b7d3f001d6060f634", null ],
    [ "copyTo", "classUaHistoryEvent.html#ad51442cd2a44635cd046b02f8ecbaa7b", null ],
    [ "detach", "classUaHistoryEvent.html#aef2d12b71905e1d5dfd9648e6b457d6b", null ],
    [ "getEvents", "classUaHistoryEvent.html#a70382dd5cc08c61ca5c0d673f0ece016", null ],
    [ "operator!=", "classUaHistoryEvent.html#a04f5abd12689d6c79cef62f0ae18f9b7", null ],
    [ "operator=", "classUaHistoryEvent.html#ad142cb79c54a106283c1a831f8c58e5f", null ],
    [ "operator==", "classUaHistoryEvent.html#a4a1b0b3af7501893af4c99b0fa805372", null ],
    [ "setEvents", "classUaHistoryEvent.html#adb0b7cca1bd1b808aea4056e2f798ba8", null ]
];