var searchData=
[
  ['high',['High',['../group__UaStackTypes.html#ga2294051bb62f7695b44300ecc1443c9a',1,'OpcUa_Range']]],
  ['historydata',['HistoryData',['../group__UaStackTypes.html#ga0c2b9accd54f041f12f5467c3a5538ac',1,'OpcUa_HistoryReadResult']]],
  ['historyreadcount',['HistoryReadCount',['../group__UaStackTypes.html#gafad28149e4dc17409b525343e00c326a',1,'OpcUa_SessionDiagnosticsDataType']]],
  ['historyreaddetails',['HistoryReadDetails',['../group__UaStackTypes.html#gaeed21ab060d62ed636264fb1271b6873',1,'OpcUa_HistoryReadRequest']]],
  ['historyupdatecount',['HistoryUpdateCount',['../group__UaStackTypes.html#gaf36f1d76873a744738f3f2dfc6331266',1,'OpcUa_SessionDiagnosticsDataType']]],
  ['historyupdatedetails',['HistoryUpdateDetails',['../group__UaStackTypes.html#ga57cc89a182e90a6c4dd299b44a24666e',1,'OpcUa_HistoryUpdateRequest']]]
];
