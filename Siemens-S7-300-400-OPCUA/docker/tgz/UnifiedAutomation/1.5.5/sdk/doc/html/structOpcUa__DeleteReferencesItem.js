var structOpcUa__DeleteReferencesItem =
[
    [ "DeleteBidirectional", "group__UaStackTypes.html#ga4d7a0e0cdb3f27256e4fc8ea6ca4de09", null ],
    [ "IsForward", "group__UaStackTypes.html#gac9fad84bd331f88effdb98d67938e242", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gaf636f42e53a49deffc2469efc982c247", null ],
    [ "SourceNodeId", "group__UaStackTypes.html#ga1fa1ba7ed6f13e0091bca59ba99f4493", null ],
    [ "TargetNodeId", "group__UaStackTypes.html#gafeb95c9ef97fe96ff2fdadce1aad6989", null ]
];