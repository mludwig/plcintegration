var structOpcUa__MonitoredItemCreateRequest =
[
    [ "ItemToMonitor", "group__UaStackTypes.html#gad2167223e6e900bfb8e333ef0d69845b", null ],
    [ "MonitoringMode", "group__UaStackTypes.html#ga8784168fb2600e99bfc5808894d90677", null ],
    [ "RequestedParameters", "group__UaStackTypes.html#gab8d65b99b64c5e4120b057eb3f55df82", null ]
];