var classOpcUa_1_1SemanticChangeEventTypeData =
[
    [ "SemanticChangeEventTypeData", "classOpcUa_1_1SemanticChangeEventTypeData.html#aad565d504fa519b3d8d593f1c52f9aba", null ],
    [ "~SemanticChangeEventTypeData", "classOpcUa_1_1SemanticChangeEventTypeData.html#a28f13ad61e6c90b5bbd39fe214ba0ce0", null ],
    [ "getChanges", "classOpcUa_1_1SemanticChangeEventTypeData.html#af4fd76e697dca9263068aec5d50ce25c", null ],
    [ "getChangesValue", "classOpcUa_1_1SemanticChangeEventTypeData.html#afdc1cbd018f487907ee0567d528ca4af", null ],
    [ "getFieldData", "classOpcUa_1_1SemanticChangeEventTypeData.html#a5e5254f2c8b12b94db27870f03e386b3", null ],
    [ "setChanges", "classOpcUa_1_1SemanticChangeEventTypeData.html#ad380a73e898b0b51e3192bf83c416b74", null ],
    [ "setChangesStatus", "classOpcUa_1_1SemanticChangeEventTypeData.html#ae4920016a1fa12476cb57297af29b3ac", null ]
];