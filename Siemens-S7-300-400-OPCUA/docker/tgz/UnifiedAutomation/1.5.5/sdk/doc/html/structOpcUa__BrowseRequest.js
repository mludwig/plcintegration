var structOpcUa__BrowseRequest =
[
    [ "NodesToBrowse", "group__UaStackTypes.html#ga516c52ede838d3fe30c127a0ccd60daf", null ],
    [ "RequestedMaxReferencesPerNode", "group__UaStackTypes.html#gadc8bb0155cb18db5792211263c0b2db4", null ],
    [ "View", "group__UaStackTypes.html#ga5aef5d9f3fdcfeacf30077f5a57c4d7d", null ]
];