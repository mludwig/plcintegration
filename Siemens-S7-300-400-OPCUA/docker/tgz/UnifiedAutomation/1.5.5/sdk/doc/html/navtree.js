var NAVTREE =
[
  [ "C++ Based OPC UA Client/Server SDK", "index.html", [
    [ "Introduction", "index.html#intro_sec", [
      [ "Binary SDK", "index.html#binary_sec", null ],
      [ "Source SDK", "index.html#source_sec", null ],
      [ "Portability", "index.html#portability_sec", null ]
    ] ],
    [ "Table of Contents", "index.html#toc", null ],
    [ "OPC Introduction", "L1OpcIntroduction.html", "L1OpcIntroduction" ],
    [ "Building the SDK and Third-Party Libraries", "L1BuildInstructions.html", "L1BuildInstructions" ],
    [ "OPC UA Fundamentals", "L1OpcUaFundamentals.html", "L1OpcUaFundamentals" ],
    [ "OPC UA Information Models", "Doc_OpcUa_OpcUaInformationModels.html", "Doc_OpcUa_OpcUaInformationModels" ],
    [ "Server SDK Introduction", "L1ServerSdkIntroduction.html", "L1ServerSdkIntroduction" ],
    [ "Client SDK Introduction", "L1ClientSdkIntroduction.html", "L1ClientSdkIntroduction" ],
    [ "Tutorials for Server development", "L1ServerTutorials.html", "L1ServerTutorials" ],
    [ "Tutorials for Client development", "L1ClientTutorials.html", "L1ClientTutorials" ],
    [ "Troubleshooting", "L1Trouble.html", [
      [ "Using Wireshark to Analyze OPC UA Binary Protocol", "L1Trouble.html#L2TroubleWireshark", [
        [ "How to Use Wireshark", "L1Trouble.html#L3TroubleWiresharkUsage", null ],
        [ "Example", "L1Trouble.html#L3TroubleWiresharkExample", null ],
        [ "Wireshark Issue: Lots of Checksum Errors due to TCP Checksum Offloading", "L1Trouble.html#L3TcpChecksumOffloading", null ]
      ] ],
      [ "Using the Trace Functionality", "L1Trouble.html#L2TroubleTrace", [
        [ "Server Trace", "L1Trouble.html#L3TroubleTraceServer", null ],
        [ "Client Trace", "L1Trouble.html#L3TroubleTraceClient", null ]
      ] ]
    ] ],
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"Doc_OpcUa_BaseModel_EventTypes.html#Doc_OpcUa_AuditUpdateMethodEventType_MethodId",
"Doc_OpcUa_DataAccess_VariableTypes.html#Doc_OpcUa_ArrayItemType_EURange",
"Doc_OpcUa_Services_DataTypes.html#Doc_OpcUa_ParsingResult",
"L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step03",
"classBaseEventTypeData.html#a7ee51e648c3f5af4e9e802374193d85b",
"classEventMonitoringContext.html#af008787cb3183f606eb7c98e4c47316b",
"classMethodManagerCallback.html#acc87de8e3fac92f2f0d9acab4a1a83b3",
"classNodeManagerUaNode.html#aa6a94e99bbb0bcf737f298aeac06e1ae",
"classOpcUaDi_1_1NetworkTypeBase.html",
"classOpcUaPlc_1_1CtrlResourceType.html#a98ebf7da7760a96090780e3402dc4eb1",
"classOpcUa_1_1AlarmConditionTypeData.html#a47e90f7d83802f30cd6d1ddef4b0b01e",
"classOpcUa_1_1AuditEventTypeData.html#a212c50b5a1b883584d48cb5b6e325107",
"classOpcUa_1_1BaseMethod.html#a7596e8e7548a57df85bddfda126c3ef3",
"classOpcUa_1_1ConditionTypeBase.html#a705781ef73b59c62a9502a2626dde0a3",
"classOpcUa_1_1DialogConditionTypeBase.html#aca1683351a2db065bf33a2cd03374ef2",
"classOpcUa_1_1FileType.html#a72faf7495fc709072ac3b862cd8ab96f",
"classOpcUa_1_1LimitAlarmTypeData.html#aaf5e502aa5ce391b5b5394ef1cf75941",
"classOpcUa_1_1OffNormalAlarmType.html",
"classOpcUa_1_1ShelvedStateMachineTypeBase.html#aa3087d53c3166c58fe7f9017133d0b35",
"classOpcUa_1_1XYArrayItemType.html#a0dd08e61ec4238efb8bebe886324bd35",
"classServerConfigXml.html",
"classUaAbstractDecoder.html#ac71d337a5b24589d5f2dafc1551d02ed",
"classUaAddNodesResults.html#a33e4ddc1ed568cd0197934ecf3ec2c7d",
"classUaAggregateConfiguration.html#aebbc646d2592e35b62560d6200ea5926",
"classUaApplicationDescription.html#ac81dad80bbc55249b3a360cf5e632c13",
"classUaAxisInformation.html#aca3107afac215abd608723e879888b7b",
"classUaBase_1_1UaNodesetXmlParser.html#a914383c67601c529c9e6d1e34cd2c444",
"classUaBooleanArray.html#ab99007d9ea56f3567bc4e11ec32c9e3e",
"classUaBrowseResults.html#aca9f7dabc546b5423cace3155713b77b",
"classUaByteString.html#a81f2b081876bb94fc046506ea35cd295",
"classUaClientSdk_1_1ClientSecurityInfo.html#a9f86cdda489732d773201d2d1fdec0b5",
"classUaClientSdk_1_1UaSession.html#ab201e99cb4473484a2dc6e3d305d5ff5",
"classUaContentFilterElement.html",
"classUaDataValue.html#a3dc6c128d80c0ab7e6e41bcd7bfc634a",
"classUaDateTime.html#aef9ad113b850ddb7a977cc0290f09e6e",
"classUaDeleteReferencesItem.html#ae383ef708c115436ff0f01cb116e8464",
"classUaDoubleComplexNumberTypes.html#accb049750a76b7416e9a49d6c8bc3365",
"classUaEndpointConfiguration.html#a8cddc70e766b7a61ed0a500808616b5a",
"classUaEndpointUrlListDataType.html",
"classUaEventData.html#ac7bc5bbe477139527be789bfcf076a9a",
"classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270a7b6b2feb20a26894ad7d9641a06dabab",
"classUaGenericMethod.html#ab76a11961f30d966c5813de61817f861",
"classUaGenericView.html#a89a354d6135ed68169621586622bf753",
"classUaHistoryEventFieldLists.html#a8b124f7bfee1d6de8bbd23d0a77d2d00",
"classUaInt16Array.html#a62e4fb333daad9d25c6742734d6e8cd5",
"classUaLocalizedTextArray.html#ae46679b30debbd0617d5c368121bade1",
"classUaMonitoredItem.html",
"classUaMonitoredItemModifyResults.html#af6d32146c0e72039f5b5e6ec5f8efa99",
"classUaNodeId.html#a1b8b6343733bf71086e35b8dda88782a",
"classUaObjectAttributes.html#a0c3d05d828c23a99cfd932efe08601ec",
"classUaOptionSets.html#a99535f5e1ab16fdc06ab58760031d83b",
"classUaPkiRevocationList.html#a5040f83906ba1756db5cab2c6b67e6c4",
"classUaPropertyMethodArgument.html#a770d1bc1703bf136834b66c853c2874c",
"classUaRange.html#ae0ace5dc348f1e181889e09f3692de0e",
"classUaRefGeneratesEvent.html#a8b98ec8e73b78dbfc7e1919fb240ac42",
"classUaRegisteredServer.html",
"classUaSByteArray.html#af80a66aacc03d7b2ca40a62ec69aabb7",
"classUaSemaphore.html#af733c3fef6fb9d7fbf40d98eb8ea4cbd",
"classUaServerOnNetwork.html",
"classUaServerStatusDataTypes.html#adde01d778eba1a4bf84208086ac5e40a",
"classUaSessionDiagnosticsDataType.html#a63964a63a93f38327a68345c84102b75",
"classUaSessionSecurityDiagnosticsDataType.html#a0672fb022db55191571885080a86f928",
"classUaSettingsSection.html#af0bf35edd1d6327110022bf8d2db370a",
"classUaSoftwareCertificate.html#ab61f137953b601bfa3cc087a9a869741",
"classUaStatusCodeArray.html#ac69ab2f268564a583578f9f55871346f",
"classUaSubscription.html#ac7ba525130ca7525a951e49acb5719f1",
"classUaSubscriptionDiagnosticsDataTypes.html#a0e1cec9230e2c0e2e0a1d5b7e582f546",
"classUaThreadPool.html#a5bb9735f8fbe80dd21429c3a1a0c2755",
"classUaTrustListDataTypes.html#a0fb848e9c968bf3b5cec02e680d77423",
"classUaUniStringList.html#ae9b59ef565d5b5e17624172f9c0d3a09",
"classUaVariableTypeAttributes.html#a2ca8ee7841cc626f7a91dbe158c2a5f8",
"classUaVariant.html#a4c8344ab561156d37fbe72b004fb2b50",
"classUaVariant.html#aa956b4451abb86898d4d41f89b433e2c",
"classUaViewAttributes.html#a341f17c52d2137025c7e9dc95411ef13",
"classUaXmlDocument.html#a9e74e15b2fa20baa97dd3556cdba987c",
"group__CppBaseLibraryClass.html#ga747ab0930f312dc019205c64a8a6cfbf",
"group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad32ea6d3a0a352575724a8973cce3b1c",
"group__UaStackTypes.html#ga2bdcc24bc9978bfddd67a5118f536a93",
"group__UaStackTypes.html#ga659c3f296949ddb1d186e291c2a57728",
"group__UaStackTypes.html#ga9a8a9d120f0749607d415cc68b8e2b60",
"group__UaStackTypes.html#gad1541d8ebb3bf7b7837a3862c6266f39",
"group__UaStackTypes.html#ggac6e323174519f2b3cb45bcfdfe83493eaa14d9a25c0ffe88da69bbd4aae3be971",
"structOpcUa__UnregisterNodesRequest.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';
var navTreeSubIndices = new Array();

function getData(varName)
{
  var i = varName.lastIndexOf('/');
  var n = i>=0 ? varName.substring(i+1) : varName;
  return eval(n.replace(/\-/g,'_'));
}

function stripPath(uri)
{
  return uri.substring(uri.lastIndexOf('/')+1);
}

function stripPath2(uri)
{
  var i = uri.lastIndexOf('/');
  var s = uri.substring(i+1);
  var m = uri.substring(0,i+1).match(/\/d\w\/d\w\w\/$/);
  return m ? uri.substring(i-6) : s;
}

function hashValue()
{
  return $(location).attr('hash').substring(1).replace(/[^\w\-]/g,'');
}

function hashUrl()
{
  return '#'+hashValue();
}

function pathName()
{
  return $(location).attr('pathname').replace(/[^-A-Za-z0-9+&@#/%?=~_|!:,.;\(\)]/g, '');
}

function localStorageSupported()
{
  try {
    return 'localStorage' in window && window['localStorage'] !== null && window.localStorage.getItem;
  }
  catch(e) {
    return false;
  }
}


function storeLink(link)
{
  if (!$("#nav-sync").hasClass('sync') && localStorageSupported()) {
      window.localStorage.setItem('navpath',link);
  }
}

function deleteLink()
{
  if (localStorageSupported()) {
    window.localStorage.setItem('navpath','');
  }
}

function cachedLink()
{
  if (localStorageSupported()) {
    return window.localStorage.getItem('navpath');
  } else {
    return '';
  }
}

function getScript(scriptName,func,show)
{
  var head = document.getElementsByTagName("head")[0]; 
  var script = document.createElement('script');
  script.id = scriptName;
  script.type = 'text/javascript';
  script.onload = func; 
  script.src = scriptName+'.js'; 
  if ($.browser.msie && $.browser.version<=8) { 
    // script.onload does not work with older versions of IE
    script.onreadystatechange = function() {
      if (script.readyState=='complete' || script.readyState=='loaded') { 
        func(); if (show) showRoot(); 
      }
    }
  }
  head.appendChild(script); 
}

function createIndent(o,domNode,node,level)
{
  var level=-1;
  var n = node;
  while (n.parentNode) { level++; n=n.parentNode; }
  if (node.childrenData) {
    var imgNode = document.createElement("img");
    imgNode.style.paddingLeft=(16*level).toString()+'px';
    imgNode.width  = 16;
    imgNode.height = 22;
    imgNode.border = 0;
    node.plus_img = imgNode;
    node.expandToggle = document.createElement("a");
    node.expandToggle.href = "javascript:void(0)";
    node.expandToggle.onclick = function() {
      if (node.expanded) {
        $(node.getChildrenUL()).slideUp("fast");
        node.plus_img.src = node.relpath+"ftv2pnode.png";
        node.expanded = false;
      } else {
        expandNode(o, node, false, false);
      }
    }
    node.expandToggle.appendChild(imgNode);
    domNode.appendChild(node.expandToggle);
    imgNode.src = node.relpath+"ftv2pnode.png";
  } else {
    var span = document.createElement("span");
    span.style.display = 'inline-block';
    span.style.width   = 16*(level+1)+'px';
    span.style.height  = '22px';
    span.innerHTML = '&#160;';
    domNode.appendChild(span);
  } 
}

var animationInProgress = false;

function gotoAnchor(anchor,aname,updateLocation)
{
  var pos, docContent = $('#doc-content');
  var ancParent = $(anchor.parent());
  if (ancParent.hasClass('memItemLeft') ||
      ancParent.hasClass('fieldname') ||
      ancParent.hasClass('fieldtype') ||
      ancParent.is(':header'))
  {
    pos = ancParent.position().top;
  } else if (anchor.position()) {
    pos = anchor.position().top;
  }
  if (pos) {
    var dist = Math.abs(Math.min(
               pos-docContent.offset().top,
               docContent[0].scrollHeight-
               docContent.height()-docContent.scrollTop()));
    animationInProgress=true;
    docContent.animate({
      scrollTop: pos + docContent.scrollTop() - docContent.offset().top
    },Math.max(50,Math.min(500,dist)),function(){
      if (updateLocation) window.location.href=aname;
      animationInProgress=false;
    });
  }
}

function newNode(o, po, text, link, childrenData, lastNode)
{
  var node = new Object();
  node.children = Array();
  node.childrenData = childrenData;
  node.depth = po.depth + 1;
  node.relpath = po.relpath;
  node.isLast = lastNode;

  node.li = document.createElement("li");
  po.getChildrenUL().appendChild(node.li);
  node.parentNode = po;

  node.itemDiv = document.createElement("div");
  node.itemDiv.className = "item";

  node.labelSpan = document.createElement("span");
  node.labelSpan.className = "label";

  createIndent(o,node.itemDiv,node,0);
  node.itemDiv.appendChild(node.labelSpan);
  node.li.appendChild(node.itemDiv);

  var a = document.createElement("a");
  node.labelSpan.appendChild(a);
  node.label = document.createTextNode(text);
  node.expanded = false;
  a.appendChild(node.label);
  if (link) {
    var url;
    if (link.substring(0,1)=='^') {
      url = link.substring(1);
      link = url;
    } else {
      url = node.relpath+link;
    }
    a.className = stripPath(link.replace('#',':'));
    if (link.indexOf('#')!=-1) {
      var aname = '#'+link.split('#')[1];
      var srcPage = stripPath(pathName());
      var targetPage = stripPath(link.split('#')[0]);
      a.href = srcPage!=targetPage ? url : "javascript:void(0)"; 
      a.onclick = function(){
        storeLink(link);
        if (!$(a).parent().parent().hasClass('selected'))
        {
          $('.item').removeClass('selected');
          $('.item').removeAttr('id');
          $(a).parent().parent().addClass('selected');
          $(a).parent().parent().attr('id','selected');
        }
        var anchor = $(aname);
        gotoAnchor(anchor,aname,true);
      };
    } else {
      a.href = url;
      a.onclick = function() { storeLink(link); }
    }
  } else {
    if (childrenData != null) 
    {
      a.className = "nolink";
      a.href = "javascript:void(0)";
      a.onclick = node.expandToggle.onclick;
    }
  }

  node.childrenUL = null;
  node.getChildrenUL = function() {
    if (!node.childrenUL) {
      node.childrenUL = document.createElement("ul");
      node.childrenUL.className = "children_ul";
      node.childrenUL.style.display = "none";
      node.li.appendChild(node.childrenUL);
    }
    return node.childrenUL;
  };

  return node;
}

function showRoot()
{
  var headerHeight = $("#top").height();
  var footerHeight = $("#nav-path").height();
  var windowHeight = $(window).height() - headerHeight - footerHeight;
  (function (){ // retry until we can scroll to the selected item
    try {
      var navtree=$('#nav-tree');
      navtree.scrollTo('#selected',0,{offset:-windowHeight/2});
    } catch (err) {
      setTimeout(arguments.callee, 0);
    }
  })();
}

function expandNode(o, node, imm, showRoot)
{
  if (node.childrenData && !node.expanded) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        expandNode(o, node, imm, showRoot);
      }, showRoot);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      } if (imm || ($.browser.msie && $.browser.version>8)) { 
        // somehow slideDown jumps to the start of tree for IE9 :-(
        $(node.getChildrenUL()).show();
      } else {
        $(node.getChildrenUL()).slideDown("fast");
      }
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
    }
  }
}

function glowEffect(n,duration)
{
  n.addClass('glow').delay(duration).queue(function(next){
    $(this).removeClass('glow');next();
  });
}

function highlightAnchor()
{
  var aname = hashUrl();
  var anchor = $(aname);
  if (anchor.parent().attr('class')=='memItemLeft'){
    var rows = $('.memberdecls tr[class$="'+hashValue()+'"]');
    glowEffect(rows.children(),300); // member without details
  } else if (anchor.parent().attr('class')=='fieldname'){
    glowEffect(anchor.parent().parent(),1000); // enum value
  } else if (anchor.parent().attr('class')=='fieldtype'){
    glowEffect(anchor.parent().parent(),1000); // struct field
  } else if (anchor.parent().is(":header")) {
    glowEffect(anchor.parent(),1000); // section header
  } else {
    glowEffect(anchor.next(),1000); // normal member
  }
  gotoAnchor(anchor,aname,false);
}

function selectAndHighlight(hash,n)
{
  var a;
  if (hash) {
    var link=stripPath(pathName())+':'+hash.substring(1);
    a=$('.item a[class$="'+link+'"]');
  }
  if (a && a.length) {
    a.parent().parent().addClass('selected');
    a.parent().parent().attr('id','selected');
    highlightAnchor();
  } else if (n) {
    $(n.itemDiv).addClass('selected');
    $(n.itemDiv).attr('id','selected');
  }
  if ($('#nav-tree-contents .item:first').hasClass('selected')) {
    $('#nav-sync').css('top','30px');
  } else {
    $('#nav-sync').css('top','5px');
  }
  showRoot();
}

function showNode(o, node, index, hash)
{
  if (node && node.childrenData) {
    if (typeof(node.childrenData)==='string') {
      var varName    = node.childrenData;
      getScript(node.relpath+varName,function(){
        node.childrenData = getData(varName);
        showNode(o,node,index,hash);
      },true);
    } else {
      if (!node.childrenVisited) {
        getNode(o, node);
      }
      $(node.getChildrenUL()).css({'display':'block'});
      if (node.isLast) {
        node.plus_img.src = node.relpath+"ftv2mlastnode.png";
      } else {
        node.plus_img.src = node.relpath+"ftv2mnode.png";
      }
      node.expanded = true;
      var n = node.children[o.breadcrumbs[index]];
      if (index+1<o.breadcrumbs.length) {
        showNode(o,n,index+1,hash);
      } else {
        if (typeof(n.childrenData)==='string') {
          var varName = n.childrenData;
          getScript(n.relpath+varName,function(){
            n.childrenData = getData(varName);
            node.expanded=false;
            showNode(o,node,index,hash); // retry with child node expanded
          },true);
        } else {
          var rootBase = stripPath(o.toroot.replace(/\..+$/, ''));
          if (rootBase=="index" || rootBase=="pages" || rootBase=="search") {
            expandNode(o, n, true, true);
          }
          selectAndHighlight(hash,n);
        }
      }
    }
  } else {
    selectAndHighlight(hash);
  }
}

function removeToInsertLater(element) {
  var parentNode = element.parentNode;
  var nextSibling = element.nextSibling;
  parentNode.removeChild(element);
  return function() {
    if (nextSibling) {
      parentNode.insertBefore(element, nextSibling);
    } else {
      parentNode.appendChild(element);
    }
  };
}

function getNode(o, po)
{
  var insertFunction = removeToInsertLater(po.li);
  po.childrenVisited = true;
  var l = po.childrenData.length-1;
  for (var i in po.childrenData) {
    var nodeData = po.childrenData[i];
    po.children[i] = newNode(o, po, nodeData[0], nodeData[1], nodeData[2],
      i==l);
  }
  insertFunction();
}

function gotoNode(o,subIndex,root,hash,relpath)
{
  var nti = navTreeSubIndices[subIndex][root+hash];
  o.breadcrumbs = $.extend(true, [], nti ? nti : navTreeSubIndices[subIndex][root]);
  if (!o.breadcrumbs && root!=NAVTREE[0][1]) { // fallback: show index
    navTo(o,NAVTREE[0][1],"",relpath);
    $('.item').removeClass('selected');
    $('.item').removeAttr('id');
  }
  if (o.breadcrumbs) {
    o.breadcrumbs.unshift(0); // add 0 for root node
    showNode(o, o.node, 0, hash);
  }
}

function navTo(o,root,hash,relpath)
{
  var link = cachedLink();
  if (link) {
    var parts = link.split('#');
    root = parts[0];
    if (parts.length>1) hash = '#'+parts[1].replace(/[^\w\-]/g,'');
    else hash='';
  }
  if (hash.match(/^#l\d+$/)) {
    var anchor=$('a[name='+hash.substring(1)+']');
    glowEffect(anchor.parent(),1000); // line number
    hash=''; // strip line number anchors
  }
  var url=root+hash;
  var i=-1;
  while (NAVTREEINDEX[i+1]<=url) i++;
  if (i==-1) { i=0; root=NAVTREE[0][1]; } // fallback: show index
  if (navTreeSubIndices[i]) {
    gotoNode(o,i,root,hash,relpath)
  } else {
    getScript(relpath+'navtreeindex'+i,function(){
      navTreeSubIndices[i] = eval('NAVTREEINDEX'+i);
      if (navTreeSubIndices[i]) {
        gotoNode(o,i,root,hash,relpath);
      }
    },true);
  }
}

function showSyncOff(n,relpath)
{
    n.html('<img src="'+relpath+'sync_off.png" title="'+SYNCOFFMSG+'"/>');
}

function showSyncOn(n,relpath)
{
    n.html('<img src="'+relpath+'sync_on.png" title="'+SYNCONMSG+'"/>');
}

function toggleSyncButton(relpath)
{
  var navSync = $('#nav-sync');
  if (navSync.hasClass('sync')) {
    navSync.removeClass('sync');
    showSyncOff(navSync,relpath);
    storeLink(stripPath2(pathName())+hashUrl());
  } else {
    navSync.addClass('sync');
    showSyncOn(navSync,relpath);
    deleteLink();
  }
}

function initNavTree(toroot,relpath)
{
  var o = new Object();
  o.toroot = toroot;
  o.node = new Object();
  o.node.li = document.getElementById("nav-tree-contents");
  o.node.childrenData = NAVTREE;
  o.node.children = new Array();
  o.node.childrenUL = document.createElement("ul");
  o.node.getChildrenUL = function() { return o.node.childrenUL; };
  o.node.li.appendChild(o.node.childrenUL);
  o.node.depth = 0;
  o.node.relpath = relpath;
  o.node.expanded = false;
  o.node.isLast = true;
  o.node.plus_img = document.createElement("img");
  o.node.plus_img.src = relpath+"ftv2pnode.png";
  o.node.plus_img.width = 16;
  o.node.plus_img.height = 22;

  if (localStorageSupported()) {
    var navSync = $('#nav-sync');
    if (cachedLink()) {
      showSyncOff(navSync,relpath);
      navSync.removeClass('sync');
    } else {
      showSyncOn(navSync,relpath);
    }
    navSync.click(function(){ toggleSyncButton(relpath); });
  }

  $(window).load(function(){
    navTo(o,toroot,hashUrl(),relpath);
    showRoot();
  });

  $(window).bind('hashchange', function(){
     if (window.location.hash && window.location.hash.length>1){
       var a;
       if ($(location).attr('hash')){
         var clslink=stripPath(pathName())+':'+hashValue();
         a=$('.item a[class$="'+clslink.replace(/</g,'\\3c ')+'"]');
       }
       if (a==null || !$(a).parent().parent().hasClass('selected')){
         $('.item').removeClass('selected');
         $('.item').removeAttr('id');
       }
       var link=stripPath2(pathName());
       navTo(o,link,hashUrl(),relpath);
     } else if (!animationInProgress) {
       $('#doc-content').scrollTop(0);
       $('.item').removeClass('selected');
       $('.item').removeAttr('id');
       navTo(o,toroot,hashUrl(),relpath);
     }
  })
}

