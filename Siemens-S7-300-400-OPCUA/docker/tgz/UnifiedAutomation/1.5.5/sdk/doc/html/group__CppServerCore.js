var group__CppServerCore =
[
    [ "Core Module Interfaces", "group__ServerCoreInterfaces.html", "group__ServerCoreInterfaces" ],
    [ "Core Module Address Model Classes", "group__ServerCoreAddressModel.html", "group__ServerCoreAddressModel" ],
    [ "Core Module Interface Base Implementations", "group__ServerCoreBaseClasses.html", "group__ServerCoreBaseClasses" ],
    [ "Core Module context and helper classes", "group__ServerCoreHelper.html", "group__ServerCoreHelper" ],
    [ "CoreModule", "classCoreModule.html", [
      [ "CoreModule", "classCoreModule.html#a180dfb4bb44767ca69df6a6db2d529d9", null ],
      [ "~CoreModule", "classCoreModule.html#af2decd09862ce4f10c011b2cf2542b0f", null ],
      [ "getServerManager", "classCoreModule.html#af0b67306012685386b115b945599ef6e", null ],
      [ "initialize", "classCoreModule.html#a09c4fc98b4122716e8c53df8ad6412c7", null ],
      [ "shutDown", "classCoreModule.html#ad447b5a73b8071378c77a63bd93ba42a", null ],
      [ "startUp", "classCoreModule.html#a63a327f34b93fd4854a396253013bc07", null ]
    ] ]
];