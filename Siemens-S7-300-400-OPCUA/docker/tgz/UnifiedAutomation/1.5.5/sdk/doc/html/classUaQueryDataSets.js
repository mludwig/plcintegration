var classUaQueryDataSets =
[
    [ "UaQueryDataSets", "classUaQueryDataSets.html#a1dc62fa3a097300d6b250ca39563f347", null ],
    [ "UaQueryDataSets", "classUaQueryDataSets.html#a03d61a3c3a7b2d6c6a9368f014b4ac80", null ],
    [ "UaQueryDataSets", "classUaQueryDataSets.html#a318c88684d9aa2fbf244ea36340852ad", null ],
    [ "~UaQueryDataSets", "classUaQueryDataSets.html#a2ea11d778b4f5e3912ec45b714581617", null ],
    [ "attach", "classUaQueryDataSets.html#a077741a3a1a102d7bd74d520add47177", null ],
    [ "attach", "classUaQueryDataSets.html#a5eafc89b21af3b2269f30819313b7eb6", null ],
    [ "clear", "classUaQueryDataSets.html#a0a30b5b40760fd3d003f77280bf98b3e", null ],
    [ "create", "classUaQueryDataSets.html#a72713ae9159631925414c1e1f8011b5e", null ],
    [ "detach", "classUaQueryDataSets.html#a8254d82d8385b61ddef02b24972a52ab", null ],
    [ "operator!=", "classUaQueryDataSets.html#a31a7f1790ea8f24ffee2b80d9c9d99fa", null ],
    [ "operator=", "classUaQueryDataSets.html#a5787244a6a7b488d5ea5d3ed4931ee85", null ],
    [ "operator==", "classUaQueryDataSets.html#a8402069761e3d9677ba10205e3bcc647", null ],
    [ "operator[]", "classUaQueryDataSets.html#a46b76e724601fbfee59aaca6754d030e", null ],
    [ "operator[]", "classUaQueryDataSets.html#a2e07793fb8e1c17ddc2d780a3e5617d9", null ],
    [ "resize", "classUaQueryDataSets.html#af91c6438f6427842c37050d210cd7a68", null ]
];