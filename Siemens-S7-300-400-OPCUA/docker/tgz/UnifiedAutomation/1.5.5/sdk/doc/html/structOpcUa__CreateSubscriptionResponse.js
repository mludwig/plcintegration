var structOpcUa__CreateSubscriptionResponse =
[
    [ "RevisedLifetimeCount", "group__UaStackTypes.html#gad362baad3fd21d6c1989bd31bf871ef8", null ],
    [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#ga7f231a6c60c92339568f8f68a3a80ea5", null ],
    [ "RevisedPublishingInterval", "group__UaStackTypes.html#gad46b9515b50081cf1afdaf4d94343770", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga279174ece7725e168e192675a1abe794", null ]
];