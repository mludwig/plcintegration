var classBrowseContext =
[
    [ "BrowseContext", "classBrowseContext.html#afd79595f2fe35015e71830b1165b7a60", null ],
    [ "~BrowseContext", "classBrowseContext.html#af914d4ad07a036ec2e8e0554d4c9ac64", null ],
    [ "bIncludeSubtype", "classBrowseContext.html#a77ee84b3271a44db8156849ad6170a2c", null ],
    [ "BrowseDirection", "classBrowseContext.html#aef4e1931eda2f7644d2242276edd4b34", null ],
    [ "detachUserData", "classBrowseContext.html#a72db31e015086c62d6c979525a714f26", null ],
    [ "isFirstBrowse", "classBrowseContext.html#ae3d3d2d07dc67ce422cdafe4cf5c73c0", null ],
    [ "isLastBrowse", "classBrowseContext.html#a3e5f6cef71ca4824d14d5c9689d1af5a", null ],
    [ "nodeToBrowseCount", "classBrowseContext.html#a1cfc007e1aa5248ab79f94655c94b33b", null ],
    [ "nodeToBrowseIndex", "classBrowseContext.html#a1a6834287c384d4e37229decf621a70d", null ],
    [ "pNodeToBrowse", "classBrowseContext.html#a2a270b9d99c0ea40e503b27409ba34ad", null ],
    [ "pReferenceTypeId", "classBrowseContext.html#acc9b2eedc68dbbfad62e75320bafa623", null ],
    [ "pView", "classBrowseContext.html#a50bf930fcd0f9aa8c12a3b9e52f63f0b", null ],
    [ "setMaxResultsToReturn", "classBrowseContext.html#ac3797fab9ff3074df6f9a644d89fd26e", null ],
    [ "setResultMask", "classBrowseContext.html#ace7246511f23c818010c81b80cb2271c", null ],
    [ "setUserData", "classBrowseContext.html#a4e4e1f36708d6f23b4c77b5c96c97a61", null ],
    [ "uMaxResultsToReturn", "classBrowseContext.html#aa08da2cb8cdcc8b4884aded12557fd47", null ],
    [ "uNodeClassMask", "classBrowseContext.html#a12c4215e60b3a303b6ce86cdc1c288d6", null ],
    [ "uResultMask", "classBrowseContext.html#a838398e2e30f3dd76980c34d42442d28", null ]
];