var classUaServiceCounterDataType =
[
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#a598216806479c0f6928164dcb8af560b", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#ae0f9f852ba0d87b155562926499e3096", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#a428a60f4d80bea9968e8d907314ee897", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#a6c570191fa1aa3c525894102aaef0eb4", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#ad38752e96b1901e42be29964ab7fb3ef", null ],
    [ "UaServiceCounterDataType", "classUaServiceCounterDataType.html#a2280fc553ebf74cb82c2c7a958d6ac1d", null ],
    [ "~UaServiceCounterDataType", "classUaServiceCounterDataType.html#a9dff5eb545f064512da9b6cac054d60a", null ],
    [ "attach", "classUaServiceCounterDataType.html#ac47f9e07ceea7548db4a94b9da7a403c", null ],
    [ "clear", "classUaServiceCounterDataType.html#a0cfebeff066dc11563fc5911f3b486e4", null ],
    [ "copy", "classUaServiceCounterDataType.html#a77cde19a26e0d33204a34d9d7d70dd87", null ],
    [ "copyTo", "classUaServiceCounterDataType.html#a4b52dbfd925c4504eb154092686d5ec2", null ],
    [ "detach", "classUaServiceCounterDataType.html#a70b19f1a0f146651c1e1fcea163f567e", null ],
    [ "getErrorCount", "classUaServiceCounterDataType.html#ae2fa9e1d1b1b3dc25bd6704cd69e0722", null ],
    [ "getTotalCount", "classUaServiceCounterDataType.html#a944d0922f8a980f0dab25ff1334eec50", null ],
    [ "operator!=", "classUaServiceCounterDataType.html#af9ce6c1233b4433b7f9be75d5f3be15d", null ],
    [ "operator=", "classUaServiceCounterDataType.html#ac75399d1abac253a4e0c0c520d178e34", null ],
    [ "operator==", "classUaServiceCounterDataType.html#a17993fb433acfc3d7945fdea7be6b686", null ],
    [ "setErrorCount", "classUaServiceCounterDataType.html#afbbb764ee27ad397f7cfafaecc4d3487", null ],
    [ "setTotalCount", "classUaServiceCounterDataType.html#af3ea90f7a5c4f735a743e4efd7537245", null ]
];