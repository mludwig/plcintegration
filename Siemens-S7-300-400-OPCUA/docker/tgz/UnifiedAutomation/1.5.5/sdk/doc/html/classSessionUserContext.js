var classSessionUserContext =
[
    [ "AccessInfoMode", "classSessionUserContext.html#a8d2fceef91731a6013687d040a53e87b", [
      [ "OwnerGroupOther", "classSessionUserContext.html#a8d2fceef91731a6013687d040a53e87ba20b94c6fd094ce337c4dba14ded97b00", null ],
      [ "RoleRoleOther", "classSessionUserContext.html#a8d2fceef91731a6013687d040a53e87ba4377cd091a93c8a2f4c430812c08bb69", null ],
      [ "UserDefined", "classSessionUserContext.html#a8d2fceef91731a6013687d040a53e87bad46a21fb5ee1726a5921aa0f408ca0dd", null ]
    ] ],
    [ "SessionUserContext", "classSessionUserContext.html#aa9cf0b845190a5676fdf92ae34249c2c", null ],
    [ "SessionUserContext", "classSessionUserContext.html#a2aa47337bb06a1bed949761c36593dd3", null ],
    [ "addMembership", "classSessionUserContext.html#a707a0e453fd7516d090f9020f78ee08a", null ],
    [ "setAccessInfoMode", "classSessionUserContext.html#a0188f95aea70f42f0ee761a42ee1b223", null ],
    [ "setDefaultPermissions", "classSessionUserContext.html#ab72b1c90559b6136ce2642be8222c858", null ],
    [ "setGroups", "classSessionUserContext.html#a1650068e863ea3521140a8f48109117e", null ],
    [ "setRootId", "classSessionUserContext.html#aa1ed62b4409b489226735c81742062f5", null ],
    [ "setUserId", "classSessionUserContext.html#a7ef7a4b7b065bc885e172b74538c2853", null ]
];