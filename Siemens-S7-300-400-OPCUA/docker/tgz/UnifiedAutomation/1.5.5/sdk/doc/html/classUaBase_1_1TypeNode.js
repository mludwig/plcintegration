var classUaBase_1_1TypeNode =
[
    [ "TypeNode", "classUaBase_1_1TypeNode.html#a920bb61914a5ab6e25cb5a798a64c053", null ],
    [ "evaluateReferences", "classUaBase_1_1TypeNode.html#a926756fe2774724c6361a4ebc657650a", null ],
    [ "isAbstract", "classUaBase_1_1TypeNode.html#aa44e30e2d1c7fbda0ceb7e93b64ca646", null ],
    [ "setIsAbstract", "classUaBase_1_1TypeNode.html#a07b4edc7bb6ee4f51158c9c9c3df1b00", null ],
    [ "setSuperTypeId", "classUaBase_1_1TypeNode.html#ab1c9465ef0326c0b65d83681de22e6aa", null ],
    [ "superTypeId", "classUaBase_1_1TypeNode.html#abe46afefbd54d678558819ea9ba9f546", null ]
];