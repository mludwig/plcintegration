var classCertificateCreateSettings =
[
    [ "CertificateCreateSettings", "classCertificateCreateSettings.html#a12f21701bce86b8bdb9c7efd63513ed5", null ],
    [ "~CertificateCreateSettings", "classCertificateCreateSettings.html#a2d6a62c29f99d1c41cf9676d33e39f12", null ],
    [ "m_arrDNSNames", "classCertificateCreateSettings.html#aad5bbc6e8784e6b9fc1f0bf7c611fd0c", null ],
    [ "m_arrIPAddresses", "classCertificateCreateSettings.html#a2dd2cc651fae9a48d284a999ee9d47ca", null ],
    [ "m_certificateType", "classCertificateCreateSettings.html#a49a12ea60798b6a7edf3a8a10acf663f", null ],
    [ "m_KeyLength", "classCertificateCreateSettings.html#a7b150c81059913ac54dc09f02fe45ec1", null ],
    [ "m_sCommonName", "classCertificateCreateSettings.html#aa3b0354c19f4ff817f70501b425855ed", null ],
    [ "m_sCountry", "classCertificateCreateSettings.html#aae21ca5f2b94ff858cfdf8344fb1f47c", null ],
    [ "m_sDomainComponent", "classCertificateCreateSettings.html#a8859a070189395a088a885d25866dc21", null ],
    [ "m_sLocality", "classCertificateCreateSettings.html#a595e0ec988330068ee80f8f122a86cb1", null ],
    [ "m_sOrganization", "classCertificateCreateSettings.html#a9e4b419753205eac1406f335ce4c5225", null ],
    [ "m_sOrganizationUnit", "classCertificateCreateSettings.html#a76b69463b80ea9e93aaedab90e7e8dc3", null ],
    [ "m_sState", "classCertificateCreateSettings.html#a2c9b2bd8325dd345921f0aeeb2bdf72d", null ],
    [ "m_yearsValidFor", "classCertificateCreateSettings.html#aef088892ed229ab7eac1f4cd4412f819", null ]
];