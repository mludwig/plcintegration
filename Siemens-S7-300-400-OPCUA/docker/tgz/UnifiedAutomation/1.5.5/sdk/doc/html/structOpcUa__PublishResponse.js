var structOpcUa__PublishResponse =
[
    [ "AvailableSequenceNumbers", "group__UaStackTypes.html#ga877a6418970c86fb58be356ed62fc701", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gab362b84a071184d437c0f43b80886cfb", null ],
    [ "MoreNotifications", "group__UaStackTypes.html#ga8ed654675e29c17b67c7358fffe9279a", null ],
    [ "NotificationMessage", "group__UaStackTypes.html#ga1f58a55a2977f5863f9d606bcd6aa0f6", null ],
    [ "Results", "group__UaStackTypes.html#ga2a66e318305bd767f798380292f2cdc6", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga18e96eace034affd9242f8747be56e3d", null ]
];