var structOpcUa__BuildInfo =
[
    [ "BuildDate", "group__UaStackTypes.html#ga5b44440c3bdacc32eaa8a64fc2d0e499", null ],
    [ "BuildNumber", "group__UaStackTypes.html#ga2a60de64f498cfefb99c303cd19112b7", null ],
    [ "ManufacturerName", "group__UaStackTypes.html#gaa0d220eaebe290b74dfcd03bbfde74b7", null ],
    [ "ProductName", "group__UaStackTypes.html#gad87a5892dfd2eb1279bc9eb0b8827018", null ],
    [ "ProductUri", "group__UaStackTypes.html#ga4fce8aa0aea3c06d7aaf317793ffd2cc", null ],
    [ "SoftwareVersion", "group__UaStackTypes.html#ga4d189e96984facd6665f7ed91d65be0d", null ]
];