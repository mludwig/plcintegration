var classUaApplicationControlCallback =
[
    [ "UaApplicationControlCallback", "classUaApplicationControlCallback.html#ac8e97e80ac914c67a77aa0ea48fae70c", null ],
    [ "~UaApplicationControlCallback", "classUaApplicationControlCallback.html#a78db7e13b9912450fc7cae31ebdf9c50", null ],
    [ "beforeEndpointOpen", "classUaApplicationControlCallback.html#a8b3ab97cee2990adf87fbfc03ae07523", null ],
    [ "closeEndpoint", "classUaApplicationControlCallback.html#a3ac1def687794d6f5d2d39e87f26a317", null ],
    [ "getEndpoint", "classUaApplicationControlCallback.html#a03cbdf7f0faf6adbda0cf1acfdfd6a8d", null ],
    [ "getEndpointCount", "classUaApplicationControlCallback.html#a9442c17959435662b71112bd143923e8", null ],
    [ "openEndpoint", "classUaApplicationControlCallback.html#a098a531375a3050895e9c5fde8402016", null ],
    [ "requestServerShutDown", "classUaApplicationControlCallback.html#a03c52d257e6dd7368dd484a598da4f91", null ]
];