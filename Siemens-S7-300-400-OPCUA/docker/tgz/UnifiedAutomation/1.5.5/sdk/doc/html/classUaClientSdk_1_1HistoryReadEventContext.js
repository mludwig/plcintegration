var classUaClientSdk_1_1HistoryReadEventContext =
[
    [ "HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html#aa7186c3d4281fef2b131389275023f7a", null ],
    [ "~HistoryReadEventContext", "classUaClientSdk_1_1HistoryReadEventContext.html#acd266005d59be85f4a24db6834992a22", null ],
    [ "bReleaseContinuationPoints", "classUaClientSdk_1_1HistoryReadEventContext.html#a9660112cb2178288bbc3179b8f438ca9", null ],
    [ "endTime", "classUaClientSdk_1_1HistoryReadEventContext.html#ae42b9637df7e21f8f9f5f80cf4297475", null ],
    [ "eventFilter", "classUaClientSdk_1_1HistoryReadEventContext.html#abf055a5c856d05a8e082b48a83b80bb7", null ],
    [ "numValuesPerNode", "classUaClientSdk_1_1HistoryReadEventContext.html#a416a0bc8fba9f8b9298543b2acd53cbc", null ],
    [ "startTime", "classUaClientSdk_1_1HistoryReadEventContext.html#a6c4b7829dc0d76e429e2e0a3a79ff285", null ],
    [ "timeStamps", "classUaClientSdk_1_1HistoryReadEventContext.html#acaa7b2043a92aa85e9daf02ba08d3d2c", null ]
];