var L1ClientSdkIntroduction =
[
    [ "Introduction", "L1ClientSdkIntroduction.html#L2ClientSdkIntroduction", null ],
    [ "Session", "L2ClientSdkSession.html", [
      [ "Overview", "L2ClientSdkSession.html#L3ClientSDKSessionOverview", null ],
      [ "Connection handling and reconnect", "L2ClientSdkSession.html#L3ClientSDKSessionConnHandling", null ],
      [ "Handles and Ids", "L2ClientSdkSession.html#L3ClientSDKSessionHandles", [
        [ "SessionId", "L2ClientSdkSession.html#L4ClientSDKSessionSessionId", null ],
        [ "ClientConnectionId", "L2ClientSdkSession.html#L4ClientSDKSessionClientConnectionId", null ],
        [ "TransactionId", "L2ClientSdkSession.html#L4ClientSDKSessionTransactionId", null ]
      ] ]
    ] ],
    [ "Subscription", "L2ClientSdkSubscription.html", [
      [ "Overview", "L2ClientSdkSubscription.html#L3ClientSdkSubscriptionOverview", null ],
      [ "Handles and Ids", "L2ClientSdkSubscription.html#L3ClientSdkSubscriptionHandles", [
        [ "SubscriptionId", "L2ClientSdkSubscription.html#L4ClientSdkSubscriptionSubscriptionId", null ],
        [ "ClientSubscriptionHandle", "L2ClientSdkSubscription.html#L3ClientSdkSubscriptionClientSubscriptionHandle", null ],
        [ "TransactionId", "L2ClientSdkSubscription.html#L3ClientSdkSubscriptionTransactionId", null ],
        [ "Monitored items", "L2ClientSdkSubscription.html#L3ClientSdkSubscriptionMonitoredItems", null ]
      ] ]
    ] ],
    [ "Client Configuration", "L2ClientSdkClientConfig.html", null ],
    [ "Starting points for users of the SDK", "L2ClientSdkStart.html", null ],
    [ "Security", "L2ClientSdkSecurity.html", [
      [ "Initial application configuration", "L2ClientSdkSecurity.html#L3ClientSdkSecurityInitialApp", null ],
      [ "Loading the certificates at start up", "L2ClientSdkSecurity.html#L3ClientSdkSecurityLoadingCertsStartup", null ],
      [ "Getting security configuration from server", "L2ClientSdkSecurity.html#L3ClientSdkSecurityDiscovery", null ]
    ] ]
];