var structOpcUa__ApplicationDescription =
[
    [ "ApplicationName", "group__UaStackTypes.html#ga89048407f80d2c1c891a6344d2e21345", null ],
    [ "ApplicationType", "group__UaStackTypes.html#gaa0ad160dd09cdfab75a96a2f2d733d52", null ],
    [ "ApplicationUri", "group__UaStackTypes.html#ga2f6625b5cf103ab1ac4fef4ba5520931", null ],
    [ "DiscoveryProfileUri", "group__UaStackTypes.html#ga8e627f84943d670ea911ee0fddf18456", null ],
    [ "DiscoveryUrls", "group__UaStackTypes.html#gac1e6aa29de609c276aedd1a6e58208bd", null ],
    [ "GatewayServerUri", "group__UaStackTypes.html#ga819373d76779c129a58f2e5120a0b7b7", null ],
    [ "ProductUri", "group__UaStackTypes.html#gac4949aea150e19dc0077a4cd37cd0cf1", null ]
];