var group__ServerCoreInterfaces =
[
    [ "EventCallback", "classEventCallback.html", [
      [ "EventCallback", "classEventCallback.html#acc90f7e0bf509b4a860dcd5e96ce2816", null ],
      [ "~EventCallback", "classEventCallback.html#af2bbd2836317a24d5d6735d0dc3a29e2", null ],
      [ "invalidateEventManager", "classEventCallback.html#a0b9932b2ffae4757e71104eda084a6a0", null ],
      [ "newEvent", "classEventCallback.html#a93143e237c95c4688c688ed940a380a3", null ]
    ] ],
    [ "EventManagerCallback", "classEventManagerCallback.html", [
      [ "EventManagerCallback", "classEventManagerCallback.html#aa63a558ff6fbee9381fa5258014e8eab", null ],
      [ "~EventManagerCallback", "classEventManagerCallback.html#ae711d60e34acc41b0f300b0b0b0b8140", null ],
      [ "finishModifyMonitoring", "classEventManagerCallback.html#a1856482cd1ce3e695a01b6d22f02d638", null ],
      [ "finishStartMonitoring", "classEventManagerCallback.html#abe87097b58ba578a0dab532a9d350c78", null ],
      [ "finishStopMonitoring", "classEventManagerCallback.html#a69952bf047b69ae3ac5d9286d8ff2d5b", null ]
    ] ],
    [ "EventManager", "classEventManager.html", [
      [ "EventTransactionType", "group__ServerCoreInterfaces.html#gad028aaf46f546e841a7e9ddd454f8cb6", [
        [ "MONITOR_BEGIN", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2bbf320ef39238b799c72376b79d1549", null ],
        [ "MONITOR_MODIFY", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2db1edefaedc6c547d85aaaf0dca449a", null ],
        [ "MONITOR_STOP", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a78bc6cc9193eff9c679f59958444d4b0", null ],
        [ "INVALID", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a444aee1d9503d934c3a2d10ab51efd33", null ]
      ] ],
      [ "EventManager", "classEventManager.html#a89099b22114f158b5c530edfea52371d", null ],
      [ "~EventManager", "classEventManager.html#afc6d01d2924c433f62afc9156351ea86", null ],
      [ "beginConditionRefresh", "classEventManager.html#ad30e8283eeda0eeb5b2f8d561c41b68b", null ],
      [ "beginEventTransaction", "classEventManager.html#af63a8d1ca228de1686c507c292bd8cd6", null ],
      [ "beginModifyMonitoring", "classEventManager.html#a3e3c794a81f265c64eebfc57c6103031", null ],
      [ "beginStartMonitoring", "classEventManager.html#a1734770b69986526745385fc8749ddf3", null ],
      [ "beginStopMonitoring", "classEventManager.html#a4662a50dbd99406fbb4bd7ee0138937b", null ],
      [ "finishEventTransaction", "classEventManager.html#ae0a33401961d790c56ed23597672c3cf", null ],
      [ "sendRefreshRequired", "classEventManager.html#a33d6f7dd876ca404757369115d144849", null ]
    ] ],
    [ "HistoryManagerCallback", "classHistoryManagerCallback.html", [
      [ "HistoryManagerCallback", "classHistoryManagerCallback.html#a4c267d0fef787652a07f2a200a8f551c", null ],
      [ "~HistoryManagerCallback", "classHistoryManagerCallback.html#a3dd9055ef7e8d5448bf3af73f78a3b26", null ],
      [ "finishHistoryReadData", "classHistoryManagerCallback.html#a66d79d656313c3bae209c9f04a029e22", null ],
      [ "finishHistoryReadEvent", "classHistoryManagerCallback.html#a33dc1429a9c497ba5adafe649eb14265", null ],
      [ "finishHistoryReadModifiedData", "classHistoryManagerCallback.html#a0cf5ed503b00be125bae561213444a73", null ],
      [ "finishHistoryUpdate", "classHistoryManagerCallback.html#a19b4a71ebd05e524ad689ccba1425b91", null ]
    ] ],
    [ "HistoryManager", "classHistoryManager.html", [
      [ "TransactionType", "group__ServerCoreInterfaces.html#gaf10634704f32afe28485102e140567b9", [
        [ "TransactionReadEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ac0e898b54ee64f998f1d336f2410e39e", null ],
        [ "TransactionReadRaw", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9afef810b34024c6fdcad554fd020a79ac", null ],
        [ "TransactionReadModified", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a044ea5e7a9b0bbfe70ce16156ca2cd77", null ],
        [ "TransactionReadProcessed", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad32ea6d3a0a352575724a8973cce3b1c", null ],
        [ "TransactionReadAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a114db17a943f4259d82210ee0d34f8f3", null ],
        [ "TransactionUpdateData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad7658e3ce5313c06ecd5aafc0e5098ca", null ],
        [ "TransactionUpdateStructureData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a14e2affacbd82b1c3bbbcf284bd7dda9", null ],
        [ "TransactionUpdateEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a9392ce956bf7352a2d40423058b6dbfc", null ],
        [ "TransactionDeleteData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a09629afe97bccc03e44ef3848e9c5167", null ],
        [ "TransactionDeleteAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad0091392b06cd5a5c521fe3083846a1a", null ],
        [ "TransactionDeleteEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a491e61c8a1d944bb4395703299b429d1", null ]
      ] ],
      [ "HistoryManager", "classHistoryManager.html#a110824c8f28cac57ce634f54581bd8a0", null ],
      [ "~HistoryManager", "classHistoryManager.html#a758a4675d4f7a69a9cf613d23147e378", null ],
      [ "beginDeleteAtTime", "classHistoryManager.html#a20ff46b9a488548f59e406bf47318d67", null ],
      [ "beginDeleteEvents", "classHistoryManager.html#a2ee5aed2c8190df152a4cbc42623716e", null ],
      [ "beginDeleteRawModified", "classHistoryManager.html#a1be75f5e3ef478ed947b970bd96c91fc", null ],
      [ "beginHistoryTransaction", "classHistoryManager.html#a78f6d5b40983060c9b5524981b336042", null ],
      [ "beginReadAtTime", "classHistoryManager.html#a064ffec0bf44fd334043e3224457c265", null ],
      [ "beginReadEvents", "classHistoryManager.html#a25864dfa1826e9c5094e4c63dcd1cadf", null ],
      [ "beginReadProcessed", "classHistoryManager.html#a2d4ab21b345050bda3e982130dbe808a", null ],
      [ "beginReadRawModified", "classHistoryManager.html#a6a91dc6eafc5bcec7a39d7a8ddd3917a", null ],
      [ "beginUpdateData", "classHistoryManager.html#ae6f7d79cb877f11ba959276d0228fbc3", null ],
      [ "beginUpdateEvents", "classHistoryManager.html#af83f2c96a14c78ec251575abca3a990b", null ],
      [ "cancelHistoryTransaction", "classHistoryManager.html#a5891169aa485280c0cde3618d30b63e5", null ],
      [ "finishHistoryTransaction", "classHistoryManager.html#ab2e6765f41c2033d5e551865cdaddf24", null ]
    ] ],
    [ "IOVariableCallback", "classIOVariableCallback.html", [
      [ "IOVariableCallback", "classIOVariableCallback.html#afcb034b8377109457918c5a3d8aba339", null ],
      [ "~IOVariableCallback", "classIOVariableCallback.html#ae7025d93d938ac620004a76ce82a8b7d", null ],
      [ "dataChange", "classIOVariableCallback.html#a97a97d7645fd97908645843ab3842d71", null ],
      [ "getLastValue", "classIOVariableCallback.html#ade66dc1f84f3d2f61bb7dce7d03b3e85", null ],
      [ "getRemainingQueueSize", "classIOVariableCallback.html#a743a797b577621f799f192afcf9d97ff", null ]
    ] ],
    [ "IOManagerCallback", "classIOManagerCallback.html", [
      [ "IOManagerCallback", "classIOManagerCallback.html#ac2a49ea84f9af40ef2077dbf1bf96822", null ],
      [ "~IOManagerCallback", "classIOManagerCallback.html#ac809878ebcb9607756ba0f6e96708cb1", null ],
      [ "finishModifyMonitoring", "classIOManagerCallback.html#ac53d75e1b45c23b568a3c556ffb44363", null ],
      [ "finishRead", "classIOManagerCallback.html#a21bf8bd3699a122d49f1dd373c8ae022", null ],
      [ "finishStartMonitoring", "classIOManagerCallback.html#a60eca40fb88260700804f206e4fb2900", null ],
      [ "finishStopMonitoring", "classIOManagerCallback.html#ab62e55f709b7ae16cc8d91575bb43b53", null ],
      [ "finishWrite", "classIOManagerCallback.html#a6e6c172463dfd4e83b632debb519967d", null ]
    ] ],
    [ "IOManager", "classIOManager.html", [
      [ "TransactionType", "group__ServerCoreInterfaces.html#gabf06d4995bae765430ac833402793d15", [
        [ "TransactionRead", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a378a62008ea530fcf23bd53aadde18a6", null ],
        [ "TransactionWrite", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15af02a18425dea0272acca3a514603f72f", null ],
        [ "TransactionMonitorBegin", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15afa3d9f34bd81c6137316ae4925c78af0", null ],
        [ "TransactionMonitorModify", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ac4173b056e6a8962579643d42459841c", null ],
        [ "TransactionMonitorStop", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a9329c2e912c5d2a200299e9c2f09afbd", null ],
        [ "TransactionInvalid", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ad8e7c08c9d329c9d5180eb71ab4e258d", null ]
      ] ],
      [ "IOManager", "classIOManager.html#afce14d2f016545728fee1e48b74f431c", null ],
      [ "~IOManager", "classIOManager.html#afc81e305edf44159e87d17347751e5a9", null ],
      [ "beginModifyMonitoring", "classIOManager.html#a411aec9192de3702f2ab08f95a4604ea", null ],
      [ "beginRead", "classIOManager.html#a55a7e546253da06c62d43cabb09cfb5b", null ],
      [ "beginStartMonitoring", "classIOManager.html#a43f14cb4e647ed4352cfe26c046bb0fb", null ],
      [ "beginStopMonitoring", "classIOManager.html#a5ae8c42c15bdc85692c64caba2de050b", null ],
      [ "beginTransaction", "classIOManager.html#a16c80044cd51e124335752f8bdc4bdf6", null ],
      [ "beginWrite", "classIOManager.html#a50e8fbd883a5c825c54b74e8e0dddf5b", null ],
      [ "finishTransaction", "classIOManager.html#a8dd9fc501ce76817cb1b53b4297fe1f2", null ]
    ] ],
    [ "IOManager2", "classIOManager2.html", [
      [ "IOManager2", "classIOManager2.html#a3e04f98111a2a684c7fa1bc2b198c4be", null ],
      [ "~IOManager2", "classIOManager2.html#a2bed4243314b9768e0909e25f84c2778", null ],
      [ "beginModifyMonitoring", "classIOManager2.html#a7d89bc623db5fb72291eb60008b64eb1", null ],
      [ "beginRead", "classIOManager2.html#a38d3977bc759bf32c3663955a46c2255", null ],
      [ "beginStartMonitoring", "classIOManager2.html#af0bbec7873ecd0ff2ffd9186a823f622", null ],
      [ "beginStopMonitoring", "classIOManager2.html#adf33e75825a0f3623cbabeff10760c3d", null ],
      [ "beginWrite", "classIOManager2.html#a08c1c2e21c0c08bffd12cf24fdf35953", null ]
    ] ],
    [ "MethodManager", "classMethodManager.html", [
      [ "MethodManager", "classMethodManager.html#ad3ad919ba8d854fe8ed2dee1ef65f67d", null ],
      [ "~MethodManager", "classMethodManager.html#ae21e8d83f8ad9b77d6802b3afe0e1ed6", null ],
      [ "beginCall", "classMethodManager.html#a99039d5fe09a2bbd0ab5eb9abc7011e8", null ]
    ] ],
    [ "MethodManagerCallback", "classMethodManagerCallback.html", [
      [ "MethodManagerCallback", "classMethodManagerCallback.html#a2f3f33bac7fe5d7336f0e35fce485729", null ],
      [ "~MethodManagerCallback", "classMethodManagerCallback.html#acc87de8e3fac92f2f0d9acab4a1a83b3", null ],
      [ "finishCall", "classMethodManagerCallback.html#a8f2385416b4e08b43fade99443c0d7cc", null ]
    ] ],
    [ "NodeManager", "classNodeManager.html", [
      [ "NodeManager", "classNodeManager.html#af1ebc3eef1c1b27c8d0961039de52938", null ],
      [ "~NodeManager", "classNodeManager.html#a8f52614e110fa17e4fab9bb5486fd77a", null ],
      [ "addNode", "classNodeManager.html#a45a02e6efe004198e325aa5ad4960967", null ],
      [ "addReference", "classNodeManager.html#af6b0403e310a90354dab4eb59d39210c", null ],
      [ "browse", "classNodeManager.html#a7715132b38088ca009cfb0b9a73d7b6e", null ],
      [ "deleteNode", "classNodeManager.html#aa3e2e68d4ac5ae4329fe5d338085e005", null ],
      [ "deleteReference", "classNodeManager.html#a42ca0acee204c47524e11458d055584b", null ],
      [ "getEventManagers", "classNodeManager.html#af5f21b086c1b44381beab6bd01f92feb", null ],
      [ "getHistoryVariableHandle", "classNodeManager.html#a8ddf6bfa1f807af5e39bdb6f7faf4fec", null ],
      [ "getMethodHandle", "classNodeManager.html#a478db5810d78f36668a986f11bfb2a9c", null ],
      [ "getNodeManagerConfig", "classNodeManager.html#a5a734344dd1319d2c2a3eac714c87c37", null ],
      [ "getNodeManagerCrossReferences", "classNodeManager.html#a7a25983a35ffc79ceee55f2b53711f09", null ],
      [ "getNodeManagerUaNode", "classNodeManager.html#af6038809dfcc6cee76b402c160fbb004", null ],
      [ "getVariableHandle", "classNodeManager.html#a0621cf9e497ffa0f65a13f103ba3db51", null ],
      [ "sessionActivated", "classNodeManager.html#a82d7d7b44982886114b0f045ff66e7e7", null ],
      [ "sessionClosed", "classNodeManager.html#add9441fd8b1ac2a80788f3f34ff8b531", null ],
      [ "sessionOpened", "classNodeManager.html#a02d146e3435ec0893096b3aef5c6327a", null ],
      [ "translateBrowsePathToNodeId", "classNodeManager.html#ac2a3d5de9bfb7ac1310e102722ee6493", null ]
    ] ],
    [ "NodeManagerCrossReferences", "classNodeManagerCrossReferences.html", [
      [ "NodeManagerCrossReferences", "classNodeManagerCrossReferences.html#a2013cf92d377bb327d3c5b60dad7dc42", null ],
      [ "~NodeManagerCrossReferences", "classNodeManagerCrossReferences.html#af2600dddef8f29c4de2774eae7161c8f", null ],
      [ "addCrossReference", "classNodeManagerCrossReferences.html#a606a4c1d78b782efc6230eb0fdedbf81", null ],
      [ "connectStartingNode", "classNodeManagerCrossReferences.html#af95bcc1853feadaba22e562d847f1767", null ],
      [ "deleteCrossReference", "classNodeManagerCrossReferences.html#a316c7846f6100c9d2e237f9b689fb746", null ],
      [ "disconnectStartingNode", "classNodeManagerCrossReferences.html#a8930ece5d12d5239867cb3f1c81a6d05", null ]
    ] ],
    [ "NodeManagerList", "classNodeManagerList.html", [
      [ "NodeManagerList", "classNodeManagerList.html#a6a63149699075c3626b902e9b385206c", null ],
      [ "~NodeManagerList", "classNodeManagerList.html#a7c77d02ce6a002c7e24e8f9c0482fab7", null ],
      [ "addNodeManager", "classNodeManagerList.html#a9d6065f6a637895ba4770ac9dbbfd6ce", null ],
      [ "getNodeManagerByNamespace", "classNodeManagerList.html#a420d61eec784ab087696590881e6a22d", null ],
      [ "removeNodeManager", "classNodeManagerList.html#a009abf7959d949c1fd03c8b28e5130ca", null ]
    ] ],
    [ "NodeManagerConfig", "classNodeManagerConfig.html", [
      [ "NodeManagerConfig", "classNodeManagerConfig.html#a272a4cd368aefc8c910da98b33b93cda", null ],
      [ "~NodeManagerConfig", "classNodeManagerConfig.html#a4c3233fec163bb23a28a183299d09d46", null ],
      [ "addNodeAndReference", "classNodeManagerConfig.html#a22e5c694e92a54ff7893f4526a812854", null ],
      [ "addNodeAndReference", "classNodeManagerConfig.html#a0436020b1bfdec95fd4f1a7ed62c353d", null ],
      [ "addUaNode", "classNodeManagerConfig.html#a242772fa36ffead4a2d058196b0b3eb2", null ],
      [ "addUaReference", "classNodeManagerConfig.html#a7e5fb19c1e5875c968f7a83c4e4e04eb", null ],
      [ "addUaReference", "classNodeManagerConfig.html#a8b5af1f9171bf7770d213780b78cbe40", null ],
      [ "deleteUaNode", "classNodeManagerConfig.html#a994bd6d0a0e89a7cd9be7b727a210aec", null ],
      [ "deleteUaReference", "classNodeManagerConfig.html#a6c643402a021712e0849d835b2043aab", null ],
      [ "deleteUaReference", "classNodeManagerConfig.html#ac29cfef992a45fee8801954537d8566d", null ],
      [ "getEventManagerUaNode", "classNodeManagerConfig.html#a2868d1968749d0af7bcbf6493f8f1508", null ],
      [ "getNameSpaceIndex", "classNodeManagerConfig.html#a18a6834e23c333d8630a945ed3661ebf", null ],
      [ "getNodeManagerNodeSetXml", "classNodeManagerConfig.html#a2ecf93773ec1a0d24257e16a0c0970c0", null ],
      [ "lockNodes", "classNodeManagerConfig.html#a50b859ba8b43dfb0ef5bb40e628d4c9e", null ],
      [ "unlockNodes", "classNodeManagerConfig.html#a21a004b54cda546735398a16e5be62b8", null ]
    ] ],
    [ "ServerConfig", "classServerConfig.html", [
      [ "ComAeEventType", "group__ServerCoreInterfaces.html#ga3b4a326ff457fa76b7434f8b05be213c", [
        [ "SIMPLE_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213ca1ede128b4ea14a0e61a554dda8241b82", null ],
        [ "TRACKING_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213ca64c28126203277db7e7334d1baccdb61", null ],
        [ "CONDITION_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213cae5d3a920e2d1c73cc139e0c27ebb27fd", null ]
      ] ],
      [ "ComDaTimestampSource", "group__ServerCoreInterfaces.html#gab56f6d9cd2ecf685e298693e7734bf62", [
        [ "INTERNAL", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62a98d89693713654a69e1718b30bbe7837", null ],
        [ "SOURCE_TIMESTAMP", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62a4d4862288a336d73fc25e88ebf06db27", null ],
        [ "SERVER_TIMESTAMP", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62ae12dd86d1ea0fb06a4bede692b6d6b6c", null ]
      ] ],
      [ "ServerConfig", "classServerConfig.html#af61bcd662c3be06dd4219770350e65e6", null ],
      [ "~ServerConfig", "classServerConfig.html#afd78bb62c20b0cfd002d3fe96979ca61", null ],
      [ "addComAeAttributeMapping", "classServerConfig.html#a6ad13fedd60caf27e76eae6151ddb4dd", null ],
      [ "addComAeEventCategoryMapping", "classServerConfig.html#ac8bad6289698799566c67b53ed347b3b", null ],
      [ "addComDaPropertyMapping", "classServerConfig.html#a5c7ef836e4d9360f2aacd79bab3122f2", null ],
      [ "addConfiguredNamespace", "classServerConfig.html#a129749ecdaf80629e6fae07cbd2ff6a8", null ],
      [ "createSession", "classServerConfig.html#ad6959f97989d4b411560d9179f9a121d", null ],
      [ "endpointOpened", "classServerConfig.html#acafb216fc12595368e777e77c26c8a5f", null ],
      [ "getAdditionalServerEntries", "classServerConfig.html#a3d939001cee9a6dcfe43a1e2edfa4222", null ],
      [ "getAvailableSamplingRates", "classServerConfig.html#a4c3b6ab8072435d75d64a4954640161a", null ],
      [ "getBrowseNextLevelForVariables", "classServerConfig.html#aded9f21fa4dff3416f10935e4393ee61", null ],
      [ "getBuildInfo", "classServerConfig.html#ae87a7c8c53c336cec42c1bbd21111cad", null ],
      [ "getCertificateStoreConfigurations", "classServerConfig.html#a94d3c835f8cd9a6f605689603c1f6bb8", null ],
      [ "getComAeAttributeMapping", "classServerConfig.html#a48ee07f7ffb0d727a883de25c9020de2", null ],
      [ "getComAeEventCategoryMapping", "classServerConfig.html#a7a456ac7fd83bf92362824887fcd7e09", null ],
      [ "getComAeNamespaceInformation", "classServerConfig.html#a32e219fddcea805242c34b49f8ff8a29", null ],
      [ "getComDaNamespaceInformation", "classServerConfig.html#a2344dc89564c2aae98dfc423f08e984a", null ],
      [ "getComDaPropertyMapping", "classServerConfig.html#a061de1289f94da19f5dd5c2328dca164", null ],
      [ "getComDaTimestampSource", "classServerConfig.html#aeabf19a6cce93e793dd248498eaaefb1", null ],
      [ "getComHdaTimestampSource", "classServerConfig.html#ac4a8d2eb59a1a8cf1ea8ce47a7590166", null ],
      [ "getConfiguredNamespaces", "classServerConfig.html#aebe810067826f3f1d9331e6b8d11d9ff", null ],
      [ "getDefaultUserCertificateStore", "classServerConfig.html#a7bb80c26824d2987586ef985be45d6b1", null ],
      [ "getDiscoveryRegistrationConfig", "classServerConfig.html#ada79485fdf2824a41f3609b5cec3e64a", null ],
      [ "getEndpointConfiguration", "classServerConfig.html#ad1ddecfd21b2bc14233e6e91845dce95", null ],
      [ "getHttpFileAccess", "classServerConfig.html#afcdd62bdd5c0ca5a4afe19918ba4ee1c", null ],
      [ "getIdsForDefaultRoles", "classServerConfig.html#af85a428afef6dc1587f19de44c68154d", null ],
      [ "getMaxBrowseContinuationPoints", "classServerConfig.html#ae30954ea3b198b1220b4d82c64e7c8ec", null ],
      [ "getMaxBrowseResults", "classServerConfig.html#af9f30b80c2d63dc1974a784309e6160f", null ],
      [ "getMaxHistoryContinuationPoints", "classServerConfig.html#a3d36b8f28e14ec1b5dcda95441104211", null ],
      [ "getMaxNodesPerHistoryService", "classServerConfig.html#afa32470e57bfb7c27b07de4d0d84d9d7", null ],
      [ "getMaxNodesPerTranslateBrowsePathsToNodeIds", "classServerConfig.html#a211024295cf9f1fe679e2746267946ff", null ],
      [ "getMaxNodesToBrowse", "classServerConfig.html#a26ce98e76711d6e28125f90b2b6770c9", null ],
      [ "getMaxRequestAge", "classServerConfig.html#ac01b947823862ad3f1f3736b101ed7c1", null ],
      [ "getMonitoredItemSettings", "classServerConfig.html#a23847ca2871a3a4fe6a3c1aaf22b317c", null ],
      [ "getNetworkRedundancySettings", "classServerConfig.html#a9185cd7259a8504c7f2f4f23549c8175", null ],
      [ "getRedundancySettings", "classServerConfig.html#ad5bc24919eb9d0eb9d916e9ced231921", null ],
      [ "getSerializerConfiguration", "classServerConfig.html#ad4c602bbeb3e9d962a6aeff271f40a9c", null ],
      [ "getServerId", "classServerConfig.html#a9748f8a0a61dbbe075f4ebac177cb56c", null ],
      [ "getServerInstanceInfo", "classServerConfig.html#af2d6c40f3c246b890c3de870fe355318", null ],
      [ "getServerSettings", "classServerConfig.html#ab3eb9b5d52dc39901ad592bad71fc844", null ],
      [ "getServerTraceSettings", "classServerConfig.html#a77a431d3451368407ad9d0b1446b77a2", null ],
      [ "getSessionSettings", "classServerConfig.html#ac6ed65b1fa4505dfb4f777db68bae4de", null ],
      [ "getStackThreadPoolSettings", "classServerConfig.html#aa5d0e47f43d8ef12c67a9c4cc441edd9", null ],
      [ "getStackTraceSettings", "classServerConfig.html#aa84c0f5c319edfccd7c29648bb365801", null ],
      [ "getSubscriptionMaxCountSettings", "classServerConfig.html#a50bfc6296e821dac5e6a7aebba4502c0", null ],
      [ "getSubscriptionSettings", "classServerConfig.html#a3314627acfc3d425c95203053609fd6e", null ],
      [ "getTagFileConfiguration", "classServerConfig.html#a461ec8b8768e3d2416f4b064b627b72f", null ],
      [ "getThreadPoolSettings", "classServerConfig.html#ad486a006b7b1655db803936e17931377", null ],
      [ "getTraceEventSettings", "classServerConfig.html#a56d11b3417ee7bbe7e14c09e26319f48", null ],
      [ "getUserIdentityTokenConfig", "classServerConfig.html#ab4ecb2592065ca84dae7abc600197644", null ],
      [ "getUserIdentityTokenSecurityPolicy", "classServerConfig.html#abc5371114e5d927a743b90d996d18565", null ],
      [ "getWindowsDiscoveryRegistrationSecuritySetup", "classServerConfig.html#aeb33e9bdb6628acdcc45eb42db7bde65", null ],
      [ "isAuditActivated", "classServerConfig.html#a432e97a000a19e8a501c4fbe915e6216", null ],
      [ "loadConfiguration", "classServerConfig.html#a117851684c8a64a176f3ac6a5ee261c1", null ],
      [ "lockConfigFile", "classServerConfig.html#a8c9ec9a694aaef8446da56a8a9ea63c3", null ],
      [ "logonSessionUser", "classServerConfig.html#aed8cb7ce84d41d8ec24ae7cf9e83e949", null ],
      [ "removeConfiguredNamespace", "classServerConfig.html#a578c3037d5b010ca164f7449d82d142c", null ],
      [ "saveConfiguration", "classServerConfig.html#a8689c4db740da82f92e86dc26a7966a2", null ],
      [ "setServerCertificateThumbprint", "classServerConfig.html#ac62265f6a7b120af364375a67c48a59f", null ],
      [ "setTagFileConfiguration", "classServerConfig.html#aa83e397b4255dbe6e76199088f507c80", null ],
      [ "shutDown", "classServerConfig.html#a007913e8609922f34f337ad87b7911c0", null ],
      [ "startUp", "classServerConfig.html#a7613b36b757834d5ca089fb8ffe9e8c2", null ],
      [ "unlockConfigFile", "classServerConfig.html#a9700402b5780db485a3b8bf697b2b575", null ]
    ] ],
    [ "VariableHandle", "classVariableHandle.html", [
      [ "ServiceType", "group__ServerCoreInterfaces.html#gaf197ed865bc4c4aaceeef1466b99383f", [
        [ "ServiceRead", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383faccd0cc408a1ffcc5b4f702e0e58079ce", null ],
        [ "ServiceWrite", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa76e73c7a1a4c9e4d4eedf9a6e13e3d3d", null ],
        [ "ServiceMonitoring", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fac2e07df03a9864953ebb8129aff17dac", null ],
        [ "ServiceRegister", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa38b0243f276da6a26eda4c25ef3728e6", null ]
      ] ],
      [ "VariableHandle", "classVariableHandle.html#a6999e11b397e92eb7eb10b0f841dcdfb", null ],
      [ "~VariableHandle", "classVariableHandle.html#aa045d85dc19cdab1ddb166ef13951f4d", null ],
      [ "m_AttributeID", "classVariableHandle.html#a55e88c8b9fda2280a3276e6d80f5150f", null ],
      [ "m_pIOManager", "classVariableHandle.html#a7faa30f3f7d704e0681d7a01883960fd", null ]
    ] ],
    [ "ComAeEventType", "group__ServerCoreInterfaces.html#ga3b4a326ff457fa76b7434f8b05be213c", [
      [ "SIMPLE_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213ca1ede128b4ea14a0e61a554dda8241b82", null ],
      [ "TRACKING_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213ca64c28126203277db7e7334d1baccdb61", null ],
      [ "CONDITION_EVENT", "group__ServerCoreInterfaces.html#gga3b4a326ff457fa76b7434f8b05be213cae5d3a920e2d1c73cc139e0c27ebb27fd", null ]
    ] ],
    [ "ComDaTimestampSource", "group__ServerCoreInterfaces.html#gab56f6d9cd2ecf685e298693e7734bf62", [
      [ "INTERNAL", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62a98d89693713654a69e1718b30bbe7837", null ],
      [ "SOURCE_TIMESTAMP", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62a4d4862288a336d73fc25e88ebf06db27", null ],
      [ "SERVER_TIMESTAMP", "group__ServerCoreInterfaces.html#ggab56f6d9cd2ecf685e298693e7734bf62ae12dd86d1ea0fb06a4bede692b6d6b6c", null ]
    ] ],
    [ "EventTransactionType", "group__ServerCoreInterfaces.html#gad028aaf46f546e841a7e9ddd454f8cb6", [
      [ "MONITOR_BEGIN", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2bbf320ef39238b799c72376b79d1549", null ],
      [ "MONITOR_MODIFY", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a2db1edefaedc6c547d85aaaf0dca449a", null ],
      [ "MONITOR_STOP", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a78bc6cc9193eff9c679f59958444d4b0", null ],
      [ "INVALID", "group__ServerCoreInterfaces.html#ggad028aaf46f546e841a7e9ddd454f8cb6a444aee1d9503d934c3a2d10ab51efd33", null ]
    ] ],
    [ "ServiceType", "group__ServerCoreInterfaces.html#gaf197ed865bc4c4aaceeef1466b99383f", [
      [ "ServiceRead", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383faccd0cc408a1ffcc5b4f702e0e58079ce", null ],
      [ "ServiceWrite", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa76e73c7a1a4c9e4d4eedf9a6e13e3d3d", null ],
      [ "ServiceMonitoring", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fac2e07df03a9864953ebb8129aff17dac", null ],
      [ "ServiceRegister", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa38b0243f276da6a26eda4c25ef3728e6", null ]
    ] ],
    [ "TransactionType", "group__ServerCoreInterfaces.html#gaf10634704f32afe28485102e140567b9", [
      [ "TransactionReadEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ac0e898b54ee64f998f1d336f2410e39e", null ],
      [ "TransactionReadRaw", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9afef810b34024c6fdcad554fd020a79ac", null ],
      [ "TransactionReadModified", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a044ea5e7a9b0bbfe70ce16156ca2cd77", null ],
      [ "TransactionReadProcessed", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad32ea6d3a0a352575724a8973cce3b1c", null ],
      [ "TransactionReadAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a114db17a943f4259d82210ee0d34f8f3", null ],
      [ "TransactionUpdateData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad7658e3ce5313c06ecd5aafc0e5098ca", null ],
      [ "TransactionUpdateStructureData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a14e2affacbd82b1c3bbbcf284bd7dda9", null ],
      [ "TransactionUpdateEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a9392ce956bf7352a2d40423058b6dbfc", null ],
      [ "TransactionDeleteData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a09629afe97bccc03e44ef3848e9c5167", null ],
      [ "TransactionDeleteAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad0091392b06cd5a5c521fe3083846a1a", null ],
      [ "TransactionDeleteEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a491e61c8a1d944bb4395703299b429d1", null ]
    ] ],
    [ "TransactionType", "group__ServerCoreInterfaces.html#gabf06d4995bae765430ac833402793d15", [
      [ "TransactionRead", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a378a62008ea530fcf23bd53aadde18a6", null ],
      [ "TransactionWrite", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15af02a18425dea0272acca3a514603f72f", null ],
      [ "TransactionMonitorBegin", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15afa3d9f34bd81c6137316ae4925c78af0", null ],
      [ "TransactionMonitorModify", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ac4173b056e6a8962579643d42459841c", null ],
      [ "TransactionMonitorStop", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15a9329c2e912c5d2a200299e9c2f09afbd", null ],
      [ "TransactionInvalid", "group__ServerCoreInterfaces.html#ggabf06d4995bae765430ac833402793d15ad8e7c08c9d329c9d5180eb71ab4e258d", null ]
    ] ]
];