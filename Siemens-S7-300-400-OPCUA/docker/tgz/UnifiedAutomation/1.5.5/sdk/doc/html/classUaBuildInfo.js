var classUaBuildInfo =
[
    [ "UaBuildInfo", "classUaBuildInfo.html#a7ec17d523667022c4636e15e2468935f", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html#ad3e075b3f7fabeb500e4cd42e849b68b", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html#ae975a1315216b90dfc9290831a04ac14", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html#a61e480e0ce7fab27b68801c5604000d5", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html#a24793eb5fa16c57c89745e4d685af6d4", null ],
    [ "UaBuildInfo", "classUaBuildInfo.html#a1d783daae7dbcd98b1db10126e50bb04", null ],
    [ "~UaBuildInfo", "classUaBuildInfo.html#a32c7a4fb685f384a7e4af1f72e72cb63", null ],
    [ "attach", "classUaBuildInfo.html#a9d8ca0f4d1b76a1e1a7e6a2ee3da0baf", null ],
    [ "clear", "classUaBuildInfo.html#a132498ed9cd5d9e416063630d2ce4511", null ],
    [ "copy", "classUaBuildInfo.html#ab3c17f5766d0ee371a7f35ecc0aa6e8e", null ],
    [ "copyTo", "classUaBuildInfo.html#a42b7666faebcaa9be0e81a4cbb91e00b", null ],
    [ "detach", "classUaBuildInfo.html#af4a1f9e87f7b85f07fbcb9f3c1d809dd", null ],
    [ "getBuildDate", "classUaBuildInfo.html#a20f7cd8c93c30e77a425042ee31538cb", null ],
    [ "getBuildNumber", "classUaBuildInfo.html#a616ef183be7cf4d389358d0fca8a45de", null ],
    [ "getManufacturerName", "classUaBuildInfo.html#a8898707e7db99885dd025092701c6133", null ],
    [ "getProductName", "classUaBuildInfo.html#ae4e88cd920e1efb152e0e1b57702d410", null ],
    [ "getProductUri", "classUaBuildInfo.html#a60898dc4bbf98919eb4774b4dc82a028", null ],
    [ "getSoftwareVersion", "classUaBuildInfo.html#a044be4e8f850ce5e69a00118624431c8", null ],
    [ "operator!=", "classUaBuildInfo.html#a6e3975f339026aae20e567614047c937", null ],
    [ "operator=", "classUaBuildInfo.html#aaec4a7616d8544e3f8a26e7d4ac6e0cd", null ],
    [ "operator==", "classUaBuildInfo.html#ac6d7311af7049f13f430241b949b73a9", null ],
    [ "setBuildDate", "classUaBuildInfo.html#a2ca5996ba9ce390ff0a1cbfb40d7328b", null ],
    [ "setBuildNumber", "classUaBuildInfo.html#a3c6dda451bb73a256d7ba6385fade309", null ],
    [ "setManufacturerName", "classUaBuildInfo.html#a82f2f6a3cd5cbf1bf52ade8dd7f98b05", null ],
    [ "setProductName", "classUaBuildInfo.html#a0ff4af51b4b48f757dc220ac7498e2c1", null ],
    [ "setProductUri", "classUaBuildInfo.html#a2ee514a442c5116c89b4c47257ca726b", null ],
    [ "setSoftwareVersion", "classUaBuildInfo.html#a3fddde722c8e7d0678915b5891afcccb", null ]
];