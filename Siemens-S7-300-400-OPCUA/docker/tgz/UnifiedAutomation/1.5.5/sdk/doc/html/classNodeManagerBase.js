var classNodeManagerBase =
[
    [ "NodeManagerBase", "classNodeManagerBase.html#a24bb7a6d94cab9e26ac161116bb4fb88", null ],
    [ "~NodeManagerBase", "classNodeManagerBase.html#a2a550bf27e458780e3ee78f0914052ee", null ],
    [ "getEventManagers", "classNodeManagerBase.html#a6488dc889f68ffb32483ce2ab251ced2", null ],
    [ "getEventManagerUaNode", "classNodeManagerBase.html#af2b51d525a64e726308fbed323c32711", null ],
    [ "getHistoryVariableHandle", "classNodeManagerBase.html#a7fc180872e5363d8c1acdf10c3227d2c", null ],
    [ "getIOManager", "classNodeManagerBase.html#a42f1fbd6a49043be310e9303ccf62aa1", null ],
    [ "removeHistoryManagerForUaNode", "classNodeManagerBase.html#aaa7941a29929fa0c5d0a3cb41e2413a6", null ],
    [ "setFiresEvents", "classNodeManagerBase.html#a57d0f28faff8df81b92aa0c571530738", null ],
    [ "setHistoryManager", "classNodeManagerBase.html#a23ee6faea4867728c32bb02348b14137", null ],
    [ "setHistoryManagerForUaNode", "classNodeManagerBase.html#a25cd9129acd95c4563d7f2c3513ee381", null ],
    [ "startUp", "classNodeManagerBase.html#a3b4850ee9aa1027ef2f9ff83ee8cfa41", null ]
];