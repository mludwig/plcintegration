var classUaReference =
[
    [ "CrossNodeManagerReferenceType", "group__ServerCoreAddressModel.html#ga7f59692b5d096cf603075e3b605bc26d", [
      [ "UA_NO_CROSSNODEMANAGER", "group__ServerCoreAddressModel.html#gga7f59692b5d096cf603075e3b605bc26da88b46e92e054c383d5d88f754bc17f3d", null ],
      [ "UA_LIST_OF_REFERENCES", "group__ServerCoreAddressModel.html#gga7f59692b5d096cf603075e3b605bc26da802d50bea9d60dde88bd077405f9cfa2", null ],
      [ "UA_SINGLE_REFERENCE", "group__ServerCoreAddressModel.html#gga7f59692b5d096cf603075e3b605bc26dab686cc877af13a4111db8f6ac2c14d07", null ]
    ] ],
    [ "UaReference", "classUaReference.html#a28933639f7c0c24f85ed5b53dc246ddf", null ],
    [ "~UaReference", "classUaReference.html#a4659861f8ad6173cf88a9d88fe0d4517", null ],
    [ "isCrossNodeManagerRef", "classUaReference.html#ac1d12e6b1881082e15cfb47c2d25f659", null ],
    [ "isOutOfServerRef", "classUaReference.html#a30c6d51c4cdd3b060ffae7d7d75e3810", null ],
    [ "isSubtypeOf", "classUaReference.html#a3f4a2f4b8c6ac3acfbb61a492d0a7ffd", null ],
    [ "pNextForwardReference", "classUaReference.html#ac5b66959d97b50feda446a7e6fbc394d", null ],
    [ "pNextInverseReference", "classUaReference.html#a5a9675a49d05bd837a39508f5afe4840", null ],
    [ "pSourceNode", "classUaReference.html#ac1556bbdf96d43e66cffcd6381819957", null ],
    [ "pTargetNode", "classUaReference.html#a1ec17a1661a2024f334e3fd2b3ceb29b", null ],
    [ "referenceTypeId", "classUaReference.html#a12a933ec8f308c2afbac3485152fd391", null ],
    [ "setNextForwardReference", "classUaReference.html#a9fb45f1a58f976566b7baa3ce171bb75", null ],
    [ "setNextInverseReference", "classUaReference.html#a0a558a34f8dee6085912fbbbda38f1ee", null ],
    [ "setSourceNodeInvalid", "classUaReference.html#a19e16b8a27ddc2e737997605579951ed", null ],
    [ "setTargetNodeInvalid", "classUaReference.html#a5214bd6524e8494be5a25104bf1eeef7", null ],
    [ "m_pNextForwardReference", "classUaReference.html#a135e03d3be28707d76773153f17b009d", null ],
    [ "m_pNextInverseReference", "classUaReference.html#ad48b898e60c8a1dd854bab59a3ad47e0", null ],
    [ "m_pSourceNode", "classUaReference.html#a5eb628fa0f9735618ad23452068c7633", null ],
    [ "m_pTargetNode", "classUaReference.html#adc90963d241eff29ec26d4f27605fe8c", null ]
];