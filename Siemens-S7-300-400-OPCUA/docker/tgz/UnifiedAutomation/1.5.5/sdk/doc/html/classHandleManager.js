var classHandleManager =
[
    [ "HandleManager", "classHandleManager.html#ac139f75fb4266a37ab182e60f9b39335", null ],
    [ "~HandleManager", "classHandleManager.html#aa5f4875902acbf1eb8be7f4567f0148a", null ],
    [ "add", "classHandleManager.html#af25ba6de92db3e76285a0c4a2f6e1358", null ],
    [ "addAt", "classHandleManager.html#a53f6a9455ef69e99ffec940b033efbbd", null ],
    [ "clearList", "classHandleManager.html#a23b3c1041dc823e2b3a21bd7a849bb4a", null ],
    [ "detach", "classHandleManager.html#ac9f4c990a92cbb4a2a54b648b428ec6a", null ],
    [ "get", "classHandleManager.html#ae553d6e134910dd153e7eababfa39b80", null ],
    [ "itemCount", "classHandleManager.html#a04318577c1f80f8a61af47b3de1499b0", null ],
    [ "maxIndex", "classHandleManager.html#ac37de1fea0dbb682be6b560f9b702757", null ],
    [ "prepareAdd", "classHandleManager.html#a2cbf6b51b0a82e123e53597297176c21", null ],
    [ "remove", "classHandleManager.html#ab457155edae5492a55cd9f8918055083", null ],
    [ "resize", "classHandleManager.html#af639fa061202da9f03121bc32ab7d515", null ]
];