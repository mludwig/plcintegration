var classUaVariant =
[
    [ "UaVariant", "classUaVariant.html#ab21187fbb3b365ce03a0b35897fca058", null ],
    [ "UaVariant", "classUaVariant.html#a5f7c378064ac6977ea0b3603d4716723", null ],
    [ "UaVariant", "classUaVariant.html#a650153a61c08f81588e33e0b2bd182a8", null ],
    [ "UaVariant", "classUaVariant.html#ad8c1be517711a0888967cda4786ebbdf", null ],
    [ "UaVariant", "classUaVariant.html#a4189c4f4a5facc62e3d6f23ba3bbfb70", null ],
    [ "UaVariant", "classUaVariant.html#aed10208de50f48a6e5b84d7ae17e6841", null ],
    [ "UaVariant", "classUaVariant.html#a075e890f283feb06a809cabe3da31f96", null ],
    [ "UaVariant", "classUaVariant.html#ae8a15c86e0b92d0f7a1319b6466a41cb", null ],
    [ "UaVariant", "classUaVariant.html#aceb567f217731ad3dcad761af15830a4", null ],
    [ "UaVariant", "classUaVariant.html#a34b91d6ccfc73d0fe9a16ae09cbc7288", null ],
    [ "UaVariant", "classUaVariant.html#a2809177d6a6ce7784e9b1c0852775725", null ],
    [ "UaVariant", "classUaVariant.html#a60baac476866ecccf80b6ba4ab0c77b0", null ],
    [ "UaVariant", "classUaVariant.html#a66cd991c5d21d00326f03d701b7aa441", null ],
    [ "UaVariant", "classUaVariant.html#a3f45637d50fc75ea0993004b73ef4c51", null ],
    [ "UaVariant", "classUaVariant.html#abdff63d06667a5b207f8e3c0019b477e", null ],
    [ "UaVariant", "classUaVariant.html#acb334c4e38615df6e5490226daec65f6", null ],
    [ "UaVariant", "classUaVariant.html#a556bf30285c63f930884a07e0d325e8d", null ],
    [ "UaVariant", "classUaVariant.html#a1f8e074d5e159341de9adeb3201e25ad", null ],
    [ "UaVariant", "classUaVariant.html#ac0c65d006817df9f29316f4c5676e1fb", null ],
    [ "UaVariant", "classUaVariant.html#a1fd2fad19d5831942ba5095958c96ec5", null ],
    [ "UaVariant", "classUaVariant.html#a5d7200ea8fd2b51325758c303f62fe02", null ],
    [ "~UaVariant", "classUaVariant.html#af4ce1e533c41d07483c6799be3299cb1", null ],
    [ "applyIndexRange", "classUaVariant.html#a4c8344ab561156d37fbe72b004fb2b50", null ],
    [ "arrayDimensions", "classUaVariant.html#a58967d248cd1d623c6b991d5b8aec71b", null ],
    [ "arraySize", "classUaVariant.html#ae7b1b16d1beecdf2e3ba18c39a436dc4", null ],
    [ "arrayType", "classUaVariant.html#a6b6c4d2865de33b12f0830cb384cdaa3", null ],
    [ "attach", "classUaVariant.html#a079cd4a2a66e30c4ed90ba0486465432", null ],
    [ "changeType", "classUaVariant.html#a866e94a4db7f8ebc21592a00b79eb95d", null ],
    [ "clear", "classUaVariant.html#aed0b1fbd3cc677e33dfe6af52ed2da2b", null ],
    [ "copy", "classUaVariant.html#a3aab69ecb8db2ff6d7fe7c1cc2a8183e", null ],
    [ "copyTo", "classUaVariant.html#ac087b6d185510522952f32b4cb358a90", null ],
    [ "dataType", "classUaVariant.html#a3f34ef8b87300b982b30aa980a346adb", null ],
    [ "dataType", "classUaVariant.html#a253d9c41620e8b296bd51d0fc3256c96", null ],
    [ "detach", "classUaVariant.html#a4964ad3ef04deb20303389c708d8a6c2", null ],
    [ "dimensionSize", "classUaVariant.html#a5372d7911b59afc7ed4ee52b25d83d2b", null ],
    [ "encoding", "classUaVariant.html#aa973ed6850aff18b2702a000fc66423c", null ],
    [ "encodingTypeId", "classUaVariant.html#a794a6c34e7890bf4aba5a85dbaaab95c", null ],
    [ "getIndexRange", "classUaVariant.html#acdb5b89bbaf206eb12af018ced34292a", null ],
    [ "isArray", "classUaVariant.html#a2d49e2d4f4658000b6416f1d4062e57b", null ],
    [ "isEmpty", "classUaVariant.html#a7f190bc4481a0f80eec786cadc8ee6e2", null ],
    [ "isMatrix", "classUaVariant.html#af317eddc079dc2ab1dccc74a2eb4508c", null ],
    [ "noOfMatrixElements", "classUaVariant.html#ae1f3b2619f35944f11d97f23a0fba39d", null ],
    [ "operator const OpcUa_Variant *", "classUaVariant.html#a0996e1bbaa4475347954d13ad1e00cae", null ],
    [ "operator!=", "classUaVariant.html#a07ca3777cd8f111a8147279dfe039f6c", null ],
    [ "operator<", "classUaVariant.html#a1140b71823ed0f1259a5ce397e8baab4", null ],
    [ "operator<=", "classUaVariant.html#a6af41a084b0872c6322cae2066a8fa42", null ],
    [ "operator=", "classUaVariant.html#a85ed6e5672adce563c582cc100aeee22", null ],
    [ "operator=", "classUaVariant.html#a0be45dc724661f2201e360535bf0f54d", null ],
    [ "operator=", "classUaVariant.html#a857e19144c2b7e3b75ed936aa2504780", null ],
    [ "operator=", "classUaVariant.html#ae1b8e0e2a573614e11b648727566fca2", null ],
    [ "operator==", "classUaVariant.html#a028a3b33e95574c32472342af32c9aef", null ],
    [ "operator>", "classUaVariant.html#af6e6eb0a370d1d3b993534ac886a1608", null ],
    [ "operator>=", "classUaVariant.html#a401c75125892bfc937d49efd19c7f6ff", null ],
    [ "operator[]", "classUaVariant.html#afa63b257252ba8b8d452517bba5cef3a", null ],
    [ "setBool", "classUaVariant.html#aafc745588e4807fb59f8b53e7b2ae02b", null ],
    [ "setBoolArray", "classUaVariant.html#a8094db1ba992f57e1b297b9396407de6", null ],
    [ "setBoolean", "classUaVariant.html#aaef4b36247f40d8a43cc94c01b1558bb", null ],
    [ "setBoolMatrix", "classUaVariant.html#a4d877b93a131874789a0b57b3e8435d7", null ],
    [ "setByte", "classUaVariant.html#a883cd42f1b8a6f738516b27c7a9338af", null ],
    [ "setByteArray", "classUaVariant.html#aee621d83caffe21d838a9141c0713e22", null ],
    [ "setByteMatrix", "classUaVariant.html#a89e8ee94e7e53c25588286560c4b6583", null ],
    [ "setByteString", "classUaVariant.html#a4ee6d2aa3e7ee7db0570e4ec356cafdc", null ],
    [ "setByteStringArray", "classUaVariant.html#a39acd478beb3984326338f126dd5b34e", null ],
    [ "setByteStringArray", "classUaVariant.html#a68f82e89482fafde2642c99cdb6d0e04", null ],
    [ "setByteStringMatrix", "classUaVariant.html#ae7fa39ed05305aaacdf4478f65a9e0f5", null ],
    [ "setByteStringMatrix", "classUaVariant.html#a828f08205ea589a2212f542c11a4bdef", null ],
    [ "setDataValue", "classUaVariant.html#af97febe992c008dc6f9200ed8cab96fa", null ],
    [ "setDataValueArray", "classUaVariant.html#a5698371b7f9b92aaa652271e7541bd45", null ],
    [ "setDataValueArray", "classUaVariant.html#a56f570591d68f54884a9bf39f4b51a9e", null ],
    [ "setDateTime", "classUaVariant.html#ae8cdb48a2a71f7afd8a53238f9e79a66", null ],
    [ "setDateTimeArray", "classUaVariant.html#a6db06c356c9e5fd6628d529d94b66add", null ],
    [ "setDateTimeMatrix", "classUaVariant.html#a3a2c0c1747fc3f97c86d9b83561212b7", null ],
    [ "setDouble", "classUaVariant.html#aca56bd39fb5f700d691410a99492e36e", null ],
    [ "setDoubleArray", "classUaVariant.html#a1efd910c559b576fea7261a400ecb5a2", null ],
    [ "setDoubleMatrix", "classUaVariant.html#a3c8bca160bb635496a6df185cec63f89", null ],
    [ "setExpandedNodeId", "classUaVariant.html#a16d7f54a282d13f2449b2604d72ac823", null ],
    [ "setExpandedNodeIdArray", "classUaVariant.html#a92d0ba63d9ed43758cfc1f30183e0496", null ],
    [ "setExpandedNodeIdMatrix", "classUaVariant.html#a5a14244f1cb0cad56287df0d5e4390cc", null ],
    [ "setExtensionObject", "classUaVariant.html#a75d9c9f0c3647b41132efd00e40f5717", null ],
    [ "setExtensionObjectArray", "classUaVariant.html#abb70fff897b233d85f182b208fd72503", null ],
    [ "setExtensionObjectMatrix", "classUaVariant.html#a040c942c203cbfeaa414afc403803a9c", null ],
    [ "setFloat", "classUaVariant.html#a03e1450756c3a5ce94d617ec67e79c56", null ],
    [ "setFloatArray", "classUaVariant.html#a59d9bd131b2906c8c457ae9b10b63861", null ],
    [ "setFloatMatrix", "classUaVariant.html#a30a70f4c467b33c0fbcaffb609ca31a2", null ],
    [ "setGuid", "classUaVariant.html#a17eb9a98205a16326975568d0686c59b", null ],
    [ "setGuidArray", "classUaVariant.html#a49e66f776c9e17e4dae12465078d450f", null ],
    [ "setGuidMatrix", "classUaVariant.html#a515e5012c4a7fd3c9cd4b68987de0623", null ],
    [ "setInt16", "classUaVariant.html#aa956b4451abb86898d4d41f89b433e2c", null ],
    [ "setInt16Array", "classUaVariant.html#a593f24dbf2077945f8518a26ab3e4371", null ],
    [ "setInt16Matrix", "classUaVariant.html#a386cb709f6a3fd843cce1db2c5e8cfab", null ],
    [ "setInt32", "classUaVariant.html#abdb6be5a4036ada4a852b41b7ca87472", null ],
    [ "setInt32Array", "classUaVariant.html#a9e4faecb233e63395866b852fb8fdd1d", null ],
    [ "setInt32Matrix", "classUaVariant.html#a9da17b4f623873290b0eb95a2a82d44b", null ],
    [ "setInt64", "classUaVariant.html#a64bb49ec070039079f94841b00d53da1", null ],
    [ "setInt64Array", "classUaVariant.html#abb1a39ccd1610684d2a33d1cd2044bb0", null ],
    [ "setInt64Matrix", "classUaVariant.html#aa413c6478be465055359ab395f2208d3", null ],
    [ "setLocalizedText", "classUaVariant.html#ad54e43d65c5fef80dcba4d5e08e2f5fa", null ],
    [ "setLocalizedTextArray", "classUaVariant.html#a98916b61a26cdb82350a6a8a4a8ec13f", null ],
    [ "setLocalizedTextArray", "classUaVariant.html#a01a4dc43909bce78ec9e0150adf700c8", null ],
    [ "setLocalizedTextMatrix", "classUaVariant.html#a7be4edc0fe975d236fdea16deb4bb32a", null ],
    [ "setLocalizedTextMatrix", "classUaVariant.html#ae4c0866a11bb7f04fe3907fdb2ff49a3", null ],
    [ "setNodeId", "classUaVariant.html#a9b814abbb2346454c36cd5058dcb5936", null ],
    [ "setNodeIdArray", "classUaVariant.html#a1c61ac9a9b559085e81c00264b8c6c28", null ],
    [ "setNodeIdMatrix", "classUaVariant.html#a916e100860cad32ad700d1037648f271", null ],
    [ "setQualifiedName", "classUaVariant.html#ac02b1ba160e924dc6a983c01e58c324b", null ],
    [ "setQualifiedNameArray", "classUaVariant.html#a07462ef9c51d7dbcdefe063052b0ec8c", null ],
    [ "setQualifiedNameArray", "classUaVariant.html#a7f7a7a5c67a8796852506b7e233cda35", null ],
    [ "setQualifiedNameMatrix", "classUaVariant.html#a883d0c390a5edb4cf74778c2e66ae83b", null ],
    [ "setQualifiedNameMatrix", "classUaVariant.html#a6a13ea51a37d1df558a77093c8766bb5", null ],
    [ "setSByte", "classUaVariant.html#a02b51fdde7594cb5f334d54c01e52dc0", null ],
    [ "setSByteArray", "classUaVariant.html#a5e906a36d20ad667316a4a077b6607e3", null ],
    [ "setSByteMatrix", "classUaVariant.html#aac92b439c23f0f16adbb7410318fa8f0", null ],
    [ "setStatusCode", "classUaVariant.html#acc4f29bccd7c7b4229d9dec5a7188601", null ],
    [ "setStatusCodeArray", "classUaVariant.html#a3b10489680e0b021d3e4afe7716af34c", null ],
    [ "setStatusCodeMatrix", "classUaVariant.html#afdf1866367c43bc056e593a9e3130fd1", null ],
    [ "setString", "classUaVariant.html#a32b0bc7457c3cea41e6c9980474fc6ac", null ],
    [ "setStringArray", "classUaVariant.html#a12159d17ec1a3f2ad34ebe79b52a24e5", null ],
    [ "setStringArray", "classUaVariant.html#ad866bc2559978fd156cfa2c9c04b88d4", null ],
    [ "setStringMatrix", "classUaVariant.html#ae3559985b926703e92360daf3615c9cf", null ],
    [ "setStringMatrix", "classUaVariant.html#a666453382e2c8eb3676b5b0fb4dd186d", null ],
    [ "setUInt16", "classUaVariant.html#aeecc9dd617e6c963934e641a3e3b5cc1", null ],
    [ "setUInt16Array", "classUaVariant.html#a108768a409165320081e301089aea010", null ],
    [ "setUInt16Matrix", "classUaVariant.html#a6007e336731ff71d2e9012114fa81958", null ],
    [ "setUInt32", "classUaVariant.html#aac0924ef83c12a99230ca043dac12fb4", null ],
    [ "setUInt32Array", "classUaVariant.html#aee8dbca3d8a6d77481cbb72d292ccb18", null ],
    [ "setUInt32Matrix", "classUaVariant.html#a568cc640d0fd13852646703652212490", null ],
    [ "setUInt64", "classUaVariant.html#a40454189c29951e2204cde6f8a1a1fee", null ],
    [ "setUInt64Array", "classUaVariant.html#a25e02ac36d12a7c219929b2f145ce72f", null ],
    [ "setUInt64Matrix", "classUaVariant.html#a6ed1301e881bc1a53156f1a1e3ab22df", null ],
    [ "setVariantArray", "classUaVariant.html#a9c5d5970ee20d8fd5020285c48deec76", null ],
    [ "setVariantArray", "classUaVariant.html#ab3e38c2a4edc0b099fc11e3c1eb8dda6", null ],
    [ "setVariantMatrix", "classUaVariant.html#a241cd06bbf18bb72232c8b1982f41b4f", null ],
    [ "setVariantMatrix", "classUaVariant.html#aebb62cddac4863a51f2f82314da07791", null ],
    [ "setXmlElement", "classUaVariant.html#a27af04bc0bc240872fa32512e1a9c39c", null ],
    [ "setXmlElement", "classUaVariant.html#a2ec7d2e147ae51b349969728f4953138", null ],
    [ "setXmlElementArray", "classUaVariant.html#aa9d64bdf90748706cfb4662e98c3d775", null ],
    [ "setXmlElementArray", "classUaVariant.html#a2ea70b1dce806ba470fa12e5562b7f87", null ],
    [ "setXmlElementMatrix", "classUaVariant.html#a42a42c65fb827a6aeabd0b0c9f7e8204", null ],
    [ "setXmlElementMatrix", "classUaVariant.html#a65699669e7c5c0b4a704f18ba6866e73", null ],
    [ "toBool", "classUaVariant.html#a91e13e84aeece8617cd1dfbaf8a33f01", null ],
    [ "toBoolArray", "classUaVariant.html#a5be09a356be53f75a8b9fc720a6c0b46", null ],
    [ "toBoolean", "classUaVariant.html#a479c5a311cc1b7e87851b344bb0ba610", null ],
    [ "toBoolMatrix", "classUaVariant.html#a1587bafe205ce651b1fab861e39abbf8", null ],
    [ "toByte", "classUaVariant.html#aa21cdbd3be00c83e687af079ccf13443", null ],
    [ "toByteArray", "classUaVariant.html#a682d70cec5ab55d01b312e2d7597b9f3", null ],
    [ "toByteMatrix", "classUaVariant.html#a4c9a3e7a1ee13433d1049d1099cfac29", null ],
    [ "toByteString", "classUaVariant.html#a60a1393a42f85f25bceb5a43de8d737c", null ],
    [ "toByteStringArray", "classUaVariant.html#a254a98f8a0d5d7450165db088d262e9e", null ],
    [ "toByteStringMatrix", "classUaVariant.html#a797862b4016f5a4a774d7763ac080dc7", null ],
    [ "toDataValue", "classUaVariant.html#a258457d5da1c3413456d6355e7f774d3", null ],
    [ "toDateTime", "classUaVariant.html#a97fc7bc323150467a9c4aff01f4307cd", null ],
    [ "toDateTimeArray", "classUaVariant.html#a3208cc95918857b9a5715f11b1cca45b", null ],
    [ "toDateTimeMatrix", "classUaVariant.html#a3e6251c349e1acb381e2855ec467d97b", null ],
    [ "toDouble", "classUaVariant.html#adcba612c1581da8bf66e10466e91616a", null ],
    [ "toDoubleArray", "classUaVariant.html#a26cb0e474941a8f61004f0cdcfb3d380", null ],
    [ "toDoubleMatrix", "classUaVariant.html#aa9fb9cab5fc96951be0b32286023705b", null ],
    [ "toExpandedNodeId", "classUaVariant.html#af365e39133f9edbce18e5c26bb52d170", null ],
    [ "toExpandedNodeIdArray", "classUaVariant.html#a71857d7f3a3a60da3be0f8a628dc7285", null ],
    [ "toExpandedNodeIdMatrix", "classUaVariant.html#a3f7864f69feedf19ec5fa13a9aef936e", null ],
    [ "toExtensionObject", "classUaVariant.html#a0d2f564d9bf1a7fd4b9cc92173b904ea", null ],
    [ "toExtensionObjectArray", "classUaVariant.html#a30ec7f764829aa845429ab0d1e215f49", null ],
    [ "toExtensionObjectMatrix", "classUaVariant.html#a3aa766fbcd65f3770dc36403c16095a8", null ],
    [ "toFloat", "classUaVariant.html#a50ee2c72bccf3dc665bd66c23a7c9094", null ],
    [ "toFloatArray", "classUaVariant.html#a19fb40c9fbb2bdf622c8f7c73796473a", null ],
    [ "toFloatMatrix", "classUaVariant.html#a6ab8d89c3456448aee4edab004e95e49", null ],
    [ "toFullString", "classUaVariant.html#a4a4045b77e5ae2a811ae627f65e44f02", null ],
    [ "toGuid", "classUaVariant.html#a958ad2b40d4fb8c9190d27537e5d4dcc", null ],
    [ "toGuidArray", "classUaVariant.html#afa6f335bcb31d4706638ae1bc1d97b77", null ],
    [ "toGuidMatrix", "classUaVariant.html#a7a55010983b7e30b69f44f0cade3ba33", null ],
    [ "toInt16", "classUaVariant.html#a965c33ca14b0afd514373a684b4455bb", null ],
    [ "toInt16Array", "classUaVariant.html#a09d73986fb185f334081ae2094133628", null ],
    [ "toInt16Matrix", "classUaVariant.html#a194dc3fdd1492ade82476487b8c009a9", null ],
    [ "toInt32", "classUaVariant.html#afdcdd51157c516f550021fe239fede12", null ],
    [ "toInt32Array", "classUaVariant.html#aa6b591c4cec524df9e4a7aaf9a80c797", null ],
    [ "toInt32Matrix", "classUaVariant.html#aa23ed90ff730904f073e845e608b4697", null ],
    [ "toInt64", "classUaVariant.html#a5366a0ae275f18d27cd4b4377b81fc95", null ],
    [ "toInt64Array", "classUaVariant.html#abceaaea7b404752cc68a02c0639bd685", null ],
    [ "toInt64Matrix", "classUaVariant.html#a26cedd0cece9aac54673e105c9c7fd21", null ],
    [ "toLocalizedText", "classUaVariant.html#a3d80645fa9f05a059261e4905a9e495b", null ],
    [ "toLocalizedTextArray", "classUaVariant.html#aada64cf8c1dce27ce59319f3609a0649", null ],
    [ "toLocalizedTextMatrix", "classUaVariant.html#af9e135cd4b2ef852875ed6dea9430aee", null ],
    [ "toNodeId", "classUaVariant.html#aeb698e071dba4714ce71c7c1523dfad8", null ],
    [ "toNodeIdArray", "classUaVariant.html#a1eedc528933591092cfde444a2d9dca3", null ],
    [ "toNodeIdMatrix", "classUaVariant.html#a806be5a6d1231cf768e08739178fdba9", null ],
    [ "toQualifiedName", "classUaVariant.html#a56c08bb06b6ce2bc40eea407eee9e042", null ],
    [ "toQualifiedNameArray", "classUaVariant.html#a4c42116005150bdc19c7bf4e1701e848", null ],
    [ "toQualifiedNameMatrix", "classUaVariant.html#aeb956f589fb47afd9375d547d72e6c13", null ],
    [ "toSByte", "classUaVariant.html#acbdc0c5f5482753867dcf4e70d918633", null ],
    [ "toSByteArray", "classUaVariant.html#a7e0c1e2f5a4edf6a13d217e3abeb37bd", null ],
    [ "toSByteMatrix", "classUaVariant.html#ab26403c03fa067ec499b5636400f66cc", null ],
    [ "toStatusCode", "classUaVariant.html#afc577641e34c646c69a17523d03b20a9", null ],
    [ "toStatusCodeArray", "classUaVariant.html#a827e2e797a0a977f362574d855c3b6e7", null ],
    [ "toStatusCodeMatrix", "classUaVariant.html#aacaa32d91dfe5415fabbfd54f4b19db5", null ],
    [ "toString", "classUaVariant.html#ab5f5bfd2b7d27e342b2d93940bba7d0a", null ],
    [ "toStringArray", "classUaVariant.html#a5345b1b3de738baf1c914adc60f99157", null ],
    [ "toStringMatrix", "classUaVariant.html#a895a3c6e5de2fd2b2bcde2dd39317170", null ],
    [ "toUInt16", "classUaVariant.html#a94de7bd8a2e99a71066621d4a89fd3d8", null ],
    [ "toUInt16Array", "classUaVariant.html#af6b1f85aa0380edf0e91d7fe191a2a6f", null ],
    [ "toUInt16Matrix", "classUaVariant.html#a53f5d912a5342d04f48b638088ce78f7", null ],
    [ "toUInt32", "classUaVariant.html#a85219a72a6d286e5f9966c5ea51af6a1", null ],
    [ "toUInt32Array", "classUaVariant.html#a3415639ee5b3b619eeb2ffad2eca398c", null ],
    [ "toUInt32Matrix", "classUaVariant.html#a76cccc6b0b8c5ee3d45c617019e81fcc", null ],
    [ "toUInt64", "classUaVariant.html#aa6d0efbcba54cadf0caf214795eab6b3", null ],
    [ "toUInt64Array", "classUaVariant.html#a8f60ec0bfa8dc0eea06785442d60bff0", null ],
    [ "toUInt64Matrix", "classUaVariant.html#ad28a75e360d3bc4b6423bf4a227c1032", null ],
    [ "toVariantArray", "classUaVariant.html#aa2568c510a416706a9b02f2dfbbc39ee", null ],
    [ "toVariantMatrix", "classUaVariant.html#a6252821bbbf2e8cef6fde9679cceeb31", null ],
    [ "toXmlElement", "classUaVariant.html#a422ed899ac342cc388f105c06df5f8a1", null ],
    [ "toXmlElementArray", "classUaVariant.html#a975233a0e470ec4f7bedfbc6f50c53b7", null ],
    [ "toXmlElementMatrix", "classUaVariant.html#afa398554723096c8d23b42dd38da3317", null ],
    [ "type", "classUaVariant.html#a783d2d70212baab33e4a726e7f8ed705", null ]
];