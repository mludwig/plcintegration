var structOpcUa__HistoryUpdateResult =
[
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga0014659bf4d0b77e3e0105983f7a024d", null ],
    [ "OperationResults", "group__UaStackTypes.html#gaff6d4a4ac7aaee3cc67427d39948fc04", null ],
    [ "StatusCode", "group__UaStackTypes.html#gaeb1d00907ddabfc3f09205c4f6e3217b", null ]
];