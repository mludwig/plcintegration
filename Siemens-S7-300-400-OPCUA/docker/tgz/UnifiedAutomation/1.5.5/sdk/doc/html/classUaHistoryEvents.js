var classUaHistoryEvents =
[
    [ "UaHistoryEvents", "classUaHistoryEvents.html#a8d8bc1b9522c227f0bbd50e13b544019", null ],
    [ "UaHistoryEvents", "classUaHistoryEvents.html#a6971c7968246c5707c8b8ad22895bf4c", null ],
    [ "UaHistoryEvents", "classUaHistoryEvents.html#a5893175d63b6f5b76788f930340c5919", null ],
    [ "~UaHistoryEvents", "classUaHistoryEvents.html#a7bdfe2e4438c2a4ed5c9b9e0eba7e28d", null ],
    [ "attach", "classUaHistoryEvents.html#a621379d7b8e4b91d993d11b0035bfcc5", null ],
    [ "attach", "classUaHistoryEvents.html#a625c30d2a992f9daefb5104c7732766d", null ],
    [ "clear", "classUaHistoryEvents.html#a208cb8b4ee0065fa93669d496e720ac1", null ],
    [ "create", "classUaHistoryEvents.html#a5a57d3b5feded44c8cf17afa7dfd5e95", null ],
    [ "detach", "classUaHistoryEvents.html#a0897f557a82dce5e1ed855e4d48e7f0f", null ],
    [ "operator!=", "classUaHistoryEvents.html#af8045e4b1e12e78b03ce3326a982baed", null ],
    [ "operator=", "classUaHistoryEvents.html#ae4f618fe6f79701d84993eb033f1d5c6", null ],
    [ "operator==", "classUaHistoryEvents.html#aa8b8ac2e73c0ce1cd29cd3dfaa278af7", null ],
    [ "operator[]", "classUaHistoryEvents.html#a96fa57d6b91a152c889bc7488e9b1fb8", null ],
    [ "operator[]", "classUaHistoryEvents.html#a6d6b320493e37639ddbca76cf361564c", null ],
    [ "resize", "classUaHistoryEvents.html#aaaf1c31fbc55e4bfbe6b50a7cab28abb", null ]
];