var structOpcUa__SetTriggeringResponse =
[
    [ "AddDiagnosticInfos", "group__UaStackTypes.html#gabb11dd8a000783c4c4eef964ebddc28b", null ],
    [ "AddResults", "group__UaStackTypes.html#gac884fa18a528af12be51d9af1ccbb9cd", null ],
    [ "RemoveDiagnosticInfos", "group__UaStackTypes.html#ga588fb6a6bbcf3c57de61a95317939e24", null ],
    [ "RemoveResults", "group__UaStackTypes.html#gac6242fdbd1f2e74b6e95757cf918f476", null ]
];