var structOpcUa__ResponseHeader =
[
    [ "AdditionalHeader", "group__UaStackTypes.html#gae7789bdc5f75288cafbc06519e8b360d", null ],
    [ "RequestHandle", "group__UaStackTypes.html#ga829cb1c02924069c3672b7d2247b99fd", null ],
    [ "ServiceDiagnostics", "group__UaStackTypes.html#ga7e47989dc5af605e03cc5f846a0355f5", null ],
    [ "ServiceResult", "group__UaStackTypes.html#ga636c7bb1f22292d9bc5704b83ca55d83", null ],
    [ "StringTable", "group__UaStackTypes.html#ga7d39e71aa3cb0a95b599020c62a69d46", null ],
    [ "Timestamp", "group__UaStackTypes.html#ga5325f74d60c89cb66ff1d2ed0e3584bc", null ]
];