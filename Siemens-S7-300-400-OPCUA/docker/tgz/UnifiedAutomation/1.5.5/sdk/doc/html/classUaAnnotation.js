var classUaAnnotation =
[
    [ "UaAnnotation", "classUaAnnotation.html#abd8cc58b2e3bd9ea0f708f4e9671487e", null ],
    [ "UaAnnotation", "classUaAnnotation.html#a4d0f554984004410ac886776c2836c13", null ],
    [ "UaAnnotation", "classUaAnnotation.html#aa7fcac04322d82279c6ce8ee826677e3", null ],
    [ "UaAnnotation", "classUaAnnotation.html#a0993a66babe3fd32dcb9931034cd6073", null ],
    [ "UaAnnotation", "classUaAnnotation.html#ace4037947af18f8a2c2946caf834cc81", null ],
    [ "UaAnnotation", "classUaAnnotation.html#ad7235d78e5c74e36992d69658c6475f3", null ],
    [ "~UaAnnotation", "classUaAnnotation.html#a11e225f1d8449195c64a06f81ad28032", null ],
    [ "attach", "classUaAnnotation.html#a410c7fca505d5773578ed934305ff546", null ],
    [ "clear", "classUaAnnotation.html#ac1e6f21cc1688e50f9e368a5c1a1af80", null ],
    [ "copy", "classUaAnnotation.html#a7978fb15a9c29e501b6112ea27789e3d", null ],
    [ "copyTo", "classUaAnnotation.html#a99aee2a40a2fdde30f8284416d018164", null ],
    [ "detach", "classUaAnnotation.html#aac6a379789fa73537656b4d9736843ce", null ],
    [ "getAnnotationTime", "classUaAnnotation.html#a5ec50a7c2999ab40daf04955d0e0e450", null ],
    [ "getMessage", "classUaAnnotation.html#a542d623727835d2cef71200917701845", null ],
    [ "getUserName", "classUaAnnotation.html#aff1253b12dbf4815df8d8aaee236417a", null ],
    [ "operator!=", "classUaAnnotation.html#a7cdb914c93614421f85e5b7c818484e5", null ],
    [ "operator=", "classUaAnnotation.html#ababb938c458905879e736e17e2616279", null ],
    [ "operator==", "classUaAnnotation.html#ac712c81a052309694ff30f94545cf235", null ],
    [ "setAnnotationTime", "classUaAnnotation.html#ae0dc393e30ca479b2685a2cb0bc1c928", null ],
    [ "setMessage", "classUaAnnotation.html#a4baa33049c547d4b34594229544cbecf", null ],
    [ "setUserName", "classUaAnnotation.html#ab1bfe99ed46df3d82832dc71f7d0fc61", null ]
];