var L2TutorialGettingStarted =
[
    [ "LibraryOverview", "L3GettingStarted_LibraryOverview.html", null ],
    [ "Lesson 1: Setting up a Basic OPC UA Server Console Application", "L3GettingStartedLesson01.html", [
      [ "Utilities Used in this Lesson", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Utilities", null ],
      [ "Step 1: Create a New Project", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step01", null ],
      [ "Step 2: Add Files to the Project", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step02", [
        [ "Create the main function in servermain.cpp", "L3GettingStartedLesson01.html#gsl01st02_01", null ],
        [ "Initializing UA Stack and XML Parser", "L3GettingStartedLesson01.html#gsl01st02_02", null ],
        [ "Create OPC UA Server", "L3GettingStartedLesson01.html#gsl01st02_03", null ],
        [ "Shut Down OPC UA Server", "L3GettingStartedLesson01.html#gsl01st02_04", null ]
      ] ],
      [ "Step 3: Add Include Directories", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step03", null ],
      [ "Step 4: Add Linker Settings", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step04", null ],
      [ "Step 5: Add Preprocessor Defines", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step05", null ],
      [ "Step 6: Set Output Path", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step06", null ],
      [ "Step 7: Run Application", "L3GettingStartedLesson01.html#L4GettingStartedLesson01_Step07", null ]
    ] ],
    [ "Lesson 2: Extending the Address Space with Real World Data", "L3GettingStartedLesson02.html", [
      [ "Preliminary Note", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_PreliminaryNote", null ],
      [ "Step 1: Creating a New NodeManager", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step01", [
        [ "NmBuildingAutomation Class Definition", "L3GettingStartedLesson02.html#Less02NmBuildingAutomation_Cls_Def", null ],
        [ "NmBuildingAutomation Class Implementation", "L3GettingStartedLesson02.html#Less02NmBuildingAutomation_Cls_Impl", null ]
      ] ],
      [ "Step 2: Integrating the New NodeManger into the UA Server", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step02", null ],
      [ "Step 3: Creating the ControllerObject Class", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step03", [
        [ "ControllerObject Class Definition", "L3GettingStartedLesson02.html#Less02ControllerObject_Cls_Def", null ],
        [ "ControllerObject Class Implementation", "L3GettingStartedLesson02.html#Less02ControllerObject_Cls_Impl", null ]
      ] ],
      [ "Step 4: Creating the AirConditionerControllerObject", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step04", [
        [ "AirConditionerControllerObject Class Definition", "L3GettingStartedLesson02.html#Less02AirConditionerControllerObject_Cls_Def", null ],
        [ "AirConditionerControllerObject Class Implementation", "L3GettingStartedLesson02.html#Less02AirConditionerControllerObject_Cls_Impl", null ]
      ] ],
      [ "Step 5: Creating the FurnaceControllerObject", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step05", null ],
      [ "Step 6: Creating Controller Objects", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step06", null ],
      [ "Step 7: Run Application", "L3GettingStartedLesson02.html#L4GettingStartedLesson02_Step07", null ]
    ] ],
    [ "Lesson 3: Connecting the Nodes to Real Time Data", "L3GettingStartedLesson03.html", [
      [ "Step 1: Introducing BaUserData", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step01", [
        [ "UserDataBase Class Definition", "L3GettingStartedLesson03.html#Less03UserDataBase_Cls_Def", null ]
      ] ],
      [ "Step 2: Use of BaUserData in Controller Classes", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step02", null ],
      [ "Step 3: Implementing IOManagerUaNode Functionality", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step03", [
        [ "NmBuildingAutomation Class Definition", "L3GettingStartedLesson03.html#Less03NmBuildingAutomation_Cls_Def", null ],
        [ "NmBuildingAutomation Class Implementation", "L3GettingStartedLesson03.html#Less03NmBuildingAutomation_Cls_Implementation", null ]
      ] ],
      [ "Step 4: Introducing BaCommunicationInterface", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step04", null ],
      [ "Step 5: Implementing NmBuildingAutomation::readValues() for Non-State Variables", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step05", null ],
      [ "Step 6: Creating Devices Based on BaCommunicationInterface Information", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step06", null ],
      [ "Step 7: Extending NmBuildingAutomation::readValues() to Access State Variables", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step07", null ],
      [ "Step 8: Implementing NmBuildingAutomation::writeValues()", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step08", null ],
      [ "Step 9: Run Application", "L3GettingStartedLesson03.html#L4GettingStartedLesson03_Step09", null ]
    ] ],
    [ "Lesson 4: Adding Support for Methods", "L3GettingStartedLesson04.html", [
      [ "Overview", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Overview", null ],
      [ "Step 1: Add Methods to Object Type", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Step01", [
        [ "Creating the InstanceDeclaration", "L3GettingStartedLesson04.html#Less04Creating_InstDecl", null ]
      ] ],
      [ "Step 2: Add Methods to Object Instances", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Step02", [
        [ "Continuing the implementation of ControllerObject::ControllerObject", "L3GettingStartedLesson04.html#Less04Cont_impl", null ]
      ] ],
      [ "Step 3: Implementing the Handling of the MethodManager", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Step03", [
        [ "getMethodHandle()", "L3GettingStartedLesson04.html#Less04getMethHandle", null ],
        [ "Provide MethodManager", "L3GettingStartedLesson04.html#Less04Prov_MethodMan", null ],
        [ "Method invocation", "L3GettingStartedLesson04.html#Less04Method_Inv", null ],
        [ "Completing First Edition of beginCall()", "L3GettingStartedLesson04.html#Less04Compl_1stEd_beginCall", null ]
      ] ],
      [ "Step 4: Calling Stop Method with UA client", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Step04", null ],
      [ "Step 5: Creating a Method Having Arguments", "L3GettingStartedLesson04.html#L4GettingStartedLesson04_Step05", [
        [ "Preparations", "L3GettingStartedLesson04.html#Less04Prep", null ],
        [ "Create InstanceDeclarations", "L3GettingStartedLesson04.html#Less04Create_InstDecl", null ],
        [ "Create Methods as Components of Object Instances", "L3GettingStartedLesson04.html#Less04Create_meth_comp_obj_inst", null ],
        [ "Complete call() Implementation in Sub Classes", "L3GettingStartedLesson04.html#Less04Compl_call_impl_sub_clses", null ],
        [ "Call StartWithSetpoint with UaExpert", "L3GettingStartedLesson04.html#Less04Call_StartWithSetpoint_w_Expert", null ]
      ] ]
    ] ],
    [ "Lesson 5: Adding Support for Events", "L3GettingStartedLesson05.html", [
      [ "Step 1: Adding ControllerEventType", "L3GettingStartedLesson05.html#L4GettingStartedLesson05_Step01", [
        [ "Create the new ObjectType", "L3GettingStartedLesson05.html#Less05Create_new_ObjType", null ],
        [ "Indicate that Controllers Fire Events", "L3GettingStartedLesson05.html#Less05Ind_Controllers_fire_events", null ]
      ] ],
      [ "Step 2: Implementing ControllerEventType", "L3GettingStartedLesson05.html#L4GettingStartedLesson05_Step02", [
        [ "Define Class BaEventData", "L3GettingStartedLesson05.html#Less05Def_cls_BaEventData", null ],
        [ "Implement Class BaEventData", "L3GettingStartedLesson05.html#Less05Impl_cls_BaEventData", null ]
      ] ],
      [ "Step 3: Implementing Handling of EventManager", "L3GettingStartedLesson05.html#L4GettingStartedLesson05_Step03", [
        [ "Implement Event Trigger", "L3GettingStartedLesson05.html#Less05Impl_Ev_trig", null ]
      ] ],
      [ "Step 4: Trigger and Receive Events with UaExpert", "L3GettingStartedLesson05.html#L4GettingStartedLesson05_Step04", null ]
    ] ],
    [ "Lesson 6: Adding Support for Alarms & Conditions", "L3GettingStartedLesson06.html", [
      [ "Step 1: Adding Alarm Instance Declaration to ControllerType", "L3GettingStartedLesson06.html#L4GettingStartedLesson06_Step01", null ],
      [ "Step 2: Adding Alarm to Controller object", "L3GettingStartedLesson06.html#L4GettingStartedLesson06_Step02", null ],
      [ "Step 3: Creating Full Event Hierarchy", "L3GettingStartedLesson06.html#L4GettingStartedLesson06_Step03", null ],
      [ "Step 4: Trigger State Change Events", "L3GettingStartedLesson06.html#L4GettingStartedLesson06_Step04", null ],
      [ "Step 5: Alarm Acknowledgment", "L3GettingStartedLesson06.html#L4GettingStartedLesson06_Step05", null ]
    ] ],
    [ "Lesson 7: Adding Support for Historical Data Access", "L3GettingStartedLesson07.html", [
      [ "Step 1: Creating a HistoryManager", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step01", null ],
      [ "Step 2: Integrating HistoryManager into NodeManager", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step02", null ],
      [ "Step 3: Changes in the Information Model", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step03", null ],
      [ "Step 4: Implement Internal Historizing", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step04", null ],
      [ "Step 5: Implement History Read Raw", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step05", null ],
      [ "Step 6: Test History Read with UaExpert", "L3GettingStartedLesson07.html#L4GettingStartedLesson07_Step06", null ]
    ] ]
];