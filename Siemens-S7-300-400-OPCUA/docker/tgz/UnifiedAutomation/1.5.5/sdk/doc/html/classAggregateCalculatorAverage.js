var classAggregateCalculatorAverage =
[
    [ "AggregateCalculatorAverage", "classAggregateCalculatorAverage.html#a0aff4a9af013568f61f3aa57e23043ac", null ],
    [ "ComputeAverage", "classAggregateCalculatorAverage.html#af9676b39dcbf6c0905649ba7656d4ad4", null ],
    [ "ComputeTimeAverage", "classAggregateCalculatorAverage.html#a3a4783aa60d21bc55a0bdad8c722fcdb", null ],
    [ "ComputeValue", "classAggregateCalculatorAverage.html#a53b7db0fbe02ca0f0baf9d4ef6726ef4", null ],
    [ "UsesInterpolatedBounds", "classAggregateCalculatorAverage.html#a822c29f8b0c5f08dbbb67506ef9268ac", null ]
];