var structOpcUa__AddReferencesItem =
[
    [ "IsForward", "group__UaStackTypes.html#ga64db012e5295407f05ab43e61220f483", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gad1952100df202e62664187354fc94406", null ],
    [ "SourceNodeId", "group__UaStackTypes.html#ga3fec4c07f6b09fa730e687c864d624a5", null ],
    [ "TargetNodeClass", "group__UaStackTypes.html#ga77bcf036a6e7cbc13869a7a706c5b6cb", null ],
    [ "TargetNodeId", "group__UaStackTypes.html#ga1fc7a78811ae1288e2e4e5fb4487e568", null ],
    [ "TargetServerUri", "group__UaStackTypes.html#gaa250e533a12ffcf5b654f2ae7f793b59", null ]
];