var classUaClientSdk_1_1UaClient =
[
    [ "ConnectServiceType", "group__UaClientLibraryHelper.html#gaa9dcb71d5bab4f170c00684102029d48", [
      [ "CertificateValidation", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a796ffefa3b9416b680a3c1b2436f8232", null ],
      [ "OpenSecureChannel", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48ad27febe76244232b5c30f4d5bf147200", null ],
      [ "CreateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48a31fca26b8ce505e46f2facbe96980813", null ],
      [ "ActivateSession", "group__UaClientLibraryHelper.html#ggaa9dcb71d5bab4f170c00684102029d48afc609ea75dc746e59427ab7b26569baf", null ]
    ] ],
    [ "ReadTypeDictionaries", "group__UaClientLibraryHelper.html#ga1e8eec334edb5b41e70294a5aa2eb795", [
      [ "ReadTypeDictionaries_FirstUse", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a1e91b48ccf2e6d99f398b558bdeb72f2", null ],
      [ "ReadTypeDictionaries_Manual", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a41235ca2d748879955e1a1ad8ccda48f", null ],
      [ "ReadTypeDictionaries_Connect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795a019ddec14445f37c8bd7e5b9fd81e118", null ],
      [ "ReadTypeDictionaries_Reconnect", "group__UaClientLibraryHelper.html#gga1e8eec334edb5b41e70294a5aa2eb795aefd27a02f34a7b519b923ba24b87d046", null ]
    ] ],
    [ "ServerStatus", "group__UaClientLibraryHelper.html#ga3f55bd54bbf50515656f2b9ab621dc7f", [
      [ "Disconnected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa5e1921caf43ec40c27e70de227f99be2", null ],
      [ "Connected", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa3bf419f6fab7bc01b0f5b54877def6bb", null ],
      [ "ConnectionWarningWatchdogTimeout", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa24e8d658c496bbdd3130ee7f609527bd", null ],
      [ "ConnectionErrorApiReconnect", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa59e55a2bc03496fa163db95406ec78ba", null ],
      [ "ServerShutdown", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fa6246b2e6f2f391874978b57880c030ef", null ],
      [ "NewSessionCreated", "group__UaClientLibraryHelper.html#gga3f55bd54bbf50515656f2b9ab621dc7fab9c69648d5ea27308f906828d9828043", null ]
    ] ]
];