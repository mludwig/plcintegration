var structOpcUa__RelativePathElement =
[
    [ "IncludeSubtypes", "group__UaStackTypes.html#ga13f9eb134196c6b690d82cd3dbb529a7", null ],
    [ "IsInverse", "group__UaStackTypes.html#ga688aabda7c10c0d05afe68a65a9ae33d", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga80f1a06cd24df19a8f2b1db54581293d", null ],
    [ "TargetName", "group__UaStackTypes.html#ga71b0593e37266683e65600f26b5d5718", null ]
];