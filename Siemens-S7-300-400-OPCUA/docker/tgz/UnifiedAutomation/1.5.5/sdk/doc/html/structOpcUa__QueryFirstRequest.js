var structOpcUa__QueryFirstRequest =
[
    [ "Filter", "group__UaStackTypes.html#ga293c50d434bdedfcee9d3ce9fabec68c", null ],
    [ "MaxDataSetsToReturn", "group__UaStackTypes.html#ga3bb2516fab83cab597be7ab2644dbdbf", null ],
    [ "MaxReferencesToReturn", "group__UaStackTypes.html#ga62e2db056ffb9b4ae499eae48ffa2803", null ],
    [ "NodeTypes", "group__UaStackTypes.html#ga56204e0a36365890c8eab2d52700dbda", null ],
    [ "View", "group__UaStackTypes.html#ga168653f0e90a93b7298e436df937bbf6", null ]
];