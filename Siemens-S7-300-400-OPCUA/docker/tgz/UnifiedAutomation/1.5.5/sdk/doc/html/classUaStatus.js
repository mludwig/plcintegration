var classUaStatus =
[
    [ "UaStatus", "classUaStatus.html#a0edf61f3ee9d9c15597e486ab012f5cb", null ],
    [ "UaStatus", "classUaStatus.html#a538054f78d42219409116cd6c1243aab", null ],
    [ "UaStatus", "classUaStatus.html#ada0ce8e3445da8540d9de3977cd7a231", null ],
    [ "UaStatus", "classUaStatus.html#a5757becfcfaa1e74113943705de65ec1", null ],
    [ "~UaStatus", "classUaStatus.html#a627fe865e2f80a9f116e5b5c19d52068", null ],
    [ "operator=", "classUaStatus.html#ae4ba4befc86c81d2dba6140792fc0b21", null ],
    [ "operator=", "classUaStatus.html#aae746a9e35d17170785a6f7880fc5fab", null ],
    [ "operator=", "classUaStatus.html#a4be109c554d09dc29c793039781cb90d", null ],
    [ "pDiagnosticInfo", "classUaStatus.html#aeaed2f497f71edf91a5370b48aac4345", null ],
    [ "setAdditionalInfo", "classUaStatus.html#a25756e9ebab488a7df85a0f6d6cca6d4", null ],
    [ "setDiagnosticInfo", "classUaStatus.html#aa4c8cadd7fcb3120a7eb96f56dd3b4a5", null ],
    [ "setDiagnosticInfo", "classUaStatus.html#aa6e7f9a2c97c906730080052ab682751", null ],
    [ "setDiagnosticInfo", "classUaStatus.html#a811f64024a720b2462e95fcdfd9e0d8c", null ],
    [ "setInnerStatus", "classUaStatus.html#afbcbd2c2608ffcfae457929ec9d15e78", null ],
    [ "setStatus", "classUaStatus.html#aa6bc5b7d901a38dd87930c3a9975caa4", null ]
];