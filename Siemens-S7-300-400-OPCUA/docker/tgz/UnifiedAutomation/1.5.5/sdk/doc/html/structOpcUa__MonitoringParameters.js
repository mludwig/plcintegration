var structOpcUa__MonitoringParameters =
[
    [ "ClientHandle", "group__UaStackTypes.html#ga833034d9d80179942078f06bc2af005e", null ],
    [ "DiscardOldest", "group__UaStackTypes.html#ga0f150bc7069b71cb659847ccddf90ef1", null ],
    [ "Filter", "group__UaStackTypes.html#gab0e83fe4687f84a1a855d779f9415427", null ],
    [ "QueueSize", "group__UaStackTypes.html#ga686fbf5f5c05abe85759d18fab0c3575", null ],
    [ "SamplingInterval", "group__UaStackTypes.html#ga6c5850dbff0e4b6587d77200db1b3aa8", null ]
];