var L2ServerSdkSecurity =
[
    [ "Application Authentication", "L2ServerSdkSecurity.html#L3ServerSdkSecurityAA", null ],
    [ "Endpoint Security Settings", "L2ServerSdkSecurity.html#L3ServerSdkSecurityESS", null ],
    [ "User Authentication and Authorization", "L3ServerSdkSecurityUA.html", [
      [ "User Authentication", "L3ServerSdkSecurityUA.html#L4ServerSdkUserAuthentication", null ],
      [ "Configuration of User Identity Types in the Server", "L3ServerSdkSecurityUA.html#L4ServerSdkSecurityUAConfig", null ],
      [ "User Authorization", "L3ServerSdkSecurityUA.html#L4ServerSdkUserAuthorization", null ],
      [ "Session Object as Key to User Authentication and Authorization in the Server", "L3ServerSdkSecurityUA.html#L4ServerSdkSecurityUASession", null ]
    ] ]
];