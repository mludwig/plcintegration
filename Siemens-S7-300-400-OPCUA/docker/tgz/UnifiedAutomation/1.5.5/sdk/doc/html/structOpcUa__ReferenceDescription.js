var structOpcUa__ReferenceDescription =
[
    [ "BrowseName", "group__UaStackTypes.html#gafb407977ec3bb60572b27676c5745fcb", null ],
    [ "DisplayName", "group__UaStackTypes.html#gac41a85d693339109fbee5373551fa89d", null ],
    [ "IsForward", "group__UaStackTypes.html#ga199f8c6092049025bff132438f7417a4", null ],
    [ "NodeClass", "group__UaStackTypes.html#ga2982ff6bbca7ff9a84cb3c9fb90d7169", null ],
    [ "NodeId", "group__UaStackTypes.html#gab4633e15c852ce3059e2a28fd6ffcf00", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gad17758f38c3913dde4847006f498a341", null ],
    [ "TypeDefinition", "group__UaStackTypes.html#gacfc9614b093ae2cd1fa085630626f7fd", null ]
];