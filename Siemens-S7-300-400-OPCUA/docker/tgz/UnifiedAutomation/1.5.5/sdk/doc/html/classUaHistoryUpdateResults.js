var classUaHistoryUpdateResults =
[
    [ "UaHistoryUpdateResults", "classUaHistoryUpdateResults.html#a06018208fa486c8a6bac93c7eaf8c57c", null ],
    [ "UaHistoryUpdateResults", "classUaHistoryUpdateResults.html#aad642ff05cd622064d8f90c2b03c838c", null ],
    [ "UaHistoryUpdateResults", "classUaHistoryUpdateResults.html#a206c018c428e39c0e78e334d4f3fad24", null ],
    [ "~UaHistoryUpdateResults", "classUaHistoryUpdateResults.html#aae4ae7196159c6e364ab5d3023df3d83", null ],
    [ "attach", "classUaHistoryUpdateResults.html#a38b55bb30f97655f17f6faaaa3a26221", null ],
    [ "attach", "classUaHistoryUpdateResults.html#a70a308b9e1cd3895ef45e690be338f64", null ],
    [ "clear", "classUaHistoryUpdateResults.html#a872c669303221f96f0661caf7e9b4240", null ],
    [ "create", "classUaHistoryUpdateResults.html#a2ff72ec3b3d525afccedadc6e234cd8f", null ],
    [ "detach", "classUaHistoryUpdateResults.html#a4627b6a31eefdadbe8879513781ecf05", null ],
    [ "operator!=", "classUaHistoryUpdateResults.html#a76826d5fe2daca669c7d8781dafe0d53", null ],
    [ "operator=", "classUaHistoryUpdateResults.html#a2bb8809b2fda964a873bc2a83109c8b6", null ],
    [ "operator==", "classUaHistoryUpdateResults.html#a2f6885c069c310dbbaed6d9eed1235d4", null ],
    [ "operator[]", "classUaHistoryUpdateResults.html#a3c63448b380450bdac9399d20818f6f6", null ],
    [ "operator[]", "classUaHistoryUpdateResults.html#a1c78e9b0c9f93837e95a2a2c4b43273d", null ],
    [ "resize", "classUaHistoryUpdateResults.html#a39e9ac611488a38d1bd27ffb4cabb4cf", null ]
];