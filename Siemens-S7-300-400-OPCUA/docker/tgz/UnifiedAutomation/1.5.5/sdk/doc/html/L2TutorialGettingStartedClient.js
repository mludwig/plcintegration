var L2TutorialGettingStartedClient =
[
    [ "Lesson 1: Connect and Read", "L3GettingStartedClientLesson01.html", [
      [ "Step 1: Create New Project", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step01", [
        [ "Set up a console application.", "L3GettingStartedClientLesson01.html#L5GettingStartedClientLesson01_Step01a", null ],
        [ "Add Files to the Project", "L3GettingStartedClientLesson01.html#L5GettingStartedClientLesson01_Step01b", null ],
        [ "Add Include Directories", "L3GettingStartedClientLesson01.html#L5GettingStartedClientLesson01_Step01c", null ],
        [ "Add Linker Settings", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step01d", null ],
        [ "Add Preprocessor Defines", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step01e", null ],
        [ "Set Output Path", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step01f", null ]
      ] ],
      [ "Step 2: Introducing UaSession and UaSessionCallback", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step02", null ],
      [ "Step 3: Initializing UA Stack and Create Instance of SampleClient", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step03", null ],
      [ "Step 4: Connect and Disconnect", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step04", [
        [ "Add connect()", "L3GettingStartedClientLesson01.html#CLess02S5Sub1", null ],
        [ "Add disconnect()", "L3GettingStartedClientLesson01.html#CLess02S5Sub2", null ]
      ] ],
      [ "Step 5: Read", "L3GettingStartedClientLesson01.html#L4GettingStartedClientLesson01_Step05", null ]
    ] ],
    [ "Lesson 2: Create DataMonitoredItem", "L3GettingStartedClientLesson02.html", [
      [ "Step 1: Introducing UaSubscription and UaSubscriptionCallback", "L3GettingStartedClientLesson02.html#L4GettingStartedClientLesson02Step01", null ],
      [ "Step 2: Creating and Deleting a Subscription", "L3GettingStartedClientLesson02.html#L4GettingStartedClientLesson02Step02", null ],
      [ "Step 3: Adding MonitoredItems to a Subscription", "L3GettingStartedClientLesson02.html#L4GettingStartedClientLesson02Step03", null ]
    ] ],
    [ "Lesson 3: Browse, Read Configuration, and Write Values", "L3GettingStartedClientLesson03.html", [
      [ "Step 1: Browse for Nodes in the Address Space", "L3GettingStartedClientLesson03.html#L4GettingStartedClientLesson03Step01", null ],
      [ "Step 2: Providing Connection Parameters and NodeIds Using a Configuration Object", "L3GettingStartedClientLesson03.html#L4GettingStartedClientLesson03Step02", null ],
      [ "Step 3: Read namespace table", "L3GettingStartedClientLesson03.html#L4GettingStartedClientLesson03Step03", null ],
      [ "Step 4: Enhance read Method to Use NodeIds from Configuration Object", "L3GettingStartedClientLesson03.html#L4GettingStartedClientLesson03Step04", null ],
      [ "Step 5: Writing a Value", "L3GettingStartedClientLesson03.html#L4GettingStartedClientLesson03Step05", null ]
    ] ],
    [ "Lesson 4: Discovery and Secure Connection", "L3GettingStartedClientLesson04.html", [
      [ "Step 1: Collect Information about the Server Using UaDiscovery::findServers, UaDiscovery::getEndpoints", "L3GettingStartedClientLesson04.html#L4GettingStartedClientLesson04_Step01", null ],
      [ "Step 2: Create and Load an Application Instance Certificate", "L3GettingStartedClientLesson04.html#L4GettingStartedClientLesson04_Step02", null ],
      [ "Step 3: Set up a Secure Connection", "L3GettingStartedClientLesson04.html#L4GettingStartedClientLesson04_Step03", null ]
    ] ],
    [ "Lesson 5: Register Nodes and Reconnection Scenarios", "L3GettingStartedClientLesson05.html", [
      [ "Step 1: Call registerNodes to Get Optimised NodeIds and Write Values Using the Optimised NodeIds", "L3GettingStartedClientLesson05.html#L4GettingStartedClientLesson05_Step01", null ],
      [ "Step 2: Read List of Monitored Items from Configuration", "L3GettingStartedClientLesson05.html#L4GettingStartedClientLesson05_Step02", null ],
      [ "Step 3: Enhance Subscription to Automatically Handle Reconnection Scenarios", "L3GettingStartedClientLesson05.html#L4GettingStartedClientLesson05_Step03", null ]
    ] ],
    [ "Lesson 6: Create EventMonitoredItem", "L3GettingStartedClientLesson06.html", [
      [ "Step 1: Create a Subscription with a Monitored Item", "L3GettingStartedClientLesson06.html#L4GettingStartedClientLesson06_Step01", null ],
      [ "Step 2: Set an Event Filter for an Event Monitored Item", "L3GettingStartedClientLesson06.html#L4GettingStartedClientLesson06_Step02", null ],
      [ "Step 3: Receive Event Notifications via UaSubscriptionCallback::newEvents()", "L3GettingStartedClientLesson06.html#L4GettingStartedClientLesson06_Step03", null ],
      [ "Step 4: Call Methods to Trigger Events on Demoserver", "L3GettingStartedClientLesson06.html#L4GettingStartedClientLesson06_Step04", null ]
    ] ]
];