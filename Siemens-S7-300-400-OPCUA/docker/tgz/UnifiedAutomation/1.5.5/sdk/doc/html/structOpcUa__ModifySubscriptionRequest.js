var structOpcUa__ModifySubscriptionRequest =
[
    [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga2c7578f38198ade678a61345ab6efa0c", null ],
    [ "Priority", "group__UaStackTypes.html#ga7dafc433b65d20ed608cfe42f4e58cde", null ],
    [ "RequestedLifetimeCount", "group__UaStackTypes.html#gaceac41f2689b98cb2b9619542112cdd9", null ],
    [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#ga4aec23ac2c0ee309c107d4ca34a5c190", null ],
    [ "RequestedPublishingInterval", "group__UaStackTypes.html#gaff0c84c498961f401c111522f6eaea5c", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga647508fdabfaec213392dfa83579717b", null ]
];