var classOpcUa_1_1AuditAddReferencesEventTypeData =
[
    [ "AuditAddReferencesEventTypeData", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#a919fe6d0d05806c7e7be8dc826af9089", null ],
    [ "~AuditAddReferencesEventTypeData", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#ae7899540179005a0a5442697c2d2c484", null ],
    [ "getFieldData", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#a03656a53612cc8f591bebd4031f20e31", null ],
    [ "getReferencesToAdd", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#aff60f62d9329079ebe6f0d7bd6e7f37f", null ],
    [ "getReferencesToAddValue", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#af88261f49b4a0a19ce4edb9a0251dfbd", null ],
    [ "setReferencesToAdd", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#a3f67859fbb97c8d24e1e4e13a8b6e7af", null ],
    [ "setReferencesToAddStatus", "classOpcUa_1_1AuditAddReferencesEventTypeData.html#a04c8bc3dc5e1e52e6ebdb075a7b206ed", null ]
];