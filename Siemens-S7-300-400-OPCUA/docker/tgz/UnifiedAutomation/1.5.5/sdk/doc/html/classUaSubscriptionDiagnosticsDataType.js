var classUaSubscriptionDiagnosticsDataType =
[
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#a2b1056cdbe43831fb8af7ba210bfc648", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#a983c5ffd713fc87dc4b43c09b31e0f96", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#a359568d72a81e8e654cf2cb7d9e7acab", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#a7a7eb497437f4354037d3e95c4604454", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#a68509ef93c64c223434d896405fcdf3e", null ],
    [ "UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#aa194d75a75eeda0cae6e9be3aaeaca6a", null ],
    [ "~UaSubscriptionDiagnosticsDataType", "classUaSubscriptionDiagnosticsDataType.html#adde21947f76467713deea9c8e475a814", null ],
    [ "attach", "classUaSubscriptionDiagnosticsDataType.html#aaf2b688fe3ebde7c19842aaff1025989", null ],
    [ "clear", "classUaSubscriptionDiagnosticsDataType.html#aaa231d36eca4cf84832bc94695b462c4", null ],
    [ "copy", "classUaSubscriptionDiagnosticsDataType.html#af5641f1bff41477ccdc84830dabe31f0", null ],
    [ "copyTo", "classUaSubscriptionDiagnosticsDataType.html#a5db0116cb4f953306e34cbe8d046a164", null ],
    [ "detach", "classUaSubscriptionDiagnosticsDataType.html#a709c36fd10aed3a1ec31e690e3ef5337", null ],
    [ "getCurrentKeepAliveCount", "classUaSubscriptionDiagnosticsDataType.html#a972124128f8b17489c2056c670f239a2", null ],
    [ "getCurrentLifetimeCount", "classUaSubscriptionDiagnosticsDataType.html#a74b17eaee1de3d23c9b6a0732c806f39", null ],
    [ "getDataChangeNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#a6e8b3092648c854bd2d075322befbe74", null ],
    [ "getDisableCount", "classUaSubscriptionDiagnosticsDataType.html#ab3f030856ceaed820e3ac036d91e571c", null ],
    [ "getDisabledMonitoredItemCount", "classUaSubscriptionDiagnosticsDataType.html#a60ef403e8615838f5cdd914679c49ae1", null ],
    [ "getDiscardedMessageCount", "classUaSubscriptionDiagnosticsDataType.html#a538db8e98b7d47746b453396cff5fde2", null ],
    [ "getEnableCount", "classUaSubscriptionDiagnosticsDataType.html#af240bc78b007cdd9821d19052cee1628", null ],
    [ "getEventNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#a7245aeb96bb936225c9de6d020c370c7", null ],
    [ "getEventQueueOverFlowCount", "classUaSubscriptionDiagnosticsDataType.html#a6fe5ad712dab1c899bd3e7408e23b20c", null ],
    [ "getLatePublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#af8e1524a12f728053a358f0903e920d0", null ],
    [ "getMaxKeepAliveCount", "classUaSubscriptionDiagnosticsDataType.html#a868dc12e0b5819ad5eff0de9feddf306", null ],
    [ "getMaxLifetimeCount", "classUaSubscriptionDiagnosticsDataType.html#a90c8133f3c03b4d0eaf01ec0633af09b", null ],
    [ "getMaxNotificationsPerPublish", "classUaSubscriptionDiagnosticsDataType.html#a4889619ff61ae6eb70e72d81a4e70ae6", null ],
    [ "getModifyCount", "classUaSubscriptionDiagnosticsDataType.html#a46cf649ffafa6a408f2dc63f3b07416d", null ],
    [ "getMonitoredItemCount", "classUaSubscriptionDiagnosticsDataType.html#a17106f23a4575bbf56fc60d61a60387c", null ],
    [ "getMonitoringQueueOverflowCount", "classUaSubscriptionDiagnosticsDataType.html#a5f443c4ecc5831cc44f5418f93ebc100", null ],
    [ "getNextSequenceNumber", "classUaSubscriptionDiagnosticsDataType.html#aa30d45ae9ed7eb301e7f2104c39e032c", null ],
    [ "getNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#aa1afdad24253489c00fcdd38f5cb5e36", null ],
    [ "getPriority", "classUaSubscriptionDiagnosticsDataType.html#aa420ae11da77d8a3fe243e1fec841a6e", null ],
    [ "getPublishingEnabled", "classUaSubscriptionDiagnosticsDataType.html#acae462775617f333beceac65f788b61f", null ],
    [ "getPublishingInterval", "classUaSubscriptionDiagnosticsDataType.html#a832fd36c9ca4a7e1e17e424da9e82bb9", null ],
    [ "getPublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#af8c0360661b88449adb01456f519aeb8", null ],
    [ "getRepublishMessageCount", "classUaSubscriptionDiagnosticsDataType.html#a863578dd148e79a06a8ca8e98dead116", null ],
    [ "getRepublishMessageRequestCount", "classUaSubscriptionDiagnosticsDataType.html#acd865f682647aca2d598459363adc54d", null ],
    [ "getRepublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a18a950573fbd1c549528441b005efcd6", null ],
    [ "getSessionId", "classUaSubscriptionDiagnosticsDataType.html#a0b75ae9265772c44a19db423c2eefb29", null ],
    [ "getSubscriptionId", "classUaSubscriptionDiagnosticsDataType.html#a98da43711f3f23107c41e3a0f97c9321", null ],
    [ "getTransferredToAltClientCount", "classUaSubscriptionDiagnosticsDataType.html#a951ef41143d276c0384626003592312a", null ],
    [ "getTransferredToSameClientCount", "classUaSubscriptionDiagnosticsDataType.html#a75a756bb127fcce2d79d8e6f3485f72e", null ],
    [ "getTransferRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a97228917ca4121ba91209e3e95cc6f22", null ],
    [ "getUnacknowledgedMessageCount", "classUaSubscriptionDiagnosticsDataType.html#ad4dcb98107d22fa6ae08117ab9cd18fd", null ],
    [ "operator!=", "classUaSubscriptionDiagnosticsDataType.html#acd2c8297c84a98294b5940f4349edcea", null ],
    [ "operator=", "classUaSubscriptionDiagnosticsDataType.html#a65d9d5fb740e364087d7ba256003da24", null ],
    [ "operator==", "classUaSubscriptionDiagnosticsDataType.html#a9593d01a45344d46a17965ba28d27e5b", null ],
    [ "setCurrentKeepAliveCount", "classUaSubscriptionDiagnosticsDataType.html#a3edfaecf29f3fb15a62d69c1af6f7f5e", null ],
    [ "setCurrentLifetimeCount", "classUaSubscriptionDiagnosticsDataType.html#aaac5ecc6fea2b659e694fb1c802efdde", null ],
    [ "setDataChangeNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#a8d0093989892108ebbb42e12232c7f76", null ],
    [ "setDisableCount", "classUaSubscriptionDiagnosticsDataType.html#aff8424cd3d7e87ce1bb839ec18ce0af8", null ],
    [ "setDisabledMonitoredItemCount", "classUaSubscriptionDiagnosticsDataType.html#aca51da0eb90a8f76852480bd66f3fda8", null ],
    [ "setDiscardedMessageCount", "classUaSubscriptionDiagnosticsDataType.html#a4eb5abd7cd051fff7916bdd2b5c29d6c", null ],
    [ "setEnableCount", "classUaSubscriptionDiagnosticsDataType.html#ac406fee35c2e19000bfb80bb277c2d3a", null ],
    [ "setEventNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#a1b733c8509430ee1467be58cc8e275ec", null ],
    [ "setEventQueueOverFlowCount", "classUaSubscriptionDiagnosticsDataType.html#a99d350a7902914cb7347e6b92e21cd6d", null ],
    [ "setLatePublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a7a85c8e5d389c023da1e4d7ddffdfa6c", null ],
    [ "setMaxKeepAliveCount", "classUaSubscriptionDiagnosticsDataType.html#aee5f2317ca942f5662144e1bcc02e6db", null ],
    [ "setMaxLifetimeCount", "classUaSubscriptionDiagnosticsDataType.html#abea47c0ac74e93f6f2bbf3bfd6db2bd4", null ],
    [ "setMaxNotificationsPerPublish", "classUaSubscriptionDiagnosticsDataType.html#a8f7cfe05447fb2bde406f75ff6f9f740", null ],
    [ "setModifyCount", "classUaSubscriptionDiagnosticsDataType.html#aad4217917e77c690fa4eeded5ef2859f", null ],
    [ "setMonitoredItemCount", "classUaSubscriptionDiagnosticsDataType.html#a693122bdc3d6d37828b5669b945efb64", null ],
    [ "setMonitoringQueueOverflowCount", "classUaSubscriptionDiagnosticsDataType.html#a06016455ad904b96b0e6529a16eff436", null ],
    [ "setNextSequenceNumber", "classUaSubscriptionDiagnosticsDataType.html#a26144f77f9af2e26156168019fec47ee", null ],
    [ "setNotificationsCount", "classUaSubscriptionDiagnosticsDataType.html#a06ff6204aac467b0aaaff0d094f29330", null ],
    [ "setPriority", "classUaSubscriptionDiagnosticsDataType.html#a5e969b04b3ca3c441da0004f9a69f502", null ],
    [ "setPublishingEnabled", "classUaSubscriptionDiagnosticsDataType.html#ac709b08a223bede2ac7739591e0a2eec", null ],
    [ "setPublishingInterval", "classUaSubscriptionDiagnosticsDataType.html#a76a17fc5bc8cf4965b4fb3c86df6807e", null ],
    [ "setPublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a8f139368f97ea35c91cea275f0d65f1a", null ],
    [ "setRepublishMessageCount", "classUaSubscriptionDiagnosticsDataType.html#ae6eb88f7789097e26a26c914862d5dae", null ],
    [ "setRepublishMessageRequestCount", "classUaSubscriptionDiagnosticsDataType.html#ae0c25d03f9190968b16139e66f40a0b3", null ],
    [ "setRepublishRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a63cf385db5e15b4312c55544d0aadea9", null ],
    [ "setSessionId", "classUaSubscriptionDiagnosticsDataType.html#a5c0a9eb638ebc7c8d5ab1114e4dea8e5", null ],
    [ "setSubscriptionId", "classUaSubscriptionDiagnosticsDataType.html#a4d918ade232ff6e9caea0173fd28fa4a", null ],
    [ "setTransferredToAltClientCount", "classUaSubscriptionDiagnosticsDataType.html#a2d1dd22c15beec8f5a8238b0a78cee79", null ],
    [ "setTransferredToSameClientCount", "classUaSubscriptionDiagnosticsDataType.html#ae7b97997d800da2abb074ce9732391aa", null ],
    [ "setTransferRequestCount", "classUaSubscriptionDiagnosticsDataType.html#a206465a6ebc44d0448fd1fed187773f6", null ],
    [ "setUnacknowledgedMessageCount", "classUaSubscriptionDiagnosticsDataType.html#a0d97467619f283b315f80adbd2f8d9dc", null ]
];