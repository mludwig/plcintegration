var L1BuildInstructions =
[
    [ "Introduction", "L1BuildInstructions.html#BuildInstructionsIntro", [
      [ "Windows Platform", "L1BuildInstructions.html#BuildInstructionsIntro_win", null ],
      [ "Linux Platform", "L1BuildInstructions.html#BuildInstructionsIntro_linux", null ]
    ] ],
    [ "Configuring the SDK with CMake", "CmakeConfigOptions.html", [
      [ "Overview", "CmakeConfigOptions.html#CMakeConfigOptionsOverview", null ],
      [ "SDK Configuration", "CmakeConfigOptions.html#CMakeConfigOptionsSDK", [
        [ "Server SDK Configuration", "CmakeConfigOptions.html#CMakeConfigOptionsServerSDK", null ],
        [ "Client SDK Configuration", "CmakeConfigOptions.html#CMakeConfigOptionsClientSDK", null ]
      ] ],
      [ "UaStack", "CmakeConfigOptions.html#CMakeConfigOptionsUaStack", [
        [ "Notes on UASTACK_SOCKETMANAGER_NUMBER_OF_SOCKETS", "CmakeConfigOptions.html#UaStackSocketmanagerNumberOfSockets", null ]
      ] ]
    ] ],
    [ "Compiling Third-Party Components", "Compile3rdParty.html", "Compile3rdParty" ],
    [ "Recompiling the SDK and Examples (Linux)", "CompileSdkLinux.html", [
      [ "Compile the SDK and Examples with the delivered Bash scripts", "CompileSdkLinux.html#CompileSdkLinuxScripts", null ],
      [ "Create Makefiles for the SDK", "CompileSdkLinux.html#CompileSdkLinMakefilesSdk", null ],
      [ "Create Makefiles for the Examples", "CompileSdkLinux.html#CompileSdkLinMakefilesExamples", null ]
    ] ],
    [ "Generating Project Files and Recompiling the SDK (Windows)", "CompileSdkWin.html", "CompileSdkWin" ],
    [ "Cross Compiling the SDK", "CrossCompiling.html", [
      [ "Requirements", "CrossCompiling.html#CrossCompilingRequirements", null ],
      [ "Toolchain File", "CrossCompiling.html#CrossCompilingToolchainFile", null ],
      [ "Create Makefiles for the SDK", "CrossCompiling.html#CrossCompilingMakefiles", null ],
      [ "Create Makefiles for the Examples", "CrossCompiling.html#CrossCompilingExamples", null ]
    ] ]
];