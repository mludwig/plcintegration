var classUaMonitoredItemEvent =
[
    [ "UaMonitoredItemEvent", "classUaMonitoredItemEvent.html#ad960dee0cdf92959f76d7e14376ae499", null ],
    [ "~UaMonitoredItemEvent", "classUaMonitoredItemEvent.html#a62c7253179ab00cf478486efefa8d7b6", null ],
    [ "addEventManager", "classUaMonitoredItemEvent.html#a1c6c456147658bda5e203f212ddb800b", null ],
    [ "getEventCount", "classUaMonitoredItemEvent.html#a7d1e2ca03b0c07a14251d25b9e333b70", null ],
    [ "getEventItemHandles", "classUaMonitoredItemEvent.html#af0e694f4d25af0142bffa9a5cb06d90a", null ],
    [ "getEventItemsSubspended", "classUaMonitoredItemEvent.html#ad0d5cd35e6316e0736bf002b674f0d19", null ],
    [ "getEventManagers", "classUaMonitoredItemEvent.html#a0253ce4e195c84c18e087ebedbe69a52", null ],
    [ "hasChanged", "classUaMonitoredItemEvent.html#ab3893471a71a290133aaf995660e8bc9", null ],
    [ "invalidateEventManager", "classUaMonitoredItemEvent.html#a8e31c318f93583c08973840924919272", null ],
    [ "newEvent", "classUaMonitoredItemEvent.html#a6a2bdbd1fdd84520362293713408b78f", null ],
    [ "reset", "classUaMonitoredItemEvent.html#a50722db31f00d400b1d26a90df0d3fac", null ],
    [ "setEventFilter", "classUaMonitoredItemEvent.html#aa3c48dd674a40c15f5e1ce973235b935", null ],
    [ "setEventItemHandle", "classUaMonitoredItemEvent.html#a7cc1837c2468ae8cd2b047bbf1953d30", null ],
    [ "setEventItemSubspended", "classUaMonitoredItemEvent.html#a1eccbd33ffe360299712b222a1534b9e", null ],
    [ "setEventManagerInvalid", "classUaMonitoredItemEvent.html#ad8308c50e5ea801a645b543b5735441e", null ],
    [ "setMonitoringMode", "classUaMonitoredItemEvent.html#aee8e50d1427a67db094209b36d206297", null ],
    [ "type", "classUaMonitoredItemEvent.html#a2d2b35a7d0c09714c90d7609507e01e0", null ]
];