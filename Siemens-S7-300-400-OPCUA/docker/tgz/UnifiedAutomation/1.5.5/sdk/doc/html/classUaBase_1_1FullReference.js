var classUaBase_1_1FullReference =
[
    [ "FullReference", "classUaBase_1_1FullReference.html#a82106a2e95c0cdb37a968dca7f0c875a", null ],
    [ "FullReference", "classUaBase_1_1FullReference.html#ab62f2e432f437839b3c9181f11069a35", null ],
    [ "operator==", "classUaBase_1_1FullReference.html#ae30f79cf39f68d53ab3dc49be89fdb4d", null ],
    [ "referenceTypeId", "classUaBase_1_1FullReference.html#aad65589d4a273e82e2fe9fb7d544691c", null ],
    [ "setReferenceTypeId", "classUaBase_1_1FullReference.html#a7b3a32b4df3e4ae392941c92f4029b13", null ],
    [ "setSourceNode", "classUaBase_1_1FullReference.html#a74be0d79035d2b0766069a30f370e7ca", null ],
    [ "setTargetNode", "classUaBase_1_1FullReference.html#a2169198e5fa055a503d317a004d229ec", null ],
    [ "sourceNode", "classUaBase_1_1FullReference.html#a125674675fbc116fe53cff4f67895dad", null ],
    [ "targetNode", "classUaBase_1_1FullReference.html#a22f73a7bd8209612bf9576c69be729bf", null ]
];