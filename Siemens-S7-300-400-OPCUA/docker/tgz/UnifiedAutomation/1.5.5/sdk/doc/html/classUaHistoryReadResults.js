var classUaHistoryReadResults =
[
    [ "UaHistoryReadResults", "classUaHistoryReadResults.html#a4d1ea6119a66464600929859a3c4a17f", null ],
    [ "UaHistoryReadResults", "classUaHistoryReadResults.html#a7a8090b3fc65f07e9be76732b06fbbae", null ],
    [ "UaHistoryReadResults", "classUaHistoryReadResults.html#ae446ce657241d76953cd95832b30c574", null ],
    [ "~UaHistoryReadResults", "classUaHistoryReadResults.html#aa746c0082af3349839ae46c306b88d6c", null ],
    [ "attach", "classUaHistoryReadResults.html#a2310a3d59bb20038e2adf7c8a2a8892a", null ],
    [ "attach", "classUaHistoryReadResults.html#ade75579039827061018a98bd3ac45a94", null ],
    [ "clear", "classUaHistoryReadResults.html#a981cccb8fbb416bd88db1531de77a7a1", null ],
    [ "create", "classUaHistoryReadResults.html#a64f19f02696c7686e3ff8952595661f3", null ],
    [ "detach", "classUaHistoryReadResults.html#a0cae9e1d251d1b5f4f12272a714aee9c", null ],
    [ "operator!=", "classUaHistoryReadResults.html#af7f0c0c58adb0338f06c80c09fde2f2e", null ],
    [ "operator=", "classUaHistoryReadResults.html#a2388470dd0d0a4ca29dbaa6494a2d993", null ],
    [ "operator==", "classUaHistoryReadResults.html#a03b46b593bcee427411bd734cf9acc40", null ],
    [ "operator[]", "classUaHistoryReadResults.html#aa5e1fcb73164d9ee2ece010fea349d4b", null ],
    [ "operator[]", "classUaHistoryReadResults.html#a88710b18de0fcffc1acd389285670737", null ],
    [ "resize", "classUaHistoryReadResults.html#a21c9ff580767ec6619f9556caf654a31", null ]
];