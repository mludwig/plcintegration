var classTypeDictionaries =
[
    [ "TypeDictionaries", "classTypeDictionaries.html#a50cd0c39102711a12c93f3d245eb30c4", null ],
    [ "TypeDictionaries", "classTypeDictionaries.html#add41f1bd5e858dbbf97ecf2dd46622e0", null ],
    [ "addEnumeratedType", "classTypeDictionaries.html#aac741a1c65516b51d8ba1b34ff0601a0", null ],
    [ "addEnumeratedTypes", "classTypeDictionaries.html#a1688addc91b38cb7ac08e24e3cb93e65", null ],
    [ "addOptionSetType", "classTypeDictionaries.html#a49bb769ea72dc308ee1bc7b610aa80fd", null ],
    [ "addOptionSetTypes", "classTypeDictionaries.html#a8914beabac3aa03c3d000f282006ad25", null ],
    [ "addStructuredType", "classTypeDictionaries.html#a71ce0bf94383113f93cc09f8f5fb33eb", null ],
    [ "addStructuredTypes", "classTypeDictionaries.html#a9303c6ae745642df39820def39d451a2", null ],
    [ "addTypeDictionary", "classTypeDictionaries.html#a17de17f730b4234fc44584b08635337d", null ],
    [ "dataTypeVersion", "classTypeDictionaries.html#a8199c04e165a6e868a795bc12f391c0b", null ],
    [ "setDataTypeVersion", "classTypeDictionaries.html#a61e8b405ded59402d3f4d68356448d90", null ],
    [ "setTypeDictionary", "classTypeDictionaries.html#a89fc00e5fcbe151e7cf7f25f0cda3988", null ],
    [ "setXmlTypeDictionary", "classTypeDictionaries.html#aaddec1d80a4f84bde70c8b086768bb79", null ]
];