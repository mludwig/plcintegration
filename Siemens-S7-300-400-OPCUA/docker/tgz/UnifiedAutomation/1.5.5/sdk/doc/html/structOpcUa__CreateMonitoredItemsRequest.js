var structOpcUa__CreateMonitoredItemsRequest =
[
    [ "ItemsToCreate", "group__UaStackTypes.html#ga51b9aa61f0ca4a07eb3bf5d49eb83976", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga45265c69449d58b0f2977468bca2adb0", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga551c5c18d267457342353e2398d347e5", null ]
];