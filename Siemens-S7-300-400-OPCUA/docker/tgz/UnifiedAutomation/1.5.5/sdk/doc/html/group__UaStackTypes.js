var group__UaStackTypes =
[
    [ "OpcUa_TrustListDataType", "structOpcUa__TrustListDataType.html", [
      [ "IssuerCertificates", "group__UaStackTypes.html#ga2110d5d4824769172fa36ebd6930b11b", null ],
      [ "IssuerCrls", "group__UaStackTypes.html#ga83f32e23cd1e3447eb640bbb5a1daf0d", null ],
      [ "SpecifiedLists", "group__UaStackTypes.html#ga07e83eefa56eb59c244a0da04acf223b", null ],
      [ "TrustedCertificates", "group__UaStackTypes.html#gaaedcd431a46299fd48c21996c932465e", null ],
      [ "TrustedCrls", "group__UaStackTypes.html#ga607826bf16d80271dda67b56ae374a46", null ]
    ] ],
    [ "OpcUa_ReferenceNode", "structOpcUa__ReferenceNode.html", null ],
    [ "OpcUa_Argument", "structOpcUa__Argument.html", [
      [ "ArrayDimensions", "group__UaStackTypes.html#gaf286c6acb5eb46279a828f68400a5ac4", null ],
      [ "DataType", "group__UaStackTypes.html#gaa9e8d5f4dc3edc78b63bf1ab626b8b4d", null ],
      [ "Description", "group__UaStackTypes.html#ga1f86a641da1ae04f7d8b0be1777955c4", null ],
      [ "Name", "group__UaStackTypes.html#ga0f2505639e6714511dbe0ef23676591b", null ],
      [ "ValueRank", "group__UaStackTypes.html#ga3fb73033eec0aa0c8050182bf7d04ad5", null ]
    ] ],
    [ "OpcUa_EnumValueType", "structOpcUa__EnumValueType.html", [
      [ "Description", "group__UaStackTypes.html#gaaab865ebdd5c74f1bb2362f7e377a054", null ],
      [ "DisplayName", "group__UaStackTypes.html#gac631575adad6fe7b6945435f8a091b73", null ],
      [ "Value", "group__UaStackTypes.html#gab73548c3060f08143718e983701b24f4", null ]
    ] ],
    [ "OpcUa_OptionSet", "structOpcUa__OptionSet.html", [
      [ "ValidBits", "group__UaStackTypes.html#ga9cb3005d8166b39e2a4bd3a5c7e966d3", null ],
      [ "Value", "group__UaStackTypes.html#ga3ca42461df9be24803d0a50daa97b157", null ]
    ] ],
    [ "OpcUa_TimeZoneDataType", "structOpcUa__TimeZoneDataType.html", [
      [ "DaylightSavingInOffset", "group__UaStackTypes.html#ga7339c10e013b0b5745aec15caa3b899b", null ],
      [ "Offset", "group__UaStackTypes.html#ga16a5a8dd6e0c1113b8816e62464af4e8", null ]
    ] ],
    [ "OpcUa_ApplicationDescription", "structOpcUa__ApplicationDescription.html", [
      [ "ApplicationName", "group__UaStackTypes.html#ga89048407f80d2c1c891a6344d2e21345", null ],
      [ "ApplicationType", "group__UaStackTypes.html#gaa0ad160dd09cdfab75a96a2f2d733d52", null ],
      [ "ApplicationUri", "group__UaStackTypes.html#ga2f6625b5cf103ab1ac4fef4ba5520931", null ],
      [ "DiscoveryProfileUri", "group__UaStackTypes.html#ga8e627f84943d670ea911ee0fddf18456", null ],
      [ "DiscoveryUrls", "group__UaStackTypes.html#gac1e6aa29de609c276aedd1a6e58208bd", null ],
      [ "GatewayServerUri", "group__UaStackTypes.html#ga819373d76779c129a58f2e5120a0b7b7", null ],
      [ "ProductUri", "group__UaStackTypes.html#gac4949aea150e19dc0077a4cd37cd0cf1", null ]
    ] ],
    [ "OpcUa_RequestHeader", "structOpcUa__RequestHeader.html", [
      [ "AdditionalHeader", "group__UaStackTypes.html#ga7f99210601fa2c7636c1eefbd7a53dcf", null ],
      [ "AuditEntryId", "group__UaStackTypes.html#ga980afeaa04b82ddb7e235566a7f396ff", null ],
      [ "AuthenticationToken", "group__UaStackTypes.html#ga4e42cebfa43efd01cc1aed5506431809", null ],
      [ "RequestHandle", "group__UaStackTypes.html#ga336632912cc9aaee6e45cdc60ca23684", null ],
      [ "ReturnDiagnostics", "group__UaStackTypes.html#gab8d43617b01dd745884b539b9ae2b4d9", null ],
      [ "TimeoutHint", "group__UaStackTypes.html#gacc2c479aa29e1e6cdb19deff583c5830", null ],
      [ "Timestamp", "group__UaStackTypes.html#ga04dac22e2b39c5bdf76790bfd2af4d5e", null ]
    ] ],
    [ "OpcUa_ResponseHeader", "structOpcUa__ResponseHeader.html", [
      [ "AdditionalHeader", "group__UaStackTypes.html#gae7789bdc5f75288cafbc06519e8b360d", null ],
      [ "RequestHandle", "group__UaStackTypes.html#ga829cb1c02924069c3672b7d2247b99fd", null ],
      [ "ServiceDiagnostics", "group__UaStackTypes.html#ga7e47989dc5af605e03cc5f846a0355f5", null ],
      [ "ServiceResult", "group__UaStackTypes.html#ga636c7bb1f22292d9bc5704b83ca55d83", null ],
      [ "StringTable", "group__UaStackTypes.html#ga7d39e71aa3cb0a95b599020c62a69d46", null ],
      [ "Timestamp", "group__UaStackTypes.html#ga5325f74d60c89cb66ff1d2ed0e3584bc", null ]
    ] ],
    [ "OpcUa_ServiceFault", "structOpcUa__ServiceFault.html", null ],
    [ "OpcUa_FindServersRequest", "structOpcUa__FindServersRequest.html", [
      [ "EndpointUrl", "group__UaStackTypes.html#ga2e5ef21252a58b177025d0954656f216", null ],
      [ "LocaleIds", "group__UaStackTypes.html#gad40764e0a7b7d5dd8b12651ce9365e6d", null ],
      [ "ServerUris", "group__UaStackTypes.html#ga92ba58bfe7e16a7924e392309fc46c82", null ]
    ] ],
    [ "OpcUa_FindServersResponse", "structOpcUa__FindServersResponse.html", [
      [ "Servers", "group__UaStackTypes.html#gab2188bb8d2650fa1e1bddcaca22c7a31", null ]
    ] ],
    [ "OpcUa_UserTokenPolicy", "structOpcUa__UserTokenPolicy.html", [
      [ "IssuedTokenType", "group__UaStackTypes.html#gadce2aa31dd85cd3cc5e59e36e70680dc", null ],
      [ "IssuerEndpointUrl", "group__UaStackTypes.html#ga8994ad28453bca253da78b705e32db58", null ],
      [ "PolicyId", "group__UaStackTypes.html#gaf026985260a40538a8cf9a2cda0ea3e9", null ],
      [ "SecurityPolicyUri", "group__UaStackTypes.html#ga6c17c4640d8c8c08c167a872773ffa29", null ],
      [ "TokenType", "group__UaStackTypes.html#ga7c06846267efe3d7706655b74afd206c", null ]
    ] ],
    [ "OpcUa_EndpointDescription", "structOpcUa__EndpointDescription.html", [
      [ "EndpointUrl", "group__UaStackTypes.html#gaca8f214d05d2ccd8bfed670b5a1da29f", null ],
      [ "SecurityLevel", "group__UaStackTypes.html#ga9879b3b610b967045cb55b03a2f64653", null ],
      [ "SecurityMode", "group__UaStackTypes.html#ga37166b39f939936490c9fa2c194a719e", null ],
      [ "SecurityPolicyUri", "group__UaStackTypes.html#ga55a8f937a8b25fd7ead84ac9a794a138", null ],
      [ "Server", "group__UaStackTypes.html#ga373512fcac41cf3db37dea0de4e3e2d4", null ],
      [ "ServerCertificate", "group__UaStackTypes.html#ga1eaf2b0426eb3e86539f66864a0f298a", null ],
      [ "TransportProfileUri", "group__UaStackTypes.html#ga0f43503dfdbcc16286f64c52e98b2014", null ],
      [ "UserIdentityTokens", "group__UaStackTypes.html#gab8b6aa89de527525b15bbae1aeaf3dcc", null ]
    ] ],
    [ "OpcUa_GetEndpointsRequest", "structOpcUa__GetEndpointsRequest.html", [
      [ "EndpointUrl", "group__UaStackTypes.html#ga01a2b332908eccb1b1bfb74b927b097a", null ],
      [ "LocaleIds", "group__UaStackTypes.html#gaf20bce1ec7913bcbe46ea6cc68be15ed", null ],
      [ "ProfileUris", "group__UaStackTypes.html#ga1638ca5557ca0d856698babba04db0cd", null ]
    ] ],
    [ "OpcUa_GetEndpointsResponse", "structOpcUa__GetEndpointsResponse.html", [
      [ "Endpoints", "group__UaStackTypes.html#gaa9b1fbe5fde0adf411fe355e83435d08", null ]
    ] ],
    [ "OpcUa_RegisteredServer", "structOpcUa__RegisteredServer.html", [
      [ "DiscoveryUrls", "group__UaStackTypes.html#gafbdcf5f21c1ca36cc5f7ca0c4a68aa4b", null ],
      [ "GatewayServerUri", "group__UaStackTypes.html#gab6dbc96e73abcb557a1eb38f6928ea68", null ],
      [ "IsOnline", "group__UaStackTypes.html#gae5e4cc1b3a56864a33934c0a128f29b8", null ],
      [ "ProductUri", "group__UaStackTypes.html#ga9a5dba95cd9fc34f84c2d34373c7e03d", null ],
      [ "SemaphoreFilePath", "group__UaStackTypes.html#ga42f1fae39569083bb5bff78a0de2277b", null ],
      [ "ServerNames", "group__UaStackTypes.html#ga90cf983d20c77ef9582d58a39852fd39", null ],
      [ "ServerType", "group__UaStackTypes.html#ga1efa0a96a110877b696ce483de3c1546", null ],
      [ "ServerUri", "group__UaStackTypes.html#gaaa25ddf5445c621a3c961a8138d70d9c", null ]
    ] ],
    [ "OpcUa_RegisterServerRequest", "structOpcUa__RegisterServerRequest.html", null ],
    [ "OpcUa_RegisterServerResponse", "structOpcUa__RegisterServerResponse.html", null ],
    [ "OpcUa_MdnsDiscoveryConfiguration", "structOpcUa__MdnsDiscoveryConfiguration.html", null ],
    [ "OpcUa_ChannelSecurityToken", "structOpcUa__ChannelSecurityToken.html", null ],
    [ "OpcUa_OpenSecureChannelRequest", "structOpcUa__OpenSecureChannelRequest.html", null ],
    [ "OpcUa_OpenSecureChannelResponse", "structOpcUa__OpenSecureChannelResponse.html", null ],
    [ "OpcUa_CloseSecureChannelRequest", "structOpcUa__CloseSecureChannelRequest.html", null ],
    [ "OpcUa_CloseSecureChannelResponse", "structOpcUa__CloseSecureChannelResponse.html", null ],
    [ "OpcUa_SignedSoftwareCertificate", "structOpcUa__SignedSoftwareCertificate.html", [
      [ "CertificateData", "group__UaStackTypes.html#ga6de3348eaa07af75da4241b9caf912f9", null ],
      [ "Signature", "group__UaStackTypes.html#ga5da93e4f70a02973c38065c4551002a6", null ]
    ] ],
    [ "OpcUa_SignatureData", "structOpcUa__SignatureData.html", null ],
    [ "OpcUa_CreateSessionRequest", "structOpcUa__CreateSessionRequest.html", null ],
    [ "OpcUa_CreateSessionResponse", "structOpcUa__CreateSessionResponse.html", null ],
    [ "OpcUa_UserIdentityToken", "structOpcUa__UserIdentityToken.html", null ],
    [ "OpcUa_AnonymousIdentityToken", "structOpcUa__AnonymousIdentityToken.html", null ],
    [ "OpcUa_UserNameIdentityToken", "structOpcUa__UserNameIdentityToken.html", null ],
    [ "OpcUa_X509IdentityToken", "structOpcUa__X509IdentityToken.html", null ],
    [ "OpcUa_IssuedIdentityToken", "structOpcUa__IssuedIdentityToken.html", null ],
    [ "OpcUa_ActivateSessionRequest", "structOpcUa__ActivateSessionRequest.html", null ],
    [ "OpcUa_ActivateSessionResponse", "structOpcUa__ActivateSessionResponse.html", null ],
    [ "OpcUa_CloseSessionRequest", "structOpcUa__CloseSessionRequest.html", null ],
    [ "OpcUa_CloseSessionResponse", "structOpcUa__CloseSessionResponse.html", null ],
    [ "OpcUa_CancelRequest", "structOpcUa__CancelRequest.html", [
      [ "RequestHandle", "group__UaStackTypes.html#ga5b0613afb536a93f4927b14e1589374b", null ]
    ] ],
    [ "OpcUa_CancelResponse", "structOpcUa__CancelResponse.html", [
      [ "CancelCount", "group__UaStackTypes.html#ga38b6ef80c971d3521d8cff6000758f30", null ]
    ] ],
    [ "OpcUa_NodeAttributes", "structOpcUa__NodeAttributes.html", null ],
    [ "OpcUa_ObjectAttributes", "structOpcUa__ObjectAttributes.html", null ],
    [ "OpcUa_VariableAttributes", "structOpcUa__VariableAttributes.html", null ],
    [ "OpcUa_MethodAttributes", "structOpcUa__MethodAttributes.html", null ],
    [ "OpcUa_ObjectTypeAttributes", "structOpcUa__ObjectTypeAttributes.html", null ],
    [ "OpcUa_VariableTypeAttributes", "structOpcUa__VariableTypeAttributes.html", null ],
    [ "OpcUa_ReferenceTypeAttributes", "structOpcUa__ReferenceTypeAttributes.html", null ],
    [ "OpcUa_DataTypeAttributes", "structOpcUa__DataTypeAttributes.html", null ],
    [ "OpcUa_ViewAttributes", "structOpcUa__ViewAttributes.html", null ],
    [ "OpcUa_AddNodesItem", "structOpcUa__AddNodesItem.html", [
      [ "BrowseName", "group__UaStackTypes.html#gae4a91e59c025d4d183cd4cc07046d166", null ],
      [ "NodeAttributes", "group__UaStackTypes.html#gab907bd71e73b553e278fcc2d37c4c625", null ],
      [ "NodeClass", "group__UaStackTypes.html#ga45b1f9ccc67aa6c8412ace444e7e4420", null ],
      [ "ParentNodeId", "group__UaStackTypes.html#gaf8819c3157ec69fbaba3e3afd58f0456", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#ga8b7c85cf95052f02c763f83df63b79e6", null ],
      [ "RequestedNewNodeId", "group__UaStackTypes.html#gae4cc4bf1878dcbe12cd5d94a73bfcdab", null ],
      [ "TypeDefinition", "group__UaStackTypes.html#ga2216807f2944fb5c05d120b63de81ec0", null ]
    ] ],
    [ "OpcUa_AddNodesResult", "structOpcUa__AddNodesResult.html", [
      [ "AddedNodeId", "group__UaStackTypes.html#gadd230891fc9dc6906f33e3abe4909d0e", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga58714f09e4930949a52160d79ad16719", null ]
    ] ],
    [ "OpcUa_AddNodesRequest", "structOpcUa__AddNodesRequest.html", [
      [ "NodesToAdd", "group__UaStackTypes.html#gac109a5d62d73b14f60b228bddffe242d", null ]
    ] ],
    [ "OpcUa_AddNodesResponse", "structOpcUa__AddNodesResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga60108bf43176e8efe20055acd59b4d07", null ],
      [ "Results", "group__UaStackTypes.html#ga2a73ca594d787af997814f0645411c55", null ]
    ] ],
    [ "OpcUa_AddReferencesItem", "structOpcUa__AddReferencesItem.html", [
      [ "IsForward", "group__UaStackTypes.html#ga64db012e5295407f05ab43e61220f483", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#gad1952100df202e62664187354fc94406", null ],
      [ "SourceNodeId", "group__UaStackTypes.html#ga3fec4c07f6b09fa730e687c864d624a5", null ],
      [ "TargetNodeClass", "group__UaStackTypes.html#ga77bcf036a6e7cbc13869a7a706c5b6cb", null ],
      [ "TargetNodeId", "group__UaStackTypes.html#ga1fc7a78811ae1288e2e4e5fb4487e568", null ],
      [ "TargetServerUri", "group__UaStackTypes.html#gaa250e533a12ffcf5b654f2ae7f793b59", null ]
    ] ],
    [ "OpcUa_AddReferencesRequest", "structOpcUa__AddReferencesRequest.html", [
      [ "ReferencesToAdd", "group__UaStackTypes.html#ga819e994457f02ee751f0d1c0e30b5c51", null ]
    ] ],
    [ "OpcUa_AddReferencesResponse", "structOpcUa__AddReferencesResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaa681ad1925220ad2d1f6c557007794ea", null ],
      [ "Results", "group__UaStackTypes.html#ga7958d49d91e1ac64e4efe1ea047c8961", null ]
    ] ],
    [ "OpcUa_DeleteNodesItem", "structOpcUa__DeleteNodesItem.html", [
      [ "DeleteTargetReferences", "group__UaStackTypes.html#ga157c48c5f06e0274b51b9f4c878b3424", null ],
      [ "NodeId", "group__UaStackTypes.html#ga4869a654b6cf4355a3d8a25a41d9e30f", null ]
    ] ],
    [ "OpcUa_DeleteNodesRequest", "structOpcUa__DeleteNodesRequest.html", [
      [ "NodesToDelete", "group__UaStackTypes.html#gaddb3c9998ac6848373a6d4c7fff43987", null ]
    ] ],
    [ "OpcUa_DeleteNodesResponse", "structOpcUa__DeleteNodesResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga6ad2333e4d0a16b1be02a9ea35c97212", null ],
      [ "Results", "group__UaStackTypes.html#ga30af67ebce87813df405f2c68d20ae1f", null ]
    ] ],
    [ "OpcUa_DeleteReferencesItem", "structOpcUa__DeleteReferencesItem.html", [
      [ "DeleteBidirectional", "group__UaStackTypes.html#ga4d7a0e0cdb3f27256e4fc8ea6ca4de09", null ],
      [ "IsForward", "group__UaStackTypes.html#gac9fad84bd331f88effdb98d67938e242", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#gaf636f42e53a49deffc2469efc982c247", null ],
      [ "SourceNodeId", "group__UaStackTypes.html#ga1fa1ba7ed6f13e0091bca59ba99f4493", null ],
      [ "TargetNodeId", "group__UaStackTypes.html#gafeb95c9ef97fe96ff2fdadce1aad6989", null ]
    ] ],
    [ "OpcUa_DeleteReferencesRequest", "structOpcUa__DeleteReferencesRequest.html", [
      [ "ReferencesToDelete", "group__UaStackTypes.html#ga0a9d1ea0b3b7de5bf607e7321ce9b388", null ]
    ] ],
    [ "OpcUa_DeleteReferencesResponse", "structOpcUa__DeleteReferencesResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaaef04f0738d0ed89c197af5eac045e37", null ],
      [ "Results", "group__UaStackTypes.html#ga235c4212ff8b999075d4db0e06e3d91a", null ]
    ] ],
    [ "OpcUa_ViewDescription", "structOpcUa__ViewDescription.html", [
      [ "Timestamp", "group__UaStackTypes.html#gaa9c45f2e632c140f1fc28ca540a09ff3", null ],
      [ "ViewId", "group__UaStackTypes.html#ga672bc50354b983a799f570f7743508a8", null ],
      [ "ViewVersion", "group__UaStackTypes.html#gad1c2ea69c8f725433711ea0fdb328649", null ]
    ] ],
    [ "OpcUa_BrowseDescription", "structOpcUa__BrowseDescription.html", [
      [ "BrowseDirection", "group__UaStackTypes.html#ga7b17318b612931e0fc438e4e3eb8b038", null ],
      [ "IncludeSubtypes", "group__UaStackTypes.html#ga8de72b1fd69c0c1719aa99e1dacb9861", null ],
      [ "NodeClassMask", "group__UaStackTypes.html#gab8d99da19b4604028580c73abb8e30f0", null ],
      [ "NodeId", "group__UaStackTypes.html#gac0dc15df7d7e6ef45c4ee0c8956897ab", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#ga1290ef0c7015cfafcfb22893680a0c54", null ],
      [ "ResultMask", "group__UaStackTypes.html#gaa050664eacde119489639ff618806b2c", null ]
    ] ],
    [ "OpcUa_ReferenceDescription", "structOpcUa__ReferenceDescription.html", [
      [ "BrowseName", "group__UaStackTypes.html#gafb407977ec3bb60572b27676c5745fcb", null ],
      [ "DisplayName", "group__UaStackTypes.html#gac41a85d693339109fbee5373551fa89d", null ],
      [ "IsForward", "group__UaStackTypes.html#ga199f8c6092049025bff132438f7417a4", null ],
      [ "NodeClass", "group__UaStackTypes.html#ga2982ff6bbca7ff9a84cb3c9fb90d7169", null ],
      [ "NodeId", "group__UaStackTypes.html#gab4633e15c852ce3059e2a28fd6ffcf00", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#gad17758f38c3913dde4847006f498a341", null ],
      [ "TypeDefinition", "group__UaStackTypes.html#gacfc9614b093ae2cd1fa085630626f7fd", null ]
    ] ],
    [ "OpcUa_BrowseResult", "structOpcUa__BrowseResult.html", [
      [ "ContinuationPoint", "group__UaStackTypes.html#ga4bcf9eab29b1bcb68935f0dd3430cea4", null ],
      [ "References", "group__UaStackTypes.html#gaa71a9fc191f9eaef9b9a4fc25587a490", null ],
      [ "StatusCode", "group__UaStackTypes.html#gabc48f6536750838eab1ec585a13b097e", null ]
    ] ],
    [ "OpcUa_BrowseRequest", "structOpcUa__BrowseRequest.html", [
      [ "NodesToBrowse", "group__UaStackTypes.html#ga516c52ede838d3fe30c127a0ccd60daf", null ],
      [ "RequestedMaxReferencesPerNode", "group__UaStackTypes.html#gadc8bb0155cb18db5792211263c0b2db4", null ],
      [ "View", "group__UaStackTypes.html#ga5aef5d9f3fdcfeacf30077f5a57c4d7d", null ]
    ] ],
    [ "OpcUa_BrowseResponse", "structOpcUa__BrowseResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gae08d37903a8508b790361c4e42131ddc", null ],
      [ "Results", "group__UaStackTypes.html#gac0e5a8d288429f1984da2e3c2cc13937", null ]
    ] ],
    [ "OpcUa_BrowseNextRequest", "structOpcUa__BrowseNextRequest.html", [
      [ "ContinuationPoints", "group__UaStackTypes.html#ga58731108fa26da1b53cb60e26e12db89", null ],
      [ "ReleaseContinuationPoints", "group__UaStackTypes.html#ga393ed0340c915232db6302c13f49bc66", null ]
    ] ],
    [ "OpcUa_BrowseNextResponse", "structOpcUa__BrowseNextResponse.html", null ],
    [ "OpcUa_RelativePathElement", "structOpcUa__RelativePathElement.html", [
      [ "IncludeSubtypes", "group__UaStackTypes.html#ga13f9eb134196c6b690d82cd3dbb529a7", null ],
      [ "IsInverse", "group__UaStackTypes.html#ga688aabda7c10c0d05afe68a65a9ae33d", null ],
      [ "ReferenceTypeId", "group__UaStackTypes.html#ga80f1a06cd24df19a8f2b1db54581293d", null ],
      [ "TargetName", "group__UaStackTypes.html#ga71b0593e37266683e65600f26b5d5718", null ]
    ] ],
    [ "OpcUa_RelativePath", "structOpcUa__RelativePath.html", [
      [ "Elements", "group__UaStackTypes.html#gaa1d32b09499a7397cdcf02f190e6ab53", null ]
    ] ],
    [ "OpcUa_BrowsePath", "structOpcUa__BrowsePath.html", [
      [ "RelativePath", "group__UaStackTypes.html#gaf2fd9d47c8fe40e18cfea10c15b20aef", null ],
      [ "StartingNode", "group__UaStackTypes.html#ga4be3098f7f910c383914cf1b29168d71", null ]
    ] ],
    [ "OpcUa_BrowsePathTarget", "structOpcUa__BrowsePathTarget.html", [
      [ "RemainingPathIndex", "group__UaStackTypes.html#ga94a73aa29f0523f5a4b6680397986e27", null ],
      [ "TargetId", "group__UaStackTypes.html#ga4de3ce1d3205970a27f4f9d477669e29", null ]
    ] ],
    [ "OpcUa_BrowsePathResult", "structOpcUa__BrowsePathResult.html", [
      [ "StatusCode", "group__UaStackTypes.html#ga970e2a9a10bb0c25cf81ca893f952078", null ],
      [ "Targets", "group__UaStackTypes.html#gaacafaf9831605c30e7e4c4a190ce6b98", null ]
    ] ],
    [ "OpcUa_TranslateBrowsePathsToNodeIdsRequest", "structOpcUa__TranslateBrowsePathsToNodeIdsRequest.html", [
      [ "BrowsePaths", "group__UaStackTypes.html#ga47d44d2bd95514a5b3973603a9f210aa", null ]
    ] ],
    [ "OpcUa_TranslateBrowsePathsToNodeIdsResponse", "structOpcUa__TranslateBrowsePathsToNodeIdsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaa434bc727e37c38fd14dd52515948840", null ],
      [ "Results", "group__UaStackTypes.html#ga57604e13ced84bcf3a04e8a9faa7810c", null ]
    ] ],
    [ "OpcUa_RegisterNodesRequest", "structOpcUa__RegisterNodesRequest.html", [
      [ "NodesToRegister", "group__UaStackTypes.html#gaae3934d0c242f230f7ce70dc7d313523", null ]
    ] ],
    [ "OpcUa_RegisterNodesResponse", "structOpcUa__RegisterNodesResponse.html", [
      [ "RegisteredNodeIds", "group__UaStackTypes.html#ga5fd3e9ac1dc6ba19555c7af56ba475ef", null ]
    ] ],
    [ "OpcUa_UnregisterNodesRequest", "structOpcUa__UnregisterNodesRequest.html", [
      [ "NodesToUnregister", "group__UaStackTypes.html#ga8e982a0700e171d917585457bfdb9d30", null ]
    ] ],
    [ "OpcUa_UnregisterNodesResponse", "structOpcUa__UnregisterNodesResponse.html", null ],
    [ "OpcUa_SoftwareCertificate", "structOpcUa__SoftwareCertificate.html", [
      [ "BuildDate", "group__UaStackTypes.html#gad3d2f9b2635c6b978907d862602e5e89", null ],
      [ "BuildNumber", "group__UaStackTypes.html#gac01990397c73d96991356de70522383a", null ],
      [ "IssueDate", "group__UaStackTypes.html#gaa0ba549d95cf17aa87eee9a6270af014", null ],
      [ "IssuedBy", "group__UaStackTypes.html#ga3e07a94d53fcb90031ad610da2ceaf92", null ],
      [ "ProductName", "group__UaStackTypes.html#ga62f0c365a7e6d9f5fd5a0e1b415c89c5", null ],
      [ "ProductUri", "group__UaStackTypes.html#gaee7dd8db947b25503b6aa4e2899e87f4", null ],
      [ "SoftwareVersion", "group__UaStackTypes.html#gaea4befd3d1a03417d6b4ef5f71e72ecf", null ],
      [ "SupportedProfiles", "group__UaStackTypes.html#ga49fed9cddc887575cf65a87b42f61ffe", null ],
      [ "VendorName", "group__UaStackTypes.html#gaa797675c8200f0eddc0e47260a482c89", null ],
      [ "VendorProductCertificate", "group__UaStackTypes.html#ga1bd41d9aad3bf1c0c7b7ea7145bca9e0", null ]
    ] ],
    [ "OpcUa_QueryDataDescription", "structOpcUa__QueryDataDescription.html", [
      [ "AttributeId", "group__UaStackTypes.html#ga39fdad3ba339a801f0c34c227dcfdac6", null ],
      [ "IndexRange", "group__UaStackTypes.html#ga23d839ff468906ca16d0ebc2fb00ea05", null ],
      [ "RelativePath", "group__UaStackTypes.html#gab2f1c995b31082641f511891d2a93cbd", null ]
    ] ],
    [ "OpcUa_NodeTypeDescription", "structOpcUa__NodeTypeDescription.html", [
      [ "DataToReturn", "group__UaStackTypes.html#ga854aebd457580678184370438bf66d52", null ],
      [ "IncludeSubTypes", "group__UaStackTypes.html#ga21fd2161ab7548ee825442949b7eeaec", null ],
      [ "TypeDefinitionNode", "group__UaStackTypes.html#ga50f53ee967712d956fb42032e589ec77", null ]
    ] ],
    [ "OpcUa_ContentFilterElement", "structOpcUa__ContentFilterElement.html", [
      [ "FilterOperands", "group__UaStackTypes.html#ga378cda096ad047d965be15b0b2b293ac", null ],
      [ "FilterOperator", "group__UaStackTypes.html#ga2da48230f67987848f637f0219fda65d", null ]
    ] ],
    [ "OpcUa_ContentFilter", "structOpcUa__ContentFilter.html", [
      [ "Elements", "group__UaStackTypes.html#gab1af9af5979e82aee4fa4e3796dd8441", null ]
    ] ],
    [ "OpcUa_ElementOperand", "structOpcUa__ElementOperand.html", [
      [ "Index", "group__UaStackTypes.html#ga45e11bc4dff634263bdebc32c6cfd7e1", null ]
    ] ],
    [ "OpcUa_AttributeOperand", "structOpcUa__AttributeOperand.html", [
      [ "Alias", "group__UaStackTypes.html#gae69a43b8baed353bce814a1236d5ffc5", null ],
      [ "AttributeId", "group__UaStackTypes.html#ga99d92903d0b454d66598d5890bdd86c3", null ],
      [ "BrowsePath", "group__UaStackTypes.html#ga8594bfa18fc5034df7e40e7dd219948d", null ],
      [ "IndexRange", "group__UaStackTypes.html#gad9d2cf1994968cf011221fd12bdc4d07", null ],
      [ "NodeId", "group__UaStackTypes.html#ga948e5020d7706a8880d6aeb02d66a136", null ]
    ] ],
    [ "OpcUa_SimpleAttributeOperand", "structOpcUa__SimpleAttributeOperand.html", [
      [ "AttributeId", "group__UaStackTypes.html#ga6e679f44d36697bef407341ab128cea0", null ],
      [ "BrowsePath", "group__UaStackTypes.html#ga2bdcc24bc9978bfddd67a5118f536a93", null ],
      [ "IndexRange", "group__UaStackTypes.html#gadc98621f5383a64cfcc2aef0043bf5e3", null ],
      [ "TypeDefinitionId", "group__UaStackTypes.html#ga8f3938a97ab3974f3da6cae1408daaa8", null ]
    ] ],
    [ "OpcUa_ContentFilterElementResult", "structOpcUa__ContentFilterElementResult.html", [
      [ "OperandDiagnosticInfos", "group__UaStackTypes.html#gaa34c095406f8c79b8c616440c3d6fb2d", null ],
      [ "OperandStatusCodes", "group__UaStackTypes.html#ga1814d3e405de70f3029b722f5dc7b5ff", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga7b91196cf8806e0f789ac6df5547a7ae", null ]
    ] ],
    [ "OpcUa_ContentFilterResult", "structOpcUa__ContentFilterResult.html", [
      [ "ElementDiagnosticInfos", "group__UaStackTypes.html#ga085385fe91588dd2db8841fed6e4c024", null ],
      [ "ElementResults", "group__UaStackTypes.html#ga792532e459ade5ae1662b30a15cee570", null ]
    ] ],
    [ "OpcUa_ParsingResult", "structOpcUa__ParsingResult.html", [
      [ "DataDiagnosticInfos", "group__UaStackTypes.html#ga05240d8a1dec4cdbbee5d23312c2bc45", null ],
      [ "DataStatusCodes", "group__UaStackTypes.html#ga039fb8a8c4fabc65c987d62573969b8c", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga85da4e53db55ecf275962b9b876bde8e", null ]
    ] ],
    [ "OpcUa_QueryFirstRequest", "structOpcUa__QueryFirstRequest.html", [
      [ "Filter", "group__UaStackTypes.html#ga293c50d434bdedfcee9d3ce9fabec68c", null ],
      [ "MaxDataSetsToReturn", "group__UaStackTypes.html#ga3bb2516fab83cab597be7ab2644dbdbf", null ],
      [ "MaxReferencesToReturn", "group__UaStackTypes.html#ga62e2db056ffb9b4ae499eae48ffa2803", null ],
      [ "NodeTypes", "group__UaStackTypes.html#ga56204e0a36365890c8eab2d52700dbda", null ],
      [ "View", "group__UaStackTypes.html#ga168653f0e90a93b7298e436df937bbf6", null ]
    ] ],
    [ "OpcUa_QueryFirstResponse", "structOpcUa__QueryFirstResponse.html", [
      [ "ContinuationPoint", "group__UaStackTypes.html#gaa4c860aaab9b7a05f14208da0aebbcb3", null ],
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga89a8b46caf8027109c58c4691f95537d", null ],
      [ "FilterResult", "group__UaStackTypes.html#gac30154e53deeefda7b2bc1a8ea3d1307", null ],
      [ "ParsingResults", "group__UaStackTypes.html#ga1b00803c29e242db5abaebd311eadaa2", null ],
      [ "QueryDataSets", "group__UaStackTypes.html#gac0580420bd29a3a3edd9aa963607b478", null ]
    ] ],
    [ "OpcUa_QueryNextRequest", "structOpcUa__QueryNextRequest.html", [
      [ "ContinuationPoint", "group__UaStackTypes.html#gad45b7156c0d565d1cf8599e93d25b813", null ],
      [ "ReleaseContinuationPoint", "group__UaStackTypes.html#gaddb62fef0ae98a5504521275cee9d689", null ]
    ] ],
    [ "OpcUa_QueryNextResponse", "structOpcUa__QueryNextResponse.html", [
      [ "QueryDataSets", "group__UaStackTypes.html#ga60b477d012a6488405fa2be59743c402", null ],
      [ "RevisedContinuationPoint", "group__UaStackTypes.html#ga1f38b89c5c7782aa0cd4cb8c0986f7b2", null ]
    ] ],
    [ "OpcUa_ReadValueId", "structOpcUa__ReadValueId.html", [
      [ "AttributeId", "group__UaStackTypes.html#gabed8bcdfefdbfa09cadf412829c9b86b", null ],
      [ "DataEncoding", "group__UaStackTypes.html#ga8a59945a3461f75d394153f5ce12ab6e", null ],
      [ "IndexRange", "group__UaStackTypes.html#ga854cc555e9a690bd81e29307a87c4f6b", null ],
      [ "NodeId", "group__UaStackTypes.html#ga724ad682827ba7f53b99d50e7afc8ceb", null ]
    ] ],
    [ "OpcUa_ReadRequest", "structOpcUa__ReadRequest.html", [
      [ "MaxAge", "group__UaStackTypes.html#gaa1c94f0992e008f313e564bd47ba7825", null ],
      [ "NodesToRead", "group__UaStackTypes.html#ga134b8963a7fe765316e34a84690a5067", null ],
      [ "TimestampsToReturn", "group__UaStackTypes.html#ga77f06c6b689c0346381f74bb649c4c23", null ]
    ] ],
    [ "OpcUa_ReadResponse", "structOpcUa__ReadResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga216dcba906fd60090cce0cc819a88d4e", null ],
      [ "Results", "group__UaStackTypes.html#ga09661ac45f3e1c1c306bfa6d5a470584", null ]
    ] ],
    [ "OpcUa_HistoryReadValueId", "structOpcUa__HistoryReadValueId.html", [
      [ "ContinuationPoint", "group__UaStackTypes.html#ga04e94c656c2cf51a7608725b158db1dd", null ],
      [ "DataEncoding", "group__UaStackTypes.html#ga20a62e3aadf460d2dc579797eea3ff60", null ],
      [ "IndexRange", "group__UaStackTypes.html#gab0706ca2862b4c943781a700fa127ce6", null ],
      [ "NodeId", "group__UaStackTypes.html#ga776613d6065cf93d1300fe5e2e8f136d", null ]
    ] ],
    [ "OpcUa_HistoryReadResult", "structOpcUa__HistoryReadResult.html", [
      [ "ContinuationPoint", "group__UaStackTypes.html#gae94c2ced5e2044333711927f00552441", null ],
      [ "HistoryData", "group__UaStackTypes.html#ga0c2b9accd54f041f12f5467c3a5538ac", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga1cdb14fb8520f28e597fb4fb53bcd978", null ]
    ] ],
    [ "OpcUa_ReadRawModifiedDetails", "structOpcUa__ReadRawModifiedDetails.html", [
      [ "EndTime", "group__UaStackTypes.html#ga02e492a428fa30ba3e53cc4dc28d0ed7", null ],
      [ "IsReadModified", "group__UaStackTypes.html#ga9a8a9d120f0749607d415cc68b8e2b60", null ],
      [ "NumValuesPerNode", "group__UaStackTypes.html#ga3968b94aedc3033d44daabe682f28f3b", null ],
      [ "ReturnBounds", "group__UaStackTypes.html#ga0683b4890d93df30bd2bbefc4efac07e", null ],
      [ "StartTime", "group__UaStackTypes.html#ga63008bb07443bc2b716a0eb2d1acc9d7", null ]
    ] ],
    [ "OpcUa_ReadAtTimeDetails", "structOpcUa__ReadAtTimeDetails.html", [
      [ "ReqTimes", "group__UaStackTypes.html#ga60db6a827ef7445055cabf78889e7fab", null ],
      [ "UseSimpleBounds", "group__UaStackTypes.html#ga8492bebdd5fb88f96d7a5ecd1bae8845", null ]
    ] ],
    [ "OpcUa_HistoryReadRequest", "structOpcUa__HistoryReadRequest.html", [
      [ "HistoryReadDetails", "group__UaStackTypes.html#gaeed21ab060d62ed636264fb1271b6873", null ],
      [ "NodesToRead", "group__UaStackTypes.html#gada119cbe9ed3b439e7ff0f1956c893f4", null ],
      [ "ReleaseContinuationPoints", "group__UaStackTypes.html#ga7f2373abbacb531e7af1dd9852c74863", null ],
      [ "TimestampsToReturn", "group__UaStackTypes.html#ga22138c8b6c2fde067a7c3c8193c3776b", null ]
    ] ],
    [ "OpcUa_HistoryReadResponse", "structOpcUa__HistoryReadResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaff8d0e232efbcf8225a4356feffb0b2c", null ],
      [ "Results", "group__UaStackTypes.html#gab6ad65fb0c58f2a941222aea7c63d597", null ]
    ] ],
    [ "OpcUa_WriteValue", "structOpcUa__WriteValue.html", [
      [ "AttributeId", "group__UaStackTypes.html#ga8a57738f90ebb130e640ff056b92d89b", null ],
      [ "IndexRange", "group__UaStackTypes.html#ga4ab5436724cc0803612bccb6ee74c2a3", null ],
      [ "NodeId", "group__UaStackTypes.html#ga603d1ac5232ad4caf9c48fa8f72b92b9", null ],
      [ "Value", "group__UaStackTypes.html#gac73eef6289dbf901c65d455164a5fc45", null ]
    ] ],
    [ "OpcUa_WriteRequest", "structOpcUa__WriteRequest.html", [
      [ "NodesToWrite", "group__UaStackTypes.html#gaa8c300b512f56e1794e10e5a46c29354", null ]
    ] ],
    [ "OpcUa_WriteResponse", "structOpcUa__WriteResponse.html", [
      [ "NoOfDiagnosticInfos", "group__UaStackTypes.html#ga33954fe253b98c43433822fcfaeece04", null ],
      [ "Results", "group__UaStackTypes.html#ga93331b06a72773f965d9c6e20169a57b", null ]
    ] ],
    [ "OpcUa_UpdateDataDetails", "structOpcUa__UpdateDataDetails.html", [
      [ "NodeId", "group__UaStackTypes.html#gace205830a7e69ac2e7260d9cc37ecae8", null ],
      [ "PerformInsertReplace", "group__UaStackTypes.html#ga675f178843e8e86988da82945ceb326d", null ],
      [ "UpdateValues", "group__UaStackTypes.html#ga87cf4ecd41f115a1f6fd3c2062e07f62", null ]
    ] ],
    [ "OpcUa_UpdateStructureDataDetails", "structOpcUa__UpdateStructureDataDetails.html", [
      [ "NodeId", "group__UaStackTypes.html#ga810a514d3263329dfacf362e7a25c8b3", null ],
      [ "PerformInsertReplace", "group__UaStackTypes.html#ga03ae66304f443674522ee8e654cd22e9", null ],
      [ "UpdateValues", "group__UaStackTypes.html#ga1d07841986dc0a00919b326ec2f62c69", null ]
    ] ],
    [ "OpcUa_DeleteRawModifiedDetails", "structOpcUa__DeleteRawModifiedDetails.html", [
      [ "EndTime", "group__UaStackTypes.html#gad166672b5dbe0c8b1f83ce68be84cfad", null ],
      [ "IsDeleteModified", "group__UaStackTypes.html#ga88235d0faa39022b594421d18afda1a8", null ],
      [ "NodeId", "group__UaStackTypes.html#gaaccbcc6d2f9d9be4738b175c94db4121", null ],
      [ "StartTime", "group__UaStackTypes.html#gaf5177406eb275d39bcaea86a5e49d9ba", null ]
    ] ],
    [ "OpcUa_DeleteAtTimeDetails", "structOpcUa__DeleteAtTimeDetails.html", [
      [ "NodeId", "group__UaStackTypes.html#ga17f121751946ad1a58b787fcb7747be5", null ],
      [ "ReqTimes", "group__UaStackTypes.html#gafc024ff104f129d0871d6a443bd95b64", null ]
    ] ],
    [ "OpcUa_DeleteEventDetails", "structOpcUa__DeleteEventDetails.html", [
      [ "EventIds", "group__UaStackTypes.html#gab70efdfaaab5d144b939961ba43efa9a", null ],
      [ "NodeId", "group__UaStackTypes.html#ga8e4a91f3bcb93a6e6f423ad32c179436", null ]
    ] ],
    [ "OpcUa_HistoryUpdateResult", "structOpcUa__HistoryUpdateResult.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga0014659bf4d0b77e3e0105983f7a024d", null ],
      [ "OperationResults", "group__UaStackTypes.html#gaff6d4a4ac7aaee3cc67427d39948fc04", null ],
      [ "StatusCode", "group__UaStackTypes.html#gaeb1d00907ddabfc3f09205c4f6e3217b", null ]
    ] ],
    [ "OpcUa_HistoryUpdateRequest", "structOpcUa__HistoryUpdateRequest.html", [
      [ "HistoryUpdateDetails", "group__UaStackTypes.html#ga57cc89a182e90a6c4dd299b44a24666e", null ]
    ] ],
    [ "OpcUa_HistoryUpdateResponse", "structOpcUa__HistoryUpdateResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gabb3f65e264bacde430b264a479d441a7", null ],
      [ "Results", "group__UaStackTypes.html#gac5ceeba95e5e75ad0e6fb83f046c4bb5", null ]
    ] ],
    [ "OpcUa_CallMethodRequest", "structOpcUa__CallMethodRequest.html", [
      [ "InputArguments", "group__UaStackTypes.html#ga78cbf2b40add095b2370ee4cd3ac4cbd", null ],
      [ "MethodId", "group__UaStackTypes.html#ga83028a07c36aff34e902e4097cc0b99f", null ],
      [ "ObjectId", "group__UaStackTypes.html#ga46fa6a08cd65f2a44c37594c4ef660af", null ]
    ] ],
    [ "OpcUa_CallMethodResult", "structOpcUa__CallMethodResult.html", [
      [ "InputArgumentDiagnosticInfos", "group__UaStackTypes.html#ga4bf33f93174146993e351aeb0ee7482e", null ],
      [ "InputArgumentResults", "group__UaStackTypes.html#ga3e0f1d2dbc5b89b1048306c89e827665", null ],
      [ "OutputArguments", "group__UaStackTypes.html#ga744a3f7302f37de1ebb0441c64c6c3ba", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga3c9c90e0070e660e1715abeccf9b6bf9", null ]
    ] ],
    [ "OpcUa_CallRequest", "structOpcUa__CallRequest.html", [
      [ "MethodsToCall", "group__UaStackTypes.html#gaf3553086265beb320fc574d831cbbe94", null ]
    ] ],
    [ "OpcUa_CallResponse", "structOpcUa__CallResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga6c28c846778e805617f84fde53543b78", null ],
      [ "Results", "group__UaStackTypes.html#ga4bd06e2f248f256736e5102e8d1eca0b", null ]
    ] ],
    [ "OpcUa_EventFilter", "structOpcUa__EventFilter.html", [
      [ "SelectClauses", "group__UaStackTypes.html#gafdc4a2055ca406e0a42933b853c857b5", null ],
      [ "WhereClause", "group__UaStackTypes.html#ga7b348261ad6e6e723d47da865e0182de", null ]
    ] ],
    [ "OpcUa_AggregateConfiguration", "structOpcUa__AggregateConfiguration.html", [
      [ "PercentDataBad", "group__UaStackTypes.html#ga0a926196cc135225c34511d0af8510c7", null ],
      [ "PercentDataGood", "group__UaStackTypes.html#gaace11fc8ab21f4b62d3eba9d50df0b38", null ],
      [ "TreatUncertainAsBad", "group__UaStackTypes.html#ga07a7cf674b279849b828741d7b241b60", null ],
      [ "UseServerCapabilitiesDefaults", "group__UaStackTypes.html#ga0fa5356f7bbce5945c8cca338a203727", null ],
      [ "UseSlopedExtrapolation", "group__UaStackTypes.html#ga335cf19cb04c84f22944bc120be57be1", null ]
    ] ],
    [ "OpcUa_EventFilterResult", "structOpcUa__EventFilterResult.html", [
      [ "SelectClauseDiagnosticInfos", "group__UaStackTypes.html#gadd36bcd423510f65020c9d4e9b6b74c6", null ],
      [ "SelectClauseResults", "group__UaStackTypes.html#gadf49b573383c1863a7bdae3b080518b7", null ],
      [ "WhereClauseResult", "group__UaStackTypes.html#ga80d7ea77bab84400e403194ff767cc65", null ]
    ] ],
    [ "OpcUa_MonitoringParameters", "structOpcUa__MonitoringParameters.html", [
      [ "ClientHandle", "group__UaStackTypes.html#ga833034d9d80179942078f06bc2af005e", null ],
      [ "DiscardOldest", "group__UaStackTypes.html#ga0f150bc7069b71cb659847ccddf90ef1", null ],
      [ "Filter", "group__UaStackTypes.html#gab0e83fe4687f84a1a855d779f9415427", null ],
      [ "QueueSize", "group__UaStackTypes.html#ga686fbf5f5c05abe85759d18fab0c3575", null ],
      [ "SamplingInterval", "group__UaStackTypes.html#ga6c5850dbff0e4b6587d77200db1b3aa8", null ]
    ] ],
    [ "OpcUa_MonitoredItemCreateRequest", "structOpcUa__MonitoredItemCreateRequest.html", [
      [ "ItemToMonitor", "group__UaStackTypes.html#gad2167223e6e900bfb8e333ef0d69845b", null ],
      [ "MonitoringMode", "group__UaStackTypes.html#ga8784168fb2600e99bfc5808894d90677", null ],
      [ "RequestedParameters", "group__UaStackTypes.html#gab8d65b99b64c5e4120b057eb3f55df82", null ]
    ] ],
    [ "OpcUa_MonitoredItemCreateResult", "structOpcUa__MonitoredItemCreateResult.html", [
      [ "FilterResult", "group__UaStackTypes.html#ga068bd04e98c885111ad2d7296985a8dc", null ],
      [ "MonitoredItemId", "group__UaStackTypes.html#ga3189263bb49e2ab983578fb9f3293d35", null ],
      [ "RevisedQueueSize", "group__UaStackTypes.html#gab45581fcab8c5a2c6708b65d3b0624ba", null ],
      [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga5662c6ccf70fdb6c2777bc54edda8573", null ],
      [ "StatusCode", "group__UaStackTypes.html#gab3175611ec1758b3f19d79a146ff816d", null ]
    ] ],
    [ "OpcUa_CreateMonitoredItemsRequest", "structOpcUa__CreateMonitoredItemsRequest.html", [
      [ "ItemsToCreate", "group__UaStackTypes.html#ga51b9aa61f0ca4a07eb3bf5d49eb83976", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga45265c69449d58b0f2977468bca2adb0", null ],
      [ "TimestampsToReturn", "group__UaStackTypes.html#ga551c5c18d267457342353e2398d347e5", null ]
    ] ],
    [ "OpcUa_CreateMonitoredItemsResponse", "structOpcUa__CreateMonitoredItemsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga0b40efddb7207e5ccb9520f7eafc459d", null ],
      [ "Results", "group__UaStackTypes.html#ga3a341a484a4360a5c757c0cdaa89d892", null ]
    ] ],
    [ "OpcUa_MonitoredItemModifyRequest", "structOpcUa__MonitoredItemModifyRequest.html", [
      [ "MonitoredItemId", "group__UaStackTypes.html#ga41ae19789706f14131ddf88980637454", null ],
      [ "RequestedParameters", "group__UaStackTypes.html#ga24796b363f2098d2ccececcec5a18172", null ]
    ] ],
    [ "OpcUa_MonitoredItemModifyResult", "structOpcUa__MonitoredItemModifyResult.html", [
      [ "FilterResult", "group__UaStackTypes.html#gab7acff3e0b3a76293430dc52e3c1e1be", null ],
      [ "RevisedQueueSize", "group__UaStackTypes.html#ga8164f775764c31ac25f49a84a638df28", null ],
      [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga53c6434a5bf94744323bc5167854e67e", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga375b01f9765aed8a49875f2c4c0aa4fe", null ]
    ] ],
    [ "OpcUa_ModifyMonitoredItemsRequest", "structOpcUa__ModifyMonitoredItemsRequest.html", [
      [ "ItemsToModify", "group__UaStackTypes.html#gafae3a19099fca7edc78828ee53ec242d", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga88799f8ec33c70d8d3d1378aec677492", null ],
      [ "TimestampsToReturn", "group__UaStackTypes.html#ga3bed32d7ed039cf1071348a9731d7276", null ]
    ] ],
    [ "OpcUa_ModifyMonitoredItemsResponse", "structOpcUa__ModifyMonitoredItemsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaae067352472c019ceeb393b98699dc22", null ],
      [ "Results", "group__UaStackTypes.html#ga48983b3ec295ebb318d49ee83bfee6ad", null ]
    ] ],
    [ "OpcUa_SetMonitoringModeRequest", "structOpcUa__SetMonitoringModeRequest.html", [
      [ "MonitoredItemIds", "group__UaStackTypes.html#gacdc2a5fec6bce2190cd2f00cf1870107", null ],
      [ "MonitoringMode", "group__UaStackTypes.html#ga7b81e872d1361fc349d44888d0697d93", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga759ca7d85c383a98236f13419c569b8f", null ]
    ] ],
    [ "OpcUa_SetMonitoringModeResponse", "structOpcUa__SetMonitoringModeResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga9eba211295e5f501c6a9fa571f088636", null ],
      [ "Results", "group__UaStackTypes.html#gabebbfd9359370d27676f4a1ffc76e58c", null ]
    ] ],
    [ "OpcUa_SetTriggeringRequest", "structOpcUa__SetTriggeringRequest.html", [
      [ "LinksToAdd", "group__UaStackTypes.html#ga7f5d1617d067a7c5ddc0daadf65812cd", null ],
      [ "LinksToRemove", "group__UaStackTypes.html#ga9178ea6c86faab0a06ef438cdd16c8b1", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#gac3feef9a809c060ba70e6c8d1ccd3d4f", null ],
      [ "TriggeringItemId", "group__UaStackTypes.html#ga542374801c73f1c7e32925a289fe2191", null ]
    ] ],
    [ "OpcUa_SetTriggeringResponse", "structOpcUa__SetTriggeringResponse.html", [
      [ "AddDiagnosticInfos", "group__UaStackTypes.html#gabb11dd8a000783c4c4eef964ebddc28b", null ],
      [ "AddResults", "group__UaStackTypes.html#gac884fa18a528af12be51d9af1ccbb9cd", null ],
      [ "RemoveDiagnosticInfos", "group__UaStackTypes.html#ga588fb6a6bbcf3c57de61a95317939e24", null ],
      [ "RemoveResults", "group__UaStackTypes.html#gac6242fdbd1f2e74b6e95757cf918f476", null ]
    ] ],
    [ "OpcUa_DeleteMonitoredItemsRequest", "structOpcUa__DeleteMonitoredItemsRequest.html", [
      [ "MonitoredItemIds", "group__UaStackTypes.html#gaaa13077ed21f6b179863da88b3c52987", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga00c76ec59b3d3709fb89bfd0fb38f244", null ]
    ] ],
    [ "OpcUa_DeleteMonitoredItemsResponse", "structOpcUa__DeleteMonitoredItemsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#gaea79ccb70081427f7075d0a9fad6be83", null ],
      [ "Results", "group__UaStackTypes.html#ga1903b2d52a4be15fb337c7ac7989cd85", null ]
    ] ],
    [ "OpcUa_CreateSubscriptionRequest", "structOpcUa__CreateSubscriptionRequest.html", [
      [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga5c6a618a41dddce7a362e20280e30889", null ],
      [ "Priority", "group__UaStackTypes.html#ga78766f534e548b0de146ce4b8c0cd9f0", null ],
      [ "PublishingEnabled", "group__UaStackTypes.html#ga6b64a2bbe7b7e55ccc5fb5840f1b2fdd", null ],
      [ "RequestedLifetimeCount", "group__UaStackTypes.html#gad987badc435c4edd8383c4da75acb222", null ],
      [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#gaba5ea661e7b9e955f7f6d7039f498aba", null ],
      [ "RequestedPublishingInterval", "group__UaStackTypes.html#gae0125058057f4090d46ed614dbbb89b1", null ]
    ] ],
    [ "OpcUa_CreateSubscriptionResponse", "structOpcUa__CreateSubscriptionResponse.html", [
      [ "RevisedLifetimeCount", "group__UaStackTypes.html#gad362baad3fd21d6c1989bd31bf871ef8", null ],
      [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#ga7f231a6c60c92339568f8f68a3a80ea5", null ],
      [ "RevisedPublishingInterval", "group__UaStackTypes.html#gad46b9515b50081cf1afdaf4d94343770", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga279174ece7725e168e192675a1abe794", null ]
    ] ],
    [ "OpcUa_ModifySubscriptionRequest", "structOpcUa__ModifySubscriptionRequest.html", [
      [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga2c7578f38198ade678a61345ab6efa0c", null ],
      [ "Priority", "group__UaStackTypes.html#ga7dafc433b65d20ed608cfe42f4e58cde", null ],
      [ "RequestedLifetimeCount", "group__UaStackTypes.html#gaceac41f2689b98cb2b9619542112cdd9", null ],
      [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#ga4aec23ac2c0ee309c107d4ca34a5c190", null ],
      [ "RequestedPublishingInterval", "group__UaStackTypes.html#gaff0c84c498961f401c111522f6eaea5c", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga647508fdabfaec213392dfa83579717b", null ]
    ] ],
    [ "OpcUa_ModifySubscriptionResponse", "structOpcUa__ModifySubscriptionResponse.html", [
      [ "RevisedLifetimeCount", "group__UaStackTypes.html#gaec034a74423e95763f5404f72ae864bb", null ],
      [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#gab2ab2749c088329be611422c57448d64", null ],
      [ "RevisedPublishingInterval", "group__UaStackTypes.html#ga7e3842bd86ee242c1bae223249a6bd28", null ]
    ] ],
    [ "OpcUa_SetPublishingModeRequest", "structOpcUa__SetPublishingModeRequest.html", [
      [ "PublishingEnabled", "group__UaStackTypes.html#ga5c079471bf166a04d5d0ff6dd582d447", null ],
      [ "SubscriptionIds", "group__UaStackTypes.html#ga3f2f1de7dbaa36acc7e87431a158548e", null ]
    ] ],
    [ "OpcUa_SetPublishingModeResponse", "structOpcUa__SetPublishingModeResponse.html", [
      [ "NoOfDiagnosticInfos", "group__UaStackTypes.html#ga69df614fa94021b1980803094e8f9c39", null ],
      [ "Results", "group__UaStackTypes.html#ga4f4ab57d678be2180bfa5c2870327d01", null ]
    ] ],
    [ "OpcUa_SubscriptionAcknowledgement", "structOpcUa__SubscriptionAcknowledgement.html", [
      [ "SequenceNumber", "group__UaStackTypes.html#gac53195c552fde1bf83358f6a557b9bc3", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga122ea5f536dc5382a99cc5c0f87b309e", null ]
    ] ],
    [ "OpcUa_PublishRequest", "structOpcUa__PublishRequest.html", [
      [ "SubscriptionAcknowledgements", "group__UaStackTypes.html#ga1d83748ffd485d2ee9ca61b3c3141442", null ]
    ] ],
    [ "OpcUa_PublishResponse", "structOpcUa__PublishResponse.html", [
      [ "AvailableSequenceNumbers", "group__UaStackTypes.html#ga877a6418970c86fb58be356ed62fc701", null ],
      [ "DiagnosticInfos", "group__UaStackTypes.html#gab362b84a071184d437c0f43b80886cfb", null ],
      [ "MoreNotifications", "group__UaStackTypes.html#ga8ed654675e29c17b67c7358fffe9279a", null ],
      [ "NotificationMessage", "group__UaStackTypes.html#ga1f58a55a2977f5863f9d606bcd6aa0f6", null ],
      [ "Results", "group__UaStackTypes.html#ga2a66e318305bd767f798380292f2cdc6", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga18e96eace034affd9242f8747be56e3d", null ]
    ] ],
    [ "OpcUa_RepublishRequest", "structOpcUa__RepublishRequest.html", [
      [ "RetransmitSequenceNumber", "group__UaStackTypes.html#ga8ff9484ceb55948643d8eea892addad4", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#ga4f70582af61ba157e97dd05463688a25", null ]
    ] ],
    [ "OpcUa_RepublishResponse", "structOpcUa__RepublishResponse.html", [
      [ "NotificationMessage", "group__UaStackTypes.html#ga7d2d1faf0dce8f8e5b41966898641355", null ]
    ] ],
    [ "OpcUa_TransferResult", "structOpcUa__TransferResult.html", [
      [ "AvailableSequenceNumbers", "group__UaStackTypes.html#ga4785ccc6cc62d3a93dc480a1fc0e43c6", null ],
      [ "StatusCode", "group__UaStackTypes.html#gad24627c532c9aef3adfe76dac7643593", null ]
    ] ],
    [ "OpcUa_TransferSubscriptionsRequest", "structOpcUa__TransferSubscriptionsRequest.html", [
      [ "SendInitialValues", "group__UaStackTypes.html#ga14cd492a4a534e8642f66af225500a31", null ],
      [ "SubscriptionIds", "group__UaStackTypes.html#gaec373b5a7cd0ec8d8426453c60765a4f", null ]
    ] ],
    [ "OpcUa_TransferSubscriptionsResponse", "structOpcUa__TransferSubscriptionsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga2033811b1a28e54c66f1b1a956fd11bf", null ],
      [ "Results", "group__UaStackTypes.html#gad1c17287d0f834149fff4197d704d4cc", null ]
    ] ],
    [ "OpcUa_DeleteSubscriptionsRequest", "structOpcUa__DeleteSubscriptionsRequest.html", [
      [ "SubscriptionIds", "group__UaStackTypes.html#gabec9a74c9d8ba4a1df31f076053a36fc", null ]
    ] ],
    [ "OpcUa_DeleteSubscriptionsResponse", "structOpcUa__DeleteSubscriptionsResponse.html", [
      [ "DiagnosticInfos", "group__UaStackTypes.html#ga518dd81c10f59de4f85ae4309d92ead8", null ],
      [ "Results", "group__UaStackTypes.html#ga36566d0feecf87771e405afe4771a0f4", null ]
    ] ],
    [ "OpcUa_ScalarTestType", "structOpcUa__ScalarTestType.html", null ],
    [ "OpcUa_ArrayTestType", "structOpcUa__ArrayTestType.html", null ],
    [ "OpcUa_BuildInfo", "structOpcUa__BuildInfo.html", [
      [ "BuildDate", "group__UaStackTypes.html#ga5b44440c3bdacc32eaa8a64fc2d0e499", null ],
      [ "BuildNumber", "group__UaStackTypes.html#ga2a60de64f498cfefb99c303cd19112b7", null ],
      [ "ManufacturerName", "group__UaStackTypes.html#gaa0d220eaebe290b74dfcd03bbfde74b7", null ],
      [ "ProductName", "group__UaStackTypes.html#gad87a5892dfd2eb1279bc9eb0b8827018", null ],
      [ "ProductUri", "group__UaStackTypes.html#ga4fce8aa0aea3c06d7aaf317793ffd2cc", null ],
      [ "SoftwareVersion", "group__UaStackTypes.html#ga4d189e96984facd6665f7ed91d65be0d", null ]
    ] ],
    [ "OpcUa_RedundantServerDataType", "structOpcUa__RedundantServerDataType.html", [
      [ "ServerId", "group__UaStackTypes.html#ga154bb3a78a96a6f888a946e93b460d0a", null ],
      [ "ServerState", "group__UaStackTypes.html#ga682107b73a1428ca43259d2a321f1ccb", null ],
      [ "ServiceLevel", "group__UaStackTypes.html#gaaf26848c7a63971722743cf4d2690fb2", null ]
    ] ],
    [ "OpcUa_EndpointUrlListDataType", "structOpcUa__EndpointUrlListDataType.html", [
      [ "EndpointUrlList", "group__UaStackTypes.html#ga03622c06c32062af1e972b3d4317fed4", null ]
    ] ],
    [ "OpcUa_NetworkGroupDataType", "structOpcUa__NetworkGroupDataType.html", [
      [ "NetworkPaths", "group__UaStackTypes.html#gafb8deec9c3c2c79a7647e0c23ff99616", null ],
      [ "ServerUri", "group__UaStackTypes.html#gad75db2d7071010f9cc0e00227a34053c", null ]
    ] ],
    [ "OpcUa_SamplingIntervalDiagnosticsDataType", "structOpcUa__SamplingIntervalDiagnosticsDataType.html", null ],
    [ "OpcUa_ServerDiagnosticsSummaryDataType", "structOpcUa__ServerDiagnosticsSummaryDataType.html", [
      [ "CumulatedSessionCount", "group__UaStackTypes.html#gaa420700e79ddb4d254a24b5798660301", null ],
      [ "CumulatedSubscriptionCount", "group__UaStackTypes.html#gae78d8296a45a445fc895ecf25c26e13f", null ],
      [ "CurrentSessionCount", "group__UaStackTypes.html#ga04b4388289b18877454262feb29df3c1", null ],
      [ "CurrentSubscriptionCount", "group__UaStackTypes.html#ga2f10b86f3397dda082b9e9f11e68ee82", null ],
      [ "PublishingIntervalCount", "group__UaStackTypes.html#gaa3fb89f1f20d223a05e79a697424d22f", null ],
      [ "RejectedRequestsCount", "group__UaStackTypes.html#ga7301bbd87a0fc6b3976dea93c00a4430", null ],
      [ "RejectedSessionCount", "group__UaStackTypes.html#ga5363d232d0fd3e8c61e85d1f884a087b", null ],
      [ "SecurityRejectedRequestsCount", "group__UaStackTypes.html#ga3c3091c00323fddc899bcbe3a35fd711", null ],
      [ "SecurityRejectedSessionCount", "group__UaStackTypes.html#gacf59aa66f039c860f36ab77de055b59a", null ],
      [ "ServerViewCount", "group__UaStackTypes.html#gabdbc069a0682a037a890f525f81d15eb", null ],
      [ "SessionAbortCount", "group__UaStackTypes.html#gacea3e5a1e21026a29b8bb6c9f8c41229", null ],
      [ "SessionTimeoutCount", "group__UaStackTypes.html#ga6e66b31ce0bc6b75c69cc6ff5393193d", null ]
    ] ],
    [ "OpcUa_ServerStatusDataType", "structOpcUa__ServerStatusDataType.html", [
      [ "BuildInfo", "group__UaStackTypes.html#ga13c4d497584bace0a48692aed794af12", null ],
      [ "CurrentTime", "group__UaStackTypes.html#gae4cdb06cb9d125b783019dd79541ad10", null ],
      [ "SecondsTillShutdown", "group__UaStackTypes.html#ga9364ff68384ae5708f8a29398918ca46", null ],
      [ "ShutdownReason", "group__UaStackTypes.html#ga932a69a816162138490390d08a6b9f8f", null ],
      [ "StartTime", "group__UaStackTypes.html#ga09dd9b5460589b0b8a2410bc99a77532", null ],
      [ "State", "group__UaStackTypes.html#ga734414aea5f9043411bab1c14cb16726", null ]
    ] ],
    [ "OpcUa_SessionSecurityDiagnosticsDataType", "structOpcUa__SessionSecurityDiagnosticsDataType.html", [
      [ "AuthenticationMechanism", "group__UaStackTypes.html#ga7701ce5a311a0986f44143da9b65ceff", null ],
      [ "ClientCertificate", "group__UaStackTypes.html#ga4a9b61e555ff62fa72d1ab7cbf29c9b2", null ],
      [ "ClientUserIdHistory", "group__UaStackTypes.html#ga7b15772e9703e220688068de82cda99a", null ],
      [ "ClientUserIdOfSession", "group__UaStackTypes.html#ga77b62ee9aeac3370560beb66432f0419", null ],
      [ "Encoding", "group__UaStackTypes.html#gace4b5b48c9be6d7d9365b9db9d195150", null ],
      [ "SecurityMode", "group__UaStackTypes.html#ga6627615dcc660f8019130f85ed779a44", null ],
      [ "SecurityPolicyUri", "group__UaStackTypes.html#ga5384385752c609824fe2a27f6fc47f1d", null ],
      [ "SessionId", "group__UaStackTypes.html#gad7abc52a3d50e9e626f910dc59c76e8d", null ],
      [ "TransportProtocol", "group__UaStackTypes.html#gae13835806aea8841ece342b4bea5c197", null ]
    ] ],
    [ "OpcUa_ServiceCounterDataType", "structOpcUa__ServiceCounterDataType.html", [
      [ "ErrorCount", "group__UaStackTypes.html#ga19775ecad4d90ef299501085b251909a", null ],
      [ "TotalCount", "group__UaStackTypes.html#ga2e76fd3be9c2a38b4b43a565cf1edcb8", null ]
    ] ],
    [ "OpcUa_SubscriptionDiagnosticsDataType", "structOpcUa__SubscriptionDiagnosticsDataType.html", [
      [ "CurrentKeepAliveCount", "group__UaStackTypes.html#gaff32d0227d7b88ce782a78d44dd0b78b", null ],
      [ "CurrentLifetimeCount", "group__UaStackTypes.html#gafcaa7135349d19d73d49a4d75c015c26", null ],
      [ "DataChangeNotificationsCount", "group__UaStackTypes.html#ga365692a07f97360b07783236806fc6ce", null ],
      [ "DisableCount", "group__UaStackTypes.html#ga1149474037e6b7b2b302540caf8e9bbc", null ],
      [ "DisabledMonitoredItemCount", "group__UaStackTypes.html#ga4bca56c3e0ccc19a45de4530b16ae18d", null ],
      [ "DiscardedMessageCount", "group__UaStackTypes.html#ga9b839aaceb5ec1d805542733f99bc3db", null ],
      [ "EnableCount", "group__UaStackTypes.html#gab7de36faac9abbf8f0befa1f1d4609a9", null ],
      [ "EventNotificationsCount", "group__UaStackTypes.html#gad0be77c1375cca7df0f0a5494d76d9cd", null ],
      [ "EventQueueOverFlowCount", "group__UaStackTypes.html#gac722e42dea98720e23642c5394f1d4f2", null ],
      [ "LatePublishRequestCount", "group__UaStackTypes.html#ga8c30221f04315966e7c268de86bc24ab", null ],
      [ "MaxKeepAliveCount", "group__UaStackTypes.html#ga189114423fe447eedcc705b7536bd1bc", null ],
      [ "MaxLifetimeCount", "group__UaStackTypes.html#ga990db0a1dd0fd1df359384d8eff63552", null ],
      [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#gae6053c1f6c7f98255944dcce5d08890c", null ],
      [ "ModifyCount", "group__UaStackTypes.html#gae06a22cd8ca744fe864ddefb2b36b75f", null ],
      [ "MonitoredItemCount", "group__UaStackTypes.html#gaaa76468cc3892ff9677906bc06432da7", null ],
      [ "MonitoringQueueOverflowCount", "group__UaStackTypes.html#gadb446ee5abb6a5d8e27a83a5927061a9", null ],
      [ "NextSequenceNumber", "group__UaStackTypes.html#ga7afb35250ece78b4c96aefdb75f8e427", null ],
      [ "NotificationsCount", "group__UaStackTypes.html#gabc3ed012d0f39a537518129517614125", null ],
      [ "Priority", "group__UaStackTypes.html#ga06b04f9671879642b6319183e59dbd4c", null ],
      [ "PublishingEnabled", "group__UaStackTypes.html#ga53f70956bc2923325a5854877ef5a1ee", null ],
      [ "PublishingInterval", "group__UaStackTypes.html#ga34f17229e0cac8bf5e86d024cc0a451c", null ],
      [ "PublishRequestCount", "group__UaStackTypes.html#ga72a51b978c19bbf59d75b5da1ce0ec9a", null ],
      [ "RepublishMessageCount", "group__UaStackTypes.html#ga090675fc24ba51923cdb0db78ea0398e", null ],
      [ "RepublishMessageRequestCount", "group__UaStackTypes.html#gafed323255c53a251b7151975171d1ee1", null ],
      [ "RepublishRequestCount", "group__UaStackTypes.html#ga07fa62571befd8722defffd237d0fa48", null ],
      [ "SessionId", "group__UaStackTypes.html#ga93dfff535efbd225c1a1412c36536fa3", null ],
      [ "SubscriptionId", "group__UaStackTypes.html#gaa8a63cf7e4de1fabf36031d8730ce5b4", null ],
      [ "TransferredToAltClientCount", "group__UaStackTypes.html#gaecc333cd045f51be9d690173c86b5dee", null ],
      [ "TransferredToSameClientCount", "group__UaStackTypes.html#ga29fd636a00021a2feb2385a8088660b6", null ],
      [ "TransferRequestCount", "group__UaStackTypes.html#gad3ea8d057b5e1b052d2dd661d044ba42", null ],
      [ "UnacknowledgedMessageCount", "group__UaStackTypes.html#gab71c4629ac13da2d37298905fc8f1cf0", null ]
    ] ],
    [ "OpcUa_ModelChangeStructureDataType", "structOpcUa__ModelChangeStructureDataType.html", [
      [ "Affected", "group__UaStackTypes.html#gafe532cccf52c65a6878f7ff772115767", null ],
      [ "AffectedType", "group__UaStackTypes.html#gaa2f31be387f80afa65142cfdb9919989", null ],
      [ "Verb", "group__UaStackTypes.html#gaa0e81ecd74ae9ef3b23bd82643a66385", null ]
    ] ],
    [ "OpcUa_SemanticChangeStructureDataType", "structOpcUa__SemanticChangeStructureDataType.html", [
      [ "Affected", "group__UaStackTypes.html#gade6cce9ba47d92017d1fd5c6f42ccb78", null ],
      [ "AffectedType", "group__UaStackTypes.html#ga65718273fce18e892af728e9bf697632", null ]
    ] ],
    [ "OpcUa_Range", "structOpcUa__Range.html", [
      [ "High", "group__UaStackTypes.html#ga2294051bb62f7695b44300ecc1443c9a", null ],
      [ "Low", "group__UaStackTypes.html#gaf71851bd746dc48dc85790d5bc0e35c2", null ]
    ] ],
    [ "OpcUa_EUInformation", "structOpcUa__EUInformation.html", [
      [ "Description", "group__UaStackTypes.html#gacd7a92ad3f976c5d623e9e07bbe817c2", null ],
      [ "DisplayName", "group__UaStackTypes.html#ga5e5757637b0e791ae2bb139377704983", null ],
      [ "NamespaceUri", "group__UaStackTypes.html#ga8b10b5dc3a955300517451fdc125fb1d", null ],
      [ "UnitId", "group__UaStackTypes.html#gab220d76cc348759759dd7c7fe359d700", null ]
    ] ],
    [ "OpcUa_AxisInformation", "structOpcUa__AxisInformation.html", [
      [ "AxisScaleType", "group__UaStackTypes.html#ga0816e2fb0c8528c274c6f9ccc655020a", null ],
      [ "AxisSteps", "group__UaStackTypes.html#ga5104be70bfecbcde2ba8e95febe51f10", null ],
      [ "EngineeringUnits", "group__UaStackTypes.html#gac59ca15321f3dbb52a1936e40a6a9bc7", null ],
      [ "EURange", "group__UaStackTypes.html#gab78f2da3ee6c460219977fb367a03828", null ],
      [ "Title", "group__UaStackTypes.html#gabdafc389b7120064dccc352c132eb696", null ]
    ] ],
    [ "OpcUa_XVType", "structOpcUa__XVType.html", [
      [ "Value", "group__UaStackTypes.html#ga3b182cdf58d719acc9b8911390408c0b", null ],
      [ "X", "group__UaStackTypes.html#gab3fc36e41c207aa086e4c5de281a3b87", null ]
    ] ],
    [ "OpcUa_Annotation", "structOpcUa__Annotation.html", [
      [ "AnnotationTime", "group__UaStackTypes.html#ga2f33b7769d2a71f178ddd496c4f73e14", null ],
      [ "Message", "group__UaStackTypes.html#ga7812025d6af498a733df8f0184f38f23", null ],
      [ "UserName", "group__UaStackTypes.html#ga1580af9f697f975f1c65c9cfd00932c2", null ]
    ] ],
    [ "OpcUa_Node", "structOpcUa__Node.html", null ],
    [ "OpcUa_ObjectNode", "structOpcUa__ObjectNode.html", null ],
    [ "OpcUa_ObjectTypeNode", "structOpcUa__ObjectTypeNode.html", null ],
    [ "OpcUa_VariableNode", "structOpcUa__VariableNode.html", null ],
    [ "OpcUa_VariableTypeNode", "structOpcUa__VariableTypeNode.html", null ],
    [ "OpcUa_ReferenceTypeNode", "structOpcUa__ReferenceTypeNode.html", null ],
    [ "OpcUa_MethodNode", "structOpcUa__MethodNode.html", null ],
    [ "OpcUa_ReadEventDetails", "structOpcUa__ReadEventDetails.html", [
      [ "EndTime", "group__UaStackTypes.html#ga357b704e0df99d805fcb35c355adb081", null ],
      [ "Filter", "group__UaStackTypes.html#gab7762de3a143fa3aa779edbab4fd94ce", null ],
      [ "NumValuesPerNode", "group__UaStackTypes.html#ga37cd99c2d78f73d4d4331452708fe262", null ],
      [ "StartTime", "group__UaStackTypes.html#gacdf57bd222360d2c59a35de7b271d1f3", null ]
    ] ],
    [ "OpcUa_ReadProcessedDetails", "structOpcUa__ReadProcessedDetails.html", [
      [ "AggregateConfiguration", "group__UaStackTypes.html#ga6c28fe30288b57a2ad2e50a618a1e3ff", null ],
      [ "AggregateType", "group__UaStackTypes.html#ga7d8ebb16bc2f25f6b6d09bba30275262", null ],
      [ "EndTime", "group__UaStackTypes.html#ga42844f2a3e8270aa7f749e3605740e87", null ],
      [ "ProcessingInterval", "group__UaStackTypes.html#gac4032d5b58553bc1d8b0368187d7058a", null ],
      [ "StartTime", "group__UaStackTypes.html#gadff860fc2204e0b9db4dabb0e4e07a46", null ]
    ] ],
    [ "OpcUa_HistoryEvent", "structOpcUa__HistoryEvent.html", [
      [ "Events", "group__UaStackTypes.html#ga7888c944d76f66f166812ea22578832a", null ]
    ] ],
    [ "OpcUa_UpdateEventDetails", "structOpcUa__UpdateEventDetails.html", [
      [ "EventData", "group__UaStackTypes.html#ga205c235a7e1920e0480691d270cab3d3", null ],
      [ "Filter", "group__UaStackTypes.html#ga9a7a17d4ebbb8c0484fc13ed218550d4", null ],
      [ "NodeId", "group__UaStackTypes.html#ga8aaf650a031d96b69307e7a42fa9fd66", null ],
      [ "PerformInsertReplace", "group__UaStackTypes.html#ga46cf77cf9807b530ef026ddd2f174775", null ]
    ] ],
    [ "OpcUa_SessionDiagnosticsDataType", "structOpcUa__SessionDiagnosticsDataType.html", [
      [ "ActualSessionTimeout", "group__UaStackTypes.html#gaff45e62f4a687bab6e4e89955603a52b", null ],
      [ "AddNodesCount", "group__UaStackTypes.html#ga788d3164fe218237cac96ee1bcbd8794", null ],
      [ "AddReferencesCount", "group__UaStackTypes.html#ga2bab589aa024fb191163f12252a59d68", null ],
      [ "BrowseCount", "group__UaStackTypes.html#gacf4607cb608f227003a591353542bdef", null ],
      [ "BrowseNextCount", "group__UaStackTypes.html#ga981d84e32b9ebef804d3389986e0cbbd", null ],
      [ "CallCount", "group__UaStackTypes.html#gaacd901ab69887a4c55e3106ba1336ec1", null ],
      [ "ClientConnectionTime", "group__UaStackTypes.html#gab027d5cb7c2ed0e1fe1358c68f656b45", null ],
      [ "ClientDescription", "group__UaStackTypes.html#ga3ca44a16a9de4c8ae4499a5ef313a0a3", null ],
      [ "ClientLastContactTime", "group__UaStackTypes.html#ga60f4e885fe56833848cf33a7e209e0d4", null ],
      [ "CreateMonitoredItemsCount", "group__UaStackTypes.html#ga80a6fdf705fa4111b08ac27468454adc", null ],
      [ "CreateSubscriptionCount", "group__UaStackTypes.html#ga51ab8953757dae7cf821fa9f5d905c64", null ],
      [ "CurrentMonitoredItemsCount", "group__UaStackTypes.html#ga1b95fb7c736080152e1b40f4fd10200e", null ],
      [ "CurrentPublishRequestsInQueue", "group__UaStackTypes.html#ga8f0e90dc4bb29e8c6d7343012251a51c", null ],
      [ "CurrentSubscriptionsCount", "group__UaStackTypes.html#ga7ceb1b6965f95b0625e7e8b3f69484a1", null ],
      [ "DeleteMonitoredItemsCount", "group__UaStackTypes.html#ga0c7f4c9347554c71ab57120dc69adc78", null ],
      [ "DeleteNodesCount", "group__UaStackTypes.html#ga8808ab370380c60b412fac530868609d", null ],
      [ "DeleteReferencesCount", "group__UaStackTypes.html#ga6ed28852591089fab58135b6539348f5", null ],
      [ "DeleteSubscriptionsCount", "group__UaStackTypes.html#gad8ef4aef57298a0fe60437ac23d2a399", null ],
      [ "EndpointUrl", "group__UaStackTypes.html#ga64bc3fbb210aec436346ac0aef707e77", null ],
      [ "HistoryReadCount", "group__UaStackTypes.html#gafad28149e4dc17409b525343e00c326a", null ],
      [ "HistoryUpdateCount", "group__UaStackTypes.html#gaf36f1d76873a744738f3f2dfc6331266", null ],
      [ "LocaleIds", "group__UaStackTypes.html#ga6ec108082bd1f8bdc167a4d37cc8defd", null ],
      [ "MaxResponseMessageSize", "group__UaStackTypes.html#ga15d6fd598539929d4688189d6787e88e", null ],
      [ "ModifyMonitoredItemsCount", "group__UaStackTypes.html#ga23d59b5dcba2b640a5dff0f4cb0b71e6", null ],
      [ "ModifySubscriptionCount", "group__UaStackTypes.html#ga621a94a114d9188c94c0ecb73a54da08", null ],
      [ "PublishCount", "group__UaStackTypes.html#gab15bca82e718927223b55a20c9cbbdb4", null ],
      [ "QueryFirstCount", "group__UaStackTypes.html#gaf47417da33504419b49f27a3e2064fbc", null ],
      [ "QueryNextCount", "group__UaStackTypes.html#gac40bbc9f0f5429b19e44470255f8eb1f", null ],
      [ "ReadCount", "group__UaStackTypes.html#gaf475d21b8750eefa232756889a9a8775", null ],
      [ "RegisterNodesCount", "group__UaStackTypes.html#ga3e18de92bf884bf2b2e7b7d7bb67e9f3", null ],
      [ "RepublishCount", "group__UaStackTypes.html#ga02778218f6886c99afa91d0b97aecd3a", null ],
      [ "ServerUri", "group__UaStackTypes.html#gac1449dce69c7ac30f225f8e551655455", null ],
      [ "SessionId", "group__UaStackTypes.html#ga28d6a7cd3d05d99dd29785032a3ed791", null ],
      [ "SessionName", "group__UaStackTypes.html#gabf4470b79dad905d2c3a9d75836ca0d2", null ],
      [ "SetMonitoringModeCount", "group__UaStackTypes.html#ga41dd022288ed8a3b865f3fe6a0fe74a9", null ],
      [ "SetPublishingModeCount", "group__UaStackTypes.html#ga88afcc3ba86319807f54e67fc3bdc2be", null ],
      [ "SetTriggeringCount", "group__UaStackTypes.html#gad39ffd13fcc307422bd2165e8a62ab70", null ],
      [ "TotalRequestCount", "group__UaStackTypes.html#gaa8325f78caea8b411182e02cdc7532b6", null ],
      [ "TransferSubscriptionsCount", "group__UaStackTypes.html#ga0f7d925bd0e2b571a7e9cb89eb74704d", null ],
      [ "TranslateBrowsePathsToNodeIdsCount", "group__UaStackTypes.html#ga6dd75a59de35ec66db1b0063dddfbbb2", null ],
      [ "UnauthorizedRequestCount", "group__UaStackTypes.html#ga6fb83e8e3550caab9d0c17b07a4494ab", null ],
      [ "UnregisterNodesCount", "group__UaStackTypes.html#ga124c5e21ff11916a85c1c32fbd8e71e6", null ],
      [ "WriteCount", "group__UaStackTypes.html#ga44d3178e3c7b86b2525a7adafb2922ee", null ]
    ] ],
    [ "OpcUa_NodeId", "structOpcUa__NodeId.html", [
      [ "IdentifierType", "group__UaStackTypes.html#ga854aaf9e3181dd2657dd00e7c25560ab", null ],
      [ "NamespaceIndex", "group__UaStackTypes.html#ga2fc647f84259b24bf3c98b7164c768eb", null ]
    ] ],
    [ "OpcUa_ExpandedNodeId", "structOpcUa__ExpandedNodeId.html", [
      [ "NamespaceUri", "group__UaStackTypes.html#gafecbd7216e9c0f8d9d339bcc6b427a7d", null ],
      [ "NodeId", "group__UaStackTypes.html#ga2e5db77bb59f2e7ddd618e389d28231c", null ],
      [ "ServerIndex", "group__UaStackTypes.html#ga2d4c93762dcf8b381a9a20e112beca0f", null ]
    ] ],
    [ "OpcUa_DiagnosticInfo", "structOpcUa__DiagnosticInfo.html", [
      [ "AdditionalInfo", "group__UaStackTypes.html#ga26be87f4f5998f0b1f985d356582e159", null ],
      [ "InnerDiagnosticInfo", "group__UaStackTypes.html#gabb0898b6f8ee1a7fa034b2471bb0afbe", null ],
      [ "InnerStatusCode", "group__UaStackTypes.html#ga6c227ee81cb5021424e3ab5dc99edbef", null ],
      [ "Locale", "group__UaStackTypes.html#ga6b894922e46884db6dd4941c1078c768", null ],
      [ "LocalizedText", "group__UaStackTypes.html#ga05d2adafce9f931e2ccd04a52130d46a", null ],
      [ "NamespaceUri", "group__UaStackTypes.html#gaf8248cd635a1a6c00ebe907d16f6909a", null ],
      [ "SymbolicId", "group__UaStackTypes.html#ga2b8a8b3fd6f9ee6d416fe90f71354bca", null ]
    ] ],
    [ "OpcUa_LocalizedText", "structOpcUa__LocalizedText.html", [
      [ "Locale", "group__UaStackTypes.html#ga7013c05ee8e0230dc014ed49d7ce1c6d", null ],
      [ "Text", "group__UaStackTypes.html#ga777fb051240e48704c77a5533f63aa98", null ]
    ] ],
    [ "OpcUa_QualifiedName", "structOpcUa__QualifiedName.html", [
      [ "Name", "group__UaStackTypes.html#gae98ac296bc390aa833885061dafb404d", null ],
      [ "NamespaceIndex", "group__UaStackTypes.html#ga659c3f296949ddb1d186e291c2a57728", null ]
    ] ],
    [ "OpcUa_ExtensionObject", "structOpcUa__ExtensionObject.html", [
      [ "Body", "group__UaStackTypes.html#ga2486cdc2ba5e2ecc20b0280390982360", null ],
      [ "BodySize", "group__UaStackTypes.html#ga6bfd27ad076d6d7c75f25ae381959b88", null ],
      [ "Encoding", "group__UaStackTypes.html#gacc281c31f020ec3ae619e25ee1986979", null ],
      [ "TypeId", "group__UaStackTypes.html#ga47e1863bd35cf7bc2299e3221f69200d", null ]
    ] ],
    [ "OpcUa_Variant", "structOpcUa__Variant.html", [
      [ "ArrayType", "group__UaStackTypes.html#gac5adb362b6034f59215f5b0337712ffa", null ],
      [ "Datatype", "group__UaStackTypes.html#ga284252dadd030c367f045f6bd0d8e72f", null ],
      [ "Value", "group__UaStackTypes.html#gad1541d8ebb3bf7b7837a3862c6266f39", null ]
    ] ],
    [ "OpcUa_DataValue", "structOpcUa__DataValue.html", [
      [ "ServerPicoseconds", "group__UaStackTypes.html#ga12ea0497b00e3636710c88bbccbbb725", null ],
      [ "ServerTimestamp", "group__UaStackTypes.html#gace2e324ee3509f81a6f8b02dfbf67ca7", null ],
      [ "SourcePicoseconds", "group__UaStackTypes.html#gaddf019c1a49eda91b2a62a120c7f6b3d", null ],
      [ "SourceTimestamp", "group__UaStackTypes.html#gaade831a742f091f31a90ebe84086ec53", null ],
      [ "StatusCode", "group__UaStackTypes.html#ga01a8fa2beacee3ea16be457be2d68c3b", null ],
      [ "Value", "group__UaStackTypes.html#ga58c8a3df4e676757166a9cb12441f8ae", null ]
    ] ],
    [ "Identifier", "unionOpcUa__NodeId_1_1Identifier.html", [
      [ "ByteString", "unionOpcUa__NodeId_1_1Identifier.html#abee59ab86aa5583f84e383abe9114d58", null ],
      [ "Guid", "unionOpcUa__NodeId_1_1Identifier.html#a9d88b75cddbc0b6ca46cf2dc2434b0dc", null ],
      [ "Numeric", "unionOpcUa__NodeId_1_1Identifier.html#a3b44707e7bdf5dfbf3f043ffddc2b4f3", null ],
      [ "String", "unionOpcUa__NodeId_1_1Identifier.html#a7fdfa48aeed10e82d1bab4f23f2f136e", null ]
    ] ],
    [ "OpcUa_String", "structOpcUa__String.html", null ],
    [ "OpcUa_ByteString", "structOpcUa__ByteString.html", null ],
    [ "OpcUa_Guid", "structOpcUa__Guid.html", null ],
    [ "OpcUa_StatusCode", "structOpcUa__StatusCode.html", null ],
    [ "OpcUa_DateTime", "structOpcUa__DateTime.html", null ],
    [ "OpcUa_XmlElement", "structOpcUa__XmlElement.html", null ],
    [ "OpcUa_ApplicationType", "group__UaStackTypes.html#ga3bef1dc90b31afd81713a6ed7ab6ce4c", [
      [ "OpcUa_ApplicationType_Server", "group__UaStackTypes.html#gga3bef1dc90b31afd81713a6ed7ab6ce4cab1fa65bfdaa3595666d8313b715ab8f8", null ],
      [ "OpcUa_ApplicationType_Client", "group__UaStackTypes.html#gga3bef1dc90b31afd81713a6ed7ab6ce4cacf52e2909564cb84c03366eb3271f7b7", null ],
      [ "OpcUa_ApplicationType_ClientAndServer", "group__UaStackTypes.html#gga3bef1dc90b31afd81713a6ed7ab6ce4ca787a730e879bbceabfc0396c6e4849e6", null ],
      [ "OpcUa_ApplicationType_DiscoveryServer", "group__UaStackTypes.html#gga3bef1dc90b31afd81713a6ed7ab6ce4caf6aafe0cb5e727750520371e9bf14659", null ]
    ] ],
    [ "OpcUa_AttributeWriteMask", "group__UaStackTypes.html#gab702a8c1b38143c9dae201c5b66ffcdc", null ],
    [ "OpcUa_AxisScaleEnumeration", "group__UaStackTypes.html#ga80b76c58cc0ef555c286305a8cf3b485", [
      [ "OpcUa_AxisScaleEnumeration_Linear", "group__UaStackTypes.html#gga80b76c58cc0ef555c286305a8cf3b485a1c454f532ecfe04328f1ef3bf7ff3a71", null ],
      [ "OpcUa_AxisScaleEnumeration_Log", "group__UaStackTypes.html#gga80b76c58cc0ef555c286305a8cf3b485a0124beec5d0ad87da651aef42d9822d7", null ],
      [ "OpcUa_AxisScaleEnumeration_Ln", "group__UaStackTypes.html#gga80b76c58cc0ef555c286305a8cf3b485a90ef36fb638aa13a8219aa58390bd35a", null ]
    ] ],
    [ "OpcUa_BrowseDirection", "group__UaStackTypes.html#gac6e323174519f2b3cb45bcfdfe83493e", [
      [ "OpcUa_BrowseDirection_Forward", "group__UaStackTypes.html#ggac6e323174519f2b3cb45bcfdfe83493eaa14d9a25c0ffe88da69bbd4aae3be971", null ],
      [ "OpcUa_BrowseDirection_Inverse", "group__UaStackTypes.html#ggac6e323174519f2b3cb45bcfdfe83493eadd7bf8f7c083855990b0d347723bcab1", null ],
      [ "OpcUa_BrowseDirection_Both", "group__UaStackTypes.html#ggac6e323174519f2b3cb45bcfdfe83493ea13828fa8c6438c9d019fcb4caa8d053c", null ]
    ] ],
    [ "OpcUa_BrowseResultMask", "group__UaStackTypes.html#ga938f1eac3a09c6af342ba3381cf9d1f1", null ],
    [ "OpcUa_ComplianceLevel", "group__UaStackTypes.html#ga00698679adabfc0d5ff0a21b845b164a", [
      [ "OpcUa_ComplianceLevel_Untested", "group__UaStackTypes.html#gga00698679adabfc0d5ff0a21b845b164aaf76bebf8131829f3a8e43d8808185322", null ],
      [ "OpcUa_ComplianceLevel_Partial", "group__UaStackTypes.html#gga00698679adabfc0d5ff0a21b845b164aa603ea90523ff440b1a6a3addf00e6b4a", null ],
      [ "OpcUa_ComplianceLevel_SelfTested", "group__UaStackTypes.html#gga00698679adabfc0d5ff0a21b845b164aa333ceb2ac568166dfcbf42d69ed56a48", null ],
      [ "OpcUa_ComplianceLevel_Certified", "group__UaStackTypes.html#gga00698679adabfc0d5ff0a21b845b164aa286ea9a7fece77c5417a1e1e0b2bcd34", null ]
    ] ],
    [ "OpcUa_DeadbandType", "group__UaStackTypes.html#gafaefe1c432ae81e67ca7827e229c358e", [
      [ "OpcUa_DeadbandType_None", "group__UaStackTypes.html#ggafaefe1c432ae81e67ca7827e229c358ea507c01da66c0b212571b329dde5d6c5b", null ],
      [ "OpcUa_DeadbandType_Absolute", "group__UaStackTypes.html#ggafaefe1c432ae81e67ca7827e229c358ea43b00e83eee5502a1824cba04d38eb83", null ],
      [ "OpcUa_DeadbandType_Percent", "group__UaStackTypes.html#ggafaefe1c432ae81e67ca7827e229c358ea37f0ff56eeb82e21b1755149468311aa", null ]
    ] ],
    [ "OpcUa_EnumeratedTestType", "group__UaStackTypes.html#gab82a17bb4cdccdcb210db29bdffac8fe", null ],
    [ "OpcUa_ExtensionObjectEncoding", "group__UaStackTypes.html#gad647cc69a23f17f13be42c995dfebbc6", [
      [ "OpcUa_ExtensionObjectEncoding_None", "group__UaStackTypes.html#ggad647cc69a23f17f13be42c995dfebbc6a13ed11c2cb073a22fbc0a5279dda9475", null ],
      [ "OpcUa_ExtensionObjectEncoding_Binary", "group__UaStackTypes.html#ggad647cc69a23f17f13be42c995dfebbc6a7ec26c02d5932112e993acfc6fdeeacb", null ],
      [ "OpcUa_ExtensionObjectEncoding_Xml", "group__UaStackTypes.html#ggad647cc69a23f17f13be42c995dfebbc6ad1843ef0988091101f1fbde6c2d280ca", null ],
      [ "OpcUa_ExtensionObjectEncoding_EncodeableObject", "group__UaStackTypes.html#ggad647cc69a23f17f13be42c995dfebbc6a10e30246b0f533271a2c23f5f08046fc", null ]
    ] ],
    [ "OpcUa_FilterOperator", "group__UaStackTypes.html#ga2fac50e4abd639780a065203f2e0505f", [
      [ "OpcUa_FilterOperator_Equals", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fac5f3da948e2cfe98d03b583b99e85e13", null ],
      [ "OpcUa_FilterOperator_IsNull", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa52649cf3c0a1cda1dd5e6f8f73546b77", null ],
      [ "OpcUa_FilterOperator_GreaterThan", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505faef1f236f1436d395b54ecd18be26cb24", null ],
      [ "OpcUa_FilterOperator_LessThan", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fae006cdc0151f0fb58a7b3dd47bacdb91", null ],
      [ "OpcUa_FilterOperator_GreaterThanOrEqual", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505faf625753c66e40e6ae45917b8dd37f764", null ],
      [ "OpcUa_FilterOperator_LessThanOrEqual", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa1973af2c90878284310532f556aada34", null ],
      [ "OpcUa_FilterOperator_Like", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa589f37fe6736ee8f3add3dc5316df152", null ],
      [ "OpcUa_FilterOperator_Not", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa45fd02297a1d62b921372a3f117c9755", null ],
      [ "OpcUa_FilterOperator_Between", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa8d7b381e5077206ce0a518e967c9765d", null ],
      [ "OpcUa_FilterOperator_InList", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fad7dad47e963552b492366acd7be1f66e", null ],
      [ "OpcUa_FilterOperator_And", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fabe9d3924df536f7bef496091eee5956b", null ],
      [ "OpcUa_FilterOperator_Or", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa45a7ff833cad4f877075f2095483f64b", null ],
      [ "OpcUa_FilterOperator_Cast", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa4317d7bac06a12afa5e8dfaaa27a521d", null ],
      [ "OpcUa_FilterOperator_InView", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505faa2610a7186125ecb1bcf862f246e5512", null ],
      [ "OpcUa_FilterOperator_OfType", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505faacb0964cf773e7e23f26ec228e52593c", null ],
      [ "OpcUa_FilterOperator_RelatedTo", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa0937b2e33b9530c84cf52aa5d7dfe86c", null ],
      [ "OpcUa_FilterOperator_BitwiseAnd", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505faf1f525cfd4703320d1317307b7a52e38", null ],
      [ "OpcUa_FilterOperator_BitwiseOr", "group__UaStackTypes.html#gga2fac50e4abd639780a065203f2e0505fa4897a16e2b3347ed6c10fd47b83a36c4", null ]
    ] ],
    [ "OpcUa_IdentifierType", "group__UaStackTypes.html#ga754bfc43335748685b66c7ccd303631b", [
      [ "OpcUa_IdentifierType_Numeric", "group__UaStackTypes.html#gga754bfc43335748685b66c7ccd303631ba3a4135bec8b1c9e0cf3a6a00ed37a8bb", null ],
      [ "OpcUa_IdentifierType_String", "group__UaStackTypes.html#gga754bfc43335748685b66c7ccd303631ba1c892463314038778ccca46215b349ab", null ],
      [ "OpcUa_IdentifierType_Guid", "group__UaStackTypes.html#gga754bfc43335748685b66c7ccd303631ba6089c14ea0f16dd627f944b1438d56d0", null ],
      [ "OpcUa_IdentifierType_Opaque", "group__UaStackTypes.html#gga754bfc43335748685b66c7ccd303631ba85a14f429e0e119affd82e11351dbb1e", null ]
    ] ],
    [ "OpcUa_IdType", "group__UaStackTypes.html#gaaf97ff614739623f4b68a957e421e907", [
      [ "OpcUa_IdType_Numeric", "group__UaStackTypes.html#ggaaf97ff614739623f4b68a957e421e907af1af3991759515b6128b881c0d7b3400", null ],
      [ "OpcUa_IdType_String", "group__UaStackTypes.html#ggaaf97ff614739623f4b68a957e421e907af3b83d1f303d504a58a67fcc46507511", null ],
      [ "OpcUa_IdType_Guid", "group__UaStackTypes.html#ggaaf97ff614739623f4b68a957e421e907a2e4aba995be6ddc5eed6c29afed53ab2", null ],
      [ "OpcUa_IdType_Opaque", "group__UaStackTypes.html#ggaaf97ff614739623f4b68a957e421e907a99868a88ffc249f166f9cd0f5b2a86fa", null ]
    ] ],
    [ "OpcUa_MessageSecurityMode", "group__UaStackTypes.html#gaa1bba9cef7119b8f02986a7415fe8e11", [
      [ "OpcUa_MessageSecurityMode_Invalid", "group__UaStackTypes.html#ggaa1bba9cef7119b8f02986a7415fe8e11aebe805c7edae9ff974f7a060de77d274", null ],
      [ "OpcUa_MessageSecurityMode_None", "group__UaStackTypes.html#ggaa1bba9cef7119b8f02986a7415fe8e11a96d0a8d5d407e52f7219a7df3464a516", null ],
      [ "OpcUa_MessageSecurityMode_Sign", "group__UaStackTypes.html#ggaa1bba9cef7119b8f02986a7415fe8e11a7d02fc6d4e4eb737c52d10772865a502", null ],
      [ "OpcUa_MessageSecurityMode_SignAndEncrypt", "group__UaStackTypes.html#ggaa1bba9cef7119b8f02986a7415fe8e11a4f46dd43167053b8ce493c52beb74cb7", null ]
    ] ],
    [ "OpcUa_MonitoringMode", "group__UaStackTypes.html#ga4feaf4f2745d4f725876bc486a72f757", [
      [ "OpcUa_MonitoringMode_Disabled", "group__UaStackTypes.html#gga4feaf4f2745d4f725876bc486a72f757a9ffe9b8536698df75ac2772ec5c46732", null ],
      [ "OpcUa_MonitoringMode_Sampling", "group__UaStackTypes.html#gga4feaf4f2745d4f725876bc486a72f757a27ee04035b7986f7ab750caf1a0c216d", null ],
      [ "OpcUa_MonitoringMode_Reporting", "group__UaStackTypes.html#gga4feaf4f2745d4f725876bc486a72f757a1dedbb5986efdc1fdacb986e93481166", null ]
    ] ],
    [ "OpcUa_NamingRuleType", "group__UaStackTypes.html#gaaa4b09efff8f2d515446decd98277fb9", null ],
    [ "OpcUa_NodeAttributesMask", "group__UaStackTypes.html#ga3cf5149b5fd8d62a78a4f6a62e89d4ab", null ],
    [ "OpcUa_NodeClass", "group__UaStackTypes.html#ga5691e8422551fdf5ecff9216efc3f147", null ],
    [ "OpcUa_NodeIdType", "group__UaStackTypes.html#gad2475c504518fba80249d5a98729e0ec", null ],
    [ "OpcUa_PerformUpdateType", "group__UaStackTypes.html#gab06c2e30d033640a6332b713635e3408", [
      [ "OpcUa_PerformUpdateType_Insert", "group__UaStackTypes.html#ggab06c2e30d033640a6332b713635e3408ab0717ecddfaed0d072fbfbf742da94f3", null ],
      [ "OpcUa_PerformUpdateType_Replace", "group__UaStackTypes.html#ggab06c2e30d033640a6332b713635e3408a362a76968c5fbfacf5aae5ddbefa89b1", null ],
      [ "OpcUa_PerformUpdateType_Update", "group__UaStackTypes.html#ggab06c2e30d033640a6332b713635e3408a9cd875720801d5959a810dc8722ca8e6", null ],
      [ "OpcUa_PerformUpdateType_Remove", "group__UaStackTypes.html#ggab06c2e30d033640a6332b713635e3408a2f09946b103b508cccbf2a10cca51a49", null ]
    ] ],
    [ "OpcUa_RedundancySupport", "group__UaStackTypes.html#ga78adbf03e7179588455daac230f373fe", [
      [ "OpcUa_RedundancySupport_None", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373fea03c5b6ef1b157ec1a61e9b2f9e7fe89a", null ],
      [ "OpcUa_RedundancySupport_Cold", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373feaf35fb0a33ce467d0c04a0e5f3239ce45", null ],
      [ "OpcUa_RedundancySupport_Warm", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373fea5463191d8f9a8a51592606cd31f3bb20", null ],
      [ "OpcUa_RedundancySupport_Hot", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373fea49d454db5fc32789cf399309d5f44714", null ],
      [ "OpcUa_RedundancySupport_Transparent", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373fea4f350add173f6a9899321a73d681368b", null ],
      [ "OpcUa_RedundancySupport_HotAndMirrored", "group__UaStackTypes.html#gga78adbf03e7179588455daac230f373fea3e4daecbf13dbf0c9998e99e86ecc1a6", null ]
    ] ],
    [ "OpcUa_SecurityTokenRequestType", "group__UaStackTypes.html#ga694cfafc5512f3c3391dbe642061fa23", [
      [ "OpcUa_SecurityTokenRequestType_Issue", "group__UaStackTypes.html#gga694cfafc5512f3c3391dbe642061fa23a1b9e4ec663367353c3fa7973aea80b57", null ],
      [ "OpcUa_SecurityTokenRequestType_Renew", "group__UaStackTypes.html#gga694cfafc5512f3c3391dbe642061fa23a2cf04886648a3004a76b12446c769a29", null ]
    ] ],
    [ "OpcUa_ServerState", "group__UaStackTypes.html#ga71a69efb0cfb76fcdeaf8039e4adc8db", [
      [ "OpcUa_ServerState_Running", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dba42f045bb1d6ea66ef2417f63baf82847", null ],
      [ "OpcUa_ServerState_Failed", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dbae0c84b056153c9ba7cf949f06ad3879f", null ],
      [ "OpcUa_ServerState_NoConfiguration", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dbaf208dbf3203072f90cef948f3264f0a8", null ],
      [ "OpcUa_ServerState_Suspended", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dba549aaeb65f1cf352087560069b607b16", null ],
      [ "OpcUa_ServerState_Shutdown", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dbaa670757873df48a8751899e67670605a", null ],
      [ "OpcUa_ServerState_Test", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dba61482c690c929b415795582b19ad7959", null ],
      [ "OpcUa_ServerState_CommunicationFault", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dbab9e3fb008d77df6c518b52ee0f22a4cf", null ],
      [ "OpcUa_ServerState_Unknown", "group__UaStackTypes.html#gga71a69efb0cfb76fcdeaf8039e4adc8dba2646826c98e81ce9c06022640f33f3d4", null ]
    ] ],
    [ "OpcUa_TimestampsToReturn", "group__UaStackTypes.html#ga1d4d8325690c12e098120853cd20ad68", [
      [ "OpcUa_TimestampsToReturn_Source", "group__UaStackTypes.html#gga1d4d8325690c12e098120853cd20ad68a269cea0e17e12cad01163c5c4e49b20e", null ],
      [ "OpcUa_TimestampsToReturn_Server", "group__UaStackTypes.html#gga1d4d8325690c12e098120853cd20ad68a69f90608e13a9dcef04d6b11ab71536c", null ],
      [ "OpcUa_TimestampsToReturn_Both", "group__UaStackTypes.html#gga1d4d8325690c12e098120853cd20ad68a89be756c64d96b7d4738e9ded251dc10", null ],
      [ "OpcUa_TimestampsToReturn_Neither", "group__UaStackTypes.html#gga1d4d8325690c12e098120853cd20ad68a09bc4ac9953ac830928870959d15047a", null ]
    ] ],
    [ "OpcUa_TrustListMasks", "group__UaStackTypes.html#ga68dbb6db65b3a5c60861f011dda980c2", [
      [ "OpcUa_TrustListMasks_None", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2a62fd8beba85fbe3bdf91fdad01b91054", null ],
      [ "OpcUa_TrustListMasks_TrustedCertificates", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2a4827b8b3893e7076a1f5ba755c9d8354", null ],
      [ "OpcUa_TrustListMasks_TrustedCrls", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2ac35733a40696872e1db00e81addc2c97", null ],
      [ "OpcUa_TrustListMasks_IssuerCertificates", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2a09ad75e537d3b04ef89d998ea1462ff1", null ],
      [ "OpcUa_TrustListMasks_IssuerCrls", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2a88b5e0ee920b127a3e8ec163f38675b3", null ],
      [ "OpcUa_TrustListMasks_All", "group__UaStackTypes.html#gga68dbb6db65b3a5c60861f011dda980c2a3fd925cc467b4b7888a84f156de7ce23", null ]
    ] ],
    [ "OpcUa_UserTokenType", "group__UaStackTypes.html#gafedb3e9fcc00f6b894a8c03c3d9ae3b5", [
      [ "OpcUa_UserTokenType_Anonymous", "group__UaStackTypes.html#ggafedb3e9fcc00f6b894a8c03c3d9ae3b5aa6b7862aa19cf12ac01c4cff56d129fd", null ],
      [ "OpcUa_UserTokenType_UserName", "group__UaStackTypes.html#ggafedb3e9fcc00f6b894a8c03c3d9ae3b5a9c8e6491d454932620ad0ba11669409c", null ],
      [ "OpcUa_UserTokenType_Certificate", "group__UaStackTypes.html#ggafedb3e9fcc00f6b894a8c03c3d9ae3b5a7bb283b69d07a95e578610afabd5ab79", null ],
      [ "OpcUa_UserTokenType_IssuedToken", "group__UaStackTypes.html#ggafedb3e9fcc00f6b894a8c03c3d9ae3b5a0f06560445f7555000d74ef958362ef2", null ]
    ] ],
    [ "ActualSessionTimeout", "group__UaStackTypes.html#gaff45e62f4a687bab6e4e89955603a52b", null ],
    [ "AddDiagnosticInfos", "group__UaStackTypes.html#gabb11dd8a000783c4c4eef964ebddc28b", null ],
    [ "AddedNodeId", "group__UaStackTypes.html#gadd230891fc9dc6906f33e3abe4909d0e", null ],
    [ "AdditionalHeader", "group__UaStackTypes.html#ga7f99210601fa2c7636c1eefbd7a53dcf", null ],
    [ "AdditionalHeader", "group__UaStackTypes.html#gae7789bdc5f75288cafbc06519e8b360d", null ],
    [ "AdditionalInfo", "group__UaStackTypes.html#ga26be87f4f5998f0b1f985d356582e159", null ],
    [ "AddNodesCount", "group__UaStackTypes.html#ga788d3164fe218237cac96ee1bcbd8794", null ],
    [ "AddReferencesCount", "group__UaStackTypes.html#ga2bab589aa024fb191163f12252a59d68", null ],
    [ "AddResults", "group__UaStackTypes.html#gac884fa18a528af12be51d9af1ccbb9cd", null ],
    [ "Affected", "group__UaStackTypes.html#gafe532cccf52c65a6878f7ff772115767", null ],
    [ "Affected", "group__UaStackTypes.html#gade6cce9ba47d92017d1fd5c6f42ccb78", null ],
    [ "AffectedType", "group__UaStackTypes.html#gaa2f31be387f80afa65142cfdb9919989", null ],
    [ "AffectedType", "group__UaStackTypes.html#ga65718273fce18e892af728e9bf697632", null ],
    [ "AggregateConfiguration", "group__UaStackTypes.html#ga6c28fe30288b57a2ad2e50a618a1e3ff", null ],
    [ "AggregateType", "group__UaStackTypes.html#ga7d8ebb16bc2f25f6b6d09bba30275262", null ],
    [ "Alias", "group__UaStackTypes.html#gae69a43b8baed353bce814a1236d5ffc5", null ],
    [ "AnnotationTime", "group__UaStackTypes.html#ga2f33b7769d2a71f178ddd496c4f73e14", null ],
    [ "ApplicationName", "group__UaStackTypes.html#ga89048407f80d2c1c891a6344d2e21345", null ],
    [ "ApplicationType", "group__UaStackTypes.html#gaa0ad160dd09cdfab75a96a2f2d733d52", null ],
    [ "ApplicationUri", "group__UaStackTypes.html#ga2f6625b5cf103ab1ac4fef4ba5520931", null ],
    [ "ArrayDimensions", "group__UaStackTypes.html#gaf286c6acb5eb46279a828f68400a5ac4", null ],
    [ "ArrayType", "group__UaStackTypes.html#gac5adb362b6034f59215f5b0337712ffa", null ],
    [ "AttributeId", "group__UaStackTypes.html#ga39fdad3ba339a801f0c34c227dcfdac6", null ],
    [ "AttributeId", "group__UaStackTypes.html#ga99d92903d0b454d66598d5890bdd86c3", null ],
    [ "AttributeId", "group__UaStackTypes.html#ga6e679f44d36697bef407341ab128cea0", null ],
    [ "AttributeId", "group__UaStackTypes.html#gabed8bcdfefdbfa09cadf412829c9b86b", null ],
    [ "AttributeId", "group__UaStackTypes.html#ga8a57738f90ebb130e640ff056b92d89b", null ],
    [ "AuditEntryId", "group__UaStackTypes.html#ga980afeaa04b82ddb7e235566a7f396ff", null ],
    [ "AuthenticationMechanism", "group__UaStackTypes.html#ga7701ce5a311a0986f44143da9b65ceff", null ],
    [ "AuthenticationToken", "group__UaStackTypes.html#ga4e42cebfa43efd01cc1aed5506431809", null ],
    [ "AvailableSequenceNumbers", "group__UaStackTypes.html#ga877a6418970c86fb58be356ed62fc701", null ],
    [ "AvailableSequenceNumbers", "group__UaStackTypes.html#ga4785ccc6cc62d3a93dc480a1fc0e43c6", null ],
    [ "AxisScaleType", "group__UaStackTypes.html#ga0816e2fb0c8528c274c6f9ccc655020a", null ],
    [ "AxisSteps", "group__UaStackTypes.html#ga5104be70bfecbcde2ba8e95febe51f10", null ],
    [ "Body", "group__UaStackTypes.html#ga2486cdc2ba5e2ecc20b0280390982360", null ],
    [ "BodySize", "group__UaStackTypes.html#ga6bfd27ad076d6d7c75f25ae381959b88", null ],
    [ "BrowseCount", "group__UaStackTypes.html#gacf4607cb608f227003a591353542bdef", null ],
    [ "BrowseDirection", "group__UaStackTypes.html#ga7b17318b612931e0fc438e4e3eb8b038", null ],
    [ "BrowseName", "group__UaStackTypes.html#gae4a91e59c025d4d183cd4cc07046d166", null ],
    [ "BrowseName", "group__UaStackTypes.html#gafb407977ec3bb60572b27676c5745fcb", null ],
    [ "BrowseNextCount", "group__UaStackTypes.html#ga981d84e32b9ebef804d3389986e0cbbd", null ],
    [ "BrowsePath", "group__UaStackTypes.html#ga8594bfa18fc5034df7e40e7dd219948d", null ],
    [ "BrowsePath", "group__UaStackTypes.html#ga2bdcc24bc9978bfddd67a5118f536a93", null ],
    [ "BrowsePaths", "group__UaStackTypes.html#ga47d44d2bd95514a5b3973603a9f210aa", null ],
    [ "BuildDate", "group__UaStackTypes.html#gad3d2f9b2635c6b978907d862602e5e89", null ],
    [ "BuildDate", "group__UaStackTypes.html#ga5b44440c3bdacc32eaa8a64fc2d0e499", null ],
    [ "BuildInfo", "group__UaStackTypes.html#ga13c4d497584bace0a48692aed794af12", null ],
    [ "BuildNumber", "group__UaStackTypes.html#gac01990397c73d96991356de70522383a", null ],
    [ "BuildNumber", "group__UaStackTypes.html#ga2a60de64f498cfefb99c303cd19112b7", null ],
    [ "CallCount", "group__UaStackTypes.html#gaacd901ab69887a4c55e3106ba1336ec1", null ],
    [ "CancelCount", "group__UaStackTypes.html#ga38b6ef80c971d3521d8cff6000758f30", null ],
    [ "CertificateData", "group__UaStackTypes.html#ga6de3348eaa07af75da4241b9caf912f9", null ],
    [ "ClientCertificate", "group__UaStackTypes.html#ga4a9b61e555ff62fa72d1ab7cbf29c9b2", null ],
    [ "ClientConnectionTime", "group__UaStackTypes.html#gab027d5cb7c2ed0e1fe1358c68f656b45", null ],
    [ "ClientDescription", "group__UaStackTypes.html#ga3ca44a16a9de4c8ae4499a5ef313a0a3", null ],
    [ "ClientHandle", "group__UaStackTypes.html#ga833034d9d80179942078f06bc2af005e", null ],
    [ "ClientLastContactTime", "group__UaStackTypes.html#ga60f4e885fe56833848cf33a7e209e0d4", null ],
    [ "ClientUserIdHistory", "group__UaStackTypes.html#ga7b15772e9703e220688068de82cda99a", null ],
    [ "ClientUserIdOfSession", "group__UaStackTypes.html#ga77b62ee9aeac3370560beb66432f0419", null ],
    [ "ComplianceDate", "group__UaStackTypes.html#ga02f78a46b8e810d5038e84b5cb6b3d82", null ],
    [ "ComplianceLevel", "group__UaStackTypes.html#gaa8b110587af89a45ace46f25e719e1eb", null ],
    [ "ComplianceTool", "group__UaStackTypes.html#gaadc15d36acb1bbae4a7ad076a3819265", null ],
    [ "ContinuationPoint", "group__UaStackTypes.html#ga4bcf9eab29b1bcb68935f0dd3430cea4", null ],
    [ "ContinuationPoint", "group__UaStackTypes.html#gaa4c860aaab9b7a05f14208da0aebbcb3", null ],
    [ "ContinuationPoint", "group__UaStackTypes.html#gad45b7156c0d565d1cf8599e93d25b813", null ],
    [ "ContinuationPoint", "group__UaStackTypes.html#ga04e94c656c2cf51a7608725b158db1dd", null ],
    [ "ContinuationPoint", "group__UaStackTypes.html#gae94c2ced5e2044333711927f00552441", null ],
    [ "ContinuationPoints", "group__UaStackTypes.html#ga58731108fa26da1b53cb60e26e12db89", null ],
    [ "CreateMonitoredItemsCount", "group__UaStackTypes.html#ga80a6fdf705fa4111b08ac27468454adc", null ],
    [ "CreateSubscriptionCount", "group__UaStackTypes.html#ga51ab8953757dae7cf821fa9f5d905c64", null ],
    [ "CumulatedSessionCount", "group__UaStackTypes.html#gaa420700e79ddb4d254a24b5798660301", null ],
    [ "CumulatedSubscriptionCount", "group__UaStackTypes.html#gae78d8296a45a445fc895ecf25c26e13f", null ],
    [ "CurrentKeepAliveCount", "group__UaStackTypes.html#gaff32d0227d7b88ce782a78d44dd0b78b", null ],
    [ "CurrentLifetimeCount", "group__UaStackTypes.html#gafcaa7135349d19d73d49a4d75c015c26", null ],
    [ "CurrentMonitoredItemsCount", "group__UaStackTypes.html#ga1b95fb7c736080152e1b40f4fd10200e", null ],
    [ "CurrentPublishRequestsInQueue", "group__UaStackTypes.html#ga8f0e90dc4bb29e8c6d7343012251a51c", null ],
    [ "CurrentSessionCount", "group__UaStackTypes.html#ga04b4388289b18877454262feb29df3c1", null ],
    [ "CurrentSubscriptionCount", "group__UaStackTypes.html#ga2f10b86f3397dda082b9e9f11e68ee82", null ],
    [ "CurrentSubscriptionsCount", "group__UaStackTypes.html#ga7ceb1b6965f95b0625e7e8b3f69484a1", null ],
    [ "CurrentTime", "group__UaStackTypes.html#gae4cdb06cb9d125b783019dd79541ad10", null ],
    [ "DataChangeNotificationsCount", "group__UaStackTypes.html#ga365692a07f97360b07783236806fc6ce", null ],
    [ "DataDiagnosticInfos", "group__UaStackTypes.html#ga05240d8a1dec4cdbbee5d23312c2bc45", null ],
    [ "DataEncoding", "group__UaStackTypes.html#ga8a59945a3461f75d394153f5ce12ab6e", null ],
    [ "DataEncoding", "group__UaStackTypes.html#ga20a62e3aadf460d2dc579797eea3ff60", null ],
    [ "DataStatusCodes", "group__UaStackTypes.html#ga039fb8a8c4fabc65c987d62573969b8c", null ],
    [ "DataToReturn", "group__UaStackTypes.html#ga854aebd457580678184370438bf66d52", null ],
    [ "Datatype", "group__UaStackTypes.html#ga284252dadd030c367f045f6bd0d8e72f", null ],
    [ "DataType", "group__UaStackTypes.html#gaa9e8d5f4dc3edc78b63bf1ab626b8b4d", null ],
    [ "DaylightSavingInOffset", "group__UaStackTypes.html#ga7339c10e013b0b5745aec15caa3b899b", null ],
    [ "DeleteBidirectional", "group__UaStackTypes.html#ga4d7a0e0cdb3f27256e4fc8ea6ca4de09", null ],
    [ "DeleteMonitoredItemsCount", "group__UaStackTypes.html#ga0c7f4c9347554c71ab57120dc69adc78", null ],
    [ "DeleteNodesCount", "group__UaStackTypes.html#ga8808ab370380c60b412fac530868609d", null ],
    [ "DeleteReferencesCount", "group__UaStackTypes.html#ga6ed28852591089fab58135b6539348f5", null ],
    [ "DeleteSubscriptionsCount", "group__UaStackTypes.html#gad8ef4aef57298a0fe60437ac23d2a399", null ],
    [ "DeleteTargetReferences", "group__UaStackTypes.html#ga157c48c5f06e0274b51b9f4c878b3424", null ],
    [ "Description", "group__UaStackTypes.html#ga1f86a641da1ae04f7d8b0be1777955c4", null ],
    [ "Description", "group__UaStackTypes.html#gaaab865ebdd5c74f1bb2362f7e377a054", null ],
    [ "Description", "group__UaStackTypes.html#gacd7a92ad3f976c5d623e9e07bbe817c2", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga60108bf43176e8efe20055acd59b4d07", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaa681ad1925220ad2d1f6c557007794ea", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga6ad2333e4d0a16b1be02a9ea35c97212", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaaef04f0738d0ed89c197af5eac045e37", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gae08d37903a8508b790361c4e42131ddc", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaa434bc727e37c38fd14dd52515948840", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga89a8b46caf8027109c58c4691f95537d", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga216dcba906fd60090cce0cc819a88d4e", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaff8d0e232efbcf8225a4356feffb0b2c", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga0014659bf4d0b77e3e0105983f7a024d", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gabb3f65e264bacde430b264a479d441a7", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga6c28c846778e805617f84fde53543b78", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga0b40efddb7207e5ccb9520f7eafc459d", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaae067352472c019ceeb393b98699dc22", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga9eba211295e5f501c6a9fa571f088636", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gaea79ccb70081427f7075d0a9fad6be83", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#gab362b84a071184d437c0f43b80886cfb", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga2033811b1a28e54c66f1b1a956fd11bf", null ],
    [ "DiagnosticInfos", "group__UaStackTypes.html#ga518dd81c10f59de4f85ae4309d92ead8", null ],
    [ "DisableCount", "group__UaStackTypes.html#ga1149474037e6b7b2b302540caf8e9bbc", null ],
    [ "DisabledMonitoredItemCount", "group__UaStackTypes.html#ga4bca56c3e0ccc19a45de4530b16ae18d", null ],
    [ "DiscardedMessageCount", "group__UaStackTypes.html#ga9b839aaceb5ec1d805542733f99bc3db", null ],
    [ "DiscardOldest", "group__UaStackTypes.html#ga0f150bc7069b71cb659847ccddf90ef1", null ],
    [ "DiscoveryProfileUri", "group__UaStackTypes.html#ga8e627f84943d670ea911ee0fddf18456", null ],
    [ "DiscoveryUrls", "group__UaStackTypes.html#gac1e6aa29de609c276aedd1a6e58208bd", null ],
    [ "DiscoveryUrls", "group__UaStackTypes.html#gafbdcf5f21c1ca36cc5f7ca0c4a68aa4b", null ],
    [ "DisplayName", "group__UaStackTypes.html#gac631575adad6fe7b6945435f8a091b73", null ],
    [ "DisplayName", "group__UaStackTypes.html#gac41a85d693339109fbee5373551fa89d", null ],
    [ "DisplayName", "group__UaStackTypes.html#ga5e5757637b0e791ae2bb139377704983", null ],
    [ "ElementDiagnosticInfos", "group__UaStackTypes.html#ga085385fe91588dd2db8841fed6e4c024", null ],
    [ "ElementResults", "group__UaStackTypes.html#ga792532e459ade5ae1662b30a15cee570", null ],
    [ "Elements", "group__UaStackTypes.html#gaa1d32b09499a7397cdcf02f190e6ab53", null ],
    [ "Elements", "group__UaStackTypes.html#gab1af9af5979e82aee4fa4e3796dd8441", null ],
    [ "EnableCount", "group__UaStackTypes.html#gab7de36faac9abbf8f0befa1f1d4609a9", null ],
    [ "Encoding", "group__UaStackTypes.html#gacc281c31f020ec3ae619e25ee1986979", null ],
    [ "Encoding", "group__UaStackTypes.html#gace4b5b48c9be6d7d9365b9db9d195150", null ],
    [ "Endpoints", "group__UaStackTypes.html#gaa9b1fbe5fde0adf411fe355e83435d08", null ],
    [ "EndpointUrl", "group__UaStackTypes.html#ga2e5ef21252a58b177025d0954656f216", null ],
    [ "EndpointUrl", "group__UaStackTypes.html#gaca8f214d05d2ccd8bfed670b5a1da29f", null ],
    [ "EndpointUrl", "group__UaStackTypes.html#ga01a2b332908eccb1b1bfb74b927b097a", null ],
    [ "EndpointUrl", "group__UaStackTypes.html#ga64bc3fbb210aec436346ac0aef707e77", null ],
    [ "EndpointUrlList", "group__UaStackTypes.html#ga03622c06c32062af1e972b3d4317fed4", null ],
    [ "EndTime", "group__UaStackTypes.html#ga02e492a428fa30ba3e53cc4dc28d0ed7", null ],
    [ "EndTime", "group__UaStackTypes.html#gad166672b5dbe0c8b1f83ce68be84cfad", null ],
    [ "EndTime", "group__UaStackTypes.html#ga357b704e0df99d805fcb35c355adb081", null ],
    [ "EndTime", "group__UaStackTypes.html#ga42844f2a3e8270aa7f749e3605740e87", null ],
    [ "EngineeringUnits", "group__UaStackTypes.html#gac59ca15321f3dbb52a1936e40a6a9bc7", null ],
    [ "ErrorCount", "group__UaStackTypes.html#ga19775ecad4d90ef299501085b251909a", null ],
    [ "EURange", "group__UaStackTypes.html#gab78f2da3ee6c460219977fb367a03828", null ],
    [ "EventData", "group__UaStackTypes.html#ga205c235a7e1920e0480691d270cab3d3", null ],
    [ "EventFields", "group__UaStackTypes.html#ga2dd19c22d9cfcf90f405dba2df2c40b9", null ],
    [ "EventIds", "group__UaStackTypes.html#gab70efdfaaab5d144b939961ba43efa9a", null ],
    [ "EventNotificationsCount", "group__UaStackTypes.html#gad0be77c1375cca7df0f0a5494d76d9cd", null ],
    [ "EventQueueOverFlowCount", "group__UaStackTypes.html#gac722e42dea98720e23642c5394f1d4f2", null ],
    [ "Events", "group__UaStackTypes.html#ga7888c944d76f66f166812ea22578832a", null ],
    [ "Filter", "group__UaStackTypes.html#ga293c50d434bdedfcee9d3ce9fabec68c", null ],
    [ "Filter", "group__UaStackTypes.html#gab0e83fe4687f84a1a855d779f9415427", null ],
    [ "Filter", "group__UaStackTypes.html#gab7762de3a143fa3aa779edbab4fd94ce", null ],
    [ "Filter", "group__UaStackTypes.html#ga9a7a17d4ebbb8c0484fc13ed218550d4", null ],
    [ "FilterOperands", "group__UaStackTypes.html#ga378cda096ad047d965be15b0b2b293ac", null ],
    [ "FilterOperator", "group__UaStackTypes.html#ga2da48230f67987848f637f0219fda65d", null ],
    [ "FilterResult", "group__UaStackTypes.html#gac30154e53deeefda7b2bc1a8ea3d1307", null ],
    [ "FilterResult", "group__UaStackTypes.html#ga068bd04e98c885111ad2d7296985a8dc", null ],
    [ "FilterResult", "group__UaStackTypes.html#gab7acff3e0b3a76293430dc52e3c1e1be", null ],
    [ "GatewayServerUri", "group__UaStackTypes.html#ga819373d76779c129a58f2e5120a0b7b7", null ],
    [ "GatewayServerUri", "group__UaStackTypes.html#gab6dbc96e73abcb557a1eb38f6928ea68", null ],
    [ "High", "group__UaStackTypes.html#ga2294051bb62f7695b44300ecc1443c9a", null ],
    [ "HistoryData", "group__UaStackTypes.html#ga0c2b9accd54f041f12f5467c3a5538ac", null ],
    [ "HistoryReadCount", "group__UaStackTypes.html#gafad28149e4dc17409b525343e00c326a", null ],
    [ "HistoryReadDetails", "group__UaStackTypes.html#gaeed21ab060d62ed636264fb1271b6873", null ],
    [ "HistoryUpdateCount", "group__UaStackTypes.html#gaf36f1d76873a744738f3f2dfc6331266", null ],
    [ "HistoryUpdateDetails", "group__UaStackTypes.html#ga57cc89a182e90a6c4dd299b44a24666e", null ],
    [ "IdentifierType", "group__UaStackTypes.html#ga854aaf9e3181dd2657dd00e7c25560ab", null ],
    [ "IncludeSubtypes", "group__UaStackTypes.html#ga8de72b1fd69c0c1719aa99e1dacb9861", null ],
    [ "IncludeSubtypes", "group__UaStackTypes.html#ga13f9eb134196c6b690d82cd3dbb529a7", null ],
    [ "IncludeSubTypes", "group__UaStackTypes.html#ga21fd2161ab7548ee825442949b7eeaec", null ],
    [ "Index", "group__UaStackTypes.html#ga45e11bc4dff634263bdebc32c6cfd7e1", null ],
    [ "IndexRange", "group__UaStackTypes.html#ga23d839ff468906ca16d0ebc2fb00ea05", null ],
    [ "IndexRange", "group__UaStackTypes.html#gad9d2cf1994968cf011221fd12bdc4d07", null ],
    [ "IndexRange", "group__UaStackTypes.html#gadc98621f5383a64cfcc2aef0043bf5e3", null ],
    [ "IndexRange", "group__UaStackTypes.html#ga854cc555e9a690bd81e29307a87c4f6b", null ],
    [ "IndexRange", "group__UaStackTypes.html#gab0706ca2862b4c943781a700fa127ce6", null ],
    [ "IndexRange", "group__UaStackTypes.html#ga4ab5436724cc0803612bccb6ee74c2a3", null ],
    [ "InnerDiagnosticInfo", "group__UaStackTypes.html#gabb0898b6f8ee1a7fa034b2471bb0afbe", null ],
    [ "InnerStatusCode", "group__UaStackTypes.html#ga6c227ee81cb5021424e3ab5dc99edbef", null ],
    [ "InputArgumentDiagnosticInfos", "group__UaStackTypes.html#ga4bf33f93174146993e351aeb0ee7482e", null ],
    [ "InputArgumentResults", "group__UaStackTypes.html#ga3e0f1d2dbc5b89b1048306c89e827665", null ],
    [ "InputArguments", "group__UaStackTypes.html#ga78cbf2b40add095b2370ee4cd3ac4cbd", null ],
    [ "IsDeleteModified", "group__UaStackTypes.html#ga88235d0faa39022b594421d18afda1a8", null ],
    [ "IsForward", "group__UaStackTypes.html#ga64db012e5295407f05ab43e61220f483", null ],
    [ "IsForward", "group__UaStackTypes.html#gac9fad84bd331f88effdb98d67938e242", null ],
    [ "IsForward", "group__UaStackTypes.html#ga199f8c6092049025bff132438f7417a4", null ],
    [ "IsInverse", "group__UaStackTypes.html#ga688aabda7c10c0d05afe68a65a9ae33d", null ],
    [ "IsOnline", "group__UaStackTypes.html#gae5e4cc1b3a56864a33934c0a128f29b8", null ],
    [ "IsReadModified", "group__UaStackTypes.html#ga9a8a9d120f0749607d415cc68b8e2b60", null ],
    [ "IssueDate", "group__UaStackTypes.html#gaa0ba549d95cf17aa87eee9a6270af014", null ],
    [ "IssuedBy", "group__UaStackTypes.html#ga3e07a94d53fcb90031ad610da2ceaf92", null ],
    [ "IssuedTokenType", "group__UaStackTypes.html#gadce2aa31dd85cd3cc5e59e36e70680dc", null ],
    [ "IssuerCertificates", "group__UaStackTypes.html#ga2110d5d4824769172fa36ebd6930b11b", null ],
    [ "IssuerCrls", "group__UaStackTypes.html#ga83f32e23cd1e3447eb640bbb5a1daf0d", null ],
    [ "IssuerEndpointUrl", "group__UaStackTypes.html#ga8994ad28453bca253da78b705e32db58", null ],
    [ "ItemsToCreate", "group__UaStackTypes.html#ga51b9aa61f0ca4a07eb3bf5d49eb83976", null ],
    [ "ItemsToModify", "group__UaStackTypes.html#gafae3a19099fca7edc78828ee53ec242d", null ],
    [ "ItemToMonitor", "group__UaStackTypes.html#gad2167223e6e900bfb8e333ef0d69845b", null ],
    [ "LatePublishRequestCount", "group__UaStackTypes.html#ga8c30221f04315966e7c268de86bc24ab", null ],
    [ "LinksToAdd", "group__UaStackTypes.html#ga7f5d1617d067a7c5ddc0daadf65812cd", null ],
    [ "LinksToRemove", "group__UaStackTypes.html#ga9178ea6c86faab0a06ef438cdd16c8b1", null ],
    [ "Locale", "group__UaStackTypes.html#ga6b894922e46884db6dd4941c1078c768", null ],
    [ "Locale", "group__UaStackTypes.html#ga7013c05ee8e0230dc014ed49d7ce1c6d", null ],
    [ "LocaleIds", "group__UaStackTypes.html#gad40764e0a7b7d5dd8b12651ce9365e6d", null ],
    [ "LocaleIds", "group__UaStackTypes.html#gaf20bce1ec7913bcbe46ea6cc68be15ed", null ],
    [ "LocaleIds", "group__UaStackTypes.html#ga6ec108082bd1f8bdc167a4d37cc8defd", null ],
    [ "LocalizedText", "group__UaStackTypes.html#ga05d2adafce9f931e2ccd04a52130d46a", null ],
    [ "Low", "group__UaStackTypes.html#gaf71851bd746dc48dc85790d5bc0e35c2", null ],
    [ "ManufacturerName", "group__UaStackTypes.html#gaa0d220eaebe290b74dfcd03bbfde74b7", null ],
    [ "MaxAge", "group__UaStackTypes.html#gaa1c94f0992e008f313e564bd47ba7825", null ],
    [ "MaxDataSetsToReturn", "group__UaStackTypes.html#ga3bb2516fab83cab597be7ab2644dbdbf", null ],
    [ "MaxKeepAliveCount", "group__UaStackTypes.html#ga189114423fe447eedcc705b7536bd1bc", null ],
    [ "MaxLifetimeCount", "group__UaStackTypes.html#ga990db0a1dd0fd1df359384d8eff63552", null ],
    [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga5c6a618a41dddce7a362e20280e30889", null ],
    [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#ga2c7578f38198ade678a61345ab6efa0c", null ],
    [ "MaxNotificationsPerPublish", "group__UaStackTypes.html#gae6053c1f6c7f98255944dcce5d08890c", null ],
    [ "MaxReferencesToReturn", "group__UaStackTypes.html#ga62e2db056ffb9b4ae499eae48ffa2803", null ],
    [ "MaxResponseMessageSize", "group__UaStackTypes.html#ga15d6fd598539929d4688189d6787e88e", null ],
    [ "Message", "group__UaStackTypes.html#ga7812025d6af498a733df8f0184f38f23", null ],
    [ "MethodId", "group__UaStackTypes.html#ga83028a07c36aff34e902e4097cc0b99f", null ],
    [ "MethodsToCall", "group__UaStackTypes.html#gaf3553086265beb320fc574d831cbbe94", null ],
    [ "ModifyCount", "group__UaStackTypes.html#gae06a22cd8ca744fe864ddefb2b36b75f", null ],
    [ "ModifyMonitoredItemsCount", "group__UaStackTypes.html#ga23d59b5dcba2b640a5dff0f4cb0b71e6", null ],
    [ "ModifySubscriptionCount", "group__UaStackTypes.html#ga621a94a114d9188c94c0ecb73a54da08", null ],
    [ "MonitoredItemCount", "group__UaStackTypes.html#gaaa76468cc3892ff9677906bc06432da7", null ],
    [ "MonitoredItemId", "group__UaStackTypes.html#ga3189263bb49e2ab983578fb9f3293d35", null ],
    [ "MonitoredItemId", "group__UaStackTypes.html#ga41ae19789706f14131ddf88980637454", null ],
    [ "MonitoredItemIds", "group__UaStackTypes.html#gacdc2a5fec6bce2190cd2f00cf1870107", null ],
    [ "MonitoredItemIds", "group__UaStackTypes.html#gaaa13077ed21f6b179863da88b3c52987", null ],
    [ "MonitoringMode", "group__UaStackTypes.html#ga8784168fb2600e99bfc5808894d90677", null ],
    [ "MonitoringMode", "group__UaStackTypes.html#ga7b81e872d1361fc349d44888d0697d93", null ],
    [ "MonitoringQueueOverflowCount", "group__UaStackTypes.html#gadb446ee5abb6a5d8e27a83a5927061a9", null ],
    [ "MoreNotifications", "group__UaStackTypes.html#ga8ed654675e29c17b67c7358fffe9279a", null ],
    [ "Name", "group__UaStackTypes.html#gae98ac296bc390aa833885061dafb404d", null ],
    [ "Name", "group__UaStackTypes.html#ga0f2505639e6714511dbe0ef23676591b", null ],
    [ "NamespaceIndex", "group__UaStackTypes.html#ga2fc647f84259b24bf3c98b7164c768eb", null ],
    [ "NamespaceIndex", "group__UaStackTypes.html#ga659c3f296949ddb1d186e291c2a57728", null ],
    [ "NamespaceUri", "group__UaStackTypes.html#gafecbd7216e9c0f8d9d339bcc6b427a7d", null ],
    [ "NamespaceUri", "group__UaStackTypes.html#gaf8248cd635a1a6c00ebe907d16f6909a", null ],
    [ "NamespaceUri", "group__UaStackTypes.html#ga8b10b5dc3a955300517451fdc125fb1d", null ],
    [ "NetworkPaths", "group__UaStackTypes.html#gafb8deec9c3c2c79a7647e0c23ff99616", null ],
    [ "NextSequenceNumber", "group__UaStackTypes.html#ga7afb35250ece78b4c96aefdb75f8e427", null ],
    [ "NodeAttributes", "group__UaStackTypes.html#gab907bd71e73b553e278fcc2d37c4c625", null ],
    [ "NodeClass", "group__UaStackTypes.html#ga45b1f9ccc67aa6c8412ace444e7e4420", null ],
    [ "NodeClass", "group__UaStackTypes.html#ga2982ff6bbca7ff9a84cb3c9fb90d7169", null ],
    [ "NodeClassMask", "group__UaStackTypes.html#gab8d99da19b4604028580c73abb8e30f0", null ],
    [ "NodeId", "group__UaStackTypes.html#ga2e5db77bb59f2e7ddd618e389d28231c", null ],
    [ "NodeId", "group__UaStackTypes.html#ga4869a654b6cf4355a3d8a25a41d9e30f", null ],
    [ "NodeId", "group__UaStackTypes.html#gac0dc15df7d7e6ef45c4ee0c8956897ab", null ],
    [ "NodeId", "group__UaStackTypes.html#gab4633e15c852ce3059e2a28fd6ffcf00", null ],
    [ "NodeId", "group__UaStackTypes.html#ga948e5020d7706a8880d6aeb02d66a136", null ],
    [ "NodeId", "group__UaStackTypes.html#ga724ad682827ba7f53b99d50e7afc8ceb", null ],
    [ "NodeId", "group__UaStackTypes.html#ga776613d6065cf93d1300fe5e2e8f136d", null ],
    [ "NodeId", "group__UaStackTypes.html#ga603d1ac5232ad4caf9c48fa8f72b92b9", null ],
    [ "NodeId", "group__UaStackTypes.html#gace205830a7e69ac2e7260d9cc37ecae8", null ],
    [ "NodeId", "group__UaStackTypes.html#ga810a514d3263329dfacf362e7a25c8b3", null ],
    [ "NodeId", "group__UaStackTypes.html#gaaccbcc6d2f9d9be4738b175c94db4121", null ],
    [ "NodeId", "group__UaStackTypes.html#ga17f121751946ad1a58b787fcb7747be5", null ],
    [ "NodeId", "group__UaStackTypes.html#ga8e4a91f3bcb93a6e6f423ad32c179436", null ],
    [ "NodeId", "group__UaStackTypes.html#ga8aaf650a031d96b69307e7a42fa9fd66", null ],
    [ "NodesToAdd", "group__UaStackTypes.html#gac109a5d62d73b14f60b228bddffe242d", null ],
    [ "NodesToBrowse", "group__UaStackTypes.html#ga516c52ede838d3fe30c127a0ccd60daf", null ],
    [ "NodesToDelete", "group__UaStackTypes.html#gaddb3c9998ac6848373a6d4c7fff43987", null ],
    [ "NodesToRead", "group__UaStackTypes.html#ga134b8963a7fe765316e34a84690a5067", null ],
    [ "NodesToRead", "group__UaStackTypes.html#gada119cbe9ed3b439e7ff0f1956c893f4", null ],
    [ "NodesToRegister", "group__UaStackTypes.html#gaae3934d0c242f230f7ce70dc7d313523", null ],
    [ "NodesToUnregister", "group__UaStackTypes.html#ga8e982a0700e171d917585457bfdb9d30", null ],
    [ "NodesToWrite", "group__UaStackTypes.html#gaa8c300b512f56e1794e10e5a46c29354", null ],
    [ "NodeTypes", "group__UaStackTypes.html#ga56204e0a36365890c8eab2d52700dbda", null ],
    [ "NoOfDiagnosticInfos", "group__UaStackTypes.html#ga33954fe253b98c43433822fcfaeece04", null ],
    [ "NoOfDiagnosticInfos", "group__UaStackTypes.html#ga69df614fa94021b1980803094e8f9c39", null ],
    [ "NoOfNotificationData", "group__UaStackTypes.html#gac12f1a5aa054bccc4992e554156f2ee8", null ],
    [ "NotificationData", "group__UaStackTypes.html#gabe0adf74ed012a791bdb44f39e54e2cf", null ],
    [ "NotificationMessage", "group__UaStackTypes.html#ga1f58a55a2977f5863f9d606bcd6aa0f6", null ],
    [ "NotificationMessage", "group__UaStackTypes.html#ga7d2d1faf0dce8f8e5b41966898641355", null ],
    [ "NotificationsCount", "group__UaStackTypes.html#gabc3ed012d0f39a537518129517614125", null ],
    [ "NumValuesPerNode", "group__UaStackTypes.html#ga3968b94aedc3033d44daabe682f28f3b", null ],
    [ "NumValuesPerNode", "group__UaStackTypes.html#ga37cd99c2d78f73d4d4331452708fe262", null ],
    [ "ObjectId", "group__UaStackTypes.html#ga46fa6a08cd65f2a44c37594c4ef660af", null ],
    [ "Offset", "group__UaStackTypes.html#ga16a5a8dd6e0c1113b8816e62464af4e8", null ],
    [ "OperandDiagnosticInfos", "group__UaStackTypes.html#gaa34c095406f8c79b8c616440c3d6fb2d", null ],
    [ "OperandStatusCodes", "group__UaStackTypes.html#ga1814d3e405de70f3029b722f5dc7b5ff", null ],
    [ "OperationResults", "group__UaStackTypes.html#gaff6d4a4ac7aaee3cc67427d39948fc04", null ],
    [ "OrganizationUri", "group__UaStackTypes.html#ga32ea3656667f67fc30ef8ef8236b964e", null ],
    [ "OutputArguments", "group__UaStackTypes.html#ga744a3f7302f37de1ebb0441c64c6c3ba", null ],
    [ "ParentNodeId", "group__UaStackTypes.html#gaf8819c3157ec69fbaba3e3afd58f0456", null ],
    [ "ParsingResults", "group__UaStackTypes.html#ga1b00803c29e242db5abaebd311eadaa2", null ],
    [ "PercentDataBad", "group__UaStackTypes.html#ga0a926196cc135225c34511d0af8510c7", null ],
    [ "PercentDataGood", "group__UaStackTypes.html#gaace11fc8ab21f4b62d3eba9d50df0b38", null ],
    [ "PerformInsertReplace", "group__UaStackTypes.html#ga675f178843e8e86988da82945ceb326d", null ],
    [ "PerformInsertReplace", "group__UaStackTypes.html#ga03ae66304f443674522ee8e654cd22e9", null ],
    [ "PerformInsertReplace", "group__UaStackTypes.html#ga46cf77cf9807b530ef026ddd2f174775", null ],
    [ "PolicyId", "group__UaStackTypes.html#gaf026985260a40538a8cf9a2cda0ea3e9", null ],
    [ "Priority", "group__UaStackTypes.html#ga78766f534e548b0de146ce4b8c0cd9f0", null ],
    [ "Priority", "group__UaStackTypes.html#ga7dafc433b65d20ed608cfe42f4e58cde", null ],
    [ "Priority", "group__UaStackTypes.html#ga06b04f9671879642b6319183e59dbd4c", null ],
    [ "ProcessingInterval", "group__UaStackTypes.html#gac4032d5b58553bc1d8b0368187d7058a", null ],
    [ "ProductName", "group__UaStackTypes.html#ga62f0c365a7e6d9f5fd5a0e1b415c89c5", null ],
    [ "ProductName", "group__UaStackTypes.html#gad87a5892dfd2eb1279bc9eb0b8827018", null ],
    [ "ProductUri", "group__UaStackTypes.html#gac4949aea150e19dc0077a4cd37cd0cf1", null ],
    [ "ProductUri", "group__UaStackTypes.html#ga9a5dba95cd9fc34f84c2d34373c7e03d", null ],
    [ "ProductUri", "group__UaStackTypes.html#gaee7dd8db947b25503b6aa4e2899e87f4", null ],
    [ "ProductUri", "group__UaStackTypes.html#ga4fce8aa0aea3c06d7aaf317793ffd2cc", null ],
    [ "ProfileId", "group__UaStackTypes.html#ga4f1d43ba03ec9bd22e2c0b49d8e383e3", null ],
    [ "ProfileUris", "group__UaStackTypes.html#ga1638ca5557ca0d856698babba04db0cd", null ],
    [ "PublishCount", "group__UaStackTypes.html#gab15bca82e718927223b55a20c9cbbdb4", null ],
    [ "PublishingEnabled", "group__UaStackTypes.html#ga6b64a2bbe7b7e55ccc5fb5840f1b2fdd", null ],
    [ "PublishingEnabled", "group__UaStackTypes.html#ga5c079471bf166a04d5d0ff6dd582d447", null ],
    [ "PublishingEnabled", "group__UaStackTypes.html#ga53f70956bc2923325a5854877ef5a1ee", null ],
    [ "PublishingInterval", "group__UaStackTypes.html#ga34f17229e0cac8bf5e86d024cc0a451c", null ],
    [ "PublishingIntervalCount", "group__UaStackTypes.html#gaa3fb89f1f20d223a05e79a697424d22f", null ],
    [ "PublishRequestCount", "group__UaStackTypes.html#ga72a51b978c19bbf59d75b5da1ce0ec9a", null ],
    [ "PublishTime", "group__UaStackTypes.html#ga14b1eef2f8f624acd733333bfd66e1b6", null ],
    [ "QueryDataSets", "group__UaStackTypes.html#gac0580420bd29a3a3edd9aa963607b478", null ],
    [ "QueryDataSets", "group__UaStackTypes.html#ga60b477d012a6488405fa2be59743c402", null ],
    [ "QueryFirstCount", "group__UaStackTypes.html#gaf47417da33504419b49f27a3e2064fbc", null ],
    [ "QueryNextCount", "group__UaStackTypes.html#gac40bbc9f0f5429b19e44470255f8eb1f", null ],
    [ "QueueSize", "group__UaStackTypes.html#ga686fbf5f5c05abe85759d18fab0c3575", null ],
    [ "ReadCount", "group__UaStackTypes.html#gaf475d21b8750eefa232756889a9a8775", null ],
    [ "References", "group__UaStackTypes.html#gaa71a9fc191f9eaef9b9a4fc25587a490", null ],
    [ "ReferencesToAdd", "group__UaStackTypes.html#ga819e994457f02ee751f0d1c0e30b5c51", null ],
    [ "ReferencesToDelete", "group__UaStackTypes.html#ga0a9d1ea0b3b7de5bf607e7321ce9b388", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga8b7c85cf95052f02c763f83df63b79e6", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gad1952100df202e62664187354fc94406", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gaf636f42e53a49deffc2469efc982c247", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga1290ef0c7015cfafcfb22893680a0c54", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#gad17758f38c3913dde4847006f498a341", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga80f1a06cd24df19a8f2b1db54581293d", null ],
    [ "RegisteredNodeIds", "group__UaStackTypes.html#ga5fd3e9ac1dc6ba19555c7af56ba475ef", null ],
    [ "RegisterNodesCount", "group__UaStackTypes.html#ga3e18de92bf884bf2b2e7b7d7bb67e9f3", null ],
    [ "RejectedRequestsCount", "group__UaStackTypes.html#ga7301bbd87a0fc6b3976dea93c00a4430", null ],
    [ "RejectedSessionCount", "group__UaStackTypes.html#ga5363d232d0fd3e8c61e85d1f884a087b", null ],
    [ "RelativePath", "group__UaStackTypes.html#gaf2fd9d47c8fe40e18cfea10c15b20aef", null ],
    [ "RelativePath", "group__UaStackTypes.html#gab2f1c995b31082641f511891d2a93cbd", null ],
    [ "ReleaseContinuationPoint", "group__UaStackTypes.html#gaddb62fef0ae98a5504521275cee9d689", null ],
    [ "ReleaseContinuationPoints", "group__UaStackTypes.html#ga393ed0340c915232db6302c13f49bc66", null ],
    [ "ReleaseContinuationPoints", "group__UaStackTypes.html#ga7f2373abbacb531e7af1dd9852c74863", null ],
    [ "RemainingPathIndex", "group__UaStackTypes.html#ga94a73aa29f0523f5a4b6680397986e27", null ],
    [ "RemoveDiagnosticInfos", "group__UaStackTypes.html#ga588fb6a6bbcf3c57de61a95317939e24", null ],
    [ "RemoveResults", "group__UaStackTypes.html#gac6242fdbd1f2e74b6e95757cf918f476", null ],
    [ "RepublishCount", "group__UaStackTypes.html#ga02778218f6886c99afa91d0b97aecd3a", null ],
    [ "RepublishMessageCount", "group__UaStackTypes.html#ga090675fc24ba51923cdb0db78ea0398e", null ],
    [ "RepublishMessageRequestCount", "group__UaStackTypes.html#gafed323255c53a251b7151975171d1ee1", null ],
    [ "RepublishRequestCount", "group__UaStackTypes.html#ga07fa62571befd8722defffd237d0fa48", null ],
    [ "ReqTimes", "group__UaStackTypes.html#ga60db6a827ef7445055cabf78889e7fab", null ],
    [ "ReqTimes", "group__UaStackTypes.html#gafc024ff104f129d0871d6a443bd95b64", null ],
    [ "RequestedLifetimeCount", "group__UaStackTypes.html#gad987badc435c4edd8383c4da75acb222", null ],
    [ "RequestedLifetimeCount", "group__UaStackTypes.html#gaceac41f2689b98cb2b9619542112cdd9", null ],
    [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#gaba5ea661e7b9e955f7f6d7039f498aba", null ],
    [ "RequestedMaxKeepAliveCount", "group__UaStackTypes.html#ga4aec23ac2c0ee309c107d4ca34a5c190", null ],
    [ "RequestedMaxReferencesPerNode", "group__UaStackTypes.html#gadc8bb0155cb18db5792211263c0b2db4", null ],
    [ "RequestedNewNodeId", "group__UaStackTypes.html#gae4cc4bf1878dcbe12cd5d94a73bfcdab", null ],
    [ "RequestedParameters", "group__UaStackTypes.html#gab8d65b99b64c5e4120b057eb3f55df82", null ],
    [ "RequestedParameters", "group__UaStackTypes.html#ga24796b363f2098d2ccececcec5a18172", null ],
    [ "RequestedPublishingInterval", "group__UaStackTypes.html#gae0125058057f4090d46ed614dbbb89b1", null ],
    [ "RequestedPublishingInterval", "group__UaStackTypes.html#gaff0c84c498961f401c111522f6eaea5c", null ],
    [ "RequestHandle", "group__UaStackTypes.html#ga336632912cc9aaee6e45cdc60ca23684", null ],
    [ "RequestHandle", "group__UaStackTypes.html#ga829cb1c02924069c3672b7d2247b99fd", null ],
    [ "RequestHandle", "group__UaStackTypes.html#ga5b0613afb536a93f4927b14e1589374b", null ],
    [ "ResultMask", "group__UaStackTypes.html#gaa050664eacde119489639ff618806b2c", null ],
    [ "Results", "group__UaStackTypes.html#ga2a73ca594d787af997814f0645411c55", null ],
    [ "Results", "group__UaStackTypes.html#ga7958d49d91e1ac64e4efe1ea047c8961", null ],
    [ "Results", "group__UaStackTypes.html#ga30af67ebce87813df405f2c68d20ae1f", null ],
    [ "Results", "group__UaStackTypes.html#ga235c4212ff8b999075d4db0e06e3d91a", null ],
    [ "Results", "group__UaStackTypes.html#gac0e5a8d288429f1984da2e3c2cc13937", null ],
    [ "Results", "group__UaStackTypes.html#ga57604e13ced84bcf3a04e8a9faa7810c", null ],
    [ "Results", "group__UaStackTypes.html#ga09661ac45f3e1c1c306bfa6d5a470584", null ],
    [ "Results", "group__UaStackTypes.html#gab6ad65fb0c58f2a941222aea7c63d597", null ],
    [ "Results", "group__UaStackTypes.html#ga93331b06a72773f965d9c6e20169a57b", null ],
    [ "Results", "group__UaStackTypes.html#gac5ceeba95e5e75ad0e6fb83f046c4bb5", null ],
    [ "Results", "group__UaStackTypes.html#ga4bd06e2f248f256736e5102e8d1eca0b", null ],
    [ "Results", "group__UaStackTypes.html#ga3a341a484a4360a5c757c0cdaa89d892", null ],
    [ "Results", "group__UaStackTypes.html#ga48983b3ec295ebb318d49ee83bfee6ad", null ],
    [ "Results", "group__UaStackTypes.html#gabebbfd9359370d27676f4a1ffc76e58c", null ],
    [ "Results", "group__UaStackTypes.html#ga1903b2d52a4be15fb337c7ac7989cd85", null ],
    [ "Results", "group__UaStackTypes.html#ga4f4ab57d678be2180bfa5c2870327d01", null ],
    [ "Results", "group__UaStackTypes.html#ga2a66e318305bd767f798380292f2cdc6", null ],
    [ "Results", "group__UaStackTypes.html#gad1c17287d0f834149fff4197d704d4cc", null ],
    [ "Results", "group__UaStackTypes.html#ga36566d0feecf87771e405afe4771a0f4", null ],
    [ "RetransmitSequenceNumber", "group__UaStackTypes.html#ga8ff9484ceb55948643d8eea892addad4", null ],
    [ "ReturnBounds", "group__UaStackTypes.html#ga0683b4890d93df30bd2bbefc4efac07e", null ],
    [ "ReturnDiagnostics", "group__UaStackTypes.html#gab8d43617b01dd745884b539b9ae2b4d9", null ],
    [ "RevisedContinuationPoint", "group__UaStackTypes.html#ga1f38b89c5c7782aa0cd4cb8c0986f7b2", null ],
    [ "RevisedLifetimeCount", "group__UaStackTypes.html#gad362baad3fd21d6c1989bd31bf871ef8", null ],
    [ "RevisedLifetimeCount", "group__UaStackTypes.html#gaec034a74423e95763f5404f72ae864bb", null ],
    [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#ga7f231a6c60c92339568f8f68a3a80ea5", null ],
    [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#gab2ab2749c088329be611422c57448d64", null ],
    [ "RevisedPublishingInterval", "group__UaStackTypes.html#gad46b9515b50081cf1afdaf4d94343770", null ],
    [ "RevisedPublishingInterval", "group__UaStackTypes.html#ga7e3842bd86ee242c1bae223249a6bd28", null ],
    [ "RevisedQueueSize", "group__UaStackTypes.html#gab45581fcab8c5a2c6708b65d3b0624ba", null ],
    [ "RevisedQueueSize", "group__UaStackTypes.html#ga8164f775764c31ac25f49a84a638df28", null ],
    [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga5662c6ccf70fdb6c2777bc54edda8573", null ],
    [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga53c6434a5bf94744323bc5167854e67e", null ],
    [ "SamplingInterval", "group__UaStackTypes.html#ga6c5850dbff0e4b6587d77200db1b3aa8", null ],
    [ "SecondsTillShutdown", "group__UaStackTypes.html#ga9364ff68384ae5708f8a29398918ca46", null ],
    [ "SecurityLevel", "group__UaStackTypes.html#ga9879b3b610b967045cb55b03a2f64653", null ],
    [ "SecurityMode", "group__UaStackTypes.html#ga37166b39f939936490c9fa2c194a719e", null ],
    [ "SecurityMode", "group__UaStackTypes.html#ga6627615dcc660f8019130f85ed779a44", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga6c17c4640d8c8c08c167a872773ffa29", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga55a8f937a8b25fd7ead84ac9a794a138", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga5384385752c609824fe2a27f6fc47f1d", null ],
    [ "SecurityRejectedRequestsCount", "group__UaStackTypes.html#ga3c3091c00323fddc899bcbe3a35fd711", null ],
    [ "SecurityRejectedSessionCount", "group__UaStackTypes.html#gacf59aa66f039c860f36ab77de055b59a", null ],
    [ "SelectClauseDiagnosticInfos", "group__UaStackTypes.html#gadd36bcd423510f65020c9d4e9b6b74c6", null ],
    [ "SelectClauseResults", "group__UaStackTypes.html#gadf49b573383c1863a7bdae3b080518b7", null ],
    [ "SelectClauses", "group__UaStackTypes.html#gafdc4a2055ca406e0a42933b853c857b5", null ],
    [ "SemaphoreFilePath", "group__UaStackTypes.html#ga42f1fae39569083bb5bff78a0de2277b", null ],
    [ "SendInitialValues", "group__UaStackTypes.html#ga14cd492a4a534e8642f66af225500a31", null ],
    [ "SequenceNumber", "group__UaStackTypes.html#ga3b2d1584113dc9a123bf0157cf6abee8", null ],
    [ "SequenceNumber", "group__UaStackTypes.html#gac53195c552fde1bf83358f6a557b9bc3", null ],
    [ "Server", "group__UaStackTypes.html#ga373512fcac41cf3db37dea0de4e3e2d4", null ],
    [ "ServerCertificate", "group__UaStackTypes.html#ga1eaf2b0426eb3e86539f66864a0f298a", null ],
    [ "ServerId", "group__UaStackTypes.html#ga154bb3a78a96a6f888a946e93b460d0a", null ],
    [ "ServerIndex", "group__UaStackTypes.html#ga2d4c93762dcf8b381a9a20e112beca0f", null ],
    [ "ServerNames", "group__UaStackTypes.html#ga90cf983d20c77ef9582d58a39852fd39", null ],
    [ "ServerPicoseconds", "group__UaStackTypes.html#ga12ea0497b00e3636710c88bbccbbb725", null ],
    [ "Servers", "group__UaStackTypes.html#gab2188bb8d2650fa1e1bddcaca22c7a31", null ],
    [ "ServerState", "group__UaStackTypes.html#ga682107b73a1428ca43259d2a321f1ccb", null ],
    [ "ServerTimestamp", "group__UaStackTypes.html#gace2e324ee3509f81a6f8b02dfbf67ca7", null ],
    [ "ServerType", "group__UaStackTypes.html#ga1efa0a96a110877b696ce483de3c1546", null ],
    [ "ServerUri", "group__UaStackTypes.html#gaaa25ddf5445c621a3c961a8138d70d9c", null ],
    [ "ServerUri", "group__UaStackTypes.html#gad75db2d7071010f9cc0e00227a34053c", null ],
    [ "ServerUri", "group__UaStackTypes.html#gac1449dce69c7ac30f225f8e551655455", null ],
    [ "ServerUris", "group__UaStackTypes.html#ga92ba58bfe7e16a7924e392309fc46c82", null ],
    [ "ServerViewCount", "group__UaStackTypes.html#gabdbc069a0682a037a890f525f81d15eb", null ],
    [ "ServiceDiagnostics", "group__UaStackTypes.html#ga7e47989dc5af605e03cc5f846a0355f5", null ],
    [ "ServiceLevel", "group__UaStackTypes.html#gaaf26848c7a63971722743cf4d2690fb2", null ],
    [ "ServiceResult", "group__UaStackTypes.html#ga636c7bb1f22292d9bc5704b83ca55d83", null ],
    [ "SessionAbortCount", "group__UaStackTypes.html#gacea3e5a1e21026a29b8bb6c9f8c41229", null ],
    [ "SessionId", "group__UaStackTypes.html#gad7abc52a3d50e9e626f910dc59c76e8d", null ],
    [ "SessionId", "group__UaStackTypes.html#ga93dfff535efbd225c1a1412c36536fa3", null ],
    [ "SessionId", "group__UaStackTypes.html#ga28d6a7cd3d05d99dd29785032a3ed791", null ],
    [ "SessionName", "group__UaStackTypes.html#gabf4470b79dad905d2c3a9d75836ca0d2", null ],
    [ "SessionTimeoutCount", "group__UaStackTypes.html#ga6e66b31ce0bc6b75c69cc6ff5393193d", null ],
    [ "SetMonitoringModeCount", "group__UaStackTypes.html#ga41dd022288ed8a3b865f3fe6a0fe74a9", null ],
    [ "SetPublishingModeCount", "group__UaStackTypes.html#ga88afcc3ba86319807f54e67fc3bdc2be", null ],
    [ "SetTriggeringCount", "group__UaStackTypes.html#gad39ffd13fcc307422bd2165e8a62ab70", null ],
    [ "ShutdownReason", "group__UaStackTypes.html#ga932a69a816162138490390d08a6b9f8f", null ],
    [ "Signature", "group__UaStackTypes.html#ga5da93e4f70a02973c38065c4551002a6", null ],
    [ "SoftwareVersion", "group__UaStackTypes.html#gaea4befd3d1a03417d6b4ef5f71e72ecf", null ],
    [ "SoftwareVersion", "group__UaStackTypes.html#ga4d189e96984facd6665f7ed91d65be0d", null ],
    [ "SourceNodeId", "group__UaStackTypes.html#ga3fec4c07f6b09fa730e687c864d624a5", null ],
    [ "SourceNodeId", "group__UaStackTypes.html#ga1fa1ba7ed6f13e0091bca59ba99f4493", null ],
    [ "SourcePicoseconds", "group__UaStackTypes.html#gaddf019c1a49eda91b2a62a120c7f6b3d", null ],
    [ "SourceTimestamp", "group__UaStackTypes.html#gaade831a742f091f31a90ebe84086ec53", null ],
    [ "SpecifiedLists", "group__UaStackTypes.html#ga07e83eefa56eb59c244a0da04acf223b", null ],
    [ "StartingNode", "group__UaStackTypes.html#ga4be3098f7f910c383914cf1b29168d71", null ],
    [ "StartTime", "group__UaStackTypes.html#ga63008bb07443bc2b716a0eb2d1acc9d7", null ],
    [ "StartTime", "group__UaStackTypes.html#gaf5177406eb275d39bcaea86a5e49d9ba", null ],
    [ "StartTime", "group__UaStackTypes.html#ga09dd9b5460589b0b8a2410bc99a77532", null ],
    [ "StartTime", "group__UaStackTypes.html#gacdf57bd222360d2c59a35de7b271d1f3", null ],
    [ "StartTime", "group__UaStackTypes.html#gadff860fc2204e0b9db4dabb0e4e07a46", null ],
    [ "State", "group__UaStackTypes.html#ga734414aea5f9043411bab1c14cb16726", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga01a8fa2beacee3ea16be457be2d68c3b", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga58714f09e4930949a52160d79ad16719", null ],
    [ "StatusCode", "group__UaStackTypes.html#gabc48f6536750838eab1ec585a13b097e", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga970e2a9a10bb0c25cf81ca893f952078", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga7b91196cf8806e0f789ac6df5547a7ae", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga85da4e53db55ecf275962b9b876bde8e", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga1cdb14fb8520f28e597fb4fb53bcd978", null ],
    [ "StatusCode", "group__UaStackTypes.html#gaeb1d00907ddabfc3f09205c4f6e3217b", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga3c9c90e0070e660e1715abeccf9b6bf9", null ],
    [ "StatusCode", "group__UaStackTypes.html#gab3175611ec1758b3f19d79a146ff816d", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga375b01f9765aed8a49875f2c4c0aa4fe", null ],
    [ "StatusCode", "group__UaStackTypes.html#gad24627c532c9aef3adfe76dac7643593", null ],
    [ "StringTable", "group__UaStackTypes.html#ga7d39e71aa3cb0a95b599020c62a69d46", null ],
    [ "SubscriptionAcknowledgements", "group__UaStackTypes.html#ga1d83748ffd485d2ee9ca61b3c3141442", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga45265c69449d58b0f2977468bca2adb0", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga88799f8ec33c70d8d3d1378aec677492", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga759ca7d85c383a98236f13419c569b8f", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#gac3feef9a809c060ba70e6c8d1ccd3d4f", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga00c76ec59b3d3709fb89bfd0fb38f244", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga279174ece7725e168e192675a1abe794", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga647508fdabfaec213392dfa83579717b", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga122ea5f536dc5382a99cc5c0f87b309e", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga18e96eace034affd9242f8747be56e3d", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga4f70582af61ba157e97dd05463688a25", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#gaa8a63cf7e4de1fabf36031d8730ce5b4", null ],
    [ "SubscriptionIds", "group__UaStackTypes.html#ga3f2f1de7dbaa36acc7e87431a158548e", null ],
    [ "SubscriptionIds", "group__UaStackTypes.html#gaec373b5a7cd0ec8d8426453c60765a4f", null ],
    [ "SubscriptionIds", "group__UaStackTypes.html#gabec9a74c9d8ba4a1df31f076053a36fc", null ],
    [ "SupportedProfiles", "group__UaStackTypes.html#ga49fed9cddc887575cf65a87b42f61ffe", null ],
    [ "SymbolicId", "group__UaStackTypes.html#ga2b8a8b3fd6f9ee6d416fe90f71354bca", null ],
    [ "TargetId", "group__UaStackTypes.html#ga4de3ce1d3205970a27f4f9d477669e29", null ],
    [ "TargetName", "group__UaStackTypes.html#ga71b0593e37266683e65600f26b5d5718", null ],
    [ "TargetNodeClass", "group__UaStackTypes.html#ga77bcf036a6e7cbc13869a7a706c5b6cb", null ],
    [ "TargetNodeId", "group__UaStackTypes.html#ga1fc7a78811ae1288e2e4e5fb4487e568", null ],
    [ "TargetNodeId", "group__UaStackTypes.html#gafeb95c9ef97fe96ff2fdadce1aad6989", null ],
    [ "Targets", "group__UaStackTypes.html#gaacafaf9831605c30e7e4c4a190ce6b98", null ],
    [ "TargetServerUri", "group__UaStackTypes.html#gaa250e533a12ffcf5b654f2ae7f793b59", null ],
    [ "Text", "group__UaStackTypes.html#ga777fb051240e48704c77a5533f63aa98", null ],
    [ "TimeoutHint", "group__UaStackTypes.html#gacc2c479aa29e1e6cdb19deff583c5830", null ],
    [ "Timestamp", "group__UaStackTypes.html#ga04dac22e2b39c5bdf76790bfd2af4d5e", null ],
    [ "Timestamp", "group__UaStackTypes.html#ga5325f74d60c89cb66ff1d2ed0e3584bc", null ],
    [ "Timestamp", "group__UaStackTypes.html#gaa9c45f2e632c140f1fc28ca540a09ff3", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga77f06c6b689c0346381f74bb649c4c23", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga22138c8b6c2fde067a7c3c8193c3776b", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga551c5c18d267457342353e2398d347e5", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga3bed32d7ed039cf1071348a9731d7276", null ],
    [ "Title", "group__UaStackTypes.html#gabdafc389b7120064dccc352c132eb696", null ],
    [ "TokenType", "group__UaStackTypes.html#ga7c06846267efe3d7706655b74afd206c", null ],
    [ "TotalCount", "group__UaStackTypes.html#ga2e76fd3be9c2a38b4b43a565cf1edcb8", null ],
    [ "TotalRequestCount", "group__UaStackTypes.html#gaa8325f78caea8b411182e02cdc7532b6", null ],
    [ "TransferredToAltClientCount", "group__UaStackTypes.html#gaecc333cd045f51be9d690173c86b5dee", null ],
    [ "TransferredToSameClientCount", "group__UaStackTypes.html#ga29fd636a00021a2feb2385a8088660b6", null ],
    [ "TransferRequestCount", "group__UaStackTypes.html#gad3ea8d057b5e1b052d2dd661d044ba42", null ],
    [ "TransferSubscriptionsCount", "group__UaStackTypes.html#ga0f7d925bd0e2b571a7e9cb89eb74704d", null ],
    [ "TranslateBrowsePathsToNodeIdsCount", "group__UaStackTypes.html#ga6dd75a59de35ec66db1b0063dddfbbb2", null ],
    [ "TransportProfileUri", "group__UaStackTypes.html#ga0f43503dfdbcc16286f64c52e98b2014", null ],
    [ "TransportProtocol", "group__UaStackTypes.html#gae13835806aea8841ece342b4bea5c197", null ],
    [ "TreatUncertainAsBad", "group__UaStackTypes.html#ga07a7cf674b279849b828741d7b241b60", null ],
    [ "TriggeringItemId", "group__UaStackTypes.html#ga542374801c73f1c7e32925a289fe2191", null ],
    [ "TrustedCertificates", "group__UaStackTypes.html#gaaedcd431a46299fd48c21996c932465e", null ],
    [ "TrustedCrls", "group__UaStackTypes.html#ga607826bf16d80271dda67b56ae374a46", null ],
    [ "TypeDefinition", "group__UaStackTypes.html#ga2216807f2944fb5c05d120b63de81ec0", null ],
    [ "TypeDefinition", "group__UaStackTypes.html#gacfc9614b093ae2cd1fa085630626f7fd", null ],
    [ "TypeDefinitionId", "group__UaStackTypes.html#ga8f3938a97ab3974f3da6cae1408daaa8", null ],
    [ "TypeDefinitionNode", "group__UaStackTypes.html#ga50f53ee967712d956fb42032e589ec77", null ],
    [ "TypeId", "group__UaStackTypes.html#ga47e1863bd35cf7bc2299e3221f69200d", null ],
    [ "UnacknowledgedMessageCount", "group__UaStackTypes.html#gab71c4629ac13da2d37298905fc8f1cf0", null ],
    [ "UnauthorizedRequestCount", "group__UaStackTypes.html#ga6fb83e8e3550caab9d0c17b07a4494ab", null ],
    [ "UnitId", "group__UaStackTypes.html#gab220d76cc348759759dd7c7fe359d700", null ],
    [ "UnregisterNodesCount", "group__UaStackTypes.html#ga124c5e21ff11916a85c1c32fbd8e71e6", null ],
    [ "UnsupportedUnitIds", "group__UaStackTypes.html#ga3f3f573ea11ae4784f535d73fd71c9ac", null ],
    [ "UpdateValues", "group__UaStackTypes.html#ga87cf4ecd41f115a1f6fd3c2062e07f62", null ],
    [ "UpdateValues", "group__UaStackTypes.html#ga1d07841986dc0a00919b326ec2f62c69", null ],
    [ "UserIdentityTokens", "group__UaStackTypes.html#gab8b6aa89de527525b15bbae1aeaf3dcc", null ],
    [ "UserName", "group__UaStackTypes.html#ga1580af9f697f975f1c65c9cfd00932c2", null ],
    [ "UseServerCapabilitiesDefaults", "group__UaStackTypes.html#ga0fa5356f7bbce5945c8cca338a203727", null ],
    [ "UseSimpleBounds", "group__UaStackTypes.html#ga8492bebdd5fb88f96d7a5ecd1bae8845", null ],
    [ "UseSlopedExtrapolation", "group__UaStackTypes.html#ga335cf19cb04c84f22944bc120be57be1", null ],
    [ "ValidBits", "group__UaStackTypes.html#ga9cb3005d8166b39e2a4bd3a5c7e966d3", null ],
    [ "Value", "group__UaStackTypes.html#gad1541d8ebb3bf7b7837a3862c6266f39", null ],
    [ "Value", "group__UaStackTypes.html#ga58c8a3df4e676757166a9cb12441f8ae", null ],
    [ "Value", "group__UaStackTypes.html#gab73548c3060f08143718e983701b24f4", null ],
    [ "Value", "group__UaStackTypes.html#ga3ca42461df9be24803d0a50daa97b157", null ],
    [ "Value", "group__UaStackTypes.html#ga44e48805a3c8b6fd7289d0dfb7fa172d", null ],
    [ "Value", "group__UaStackTypes.html#gac73eef6289dbf901c65d455164a5fc45", null ],
    [ "Value", "group__UaStackTypes.html#ga3b182cdf58d719acc9b8911390408c0b", null ],
    [ "ValueRank", "group__UaStackTypes.html#ga3fb73033eec0aa0c8050182bf7d04ad5", null ],
    [ "VendorName", "group__UaStackTypes.html#gaa797675c8200f0eddc0e47260a482c89", null ],
    [ "VendorProductCertificate", "group__UaStackTypes.html#ga1bd41d9aad3bf1c0c7b7ea7145bca9e0", null ],
    [ "Verb", "group__UaStackTypes.html#gaa0e81ecd74ae9ef3b23bd82643a66385", null ],
    [ "View", "group__UaStackTypes.html#ga5aef5d9f3fdcfeacf30077f5a57c4d7d", null ],
    [ "View", "group__UaStackTypes.html#ga168653f0e90a93b7298e436df937bbf6", null ],
    [ "ViewId", "group__UaStackTypes.html#ga672bc50354b983a799f570f7743508a8", null ],
    [ "ViewVersion", "group__UaStackTypes.html#gad1c2ea69c8f725433711ea0fdb328649", null ],
    [ "WhereClause", "group__UaStackTypes.html#ga7b348261ad6e6e723d47da865e0182de", null ],
    [ "WhereClauseResult", "group__UaStackTypes.html#ga80d7ea77bab84400e403194ff767cc65", null ],
    [ "WriteCount", "group__UaStackTypes.html#ga44d3178e3c7b86b2525a7adafb2922ee", null ],
    [ "X", "group__UaStackTypes.html#gab3fc36e41c207aa086e4c5de281a3b87", null ]
];