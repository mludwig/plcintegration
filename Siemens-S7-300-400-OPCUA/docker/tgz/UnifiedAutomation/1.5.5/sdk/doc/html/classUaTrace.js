var classUaTrace =
[
    [ "TraceLevel", "group__CppBaseLibraryClass.html#gaed5f19c634ecf65fdd930643a6eb43ed", [
      [ "NoTrace", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda84367d59cf76ffb0510ee9a1587e7fcb", null ],
      [ "Errors", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43edac83dac97e5c432fe6e1f03c53461958b", null ],
      [ "Warning", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda2235b328f7bc4b1209cbe1923638cbd1", null ],
      [ "Info", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda95fad2c345a0ea81a0173a6588ea2b18", null ],
      [ "InterfaceCall", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda39ece9126fc39c6aa08963d6cc463ae4", null ],
      [ "CtorDtor", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda28d331406a22b5e685b7e6d19ffcd1c0", null ],
      [ "ProgramFlow", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda8230a46ef07b4e760392c871d4bd861e", null ],
      [ "Data", "group__CppBaseLibraryClass.html#ggaed5f19c634ecf65fdd930643a6eb43eda833c9525ba4580fe5ec57771a2cf5ffe", null ]
    ] ],
    [ "UaTrace", "classUaTrace.html#a3ffb3f3998760f7539bfcd764a78875d", null ],
    [ "~UaTrace", "classUaTrace.html#a0223b0cbe9f1cc3d98e0743cffef060f", null ]
];