var structOpcUa__ServerStatusDataType =
[
    [ "BuildInfo", "group__UaStackTypes.html#ga13c4d497584bace0a48692aed794af12", null ],
    [ "CurrentTime", "group__UaStackTypes.html#gae4cdb06cb9d125b783019dd79541ad10", null ],
    [ "SecondsTillShutdown", "group__UaStackTypes.html#ga9364ff68384ae5708f8a29398918ca46", null ],
    [ "ShutdownReason", "group__UaStackTypes.html#ga932a69a816162138490390d08a6b9f8f", null ],
    [ "StartTime", "group__UaStackTypes.html#ga09dd9b5460589b0b8a2410bc99a77532", null ],
    [ "State", "group__UaStackTypes.html#ga734414aea5f9043411bab1c14cb16726", null ]
];