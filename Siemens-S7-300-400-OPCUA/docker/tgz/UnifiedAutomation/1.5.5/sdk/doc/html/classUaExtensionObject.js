var classUaExtensionObject =
[
    [ "ExtensionObjectEncoding", "classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270", [
      [ "None", "classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270a2c68173301127b3dd1b7c0d835d9e612", null ],
      [ "Binary", "classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270a56db99a0689f5d6e0cb7d8c1c326d9c5", null ],
      [ "Xml", "classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270aa8b6b64d206fd69966a9777eb03ee5e6", null ],
      [ "EncodeableObject", "classUaExtensionObject.html#a1689fe644ca288a05df3f22e6eecd270a7b6b2feb20a26894ad7d9641a06dabab", null ]
    ] ],
    [ "UaExtensionObject", "classUaExtensionObject.html#a762e40a82575e0673c422ac6e80c55ad", null ],
    [ "UaExtensionObject", "classUaExtensionObject.html#a17b4e5225534fe134abb22babb4e1d90", null ],
    [ "UaExtensionObject", "classUaExtensionObject.html#a2fd88fabcd2d12f2e32106bd7a5e3872", null ],
    [ "~UaExtensionObject", "classUaExtensionObject.html#a88134d3bb7b0edf4ca8c18e5723e907d", null ],
    [ "attach", "classUaExtensionObject.html#abb0bfd37faa6f64ae2197d19bea3c611", null ],
    [ "binary", "classUaExtensionObject.html#a88266f9d61edabdcd33d6eb1a53dc19a", null ],
    [ "changeEncoding", "classUaExtensionObject.html#a8599105c493c9660309c529eda632c0b", null ],
    [ "clear", "classUaExtensionObject.html#a41dc3ae8cac1ec89580d56f4e40506bb", null ],
    [ "copy", "classUaExtensionObject.html#a4b5a73be33836ab29102af6e0c9e26d0", null ],
    [ "copyTo", "classUaExtensionObject.html#a984c7425cb6b85fb0e251eb45919d5ef", null ],
    [ "dataTypeId", "classUaExtensionObject.html#aa54c3cc4d071cc2913741304cbdfd778", null ],
    [ "dataTypeId", "classUaExtensionObject.html#a7af0bd52f05be414ae81d35a595a9e99", null ],
    [ "detach", "classUaExtensionObject.html#a113c13b900e5ee53108eb08746054fac", null ],
    [ "encoding", "classUaExtensionObject.html#a18f3702261874d4a68894eb29f28ea0f", null ],
    [ "encodingTypeId", "classUaExtensionObject.html#ae20991caac0ed265798194ef6dbdb6c1", null ],
    [ "object", "classUaExtensionObject.html#aeb3fb270da4c81f7fd990a747761e28f", null ],
    [ "objectType", "classUaExtensionObject.html#a558ff3ace0f69cca84fe32b1a0a0a7c1", null ],
    [ "operator const OpcUa_ExtensionObject *", "classUaExtensionObject.html#a1635c95eb187c7ad09439e21c96b6eaa", null ],
    [ "operator=", "classUaExtensionObject.html#a1838f0220554fcd1e7e055af6e4fa65a", null ],
    [ "setValue", "classUaExtensionObject.html#a69eaa5c1205f66ca3bdce4ce5407070f", null ],
    [ "value", "classUaExtensionObject.html#a731deda0db39d13984d4a17396d9a54b", null ],
    [ "xml", "classUaExtensionObject.html#aac141a6a234cf0eac51b93f7dbbae82b", null ]
];