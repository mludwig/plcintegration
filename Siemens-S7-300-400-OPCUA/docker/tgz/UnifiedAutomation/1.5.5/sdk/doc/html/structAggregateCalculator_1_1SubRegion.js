var structAggregateCalculator_1_1SubRegion =
[
    [ "DataPoint", "structAggregateCalculator_1_1SubRegion.html#a525ed1a33eb8566b0dc3aa4f9e7963ec", null ],
    [ "Duration", "structAggregateCalculator_1_1SubRegion.html#a1dfc913294efe92aa0c568b9d1bc866d", null ],
    [ "EndValue", "structAggregateCalculator_1_1SubRegion.html#a3261d817bc91c7a36619b5c331c1ccff", null ],
    [ "StartTime", "structAggregateCalculator_1_1SubRegion.html#a2e093432936693f763b3e30319238325", null ],
    [ "StartValue", "structAggregateCalculator_1_1SubRegion.html#a973a443fa71fe94ef684896cac43a2fb", null ],
    [ "StatusCode", "structAggregateCalculator_1_1SubRegion.html#a27f6381286dd83f03ed6e614e31cb895", null ]
];