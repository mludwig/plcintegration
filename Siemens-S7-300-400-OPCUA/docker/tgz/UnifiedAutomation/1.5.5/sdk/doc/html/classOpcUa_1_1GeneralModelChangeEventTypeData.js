var classOpcUa_1_1GeneralModelChangeEventTypeData =
[
    [ "GeneralModelChangeEventTypeData", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a58a848b1dff2e7aa114e1adcb6a3329f", null ],
    [ "~GeneralModelChangeEventTypeData", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#aa2789970950dd310775bc2c3079c6358", null ],
    [ "getChanges", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a332711f1386b42d88c5da61267143f28", null ],
    [ "getChangesValue", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a3cca3bb005a9d380d9264e377497c0df", null ],
    [ "getFieldData", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a9f4ca71bfb4017a2db1619c48d0e29d3", null ],
    [ "setChanges", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a68a6d1c47d0339dade0e5ebda3839f98", null ],
    [ "setChangesStatus", "classOpcUa_1_1GeneralModelChangeEventTypeData.html#a6a07bf62b37c149fa17f8091c5fee7c9", null ]
];