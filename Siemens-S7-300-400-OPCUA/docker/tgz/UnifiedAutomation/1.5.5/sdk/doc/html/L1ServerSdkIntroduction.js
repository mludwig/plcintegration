var L1ServerSdkIntroduction =
[
    [ "Introduction", "L1ServerSdkIntroduction.html#L2ServerSdkIntroduction", [
      [ "Base", "L1ServerSdkIntroduction.html#L3ServerSdkIntroBase", null ],
      [ "Core Module", "L1ServerSdkIntroduction.html#L3ServerSdkIntroCoreModule", null ],
      [ "UA Module", "L1ServerSdkIntroduction.html#L3ServerSdkIntroUaModule", null ],
      [ "Server Application", "L1ServerSdkIntroduction.html#L3ServerSdkIntroServerApp", null ],
      [ "System Integration", "L1ServerSdkIntroduction.html#L3ServerSdkIntroSysIntegration", null ]
    ] ],
    [ "Starting Points for Users of the SDK", "L2ServerSdkStart.html", [
      [ "Implementing SDK Interfaces (Using SDK Level)", "L2ServerSdkStart.html#L3ServerSdkStartImplInterfaces", null ],
      [ "Using Generic Model and Base Classes (Using Toolkit Level)", "L2ServerSdkStart.html#L3ServerSdkStartGenericModel", null ]
    ] ],
    [ "Server Configuration", "L2ServerSdkServerConfig.html", [
      [ "XML Configuration File", "L2ServerSdkServerConfig.html#server_config_xml_file", [
        [ "Trace", "L2ServerSdkServerConfig.html#server_config_xml_file_trace", null ],
        [ "Default Application Certificate Store", "L2ServerSdkServerConfig.html#server_config_xml_file_defaultappcertificatestore", null ],
        [ "Endpoint Configuration", "L2ServerSdkServerConfig.html#server_config_xml_file_endpointconfig", [
          [ "List of Configured Endpoints", "L2ServerSdkServerConfig.html#server_config_xml_file_endpoint", null ]
        ] ],
        [ "Server Settings", "L2ServerSdkServerConfig.html#server_config_xml_file_serversettings", null ],
        [ "Build Information for the Server Application", "L2ServerSdkServerConfig.html#server_config_xml_file_buildinfo", null ],
        [ "Server Instance Information", "L2ServerSdkServerConfig.html#server_config_xml_file_serverinst", null ],
        [ "User Identity Tokens", "L2ServerSdkServerConfig.html#server_config_xml_file_user_identity_tokens", null ],
        [ "IDs for Default Roles", "L2ServerSdkServerConfig.html#server_config_xml_file_ids", null ],
        [ "Discovery Registration", "L2ServerSdkServerConfig.html#server_config_xml_file_discoveryregistration", null ],
        [ "Redundancy Support and Additional Server Entries", "L2ServerSdkServerConfig.html#server_config_xml_file_redundancyadditionalserverentries", [
          [ "Redundancy Settings", "L2ServerSdkServerConfig.html#server_config_xml_file_redundancy", null ],
          [ "Additional Server Entries", "L2ServerSdkServerConfig.html#server_config_xml_file_additionalserverentries", null ]
        ] ],
        [ "Serializer", "L2ServerSdkServerConfig.html#server_config_xml_file_serializer", null ],
        [ "Stack Thread Pool Settings", "L2ServerSdkServerConfig.html#server_config_xml_file_stackthreadpool", null ]
      ] ],
      [ "INI Configuration File", "L2ServerSdkServerConfig.html#server_config_ini_file", [
        [ "Build Information for the Server Application", "L2ServerSdkServerConfig.html#server_config_ini_file_buildinfo", null ],
        [ "Server Instance Information", "L2ServerSdkServerConfig.html#server_config_ini_file_serverinst", null ],
        [ "Trace", "L2ServerSdkServerConfig.html#server_config_ini_file_trace", null ],
        [ "Default Application Certificate Store", "L2ServerSdkServerConfig.html#server_config_ini_file_defaultappcertificatestore", null ],
        [ "Server Settings", "L2ServerSdkServerConfig.html#server_config_ini_file_serversettings", null ],
        [ "User Identity Tokens", "L2ServerSdkServerConfig.html#server_config_ini_file_useridentitytokens", null ],
        [ "IDs for Default Roles", "L2ServerSdkServerConfig.html#server_config_ini_file_ids", null ],
        [ "Serializer", "L2ServerSdkServerConfig.html#server_config_ini_file_serializer", null ],
        [ "Stack Thread Pool Settings", "L2ServerSdkServerConfig.html#server_config_ini_file_stackthreadpool", null ],
        [ "Discovery Registration", "L2ServerSdkServerConfig.html#server_config_ini_file_discovery", null ],
        [ "Redundancy Support and Additional Server Entries", "L2ServerSdkServerConfig.html#server_config_ini_file_redundancyadditionalserverentries", [
          [ "Redundancy Settings", "L2ServerSdkServerConfig.html#server_config_ini_file_redundancy", null ],
          [ "Additional Server Entries", "L2ServerSdkServerConfig.html#server_config_ini_file_additionalserverentries", null ]
        ] ],
        [ "Endpoint Configuration", "L2ServerSdkServerConfig.html#server_config_ini_file_uaendpoint", null ]
      ] ]
    ] ],
    [ "Multiplexing of Service Calls", "L2ServerSdkMultiplex.html", [
      [ "UA Message Exchange is Asynchronous", "L2ServerSdkMultiplex.html#L3ServerSdkMultiplexUaMessage", null ],
      [ "Multiplexing", "L2ServerSdkMultiplex.html#L3ServerSdkMultiplexMultiplex", null ]
    ] ],
    [ "Security", "L2ServerSdkSecurity.html", "L2ServerSdkSecurity" ],
    [ "Internal Client", "L2ServerSdkInternalClient.html", [
      [ "Creating a Session", "L2ServerSdkInternalClient.html#InternalClient_CreateSession", null ],
      [ "Browse", "L2ServerSdkInternalClient.html#InternalClient_Browse", null ],
      [ "Read", "L2ServerSdkInternalClient.html#InternalClient_Read", null ],
      [ "Write", "L2ServerSdkInternalClient.html#InternalClient_Write", null ],
      [ "Create Monitored Items", "L2ServerSdkInternalClient.html#InternalClient_Monitor", null ]
    ] ],
    [ "Redundancy", "L2ServerRedundancy.html", [
      [ "Introduction", "L2ServerRedundancy.html#ServerRedundancyIntro", [
        [ "Non-Transparent Redundancy", "L2ServerRedundancy.html#ServerRedundancyNonTransparent", null ],
        [ "Transparent Redundancy", "L2ServerRedundancy.html#ServerRedundancyTransparent", null ]
      ] ],
      [ "Configuration for Non-Transparent Redundancy", "L2ServerRedundancy.html#NonTransparentServerRedundancyConfig", null ],
      [ "ServiceLevel", "L2ServerRedundancy.html#ServerRedundancyServiceLevel", null ]
    ] ]
];