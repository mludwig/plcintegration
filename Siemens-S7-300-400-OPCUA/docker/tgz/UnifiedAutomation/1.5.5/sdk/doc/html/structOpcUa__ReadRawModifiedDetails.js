var structOpcUa__ReadRawModifiedDetails =
[
    [ "EndTime", "group__UaStackTypes.html#ga02e492a428fa30ba3e53cc4dc28d0ed7", null ],
    [ "IsReadModified", "group__UaStackTypes.html#ga9a8a9d120f0749607d415cc68b8e2b60", null ],
    [ "NumValuesPerNode", "group__UaStackTypes.html#ga3968b94aedc3033d44daabe682f28f3b", null ],
    [ "ReturnBounds", "group__UaStackTypes.html#ga0683b4890d93df30bd2bbefc4efac07e", null ],
    [ "StartTime", "group__UaStackTypes.html#ga63008bb07443bc2b716a0eb2d1acc9d7", null ]
];