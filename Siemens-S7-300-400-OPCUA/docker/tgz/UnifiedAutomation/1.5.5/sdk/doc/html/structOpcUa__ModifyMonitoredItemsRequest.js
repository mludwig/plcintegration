var structOpcUa__ModifyMonitoredItemsRequest =
[
    [ "ItemsToModify", "group__UaStackTypes.html#gafae3a19099fca7edc78828ee53ec242d", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga88799f8ec33c70d8d3d1378aec677492", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga3bed32d7ed039cf1071348a9731d7276", null ]
];