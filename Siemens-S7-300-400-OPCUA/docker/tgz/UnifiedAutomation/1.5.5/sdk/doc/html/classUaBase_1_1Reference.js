var classUaBase_1_1Reference =
[
    [ "Reference", "classUaBase_1_1Reference.html#ac9a83ef6b2d0cb24ec8dec3b61d1ca4e", null ],
    [ "Reference", "classUaBase_1_1Reference.html#a4d575b69f191c77b0089948e8862cc27", null ],
    [ "~Reference", "classUaBase_1_1Reference.html#afe5fcf007206777241c39252e4f4be70", null ],
    [ "isForward", "classUaBase_1_1Reference.html#a554a2602461b8100ef981c538b87699e", null ],
    [ "operator==", "classUaBase_1_1Reference.html#a96b709f9f6fa7b4ab9b6646ae3aa10e0", null ],
    [ "referenceTypeId", "classUaBase_1_1Reference.html#a81f45b6aeb4b51af601e8f9cc6f4a7e5", null ],
    [ "setIsForward", "classUaBase_1_1Reference.html#a6c151a4732e0d2a24db45716ebae7bcb", null ],
    [ "setReferenceTypeId", "classUaBase_1_1Reference.html#a62e084e95b6fe5a96ea50e354ba15f0e", null ],
    [ "setTargetNodeId", "classUaBase_1_1Reference.html#a4b9c3d6ea448a76ad194686813148489", null ],
    [ "targetNodeId", "classUaBase_1_1Reference.html#a465116d7a94346bcfa7c21381b597ac1", null ]
];