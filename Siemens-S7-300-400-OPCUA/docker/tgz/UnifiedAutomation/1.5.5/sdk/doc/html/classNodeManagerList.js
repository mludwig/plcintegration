var classNodeManagerList =
[
    [ "NodeManagerList", "classNodeManagerList.html#a6a63149699075c3626b902e9b385206c", null ],
    [ "~NodeManagerList", "classNodeManagerList.html#a7c77d02ce6a002c7e24e8f9c0482fab7", null ],
    [ "addNodeManager", "classNodeManagerList.html#a9d6065f6a637895ba4770ac9dbbfd6ce", null ],
    [ "getNodeManagerByNamespace", "classNodeManagerList.html#a420d61eec784ab087696590881e6a22d", null ],
    [ "removeNodeManager", "classNodeManagerList.html#a009abf7959d949c1fd03c8b28e5130ca", null ]
];