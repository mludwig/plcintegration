var structOpcUa__HistoryReadResult =
[
    [ "ContinuationPoint", "group__UaStackTypes.html#gae94c2ced5e2044333711927f00552441", null ],
    [ "HistoryData", "group__UaStackTypes.html#ga0c2b9accd54f041f12f5467c3a5538ac", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga1cdb14fb8520f28e597fb4fb53bcd978", null ]
];