var classUaMonitoredItem =
[
    [ "MonitoredItemType", "classUaMonitoredItem.html#ac09c19a943f5f5bdcd115cb165bb4fb6", [
      [ "DATA", "classUaMonitoredItem.html#ac09c19a943f5f5bdcd115cb165bb4fb6a90754a14dc74d2a4436d32166cab1815", null ],
      [ "EVENT", "classUaMonitoredItem.html#ac09c19a943f5f5bdcd115cb165bb4fb6af7bc764ab9aeabf6a221d2e0c22f4737", null ]
    ] ],
    [ "UaMonitoredItem", "classUaMonitoredItem.html#a5a813bc28efb7657b30ed17f86ca90ba", null ],
    [ "~UaMonitoredItem", "classUaMonitoredItem.html#a95636bd75a3322197fe0ad6b66055ab0", null ],
    [ "clientHandle", "classUaMonitoredItem.html#aad30abcf6bf31c08f2aec22d185b8fb0", null ],
    [ "hasChanged", "classUaMonitoredItem.html#aad14b524932dc68986208a114fcea174", null ],
    [ "links", "classUaMonitoredItem.html#ab8e6bae49bb6a584c00e81aa5bd39340", null ],
    [ "monitoringMode", "classUaMonitoredItem.html#a038ed207cff95e34915b1b7746567669", null ],
    [ "nodeId", "classUaMonitoredItem.html#a1b23c92f6c66d7c5b1ca9b8bb6cf9036", null ],
    [ "reset", "classUaMonitoredItem.html#a344f3f716191f043fce513c2ef69b30f", null ],
    [ "resetTriggeredStatus", "classUaMonitoredItem.html#ab3569c94d6893a00b18c066953b188df", null ],
    [ "samplingInterval", "classUaMonitoredItem.html#abc377cc9c3902f7c7a2d278285d63947", null ],
    [ "setClientHandle", "classUaMonitoredItem.html#a079e0b5f1ff20b4266c567db13ccef1e", null ],
    [ "setLinks", "classUaMonitoredItem.html#a8defd79c8d4674fec80aab8e491c5899", null ],
    [ "setMonitoringMode", "classUaMonitoredItem.html#a55e14932826ec726ad02b9b10f19a5dd", null ],
    [ "setSamplingInterval", "classUaMonitoredItem.html#a10fe30d845d2e24c6ca85523f2ff215d", null ],
    [ "setTimestampsToReturn", "classUaMonitoredItem.html#ae2bba3610041dd18ecaa73f5ff4537c1", null ],
    [ "timestampsToReturn", "classUaMonitoredItem.html#ac44d0f563976943dea142afa4ae47385", null ],
    [ "type", "classUaMonitoredItem.html#ac6d48f9b524649b1b4007b6149abdc63", null ],
    [ "wasTriggered", "classUaMonitoredItem.html#a83003278b677d512cfaefec12d64c0e8", null ],
    [ "m_pUaSubscription", "classUaMonitoredItem.html#a50a72ac5a09cbf1f63c026d2da2863ed", null ]
];