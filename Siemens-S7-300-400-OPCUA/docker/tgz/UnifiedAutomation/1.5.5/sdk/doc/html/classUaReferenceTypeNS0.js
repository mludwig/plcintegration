var classUaReferenceTypeNS0 =
[
    [ "~UaReferenceTypeNS0", "classUaReferenceTypeNS0.html#ab801c064d356016a76010f5f03f86218", null ],
    [ "UaReferenceTypeNS0", "classUaReferenceTypeNS0.html#ae344a536daa7bacc81d7d84a70d6431e", null ],
    [ "browse", "classUaReferenceTypeNS0.html#a6128b010f122883c26841bc4a3c65795", null ],
    [ "browseName", "classUaReferenceTypeNS0.html#a5c3d52cba811e4ed44d9c0b59ae1c3cc", null ],
    [ "description", "classUaReferenceTypeNS0.html#aaa285b8989abd19e215ddbc9fef599b1", null ],
    [ "displayName", "classUaReferenceTypeNS0.html#a9b0332d0ad4593bd78d3634a3c1bd21c", null ],
    [ "getUaNode", "classUaReferenceTypeNS0.html#a310897bda3db53ff51815e4e4b3a26ca", null ],
    [ "getUaReferenceLists", "classUaReferenceTypeNS0.html#ac766b1a375bad6c1e52d047e3a5fec04", null ],
    [ "inverseName", "classUaReferenceTypeNS0.html#aca584250a69ee0509238836ac4ece624", null ],
    [ "isAbstract", "classUaReferenceTypeNS0.html#aba3af14c93a34cb5d860b1f702182f97", null ],
    [ "isDescriptionSupported", "classUaReferenceTypeNS0.html#acb4b72e2d767a9cb73ff0f21a3d9b5fb", null ],
    [ "isInverseNameSupported", "classUaReferenceTypeNS0.html#a6613081bb0c8233da344f9a9b603c449", null ],
    [ "isSubtypeOf", "classUaReferenceTypeNS0.html#aae09f9e2d5df17d8a3d43f58fe6b1d97", null ],
    [ "isUserWriteMaskSupported", "classUaReferenceTypeNS0.html#a382cc25929db95bfb1de8de11cdecffe", null ],
    [ "isWriteMaskSupported", "classUaReferenceTypeNS0.html#aab87cf655f57a125859acd3abbdeb07c", null ],
    [ "nodeId", "classUaReferenceTypeNS0.html#aa0de9a14d7cc01cc8590e8211149038a", null ],
    [ "symmetric", "classUaReferenceTypeNS0.html#aa75d82ea3103362192878d68ff945bbd", null ],
    [ "userWriteMask", "classUaReferenceTypeNS0.html#a53d4be3634b224cf29b7b540c72e9094", null ],
    [ "writeMask", "classUaReferenceTypeNS0.html#a99a6c414e6bb4aabb3ff8df6de4966d7", null ]
];