var classHistoryManagerCallback =
[
    [ "HistoryManagerCallback", "classHistoryManagerCallback.html#a4c267d0fef787652a07f2a200a8f551c", null ],
    [ "~HistoryManagerCallback", "classHistoryManagerCallback.html#a3dd9055ef7e8d5448bf3af73f78a3b26", null ],
    [ "finishHistoryReadData", "classHistoryManagerCallback.html#a66d79d656313c3bae209c9f04a029e22", null ],
    [ "finishHistoryReadEvent", "classHistoryManagerCallback.html#a33dc1429a9c497ba5adafe649eb14265", null ],
    [ "finishHistoryReadModifiedData", "classHistoryManagerCallback.html#a0cf5ed503b00be125bae561213444a73", null ],
    [ "finishHistoryUpdate", "classHistoryManagerCallback.html#a19b4a71ebd05e524ad689ccba1425b91", null ]
];