var structOpcUa__MonitoredItemModifyResult =
[
    [ "FilterResult", "group__UaStackTypes.html#gab7acff3e0b3a76293430dc52e3c1e1be", null ],
    [ "RevisedQueueSize", "group__UaStackTypes.html#ga8164f775764c31ac25f49a84a638df28", null ],
    [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga53c6434a5bf94744323bc5167854e67e", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga375b01f9765aed8a49875f2c4c0aa4fe", null ]
];