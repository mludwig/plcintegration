var structOpcUa__ServerDiagnosticsSummaryDataType =
[
    [ "CumulatedSessionCount", "group__UaStackTypes.html#gaa420700e79ddb4d254a24b5798660301", null ],
    [ "CumulatedSubscriptionCount", "group__UaStackTypes.html#gae78d8296a45a445fc895ecf25c26e13f", null ],
    [ "CurrentSessionCount", "group__UaStackTypes.html#ga04b4388289b18877454262feb29df3c1", null ],
    [ "CurrentSubscriptionCount", "group__UaStackTypes.html#ga2f10b86f3397dda082b9e9f11e68ee82", null ],
    [ "PublishingIntervalCount", "group__UaStackTypes.html#gaa3fb89f1f20d223a05e79a697424d22f", null ],
    [ "RejectedRequestsCount", "group__UaStackTypes.html#ga7301bbd87a0fc6b3976dea93c00a4430", null ],
    [ "RejectedSessionCount", "group__UaStackTypes.html#ga5363d232d0fd3e8c61e85d1f884a087b", null ],
    [ "SecurityRejectedRequestsCount", "group__UaStackTypes.html#ga3c3091c00323fddc899bcbe3a35fd711", null ],
    [ "SecurityRejectedSessionCount", "group__UaStackTypes.html#gacf59aa66f039c860f36ab77de055b59a", null ],
    [ "ServerViewCount", "group__UaStackTypes.html#gabdbc069a0682a037a890f525f81d15eb", null ],
    [ "SessionAbortCount", "group__UaStackTypes.html#gacea3e5a1e21026a29b8bb6c9f8c41229", null ],
    [ "SessionTimeoutCount", "group__UaStackTypes.html#ga6e66b31ce0bc6b75c69cc6ff5393193d", null ]
];