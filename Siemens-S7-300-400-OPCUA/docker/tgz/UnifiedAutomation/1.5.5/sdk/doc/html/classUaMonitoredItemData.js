var classUaMonitoredItemData =
[
    [ "UaMonitoredItemData", "classUaMonitoredItemData.html#ae1b08fbdbe37fa9cd9da8933a2151dea", null ],
    [ "~UaMonitoredItemData", "classUaMonitoredItemData.html#ad243f1b73370628d2d0f7542780c3ab5", null ],
    [ "dataChange", "classUaMonitoredItemData.html#a8c91321826b8d3caaf99def44937e386", null ],
    [ "dataChangeTrigger", "classUaMonitoredItemData.html#a3720ab8dab430d07fecaf3aa66a9262c", null ],
    [ "discardOldest", "classUaMonitoredItemData.html#a87e99ac2618caed732e182a1c260baa5", null ],
    [ "getChangedValue", "classUaMonitoredItemData.html#a0183ded8f244fc061a2ab37c9c2c8178", null ],
    [ "getLastValue", "classUaMonitoredItemData.html#a48016147181300ba0c7d255b0aa71249", null ],
    [ "getRemainingQueueSize", "classUaMonitoredItemData.html#abeab61e94ccc76828e547909915691b9", null ],
    [ "hasChanged", "classUaMonitoredItemData.html#ad5f355ed11828144747cd238448d86b4", null ],
    [ "pIOManager", "classUaMonitoredItemData.html#a8e45958930908da6f4f68669160e20ea", null ],
    [ "pVariableHandle", "classUaMonitoredItemData.html#a80dc81575c1581f8c636ae3c2a64e5d6", null ],
    [ "queueSize", "classUaMonitoredItemData.html#a1c2bb00a5a9ecd4dad5d5307033581b6", null ],
    [ "reset", "classUaMonitoredItemData.html#a5ae247fe373a1caccfa0db60d4ed9df3", null ],
    [ "setAbsolutDeadband", "classUaMonitoredItemData.html#a7238007088103ce82891a56a7b0a601c", null ],
    [ "setDataChangeTrigger", "classUaMonitoredItemData.html#a8b76589a6852a0e0f57a58860f2adba7", null ],
    [ "setDiscardPolicy", "classUaMonitoredItemData.html#a542fc41e2f5647ba9a382c150556ce00", null ],
    [ "setIOVariableHandle", "classUaMonitoredItemData.html#a7a33bcecc29b2ebdb70328fcc0b25b76", null ],
    [ "setMonitoringMode", "classUaMonitoredItemData.html#a05f36e555c78557d90fbb61482169f71", null ],
    [ "setQueueSize", "classUaMonitoredItemData.html#ac3e821c17a304a3f6f002c2bc5c5895b", null ],
    [ "type", "classUaMonitoredItemData.html#ab22e72c023bcc6078bc46a68604d1110", null ]
];