var classUaEnumDefinition =
[
    [ "UaEnumDefinition", "classUaEnumDefinition.html#a5c79f85b380c157a872b5ed41d774fa5", null ],
    [ "UaEnumDefinition", "classUaEnumDefinition.html#a6298820b694e3b67016e1132caa8af6d", null ],
    [ "addChild", "classUaEnumDefinition.html#ac926c2859014ca1627456c49a3bbcc4d", null ],
    [ "child", "classUaEnumDefinition.html#af6450b10d84994272022151cee5d2fc8", null ],
    [ "childrenCount", "classUaEnumDefinition.html#af8dbdd08d314d4e912983b3a8d4d2f7a", null ],
    [ "clear", "classUaEnumDefinition.html#aed20660823c8ce75f3a78a06088aa59e", null ],
    [ "dataTypeId", "classUaEnumDefinition.html#a53abbdbce54a26accc115b70b7f7429e", null ],
    [ "documentation", "classUaEnumDefinition.html#ae0664317b27450218f6e47edb025d952", null ],
    [ "enumValue", "classUaEnumDefinition.html#a5a1791404dbabe089b1bc2757a6cb53b", null ],
    [ "getNamespace", "classUaEnumDefinition.html#a9e7cd4b7dfe945be26418040286e5fe6", null ],
    [ "isNull", "classUaEnumDefinition.html#a78685d2900d364f0e02f81afffc1d170", null ],
    [ "name", "classUaEnumDefinition.html#a1153b7f36d51ffbd6e3b97dbedd87dbd", null ],
    [ "operator=", "classUaEnumDefinition.html#a27364e761f476592f339766977a589eb", null ],
    [ "setDataTypeId", "classUaEnumDefinition.html#acc306abaa931d32f8e32317db9565780", null ],
    [ "setDocumentation", "classUaEnumDefinition.html#aad4fd17d838204c144b2e02c0187a523", null ],
    [ "setName", "classUaEnumDefinition.html#a682e993e50e4c3e8a68d0f7fa247025f", null ],
    [ "setNamespace", "classUaEnumDefinition.html#a4b9feaab705b8a275c33586aa8fbef25", null ]
];