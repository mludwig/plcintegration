var classUaComplexNumberType =
[
    [ "UaComplexNumberType", "classUaComplexNumberType.html#ae8fbe91c7382e6065eb83faa87d857e6", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html#aec7f00b512423032098a6c1b6bbf7bd6", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html#a929ae32a641af19767bbeca57616828a", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html#a349325a42661c81dbd7d127509d86d98", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html#ad6f45bbf089d863d62e45f4388de3d47", null ],
    [ "UaComplexNumberType", "classUaComplexNumberType.html#a7a3388748485d4422e54bd1c63badb86", null ],
    [ "~UaComplexNumberType", "classUaComplexNumberType.html#a1ff1352cf1c3b12f6ac013abc594d4a4", null ],
    [ "attach", "classUaComplexNumberType.html#ad7a928ac6e300488df5d4a049f9f0d05", null ],
    [ "clear", "classUaComplexNumberType.html#a7d740655b0b4034df3f905fbd10d3f99", null ],
    [ "copy", "classUaComplexNumberType.html#a5d1ab3d00d7f8ee8130cd897f4793c9a", null ],
    [ "copyTo", "classUaComplexNumberType.html#a5621abfe77da0bbab2b5c1aed11fc309", null ],
    [ "detach", "classUaComplexNumberType.html#a8cd3703199b4e83a51eba5cdcc49d40e", null ],
    [ "getImaginary", "classUaComplexNumberType.html#a5ca5701f34ddd2d2b993559cdc8d0f71", null ],
    [ "getReal", "classUaComplexNumberType.html#a7e8281ba8e36d68f31cc2521a98e1e5d", null ],
    [ "operator!=", "classUaComplexNumberType.html#a094cd6e8e2f4faf95e23ba2229152ba4", null ],
    [ "operator=", "classUaComplexNumberType.html#a7cecf93707ce138cfa9b0434760c86bc", null ],
    [ "operator==", "classUaComplexNumberType.html#a9bd14b9f820729cf53a9647e062a23a1", null ],
    [ "setImaginary", "classUaComplexNumberType.html#a44d35124439381b45bfcb7e8c23c23d9", null ],
    [ "setReal", "classUaComplexNumberType.html#a3e2f48b069130c6486e465f31422a33a", null ]
];