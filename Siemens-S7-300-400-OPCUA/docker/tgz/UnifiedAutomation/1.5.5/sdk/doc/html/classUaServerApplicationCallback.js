var classUaServerApplicationCallback =
[
    [ "UaServerApplicationCallback", "classUaServerApplicationCallback.html#a48915e6210c6761b932a85e46068af8b", null ],
    [ "~UaServerApplicationCallback", "classUaServerApplicationCallback.html#a64a3a59d6bc1df0b5eb720db25cecadd", null ],
    [ "afterLoadConfiguration", "classUaServerApplicationCallback.html#a0d4535daa0d8203692ce0647a69a2004", null ],
    [ "beforeEndpointOpen", "classUaServerApplicationCallback.html#a52406f0b241df56f63a0b699b20b5af4", null ],
    [ "createSession", "classUaServerApplicationCallback.html#a53d0a5db12895df3b4db4ad25dda0e12", null ],
    [ "createUaServer", "classUaServerApplicationCallback.html#a69842f1c7526c48097e12e7cec4508d1", null ],
    [ "logonSessionUser", "classUaServerApplicationCallback.html#a3a586ac984dea77b6a9c757e576bc7a8", null ],
    [ "requestServerShutDown", "classUaServerApplicationCallback.html#a705c9d086876405a2833ca2d9f348918", null ]
];