var searchData=
[
  ['xml',['xml',['../classUaExtensionObject.html#aac141a6a234cf0eac51b93f7dbbae82b',1,'UaExtensionObject']]],
  ['xmlencodingid',['xmlEncodingId',['../classUaOptionSetDefinition.html#a34e056dcaae66ae0fc4c57161cc187ff',1,'UaOptionSetDefinition::xmlEncodingId()'],['../classUaStructureDefinition.html#a297b5cd2dad09973f35cd412498faf4e',1,'UaStructureDefinition::xmlEncodingId()']]],
  ['xmlnamespace',['xmlNamespace',['../classUaBase_1_1Extension.html#af92ced08ac9fbdc02f7e07c8f8a903ba',1,'UaBase::Extension']]],
  ['xmluanodefactorymanager',['XmlUaNodeFactoryManager',['../classXmlUaNodeFactoryManager.html#abb62b6064e723f6922aecd8593e9d5df',1,'XmlUaNodeFactoryManager']]],
  ['xyarrayitemtype',['XYArrayItemType',['../classOpcUa_1_1XYArrayItemType.html#abc5195dfd2945065cc217d6e664c0f33',1,'OpcUa::XYArrayItemType::XYArrayItemType(UaNode *pParentNode, UaVariable *pInstanceDeclarationVariable, NodeManagerConfig *pNodeConfig, UaMutexRefCounted *pSharedMutex=NULL)'],['../classOpcUa_1_1XYArrayItemType.html#ab77fde44768efab88212a0e69d188bd9',1,'OpcUa::XYArrayItemType::XYArrayItemType(const UaNodeId &amp;nodeId, const UaString &amp;name, OpcUa_UInt16 browseNameNameSpaceIndex, const UaVariant &amp;initialValue, OpcUa_Byte accessLevel, NodeManagerConfig *pNodeConfig, UaMutexRefCounted *pSharedMutex=NULL)'],['../classOpcUa_1_1XYArrayItemType.html#a5762618a174aee606ca50214f040178f',1,'OpcUa::XYArrayItemType::XYArrayItemType(UaBase::Variable *pBaseNode, XmlUaNodeFactoryManager *pFactory, NodeManagerConfig *pNodeConfig, UaMutexRefCounted *pSharedMutex=NULL)']]]
];
