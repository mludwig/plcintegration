var structOpcUa__ReadRequest =
[
    [ "MaxAge", "group__UaStackTypes.html#gaa1c94f0992e008f313e564bd47ba7825", null ],
    [ "NodesToRead", "group__UaStackTypes.html#ga134b8963a7fe765316e34a84690a5067", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga77f06c6b689c0346381f74bb649c4c23", null ]
];