var structOpcUa__SimpleAttributeOperand =
[
    [ "AttributeId", "group__UaStackTypes.html#ga6e679f44d36697bef407341ab128cea0", null ],
    [ "BrowsePath", "group__UaStackTypes.html#ga2bdcc24bc9978bfddd67a5118f536a93", null ],
    [ "IndexRange", "group__UaStackTypes.html#gadc98621f5383a64cfcc2aef0043bf5e3", null ],
    [ "TypeDefinitionId", "group__UaStackTypes.html#ga8f3938a97ab3974f3da6cae1408daaa8", null ]
];