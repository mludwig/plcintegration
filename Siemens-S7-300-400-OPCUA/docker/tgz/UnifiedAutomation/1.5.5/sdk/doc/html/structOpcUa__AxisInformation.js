var structOpcUa__AxisInformation =
[
    [ "AxisScaleType", "group__UaStackTypes.html#ga0816e2fb0c8528c274c6f9ccc655020a", null ],
    [ "AxisSteps", "group__UaStackTypes.html#ga5104be70bfecbcde2ba8e95febe51f10", null ],
    [ "EngineeringUnits", "group__UaStackTypes.html#gac59ca15321f3dbb52a1936e40a6a9bc7", null ],
    [ "EURange", "group__UaStackTypes.html#gab78f2da3ee6c460219977fb367a03828", null ],
    [ "Title", "group__UaStackTypes.html#gabdafc389b7120064dccc352c132eb696", null ]
];