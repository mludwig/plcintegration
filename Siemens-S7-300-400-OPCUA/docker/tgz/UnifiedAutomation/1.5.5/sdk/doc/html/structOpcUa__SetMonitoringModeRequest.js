var structOpcUa__SetMonitoringModeRequest =
[
    [ "MonitoredItemIds", "group__UaStackTypes.html#gacdc2a5fec6bce2190cd2f00cf1870107", null ],
    [ "MonitoringMode", "group__UaStackTypes.html#ga7b81e872d1361fc349d44888d0697d93", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#ga759ca7d85c383a98236f13419c569b8f", null ]
];