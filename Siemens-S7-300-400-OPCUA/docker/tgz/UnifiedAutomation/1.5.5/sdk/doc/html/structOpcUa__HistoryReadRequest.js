var structOpcUa__HistoryReadRequest =
[
    [ "HistoryReadDetails", "group__UaStackTypes.html#gaeed21ab060d62ed636264fb1271b6873", null ],
    [ "NodesToRead", "group__UaStackTypes.html#gada119cbe9ed3b439e7ff0f1956c893f4", null ],
    [ "ReleaseContinuationPoints", "group__UaStackTypes.html#ga7f2373abbacb531e7af1dd9852c74863", null ],
    [ "TimestampsToReturn", "group__UaStackTypes.html#ga22138c8b6c2fde067a7c3c8193c3776b", null ]
];