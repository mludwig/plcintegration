var classUaVariable =
[
    [ "~UaVariable", "classUaVariable.html#a32572d05206750c463cb4e797ffff436", null ],
    [ "UaVariable", "classUaVariable.html#a4479e26393fd79abf04ad83859179164", null ],
    [ "accessLevel", "classUaVariable.html#a6ab473854d77eba468b161391c506f5c", null ],
    [ "arrayDimensions", "classUaVariable.html#a344d7721f39d8cb2704f117dc7583aeb", null ],
    [ "dataType", "classUaVariable.html#adeeb352af75708c0a13ddd422d2caa50", null ],
    [ "getAttributeValue", "classUaVariable.html#a3bb0eaa464cc0b77b7432130c194fe9b", null ],
    [ "historizing", "classUaVariable.html#a10738e868b6f7648118e6ac70af9c973", null ],
    [ "isArrayDimensionsSupported", "classUaVariable.html#a286459a2c29a6cace032ee5412d4f36d", null ],
    [ "isMinimumSamplingIntervalSupported", "classUaVariable.html#a901f4c56237734f254f52ba9233796a8", null ],
    [ "minimumSamplingInterval", "classUaVariable.html#aabca6d47998c63d4c019febba9efe436", null ],
    [ "nodeClass", "classUaVariable.html#aa2f3eeff86a52d74c037fc9b988c4075", null ],
    [ "pVariableHandle", "classUaVariable.html#a92d8fd19596d207e821643bd04add649", null ],
    [ "setAttributeValue", "classUaVariable.html#a54732065faf6dd7a8e9b5df719ecdb95", null ],
    [ "setValue", "classUaVariable.html#a23bf883ad324d2826690b0e06419c608", null ],
    [ "setValueHandling", "classUaVariable.html#a370c76a0b406d2b1f42ede350f246be6", null ],
    [ "userAccessLevel", "classUaVariable.html#a4c05e0a0ca47e79853eb15be65c16da1", null ],
    [ "value", "classUaVariable.html#abe3ab77ad084493ae2caab721985aa7b", null ],
    [ "valueHandling", "classUaVariable.html#ab58c41477585a5536d8cb0d401280c18", null ],
    [ "valueRank", "classUaVariable.html#a89af8d4a170dc08392fcc1ac339b700a", null ]
];