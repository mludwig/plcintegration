var structOpcUa__MonitoredItemCreateResult =
[
    [ "FilterResult", "group__UaStackTypes.html#ga068bd04e98c885111ad2d7296985a8dc", null ],
    [ "MonitoredItemId", "group__UaStackTypes.html#ga3189263bb49e2ab983578fb9f3293d35", null ],
    [ "RevisedQueueSize", "group__UaStackTypes.html#gab45581fcab8c5a2c6708b65d3b0624ba", null ],
    [ "RevisedSamplingInterval", "group__UaStackTypes.html#ga5662c6ccf70fdb6c2777bc54edda8573", null ],
    [ "StatusCode", "group__UaStackTypes.html#gab3175611ec1758b3f19d79a146ff816d", null ]
];