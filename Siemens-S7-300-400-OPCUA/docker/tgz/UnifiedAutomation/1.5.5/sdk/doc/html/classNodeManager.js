var classNodeManager =
[
    [ "NodeManager", "classNodeManager.html#af1ebc3eef1c1b27c8d0961039de52938", null ],
    [ "~NodeManager", "classNodeManager.html#a8f52614e110fa17e4fab9bb5486fd77a", null ],
    [ "addNode", "classNodeManager.html#a45a02e6efe004198e325aa5ad4960967", null ],
    [ "addReference", "classNodeManager.html#af6b0403e310a90354dab4eb59d39210c", null ],
    [ "browse", "classNodeManager.html#a7715132b38088ca009cfb0b9a73d7b6e", null ],
    [ "deleteNode", "classNodeManager.html#aa3e2e68d4ac5ae4329fe5d338085e005", null ],
    [ "deleteReference", "classNodeManager.html#a42ca0acee204c47524e11458d055584b", null ],
    [ "getEventManagers", "classNodeManager.html#af5f21b086c1b44381beab6bd01f92feb", null ],
    [ "getHistoryVariableHandle", "classNodeManager.html#a8ddf6bfa1f807af5e39bdb6f7faf4fec", null ],
    [ "getMethodHandle", "classNodeManager.html#a478db5810d78f36668a986f11bfb2a9c", null ],
    [ "getNodeManagerConfig", "classNodeManager.html#a5a734344dd1319d2c2a3eac714c87c37", null ],
    [ "getNodeManagerCrossReferences", "classNodeManager.html#a7a25983a35ffc79ceee55f2b53711f09", null ],
    [ "getNodeManagerUaNode", "classNodeManager.html#af6038809dfcc6cee76b402c160fbb004", null ],
    [ "getVariableHandle", "classNodeManager.html#a0621cf9e497ffa0f65a13f103ba3db51", null ],
    [ "sessionActivated", "classNodeManager.html#a82d7d7b44982886114b0f045ff66e7e7", null ],
    [ "sessionClosed", "classNodeManager.html#add9441fd8b1ac2a80788f3f34ff8b531", null ],
    [ "sessionOpened", "classNodeManager.html#a02d146e3435ec0893096b3aef5c6327a", null ],
    [ "translateBrowsePathToNodeId", "classNodeManager.html#ac2a3d5de9bfb7ac1310e102722ee6493", null ]
];