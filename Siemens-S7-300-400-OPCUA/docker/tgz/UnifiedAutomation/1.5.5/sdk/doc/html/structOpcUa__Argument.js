var structOpcUa__Argument =
[
    [ "ArrayDimensions", "group__UaStackTypes.html#gaf286c6acb5eb46279a828f68400a5ac4", null ],
    [ "DataType", "group__UaStackTypes.html#gaa9e8d5f4dc3edc78b63bf1ab626b8b4d", null ],
    [ "Description", "group__UaStackTypes.html#ga1f86a641da1ae04f7d8b0be1777955c4", null ],
    [ "Name", "group__UaStackTypes.html#ga0f2505639e6714511dbe0ef23676591b", null ],
    [ "ValueRank", "group__UaStackTypes.html#ga3fb73033eec0aa0c8050182bf7d04ad5", null ]
];