var classUaDoubleComplexNumberType =
[
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#a25b017db9ee5c5dfc131fb5357788b4c", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#adf42176472edb22712b2ed68dc1546ad", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#a05cb441da0843db8c1449cef2129ec7e", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#a8fb2a150301c2b3e0ee512b313b5ee13", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#a79b3ba772e52eb2c6fe26690e0754491", null ],
    [ "UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#ace842d0c4128ca1e38f56c4cba38e744", null ],
    [ "~UaDoubleComplexNumberType", "classUaDoubleComplexNumberType.html#a6b1a61f7d54f57d941b1df7104066e6c", null ],
    [ "attach", "classUaDoubleComplexNumberType.html#ab3ea62022139a91cb6d889de21dc5e73", null ],
    [ "clear", "classUaDoubleComplexNumberType.html#a5a1e1d8665b873b112efc0a1e2aca71a", null ],
    [ "copy", "classUaDoubleComplexNumberType.html#a13a39ed0c740c9a16799f0847408a4b5", null ],
    [ "copyTo", "classUaDoubleComplexNumberType.html#a1aef0122e8f6d9aa984b75553dd4f001", null ],
    [ "detach", "classUaDoubleComplexNumberType.html#a726389fde0c3ebf5e8a7545fd9caf0e8", null ],
    [ "getImaginary", "classUaDoubleComplexNumberType.html#a6b6f2e6d0e07c04ff91d9f5e61885e5c", null ],
    [ "getReal", "classUaDoubleComplexNumberType.html#a7915556863c7ecf6db974975eb2dab7c", null ],
    [ "operator!=", "classUaDoubleComplexNumberType.html#a77e248df047c7e628f7a808555ca53a9", null ],
    [ "operator=", "classUaDoubleComplexNumberType.html#a30d7393516471b6ee62607d7f1b6b18c", null ],
    [ "operator==", "classUaDoubleComplexNumberType.html#ac303cd142bbd0f5b0559738319b9a980", null ],
    [ "setImaginary", "classUaDoubleComplexNumberType.html#ae3951a45916e74078847c19a2c71ab91", null ],
    [ "setReal", "classUaDoubleComplexNumberType.html#ac4cfc924ca7d7c952fc0b41c7302ff42", null ]
];