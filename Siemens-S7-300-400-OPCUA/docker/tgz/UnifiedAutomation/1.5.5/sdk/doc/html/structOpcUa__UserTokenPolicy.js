var structOpcUa__UserTokenPolicy =
[
    [ "IssuedTokenType", "group__UaStackTypes.html#gadce2aa31dd85cd3cc5e59e36e70680dc", null ],
    [ "IssuerEndpointUrl", "group__UaStackTypes.html#ga8994ad28453bca253da78b705e32db58", null ],
    [ "PolicyId", "group__UaStackTypes.html#gaf026985260a40538a8cf9a2cda0ea3e9", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga6c17c4640d8c8c08c167a872773ffa29", null ],
    [ "TokenType", "group__UaStackTypes.html#ga7c06846267efe3d7706655b74afd206c", null ]
];