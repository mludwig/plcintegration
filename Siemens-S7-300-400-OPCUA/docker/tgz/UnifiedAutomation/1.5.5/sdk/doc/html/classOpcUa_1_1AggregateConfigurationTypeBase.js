var classOpcUa_1_1AggregateConfigurationTypeBase =
[
    [ "~AggregateConfigurationTypeBase", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a751839ec7ab493ab2dbd377d697c99d7", null ],
    [ "AggregateConfigurationTypeBase", "classOpcUa_1_1AggregateConfigurationTypeBase.html#abc31a869fbfebef4714a76dc5f5d8927", null ],
    [ "AggregateConfigurationTypeBase", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a93e2116b32a48afc83bb7e79f265aa67", null ],
    [ "AggregateConfigurationTypeBase", "classOpcUa_1_1AggregateConfigurationTypeBase.html#aefab7c1c5239229be7c195b82513c1b2", null ],
    [ "getPercentDataBad", "classOpcUa_1_1AggregateConfigurationTypeBase.html#adba1664fc644067762b6ea520d47aea4", null ],
    [ "getPercentDataBadNode", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a898cadd48eee5c76f91c8d165f6d0ef1", null ],
    [ "getPercentDataGood", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a652b18bda7b40179602865dddfae5e30", null ],
    [ "getPercentDataGoodNode", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a7adbdf937c6e687c5d5637bcd72c36c4", null ],
    [ "getTreatUncertainAsBad", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a31d319bd63f6a273ae8993d729db9725", null ],
    [ "getTreatUncertainAsBadNode", "classOpcUa_1_1AggregateConfigurationTypeBase.html#ae1ff52552c9db5d6e984c7e35d66325a", null ],
    [ "getUseSlopedExtrapolation", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a4e8ce6fdf1bd3ae6c4e1d5cb5657983e", null ],
    [ "getUseSlopedExtrapolationNode", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a16b93d5fc5c4d52edce38063f495b2f1", null ],
    [ "setPercentDataBad", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a916eda37c84c8f4946bc44f4c63b8f2a", null ],
    [ "setPercentDataGood", "classOpcUa_1_1AggregateConfigurationTypeBase.html#aac67160c8fcaaa4c848c75dda5373931", null ],
    [ "setTreatUncertainAsBad", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a3f5606c24502059a31e925b1cbbf84c5", null ],
    [ "setUseSlopedExtrapolation", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a6f3b1911118f3f0bc0281d4534e844c6", null ],
    [ "typeDefinitionId", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a42e73a8b8a93086aab2054658cd4a9f0", null ],
    [ "useAccessInfoFromInstance", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a537626c927972eea20f1710ffcd49947", null ],
    [ "useAccessInfoFromType", "classOpcUa_1_1AggregateConfigurationTypeBase.html#a22093b399af64dbd164a8d75116b420f", null ]
];