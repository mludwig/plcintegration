var structOpcUa__ReadProcessedDetails =
[
    [ "AggregateConfiguration", "group__UaStackTypes.html#ga6c28fe30288b57a2ad2e50a618a1e3ff", null ],
    [ "AggregateType", "group__UaStackTypes.html#ga7d8ebb16bc2f25f6b6d09bba30275262", null ],
    [ "EndTime", "group__UaStackTypes.html#ga42844f2a3e8270aa7f749e3605740e87", null ],
    [ "ProcessingInterval", "group__UaStackTypes.html#gac4032d5b58553bc1d8b0368187d7058a", null ],
    [ "StartTime", "group__UaStackTypes.html#gadff860fc2204e0b9db4dabb0e4e07a46", null ]
];