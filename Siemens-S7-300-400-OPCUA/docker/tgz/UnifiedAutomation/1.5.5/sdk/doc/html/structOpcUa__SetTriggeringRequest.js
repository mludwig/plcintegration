var structOpcUa__SetTriggeringRequest =
[
    [ "LinksToAdd", "group__UaStackTypes.html#ga7f5d1617d067a7c5ddc0daadf65812cd", null ],
    [ "LinksToRemove", "group__UaStackTypes.html#ga9178ea6c86faab0a06ef438cdd16c8b1", null ],
    [ "SubscriptionId", "group__UaStackTypes.html#gac3feef9a809c060ba70e6c8d1ccd3d4f", null ],
    [ "TriggeringItemId", "group__UaStackTypes.html#ga542374801c73f1c7e32925a289fe2191", null ]
];