var structOpcUa__BrowseDescription =
[
    [ "BrowseDirection", "group__UaStackTypes.html#ga7b17318b612931e0fc438e4e3eb8b038", null ],
    [ "IncludeSubtypes", "group__UaStackTypes.html#ga8de72b1fd69c0c1719aa99e1dacb9861", null ],
    [ "NodeClassMask", "group__UaStackTypes.html#gab8d99da19b4604028580c73abb8e30f0", null ],
    [ "NodeId", "group__UaStackTypes.html#gac0dc15df7d7e6ef45c4ee0c8956897ab", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga1290ef0c7015cfafcfb22893680a0c54", null ],
    [ "ResultMask", "group__UaStackTypes.html#gaa050664eacde119489639ff618806b2c", null ]
];