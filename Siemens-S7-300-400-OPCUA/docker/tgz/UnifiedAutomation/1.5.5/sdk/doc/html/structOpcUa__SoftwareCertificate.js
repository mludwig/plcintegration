var structOpcUa__SoftwareCertificate =
[
    [ "BuildDate", "group__UaStackTypes.html#gad3d2f9b2635c6b978907d862602e5e89", null ],
    [ "BuildNumber", "group__UaStackTypes.html#gac01990397c73d96991356de70522383a", null ],
    [ "IssueDate", "group__UaStackTypes.html#gaa0ba549d95cf17aa87eee9a6270af014", null ],
    [ "IssuedBy", "group__UaStackTypes.html#ga3e07a94d53fcb90031ad610da2ceaf92", null ],
    [ "ProductName", "group__UaStackTypes.html#ga62f0c365a7e6d9f5fd5a0e1b415c89c5", null ],
    [ "ProductUri", "group__UaStackTypes.html#gaee7dd8db947b25503b6aa4e2899e87f4", null ],
    [ "SoftwareVersion", "group__UaStackTypes.html#gaea4befd3d1a03417d6b4ef5f71e72ecf", null ],
    [ "SupportedProfiles", "group__UaStackTypes.html#ga49fed9cddc887575cf65a87b42f61ffe", null ],
    [ "VendorName", "group__UaStackTypes.html#gaa797675c8200f0eddc0e47260a482c89", null ],
    [ "VendorProductCertificate", "group__UaStackTypes.html#ga1bd41d9aad3bf1c0c7b7ea7145bca9e0", null ]
];