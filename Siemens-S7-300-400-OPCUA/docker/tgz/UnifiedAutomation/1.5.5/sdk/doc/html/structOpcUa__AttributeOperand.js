var structOpcUa__AttributeOperand =
[
    [ "Alias", "group__UaStackTypes.html#gae69a43b8baed353bce814a1236d5ffc5", null ],
    [ "AttributeId", "group__UaStackTypes.html#ga99d92903d0b454d66598d5890bdd86c3", null ],
    [ "BrowsePath", "group__UaStackTypes.html#ga8594bfa18fc5034df7e40e7dd219948d", null ],
    [ "IndexRange", "group__UaStackTypes.html#gad9d2cf1994968cf011221fd12bdc4d07", null ],
    [ "NodeId", "group__UaStackTypes.html#ga948e5020d7706a8880d6aeb02d66a136", null ]
];