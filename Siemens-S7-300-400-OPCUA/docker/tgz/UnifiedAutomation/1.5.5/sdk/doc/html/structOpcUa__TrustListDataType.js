var structOpcUa__TrustListDataType =
[
    [ "IssuerCertificates", "group__UaStackTypes.html#ga2110d5d4824769172fa36ebd6930b11b", null ],
    [ "IssuerCrls", "group__UaStackTypes.html#ga83f32e23cd1e3447eb640bbb5a1daf0d", null ],
    [ "SpecifiedLists", "group__UaStackTypes.html#ga07e83eefa56eb59c244a0da04acf223b", null ],
    [ "TrustedCertificates", "group__UaStackTypes.html#gaaedcd431a46299fd48c21996c932465e", null ],
    [ "TrustedCrls", "group__UaStackTypes.html#ga607826bf16d80271dda67b56ae374a46", null ]
];