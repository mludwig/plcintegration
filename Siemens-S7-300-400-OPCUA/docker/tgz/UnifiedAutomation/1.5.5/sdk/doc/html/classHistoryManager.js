var classHistoryManager =
[
    [ "TransactionType", "group__ServerCoreInterfaces.html#gaf10634704f32afe28485102e140567b9", [
      [ "TransactionReadEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ac0e898b54ee64f998f1d336f2410e39e", null ],
      [ "TransactionReadRaw", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9afef810b34024c6fdcad554fd020a79ac", null ],
      [ "TransactionReadModified", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a044ea5e7a9b0bbfe70ce16156ca2cd77", null ],
      [ "TransactionReadProcessed", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad32ea6d3a0a352575724a8973cce3b1c", null ],
      [ "TransactionReadAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a114db17a943f4259d82210ee0d34f8f3", null ],
      [ "TransactionUpdateData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad7658e3ce5313c06ecd5aafc0e5098ca", null ],
      [ "TransactionUpdateStructureData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a14e2affacbd82b1c3bbbcf284bd7dda9", null ],
      [ "TransactionUpdateEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a9392ce956bf7352a2d40423058b6dbfc", null ],
      [ "TransactionDeleteData", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a09629afe97bccc03e44ef3848e9c5167", null ],
      [ "TransactionDeleteAtTime", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9ad0091392b06cd5a5c521fe3083846a1a", null ],
      [ "TransactionDeleteEvents", "group__ServerCoreInterfaces.html#ggaf10634704f32afe28485102e140567b9a491e61c8a1d944bb4395703299b429d1", null ]
    ] ],
    [ "HistoryManager", "classHistoryManager.html#a110824c8f28cac57ce634f54581bd8a0", null ],
    [ "~HistoryManager", "classHistoryManager.html#a758a4675d4f7a69a9cf613d23147e378", null ],
    [ "beginDeleteAtTime", "classHistoryManager.html#a20ff46b9a488548f59e406bf47318d67", null ],
    [ "beginDeleteEvents", "classHistoryManager.html#a2ee5aed2c8190df152a4cbc42623716e", null ],
    [ "beginDeleteRawModified", "classHistoryManager.html#a1be75f5e3ef478ed947b970bd96c91fc", null ],
    [ "beginHistoryTransaction", "classHistoryManager.html#a78f6d5b40983060c9b5524981b336042", null ],
    [ "beginReadAtTime", "classHistoryManager.html#a064ffec0bf44fd334043e3224457c265", null ],
    [ "beginReadEvents", "classHistoryManager.html#a25864dfa1826e9c5094e4c63dcd1cadf", null ],
    [ "beginReadProcessed", "classHistoryManager.html#a2d4ab21b345050bda3e982130dbe808a", null ],
    [ "beginReadRawModified", "classHistoryManager.html#a6a91dc6eafc5bcec7a39d7a8ddd3917a", null ],
    [ "beginUpdateData", "classHistoryManager.html#ae6f7d79cb877f11ba959276d0228fbc3", null ],
    [ "beginUpdateEvents", "classHistoryManager.html#af83f2c96a14c78ec251575abca3a990b", null ],
    [ "cancelHistoryTransaction", "classHistoryManager.html#a5891169aa485280c0cde3618d30b63e5", null ],
    [ "finishHistoryTransaction", "classHistoryManager.html#ab2e6765f41c2033d5e551865cdaddf24", null ]
];