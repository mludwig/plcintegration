var classUaVariantException =
[
    [ "Error", "group__CppBaseLibraryClass.html#ga6126f6796700f581b7a65bc430104fa5", [
      [ "UA_OVERFLOW", "group__CppBaseLibraryClass.html#gga6126f6796700f581b7a65bc430104fa5a2c03eb8cfed1d4b34217bed6561852e5", null ],
      [ "UA_INVALIDTYPE", "group__CppBaseLibraryClass.html#gga6126f6796700f581b7a65bc430104fa5a6c10106523c22e2aed37c683b6a4213e", null ],
      [ "UA_INVALIDCONVERSION", "group__CppBaseLibraryClass.html#gga6126f6796700f581b7a65bc430104fa5a4f055912f31a2edd6332f4d82a730367", null ],
      [ "UA_NOTSUPPORTED", "group__CppBaseLibraryClass.html#gga6126f6796700f581b7a65bc430104fa5a118d5e8bc47b477d0b68d8f1f3c4cccb", null ]
    ] ]
];