var structOpcUa__DataValue =
[
    [ "ServerPicoseconds", "group__UaStackTypes.html#ga12ea0497b00e3636710c88bbccbbb725", null ],
    [ "ServerTimestamp", "group__UaStackTypes.html#gace2e324ee3509f81a6f8b02dfbf67ca7", null ],
    [ "SourcePicoseconds", "group__UaStackTypes.html#gaddf019c1a49eda91b2a62a120c7f6b3d", null ],
    [ "SourceTimestamp", "group__UaStackTypes.html#gaade831a742f091f31a90ebe84086ec53", null ],
    [ "StatusCode", "group__UaStackTypes.html#ga01a8fa2beacee3ea16be457be2d68c3b", null ],
    [ "Value", "group__UaStackTypes.html#ga58c8a3df4e676757166a9cb12441f8ae", null ]
];