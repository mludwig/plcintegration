var classUaAbstractDictionaryReader =
[
    [ "browse", "classUaAbstractDictionaryReader.html#a30da5132c0c55df0ca58dfce0524dfca", null ],
    [ "browseList", "classUaAbstractDictionaryReader.html#af28d7d6723e02740106a8c0caca9425d", null ],
    [ "browseList", "classUaAbstractDictionaryReader.html#ac24e4996c2ff6cbe286325f78d4df871", null ],
    [ "readDictionaries", "classUaAbstractDictionaryReader.html#adf7ba73108693083fc3f871eaa6fd4a6", null ],
    [ "readDictionaries", "classUaAbstractDictionaryReader.html#a26ae5337157c93f6cbb166a2cd865dd5", null ],
    [ "readDictionaries", "classUaAbstractDictionaryReader.html#aa093c56ad63461882c2dc24981e1f1f5", null ],
    [ "readDictionaries", "classUaAbstractDictionaryReader.html#a827c9c94e1955f0676f4b4220823c5f5", null ],
    [ "translate", "classUaAbstractDictionaryReader.html#a95dc6e860bde5753b411023a0624211b", null ],
    [ "translateBrowsePaths", "classUaAbstractDictionaryReader.html#a32b686cfc01f00aa74c996988ad5b1a7", null ],
    [ "translateBrowsePaths", "classUaAbstractDictionaryReader.html#afcbcd1c43633976ae630f2eee4d07092", null ]
];