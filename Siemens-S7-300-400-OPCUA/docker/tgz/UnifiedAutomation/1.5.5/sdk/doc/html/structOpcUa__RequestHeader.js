var structOpcUa__RequestHeader =
[
    [ "AdditionalHeader", "group__UaStackTypes.html#ga7f99210601fa2c7636c1eefbd7a53dcf", null ],
    [ "AuditEntryId", "group__UaStackTypes.html#ga980afeaa04b82ddb7e235566a7f396ff", null ],
    [ "AuthenticationToken", "group__UaStackTypes.html#ga4e42cebfa43efd01cc1aed5506431809", null ],
    [ "RequestHandle", "group__UaStackTypes.html#ga336632912cc9aaee6e45cdc60ca23684", null ],
    [ "ReturnDiagnostics", "group__UaStackTypes.html#gab8d43617b01dd745884b539b9ae2b4d9", null ],
    [ "TimeoutHint", "group__UaStackTypes.html#gacc2c479aa29e1e6cdb19deff583c5830", null ],
    [ "Timestamp", "group__UaStackTypes.html#ga04dac22e2b39c5bdf76790bfd2af4d5e", null ]
];