var classAggregateCalculatorInterface =
[
    [ "GetAggregateId", "classAggregateCalculatorInterface.html#a67d8689f0b64be5bdf78ac4eab8d73f5", null ],
    [ "GetProcessedValue", "classAggregateCalculatorInterface.html#a654530a451363dbdf069cc9a368110c9", null ],
    [ "HasEndTimePassed", "classAggregateCalculatorInterface.html#a35bb46c9a79e1bcab9d028645db0d530", null ],
    [ "QueueRawValue", "classAggregateCalculatorInterface.html#a644dc62f0963b41f2873bf8a69e19e8b", null ],
    [ "UsesInterpolatedBounds", "classAggregateCalculatorInterface.html#aec9121ba3a034be8ea8f9005ac39afa8", null ]
];