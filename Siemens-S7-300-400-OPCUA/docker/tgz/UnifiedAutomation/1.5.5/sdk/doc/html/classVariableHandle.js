var classVariableHandle =
[
    [ "ServiceType", "group__ServerCoreInterfaces.html#gaf197ed865bc4c4aaceeef1466b99383f", [
      [ "ServiceRead", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383faccd0cc408a1ffcc5b4f702e0e58079ce", null ],
      [ "ServiceWrite", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa76e73c7a1a4c9e4d4eedf9a6e13e3d3d", null ],
      [ "ServiceMonitoring", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fac2e07df03a9864953ebb8129aff17dac", null ],
      [ "ServiceRegister", "group__ServerCoreInterfaces.html#ggaf197ed865bc4c4aaceeef1466b99383fa38b0243f276da6a26eda4c25ef3728e6", null ]
    ] ],
    [ "VariableHandle", "classVariableHandle.html#a6999e11b397e92eb7eb10b0f841dcdfb", null ],
    [ "~VariableHandle", "classVariableHandle.html#aa045d85dc19cdab1ddb166ef13951f4d", null ],
    [ "m_AttributeID", "classVariableHandle.html#a55e88c8b9fda2280a3276e6d80f5150f", null ],
    [ "m_pIOManager", "classVariableHandle.html#a7faa30f3f7d704e0681d7a01883960fd", null ]
];