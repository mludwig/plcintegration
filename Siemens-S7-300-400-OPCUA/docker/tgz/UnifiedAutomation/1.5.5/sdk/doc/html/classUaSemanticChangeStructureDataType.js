var classUaSemanticChangeStructureDataType =
[
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#a68072f576213f471380273851c268392", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#ac0cd5298d526b8f58f45d0e185407e93", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#a4bb14e44fbfbbe80cf2bf93bb3968fea", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#ac1aa03774dac3f39c241a450cac1cff6", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#a7a6104950360bd33f9f1980d53019c07", null ],
    [ "UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#a6bb148232e6e79bcc8715eeca3651af6", null ],
    [ "~UaSemanticChangeStructureDataType", "classUaSemanticChangeStructureDataType.html#a7c20a5444d94206d9c988da37f0f7b86", null ],
    [ "attach", "classUaSemanticChangeStructureDataType.html#ab504fba2b92c59bb5b5331838d121255", null ],
    [ "clear", "classUaSemanticChangeStructureDataType.html#a713550646ab8d1fcf1be37ad6147d710", null ],
    [ "copy", "classUaSemanticChangeStructureDataType.html#a6d8eb18ecc0ec514acf0445a23ddde1e", null ],
    [ "copyTo", "classUaSemanticChangeStructureDataType.html#a696e090b0b71c90f39155b6385675153", null ],
    [ "detach", "classUaSemanticChangeStructureDataType.html#a1d6cf4b0a14c7684f454137aff6a4335", null ],
    [ "getAffected", "classUaSemanticChangeStructureDataType.html#a79230ddc3bdd2f95fa0a0ba4e7971961", null ],
    [ "getAffectedType", "classUaSemanticChangeStructureDataType.html#a1c26d265700f439774c0b87bb0b6309b", null ],
    [ "operator!=", "classUaSemanticChangeStructureDataType.html#ab04254234b179fa9f85266a163c37157", null ],
    [ "operator=", "classUaSemanticChangeStructureDataType.html#adcb4ae50d5c595171f292fbb8445a014", null ],
    [ "operator==", "classUaSemanticChangeStructureDataType.html#afbf54eb91629d46ce16911854e3355e5", null ],
    [ "setAffected", "classUaSemanticChangeStructureDataType.html#af07d9e77e5fe2b269d8d350686ffffcf", null ],
    [ "setAffectedType", "classUaSemanticChangeStructureDataType.html#a7003bbcbe449e8ab62296b41e3d8b145", null ]
];