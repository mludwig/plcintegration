var structOpcUa__EndpointDescription =
[
    [ "EndpointUrl", "group__UaStackTypes.html#gaca8f214d05d2ccd8bfed670b5a1da29f", null ],
    [ "SecurityLevel", "group__UaStackTypes.html#ga9879b3b610b967045cb55b03a2f64653", null ],
    [ "SecurityMode", "group__UaStackTypes.html#ga37166b39f939936490c9fa2c194a719e", null ],
    [ "SecurityPolicyUri", "group__UaStackTypes.html#ga55a8f937a8b25fd7ead84ac9a794a138", null ],
    [ "Server", "group__UaStackTypes.html#ga373512fcac41cf3db37dea0de4e3e2d4", null ],
    [ "ServerCertificate", "group__UaStackTypes.html#ga1eaf2b0426eb3e86539f66864a0f298a", null ],
    [ "TransportProfileUri", "group__UaStackTypes.html#ga0f43503dfdbcc16286f64c52e98b2014", null ],
    [ "UserIdentityTokens", "group__UaStackTypes.html#gab8b6aa89de527525b15bbae1aeaf3dcc", null ]
];