var classQueryContext =
[
    [ "QueryContext", "classQueryContext.html#a4edc7813dbd13d59692bf62186fb8a48", null ],
    [ "~QueryContext", "classQueryContext.html#a12d335c2a9e959efcc6e15299864c65a", null ],
    [ "detachUserData", "classQueryContext.html#a70e20b8b1ea4e24328b7f52b4bdd189f", null ],
    [ "isFirstQuery", "classQueryContext.html#a6df0383b4f41cc1d2aa36778f82aaa56", null ],
    [ "isLastQuery", "classQueryContext.html#a2872934fd557547a8ac59ad09610e25a", null ],
    [ "noOfNodeTypes", "classQueryContext.html#aa0e7f8b751bb02cda1b3e61ea33ab9c9", null ],
    [ "pFilter", "classQueryContext.html#a542dbdeccf87fb89dbc8e7cbd6d8e48c", null ],
    [ "pNodeTypes", "classQueryContext.html#a5b360e043ceb34eeaa4f8212144bae2b", null ],
    [ "pView", "classQueryContext.html#a0cb526791147060ca690a31d78ef0983", null ],
    [ "setUserData", "classQueryContext.html#ae4a4bfa3167ee52e3559019a59b2a1c6", null ],
    [ "uMaxDataSetsToReturn", "classQueryContext.html#aba930c111ea4fd30dad40ea5bd403036", null ],
    [ "uMaxReferencesToReturn", "classQueryContext.html#a5a85c3b45df28e90354d33949cfb29c6", null ]
];