var CompileSdkWin =
[
    [ "Requirements", "CompileSdkWin.html#CompileSdkWinRequirements", null ],
    [ "Generating Project Files for Visual Studio", "CompileSdkVS.html", [
      [ "Generate SDK Projects", "CompileSdkVS.html#CompileSdkVS2008SdkProjects", null ],
      [ "Build the SDK with Visual Studio", "CompileSdkVS.html#CompileSdkVS2008SBuildSdk", null ],
      [ "Generate Example Projects", "CompileSdkVS.html#CompileSdkVS2008ExampleProjects", null ]
    ] ],
    [ "Compiling the SDK and the Examples with NMake", "CompileSdkNmake.html", [
      [ "Generate SDK Makefiles", "CompileSdkNmake.html#CompileSdkNmakeMakefilesSdk", null ],
      [ "Build the SDK with NMake", "CompileSdkNmake.html#CompileSdkNmakeBuildSdk", null ],
      [ "Generate Makefiles for the Examples", "CompileSdkNmake.html#CompileSdkNmakeExampleMakefiles", null ]
    ] ],
    [ "Compiling the SDK and Examples with MinGW", "CompileSdkMinGW.html", [
      [ "Requirements", "CompileSdkMinGW.html#CompileSdkMinGWRequirements", null ],
      [ "Create Makefiles for the SDK", "CompileSdkMinGW.html#CompileSdkMinGWMakefilesSdk", null ],
      [ "Create Makefiles for the Examples", "CompileSdkMinGW.html#CompileSdkMinGWMakefilesExamples", null ]
    ] ]
];