var classAggregateCalculatorStatus =
[
    [ "AggregateCalculatorStatus", "classAggregateCalculatorStatus.html#a9e1d8e6fc5909b9879f3730aafa7ec49", null ],
    [ "ComputeDurationGoodBad", "classAggregateCalculatorStatus.html#af081192a7a2f04808bec16d7d9ea4bf6", null ],
    [ "ComputeValue", "classAggregateCalculatorStatus.html#a2f72ed48587d1a4f1a14b1248e6fa74d", null ],
    [ "ComputeWorstQuality", "classAggregateCalculatorStatus.html#a8184f1ae1122676b366b655642ab2d05", null ],
    [ "UsesInterpolatedBounds", "classAggregateCalculatorStatus.html#aed14ad26e29e7ad19670b92822738d32", null ]
];