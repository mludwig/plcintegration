var structOpcUa__EventFilterResult =
[
    [ "SelectClauseDiagnosticInfos", "group__UaStackTypes.html#gadd36bcd423510f65020c9d4e9b6b74c6", null ],
    [ "SelectClauseResults", "group__UaStackTypes.html#gadf49b573383c1863a7bdae3b080518b7", null ],
    [ "WhereClauseResult", "group__UaStackTypes.html#ga80d7ea77bab84400e403194ff767cc65", null ]
];