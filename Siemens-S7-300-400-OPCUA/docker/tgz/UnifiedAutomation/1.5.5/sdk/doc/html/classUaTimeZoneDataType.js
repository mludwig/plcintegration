var classUaTimeZoneDataType =
[
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#a08a27f93569a6f9b62136c2b824e8e04", null ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#a235ffcfb8b1f2071663ca58576ab3f4a", null ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#ae1248446bc6b5e0c6f8e98baf6df7a40", null ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#a540e18b794fdc15bfeca4fbf0c184802", null ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#af7d303f7fc9de091f57da63a962094ac", null ],
    [ "UaTimeZoneDataType", "classUaTimeZoneDataType.html#a2095837eff8f77ec1fa24f21e422a500", null ],
    [ "~UaTimeZoneDataType", "classUaTimeZoneDataType.html#ab200e709f219da80e7faea5ef6ef89f5", null ],
    [ "attach", "classUaTimeZoneDataType.html#aa6423ff6c479de801e143a70ef8336a6", null ],
    [ "clear", "classUaTimeZoneDataType.html#a919afcacebb1e35eef90ceaa9fc751c9", null ],
    [ "copy", "classUaTimeZoneDataType.html#a676bfe9e8a65da0d8c7039dc959c044c", null ],
    [ "copyTo", "classUaTimeZoneDataType.html#af3d247e2c5d224152153b1d5cae2f977", null ],
    [ "detach", "classUaTimeZoneDataType.html#ae3594ed07253069b542bec0acd17c182", null ],
    [ "getDaylightSavingInOffset", "classUaTimeZoneDataType.html#ab543f5c4a2295004032bd7e5d818af79", null ],
    [ "getOffset", "classUaTimeZoneDataType.html#aa6f3115d3a18d258e084c0810b516cbc", null ],
    [ "operator!=", "classUaTimeZoneDataType.html#a701d100f06137a52ef294cbd8c09c41d", null ],
    [ "operator=", "classUaTimeZoneDataType.html#acfacd4d911299e57097aca43f73537df", null ],
    [ "operator==", "classUaTimeZoneDataType.html#a79be889b0d92ce8760a6389c1d53f5b7", null ],
    [ "setDaylightSavingInOffset", "classUaTimeZoneDataType.html#a542a21e68adac2decb82a4fa568a3c07", null ],
    [ "setOffset", "classUaTimeZoneDataType.html#ab02cc6ca9d14ca38770e828d929f7394", null ]
];