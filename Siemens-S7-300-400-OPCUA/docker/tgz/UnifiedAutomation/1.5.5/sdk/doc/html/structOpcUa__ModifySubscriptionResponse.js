var structOpcUa__ModifySubscriptionResponse =
[
    [ "RevisedLifetimeCount", "group__UaStackTypes.html#gaec034a74423e95763f5404f72ae864bb", null ],
    [ "RevisedMaxKeepAliveCount", "group__UaStackTypes.html#gab2ab2749c088329be611422c57448d64", null ],
    [ "RevisedPublishingInterval", "group__UaStackTypes.html#ga7e3842bd86ee242c1bae223249a6bd28", null ]
];