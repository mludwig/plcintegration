var structOpcUa__AddNodesItem =
[
    [ "BrowseName", "group__UaStackTypes.html#gae4a91e59c025d4d183cd4cc07046d166", null ],
    [ "NodeAttributes", "group__UaStackTypes.html#gab907bd71e73b553e278fcc2d37c4c625", null ],
    [ "NodeClass", "group__UaStackTypes.html#ga45b1f9ccc67aa6c8412ace444e7e4420", null ],
    [ "ParentNodeId", "group__UaStackTypes.html#gaf8819c3157ec69fbaba3e3afd58f0456", null ],
    [ "ReferenceTypeId", "group__UaStackTypes.html#ga8b7c85cf95052f02c763f83df63b79e6", null ],
    [ "RequestedNewNodeId", "group__UaStackTypes.html#gae4cc4bf1878dcbe12cd5d94a73bfcdab", null ],
    [ "TypeDefinition", "group__UaStackTypes.html#ga2216807f2944fb5c05d120b63de81ec0", null ]
];