var classUaNetworkBrowseResult =
[
    [ "NetworkNodeType", "group__CppBaseLibraryClass.html#gafe64994b78c6dc8871bc0356108355b3", [
      [ "NODE_TYPE_NETWORK", "group__CppBaseLibraryClass.html#ggafe64994b78c6dc8871bc0356108355b3a49ce30b5e56ed7d04a7ba0bc39b2295c", null ],
      [ "NODE_TYPE_DOMAIN", "group__CppBaseLibraryClass.html#ggafe64994b78c6dc8871bc0356108355b3ac5ef3ff757d21129b11f595f52011a36", null ],
      [ "NODE_TYPE_HOST", "group__CppBaseLibraryClass.html#ggafe64994b78c6dc8871bc0356108355b3a1db91a955e3c26847ccc321afd254027", null ],
      [ "NODE_TYPE_INVALID", "group__CppBaseLibraryClass.html#ggafe64994b78c6dc8871bc0356108355b3a715a3d0fdae1588163334d793c006ba9", null ]
    ] ],
    [ "nodeType", "classUaNetworkBrowseResult.html#aff8bf171a65aee2db6fda57defe2e0c6", null ],
    [ "sNodeName", "classUaNetworkBrowseResult.html#a2925d288b71a0607faf4e9817481d04b", null ]
];