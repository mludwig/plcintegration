var classNodeManagerRoot =
[
    [ "~NodeManagerRoot", "classNodeManagerRoot.html#a50a325e762e73aaaac981114708fae9d", null ],
    [ "addNode", "classNodeManagerRoot.html#a4be6ba733833b8968a9b1e89777f1c58", null ],
    [ "addNodeManager", "classNodeManagerRoot.html#a5786e86f88cc391298e2d779738f6dd5", null ],
    [ "addReference", "classNodeManagerRoot.html#aae8691d9637af3450984c3c071ff05ac", null ],
    [ "afterStartUp", "classNodeManagerRoot.html#af5f97f123dba403fff853249cd41d288", null ],
    [ "beforeShutDown", "classNodeManagerRoot.html#aff530c6f88d442981c92d0ab4a21c04c", null ],
    [ "browse", "classNodeManagerRoot.html#a967efb530772be5f1bd0e1a3184b4127", null ],
    [ "browse", "classNodeManagerRoot.html#abf249929c966554704853c1e7e01b5e7", null ],
    [ "browse", "classNodeManagerRoot.html#ab2f590429a5be4f67d228586026e10e9", null ],
    [ "browse", "classNodeManagerRoot.html#a7057cdb6fb8c8877d2b3a74159e3ac1b", null ],
    [ "changeServerState", "classNodeManagerRoot.html#abfcc5bfcbee9f4370e3340bb86f4cc12", null ],
    [ "changeServiceLevel", "classNodeManagerRoot.html#a907afcf39fedacb63f55e33d8c6118b4", null ],
    [ "deleteNode", "classNodeManagerRoot.html#a69ef5acb35678def4000cdc83309ca9a", null ],
    [ "deleteReference", "classNodeManagerRoot.html#a48a8cd62fb828c9581e175acf24fee32", null ],
    [ "getComAliasFromIndex", "classNodeManagerRoot.html#a2b4bdd8192a4f6db2becc66204514c11", null ],
    [ "getEventManagers", "classNodeManagerRoot.html#ae3102bff0eb93472dfbb4a77609bcdec", null ],
    [ "getHistoryVariableHandle", "classNodeManagerRoot.html#a7308f3c0493ec572363c5c18767f2964", null ],
    [ "getListOfSuperTypes", "classNodeManagerRoot.html#a5e295194eacf584d0eafa48076b49783", null ],
    [ "getMethodHandle", "classNodeManagerRoot.html#a04806fd10db515a4c1e6c87e3d398bf5", null ],
    [ "getNamespaceIndexFromComAlias", "classNodeManagerRoot.html#aac91681f7c74e2616d39529aa3912774", null ],
    [ "getNamespaceIndexFromUri", "classNodeManagerRoot.html#a0b80428a451ededc2e4e092b76a8aa41", null ],
    [ "getNamespaceUriFromIndex", "classNodeManagerRoot.html#adb6716ef9bab965fd529c4df15cc23e5", null ],
    [ "getNode", "classNodeManagerRoot.html#ae2c32645c17203d03a7bbc42c3da1f70", null ],
    [ "getNodeManagerByNamespace", "classNodeManagerRoot.html#a7ee16385a0240b27baac3be8db606f09", null ],
    [ "getNodeManagerConfig", "classNodeManagerRoot.html#a393304e95c46dbcc11b65964aba44979", null ],
    [ "getNodeManagerCrossReferences", "classNodeManagerRoot.html#a7a76a59b23e5ace5610cb189b99bde27", null ],
    [ "getVariableHandle", "classNodeManagerRoot.html#a8640297937b91818bac84c14dc92f0a0", null ],
    [ "pServerManager", "classNodeManagerRoot.html#af14625ba29a11da40bb93b7b3647fa0c", null ],
    [ "pServerObject", "classNodeManagerRoot.html#ad1b34e8cf6e245fb0424e83d0f901058", null ],
    [ "readValues", "classNodeManagerRoot.html#a6154c9c86518de6cef2ff582cd303fd3", null ],
    [ "removeNodeManager", "classNodeManagerRoot.html#a8453e0b787b96fb0152fd57685aca01f", null ],
    [ "sessionActivated", "classNodeManagerRoot.html#a908914da4a10931da404e2d594686804", null ],
    [ "sessionClosed", "classNodeManagerRoot.html#a80db5de66c1797f336b5c5cd1c4bb6a9", null ],
    [ "sessionOpened", "classNodeManagerRoot.html#ae504b37705190eb9f64e01387896fc99", null ],
    [ "startServerShutDown", "classNodeManagerRoot.html#a81aea9c024c96738061fd0d6103d5f32", null ],
    [ "startServerShutDown", "classNodeManagerRoot.html#ac9f1824dba04708723aa8d9f63f36137", null ],
    [ "startUp", "classNodeManagerRoot.html#a9fadb6330b65a1ea033c015470a7efa5", null ],
    [ "translateBrowsePathToNodeId", "classNodeManagerRoot.html#a2c0e0b5d81098981c0b09a7dee8bfa48", null ],
    [ "writeValues", "classNodeManagerRoot.html#a60f5787af3ee1b9499d26ea9722ba005", null ]
];