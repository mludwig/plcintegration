var classUaClientSdk_1_1SubscriptionSettings =
[
    [ "SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html#ab03cc4512f0fc5d7c7152213e25e7082", null ],
    [ "~SubscriptionSettings", "classUaClientSdk_1_1SubscriptionSettings.html#af909c75191f2b7b7d707ff2062bd88a1", null ],
    [ "lifetimeCount", "classUaClientSdk_1_1SubscriptionSettings.html#ac27d5468e2052beaa5b6818a69a4a949", null ],
    [ "maxKeepAliveCount", "classUaClientSdk_1_1SubscriptionSettings.html#a1753fb167c29a2c3313a2f8f2c7b17f3", null ],
    [ "maxNotificationsPerPublish", "classUaClientSdk_1_1SubscriptionSettings.html#a750cda928a8fa58ee29e8df1a290a785", null ],
    [ "priority", "classUaClientSdk_1_1SubscriptionSettings.html#a50e4fd24311c36faa821f822a8b90be9", null ],
    [ "publishingInterval", "classUaClientSdk_1_1SubscriptionSettings.html#a29ab09bbd318526adb3ffec8f7de4db6", null ]
];