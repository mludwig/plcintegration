var classRingBuffer =
[
    [ "RingBuffer", "classRingBuffer.html#ac71cf5c194035a0184a03ea850af99cd", null ],
    [ "~RingBuffer", "classRingBuffer.html#a2715b2e99ea24521ef7a586c2f33e1c9", null ],
    [ "at", "classRingBuffer.html#a2a7a2e5c4bfb241d085b17c9de513e4e", null ],
    [ "changeSize", "classRingBuffer.html#a6eef3a44093a4757192c5881d79cdfd2", null ],
    [ "getCount", "classRingBuffer.html#a454a5e773e437c7428557da9df2fee9b", null ],
    [ "getSize", "classRingBuffer.html#a1895343cf60b771c7de4a5c4663d7afe", null ],
    [ "getSpace", "classRingBuffer.html#a91c7e9fd13214f7b7096e2d2feb8a3f7", null ],
    [ "pop", "classRingBuffer.html#a0624b2fa7d26e9426b62f0cbaf20bfc5", null ],
    [ "push", "classRingBuffer.html#aba2fdfc918c7fc8a274616098e5c693f", null ],
    [ "pushOverwrite", "classRingBuffer.html#a11e1caaec994b6cffbda0dfd01e7be36", null ]
];