var classOpcUa_1_1AuditHistoryEventDeleteEventTypeData =
[
    [ "AuditHistoryEventDeleteEventTypeData", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a41c700a876523e9734b2918515b9a1ec", null ],
    [ "~AuditHistoryEventDeleteEventTypeData", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a87b885ee0479cce509058a0eaf3825ac", null ],
    [ "getEventIds", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a7b318fbbe382f33e1a4c81db8d78f40c", null ],
    [ "getEventIdsValue", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#abf4765613f6e4c765be8426b1ef4d369", null ],
    [ "getFieldData", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a74e9725a0d65fbe3f33bf7108f51e93e", null ],
    [ "getOldValues", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a334a1a5a7f7183f57bfe5436bcf25533", null ],
    [ "getOldValuesValue", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a1d7037bf2a55ed6f6c8e8f558f05c219", null ],
    [ "setEventIds", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a2c7b2f90ea16aa01147b968c5e1d6a40", null ],
    [ "setEventIdsStatus", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a9f230d96a522bdfe8d7c7e44414cc9b0", null ],
    [ "setOldValues", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#a1df6b9ef06afbc059e40eb8e3f50c6b2", null ],
    [ "setOldValuesStatus", "classOpcUa_1_1AuditHistoryEventDeleteEventTypeData.html#aad44944deca4d1551a4dbf1819274927", null ]
];