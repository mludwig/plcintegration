var classUserDataBase =
[
    [ "UserDataType", "classUserDataBase.html#a99c35e1d321d3958efa45d090fbe6f9c", [
      [ "UserDataType_Custom", "classUserDataBase.html#a99c35e1d321d3958efa45d090fbe6f9ca31100670a3f94bbce33d0bb8a4d953d1", null ],
      [ "UserDataType_GetValue", "classUserDataBase.html#a99c35e1d321d3958efa45d090fbe6f9cac5345d48560007b7973c1204da9f287e", null ]
    ] ],
    [ "UserDataBase", "classUserDataBase.html#aeaea27c4bd7e1e7312835849369c3dea", null ],
    [ "~UserDataBase", "classUserDataBase.html#a3287257405caca4718c4f4380f24ae5c", null ],
    [ "getUserDataType", "classUserDataBase.html#abd2c752133c5ff88e238456e0c581781", null ],
    [ "setInvalid", "classUserDataBase.html#a39d29406ba58747e33e5b896048f802f", null ]
];