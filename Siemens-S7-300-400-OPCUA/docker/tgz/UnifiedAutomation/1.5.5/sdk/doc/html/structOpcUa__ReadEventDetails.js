var structOpcUa__ReadEventDetails =
[
    [ "EndTime", "group__UaStackTypes.html#ga357b704e0df99d805fcb35c355adb081", null ],
    [ "Filter", "group__UaStackTypes.html#gab7762de3a143fa3aa779edbab4fd94ce", null ],
    [ "NumValuesPerNode", "group__UaStackTypes.html#ga37cd99c2d78f73d4d4331452708fe262", null ],
    [ "StartTime", "group__UaStackTypes.html#gacdf57bd222360d2c59a35de7b271d1f3", null ]
];