/*
 * DriverUtilsTest.h
 *
 *  Created on: Oct 8, 2015
 *      Author: dabalo
 */

#ifndef DRIVERUTILSTEST_H_
#define DRIVERUTILSTEST_H_

#include "gtest/gtest.h"

#include "DriverUtils.h"

class DriverUtilsTest;

class DriverUtilsTest  : public ::testing::Test
{
public:

	DriverUtilsTest();
	virtual ~DriverUtilsTest();
};

#endif /* DRIVERUTILSTEST_H_ */
