/*
 * CommandQueueTest.cpp
 *
 *  Created on: Oct 8, 2015
 *      Author: dabalo
 */

#include "CommandQueueTest.h"
#include <stdint.h>
#include <LogIt.h>
#include <LogLevels.h>



TEST(CommandQueueTest, commandQueueTest)
{
	CommandQueue::commandPut(CommandItem("1", static_cast<float>(1.1)));
	EXPECT_EQ(CommandQueue::commandQueueSize(), 1);
	CommandItem it1 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<float>(1.1), it1.m_value_f);
	EXPECT_EQ(CommandItem::FLOAT32, it1.m_type);
	
	CommandQueue::commandPut(CommandItem("1", static_cast<float>(1.1)));
	CommandQueue::commandPut(CommandItem("2", static_cast<float>(2.2)));
	CommandQueue::commandPut(CommandItem("3", static_cast<uint32_t>(3)));
	CommandQueue::commandPut(CommandItem("4", static_cast<uint32_t>(4)));
	CommandQueue::commandPut(CommandItem("5", static_cast<uint16_t>(5)));
	CommandQueue::commandPut(CommandItem("6", static_cast<uint16_t>(6)));
	EXPECT_EQ(CommandQueue::commandQueueSize(), 6);
	it1 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<float>(1.1), it1.m_value_f);
	EXPECT_EQ(CommandItem::FLOAT32, it1.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 5);
	CommandItem it2 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<float>(2.2), it2.m_value_f);
	EXPECT_EQ(CommandItem::FLOAT32, it2.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 4);
	CommandItem it3 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<uint32_t>(3), it3.m_value_i32);
	EXPECT_EQ(CommandItem::UINT32, it3.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 3);
	CommandItem it4 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<uint32_t>(4), it4.m_value_i32);
	EXPECT_EQ(CommandItem::UINT32, it4.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 2);
	CommandItem it5 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<uint16_t>(5), it5.m_value_i16);
	EXPECT_EQ(CommandItem::UINT16, it5.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 1);
	CommandItem it6 = CommandQueue::commandBlockingTake();
	EXPECT_EQ(static_cast<uint16_t>(6), it6.m_value_i16);
	EXPECT_EQ(CommandItem::UINT16, it6.m_type);
	EXPECT_EQ(CommandQueue::commandQueueSize(), 0);
}


CommandQueueTest::CommandQueueTest()
{
	Log::initializeLogging(Log::INF);
}

CommandQueueTest::~CommandQueueTest()
{}
