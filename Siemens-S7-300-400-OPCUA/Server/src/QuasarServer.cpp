/* © Copyright CERN, Universidad de Oviedo, 2015.  All rights not expressly granted are reserved.
 * QuasarServer.cpp
 *
 *  Created on: Nov 6, 2015
 * 		Author: Damian Abalo Miron <damian.abalo@cern.ch>
 *      Author: Piotr Nikiel <piotr@nikiel.info>
 *
 *  This file is part of Quasar.
 *
 *  Quasar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public Licence as published by
 *  the Free Software Foundation, either version 3 of the Licence.
 *
 *  Quasar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public Licence for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Quasar.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "QuasarServer.h"
#include <LogIt.h>
#include <string.h>
#include <shutdown.h>
#include "ASReadableUInt16.h"
#include "ASReadableUInt32.h"
#include "ASReadableFloat32.h"
#include <ASNodeQueries.h>
#include <LogLevels.h>
#include "DriverUtils.h"
#include "PropertiesMap.h"
#include "SharedQueue.h"
#include "CommunicationThread.h"
#include "TSPPProcessingThread.h"
#include <boost/foreach.hpp>
#include <boost/thread/thread.hpp>

QuasarServer::QuasarServer() : BaseQuasarServer()
{

}

QuasarServer::~QuasarServer()
{

}

void QuasarServer::mainLoop()
{
	SharedPacketQueue *TSPPQueue = new SharedPacketQueue();
	CommunicationThread *s7Com = new CommunicationThread(TSPPQueue);
	TSPPProcessingThread *s7Process = new TSPPProcessingThread(TSPPQueue);
	LOG(Log::INF) << "Starting Communication and Processing threads";
	boost::thread s7ComThread(&CommunicationThread::run, s7Com);
	boost::thread s7ProcessThread(&TSPPProcessingThread::run, s7Process);
	printServerMsg("Press "+std::string(SHUTDOWN_SEQUENCE)+" to shutdown server");

	// Wait for user command to terminate the server thread.
	while(ShutDownFlag() == 0)
	{
		UaThread::sleep (1);
	}
	printServerMsg(" Shutting down server");
}

void QuasarServer::initialize()
{
	UaString addr;
	std::vector< AddressSpace::ASReadableUInt16 * > objectsuint16;
	std::string pattern (".*");
	AddressSpace::findAllByPattern<AddressSpace::ASReadableUInt16> (m_nodeManager,  m_nodeManager->getNode(UaNodeId(OpcUaId_ObjectsFolder, 0)), OpcUa_NodeClass_Object, pattern, objectsuint16);
	BOOST_FOREACH(AddressSpace::ASReadableUInt16 *a, objectsuint16)
	{
		a->getAddress(addr);
		PropertiesMap::pushUint16(addr.toUtf8(), a->getDeviceLink());
	}
	std::vector< AddressSpace::ASReadableUInt32 * > objectsuint32;
	AddressSpace::findAllByPattern<AddressSpace::ASReadableUInt32> ( m_nodeManager, m_nodeManager->getNode(UaNodeId(OpcUaId_ObjectsFolder, 0)), OpcUa_NodeClass_Object, pattern, objectsuint32);
	BOOST_FOREACH(AddressSpace::ASReadableUInt32 *a, objectsuint32)
	{
		std::string id = a->nodeId().toFullString().toUtf8();
		size_t found = id.find("evStsReg");
		if(found != std::string::npos)
		{//It is an event property, and hence, it is put inside the corresponding map
			a->getAddress(addr);
			PropertiesMap::pushEventUint32(addr.toUtf8(), a->getDeviceLink());
		}
		else
		{//It is a normal property
			a->getAddress(addr);
			PropertiesMap::pushUint32(addr.toUtf8(), a->getDeviceLink());
		}
	}

	std::vector< AddressSpace::ASReadableFloat32 * > objectsfloat32;
	AddressSpace::findAllByPattern<AddressSpace::ASReadableFloat32> ( m_nodeManager, m_nodeManager->getNode(UaNodeId(OpcUaId_ObjectsFolder, 0)), OpcUa_NodeClass_Object, pattern, objectsfloat32);
	BOOST_FOREACH(AddressSpace::ASReadableFloat32 *a, objectsfloat32)
	{
		a->getAddress(addr);
		PropertiesMap::pushFloat32(addr.toUtf8(), a->getDeviceLink());
	}

}

void QuasarServer::shutdown()
{
	LOG(Log::INF) << "Shutting down Quasar server.";
}

void QuasarServer::initializeLogIt()
{
	Log::LOG_LEVEL loglevel = Log::TRC;
	Log::initializeLogging( loglevel );
	LOG(Log::INF) << "Logging initialized " << loglevel;
}
