#!/bin/bash
# this is an old version of quasar, sth like 1.0 @2016 or so
#python ./quasar.py generate device ReadableUInt16
#python ./quasar.py generate device ReadableUInt32
#python ./quasar.py generate device ReadableFloat32
#python ./quasar.py generate device WritableUInt16
#python ./quasar.py generate device WritableUInt32
#python ./quasar.py generate device WritableFloat32

export BOOST_HOME=/home/mludwig/3rdPartySoftware/boost/boost_1_69_0
export UNIFIED_AUTOMATION_HOME=/home/mludwig/3rdPartySoftware/UnifiedAutomation/1.5.5/sdk

python ./quasar.py prepare_build
python ./quasar.py build pcbe13632_build.cmake
#
cp -v ./bin/OpcUaServer.S7-300-400.* ./run
