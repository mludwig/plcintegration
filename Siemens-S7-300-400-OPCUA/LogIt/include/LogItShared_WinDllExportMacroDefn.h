
#ifndef LogItShared_EXPORT_FUNCTION_H
#define LogItShared_EXPORT_FUNCTION_H

#ifdef LogItShared_BUILT_AS_STATIC
#  define LogItShared_EXPORT_FUNCTION
#  define LOGITSHARED_NO_EXPORT
#else
#  ifndef LogItShared_EXPORT_FUNCTION
#    ifdef LogItShared_EXPORTS
        /* We are building this library */
#      define LogItShared_EXPORT_FUNCTION __attribute__((visibility("default")))
#    else
        /* We are using this library */
#      define LogItShared_EXPORT_FUNCTION __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef LOGITSHARED_NO_EXPORT
#    define LOGITSHARED_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef LOGITSHARED_DEPRECATED
#  define LOGITSHARED_DEPRECATED __attribute__ ((__deprecated__))
#  define LOGITSHARED_DEPRECATED_EXPORT LogItShared_EXPORT_FUNCTION __attribute__ ((__deprecated__))
#  define LOGITSHARED_DEPRECATED_NO_EXPORT LOGITSHARED_NO_EXPORT __attribute__ ((__deprecated__))
#endif

#define DEFINE_NO_DEPRECATED 0
#if DEFINE_NO_DEPRECATED
# define LOGITSHARED_NO_DEPRECATED
#endif

#endif
