Siemens OPC-UA TSPP server.
The Siemens OPC-UA server was developed with the objective of connection any supervision or simple HMI (e.g. Labview) to a UNICOS PLC, acting as it supervisor trough this OPC-UA server.  The sever will communicate with the UNICOS PLC using the TSPP (Time Stamp Push Protocol), developed at cern, and also S7, which is the standard communication protocol used in Siemens PLCs. The servers will hide the TSPP protocol from the client but keeping the event-mechanism, as they will publish data on change and with the time stamp of the origin (PLC).
This server was developed using the proyect QUASAR at it's early stages.
To build this server, do it as a normal QUASAR server. You also need to do all configuring needed to set up the Softnet library, as described in this document: https://wikis.web.cern.ch/wikis/display/EN/Softnet+S7+and+TSPP+Tutorial