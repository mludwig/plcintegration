
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
    Authors(from Quasar team): Piotr Nikiel

    This file is part of Quasar.

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

    This file was completely generated by Quasar (additional info: using transform designToDeviceBaseHeader.xslt)
    on 2020-02-27T15:06:36.074+01:00
 */



#ifndef __Base_DReadableFloat32__H__
#define __Base_DReadableFloat32__H__

#include <vector>
#include <string>
#include <list>
#include <boost/thread/mutex.hpp>


#include <DRoot.h>
#include <Configuration.hxx>

#include <opcua_platformdefs.h>

/* forward decl for AddressSpace */
namespace AddressSpace {
class
    ASReadableFloat32
    ;
}
/* forward decl for Parent */

#include <DDevice.h>


namespace Device
{


typedef DDevice Parent_DReadableFloat32 ;



class DReadableFloat32;
/* forward declarations (comes from design.class.hasObjects) */


class
    Base_DReadableFloat32
{

public:
    /* Constructor */
    explicit Base_DReadableFloat32 (
        const Configuration::ReadableFloat32 & config,
        Parent_DReadableFloat32 * parent
    ) ;

private:
    /* No copy constructors or assignment operators */
    Base_DReadableFloat32 (const Base_DReadableFloat32 & other);
    Base_DReadableFloat32 & operator=(const Base_DReadableFloat32 & other);

public:

    /* sample dtr */
    virtual ~Base_DReadableFloat32 ();

    Parent_DReadableFloat32 * getParent () const {
        return m_parent;
    }

    /* add/remove for handling device structure */


    /* to safely quit */
    unsigned int unlinkAllChildren ();


    void linkAddressSpace (AddressSpace::ASReadableFloat32 * as, const std::string & stringAddress);
    AddressSpace::ASReadableFloat32 * getAddressSpaceLink () const {
        return m_addressSpaceLink;
    }


    /* find methods for children */


    /* getters for values which are keys */



    /* mutex operations */


    /* variable-wise locks */


    /* query address-space for full name (mostly for debug purposes) */
    std::string getFullName() const {
        return m_stringAddress;
    };

    static std::list < DReadableFloat32 * > s_orphanedObjects;
    static void registerOrphanedObject( DReadableFloat32 * object ) {
        s_orphanedObjects.push_back( object );
    }
    static std::list < DReadableFloat32* > & orphanedObjects() {
        return  s_orphanedObjects;
    }

private:

    Parent_DReadableFloat32 * m_parent;

public:	  // TODO: reconsider this! shoudln't be public!
    AddressSpace::
    ASReadableFloat32
    *m_addressSpaceLink;



private:
    std::string m_stringAddress;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* if any of our config entries has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


    /* object-wise lock */


    /* variable-wise locks */





};





}

#endif // include guard
