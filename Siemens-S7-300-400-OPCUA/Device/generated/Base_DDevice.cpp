
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.
    Authors(from Quasar team): Piotr Nikiel

    This file is part of Quasar.

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.

    This file was completely generated by Quasar (additional info: using transform designToDeviceBaseBody.xslt)
    on 2020-02-27T15:06:33.928+01:00
 */




#include <boost/foreach.hpp>

#include <Configuration.hxx>


#include <Base_DDevice.h>




#include <DReadableUInt16.h>

#include <DReadableUInt32.h>

#include <DReadableFloat32.h>

#include <DWritableUInt16.h>

#include <DWritableUInt32.h>

#include <DWritableFloat32.h>




namespace Device
{



void Base_DDevice::linkAddressSpace( AddressSpace::ASDevice *addressSpaceLink, const std::string & stringAddress)
{
    if (m_addressSpaceLink != 0)
    {
        /* signalize nice error from here. */
        abort();
    }
    else
        m_addressSpaceLink = addressSpaceLink;
    m_stringAddress.assign( stringAddress );
}

/* add/remove */

void Base_DDevice::add ( DReadableUInt16 *device)
{
    m_ReadableUInt16s.push_back (device);
}

void Base_DDevice::add ( DReadableUInt32 *device)
{
    m_ReadableUInt32s.push_back (device);
}

void Base_DDevice::add ( DReadableFloat32 *device)
{
    m_ReadableFloat32s.push_back (device);
}

void Base_DDevice::add ( DWritableUInt16 *device)
{
    m_WritableUInt16s.push_back (device);
}

void Base_DDevice::add ( DWritableUInt32 *device)
{
    m_WritableUInt32s.push_back (device);
}

void Base_DDevice::add ( DWritableFloat32 *device)
{
    m_WritableFloat32s.push_back (device);
}


//! Disconnects AddressSpace part from the Device logic, and does the same for all children
//! Returns number of unlinked objects including self
unsigned int Base_DDevice::unlinkAllChildren ()
{
    unsigned int objectCounter=1;  // 1 is for self
    m_addressSpaceLink = 0;
    /* Fill up: call unlinkAllChildren on all children */

    BOOST_FOREACH( DReadableUInt16 *d, m_ReadableUInt16s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DReadableUInt32 *d, m_ReadableUInt32s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DReadableFloat32 *d, m_ReadableFloat32s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DWritableUInt16 *d, m_WritableUInt16s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DWritableUInt32 *d, m_WritableUInt32s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    BOOST_FOREACH( DWritableFloat32 *d, m_WritableFloat32s )
    {
        objectCounter += d->unlinkAllChildren();
    }

    return objectCounter;

}


/* find methods for children */






/* Constructor */
Base_DDevice::Base_DDevice (
    const Configuration::Device & config,
    Parent_DDevice * parent
):

    m_parent(parent),
    m_addressSpaceLink(0),
    m_stringAddress("**NB**")


{

}

Base_DDevice::~Base_DDevice ()
{
    /* remove children */

    BOOST_FOREACH( DReadableUInt16 *d, m_ReadableUInt16s )
    {
        delete d;
    }

    BOOST_FOREACH( DReadableUInt32 *d, m_ReadableUInt32s )
    {
        delete d;
    }

    BOOST_FOREACH( DReadableFloat32 *d, m_ReadableFloat32s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableUInt16 *d, m_WritableUInt16s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableUInt32 *d, m_WritableUInt32s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableFloat32 *d, m_WritableFloat32s )
    {
        delete d;
    }

}



std::list<DDevice*> Base_DDevice::s_orphanedObjects;




}


