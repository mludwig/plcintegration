
/* This is device header stub */


#ifndef __DReadableUInt16__H__
#define __DReadableUInt16__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../generated/Base_DReadableUInt16.h"


namespace Device
{




class
    DReadableUInt16
    : public Base_DReadableUInt16
{

public:
    /* sample constructor */
    explicit DReadableUInt16 ( const Configuration::ReadableUInt16 & config

                               , DDevice * parent

                             ) ;
    /* sample dtr */
    ~DReadableUInt16 ();



    /* add/remove for handling device structure */
    void update(uint16_t value, std::string serverTime, int miliseconds);


    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DReadableUInt16( const DReadableUInt16 & );
    DReadableUInt16& operator=(const DReadableUInt16 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

private:


};





}

#endif // include guard
