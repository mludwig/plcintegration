
/* This is device header stub */


#ifndef __DWritableUInt16__H__
#define __DWritableUInt16__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../generated/Base_DWritableUInt16.h"


namespace Device
{




class
    DWritableUInt16
    : public Base_DWritableUInt16
{

public:
    /* sample constructor */
    explicit DWritableUInt16 ( const Configuration::WritableUInt16 & config

                               , DDevice * parent

                             ) ;
    /* sample dtr */
    ~DWritableUInt16 ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeValue ( const OpcUa_UInt16 & v);


private:
    /* Delete copy constructor and assignment operator */
    DWritableUInt16( const DWritableUInt16 & );
    DWritableUInt16& operator=(const DWritableUInt16 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:

private:


};





}

#endif // include guard
