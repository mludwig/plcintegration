
/* This is device header stub */


#ifndef __DReadableFloat32__H__
#define __DReadableFloat32__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include "../generated/Base_DReadableFloat32.h"


namespace Device
{




class
    DReadableFloat32
    : public Base_DReadableFloat32
{

public:
    /* sample constructor */
    explicit DReadableFloat32 ( const Configuration::ReadableFloat32 & config

                                , DDevice * parent

                              ) ;
    /* sample dtr */
    ~DReadableFloat32 ();

    DDevice * getParent () const {
        return m_parent;
    }


    /* add/remove for handling device structure */
    void update(float value, std::string serverTime, int miliseconds);


    /* find methods for children */


    /* getters for values which are keys */

private:
    /* Delete copy constructor and assignment operator */
    DReadableFloat32( const DReadableFloat32 & );
    DReadableFloat32& operator=(const DReadableFloat32 &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

    DDevice * m_parent;

    /* if any of our cachevariables has isKey=true then we shall keep its copy here for find functions  (it is const, either way) */


};




}

#endif // include guard
