
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDevice.h>
#include <ASDevice.h>


#include <DReadableUInt16.h>

#include <DReadableUInt32.h>

#include <DReadableFloat32.h>

#include <DWritableUInt16.h>

#include <DWritableUInt32.h>

#include <DWritableFloat32.h>




namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDevice::DDevice (const Configuration::Device & config

                  , DRoot * parent

                 ):
    Base_DDevice( config

                  , parent

                )
/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DDevice::~DDevice ()
{
    /* remove children */

    BOOST_FOREACH( DReadableUInt16 *d, m_ReadableUInt16s )
    {
        delete d;
    }

    BOOST_FOREACH( DReadableUInt32 *d, m_ReadableUInt32s )
    {
        delete d;
    }

    BOOST_FOREACH( DReadableFloat32 *d, m_ReadableFloat32s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableUInt16 *d, m_WritableUInt16s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableUInt32 *d, m_WritableUInt32s )
    {
        delete d;
    }

    BOOST_FOREACH( DWritableFloat32 *d, m_WritableFloat32s )
    {
        delete d;
    }

}

/* delegators for cachevariables and externalvariables */


/* add/remove */

void DDevice::add ( DReadableUInt16 *device)
{
    /* fill it up  --  only proposal given */
    m_ReadableUInt16s.push_back (device);
}

void DDevice::add ( DReadableUInt32 *device)
{
    /* fill it up  --  only proposal given */
    m_ReadableUInt32s.push_back (device);
}

void DDevice::add ( DReadableFloat32 *device)
{
    /* fill it up  --  only proposal given */
    m_ReadableFloat32s.push_back (device);
}

void DDevice::add ( DWritableUInt16 *device)
{
    /* fill it up  --  only proposal given */
    m_WritableUInt16s.push_back (device);
}

void DDevice::add ( DWritableUInt32 *device)
{
    /* fill it up  --  only proposal given */
    m_WritableUInt32s.push_back (device);
}

void DDevice::add ( DWritableFloat32 *device)
{
    /* fill it up  --  only proposal given */
    m_WritableFloat32s.push_back (device);
}




}


